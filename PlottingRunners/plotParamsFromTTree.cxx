//
//  plotParamsFromTTree.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/MSVxHistGroup.h"
#include "PlottingPackage/VertexHistograms.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/InputFiles.h"

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
double jetSliceWeights[13];
TStopwatch timer;



int main(int argc, char **argv){
    
    std::cout << "running program!" << std::endl;
    
    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");
    
    TChain *chain = new TChain("recoTree");
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
        
    InputFiles::AddFilesToChain(TString(argv[1]), chain);
        
    //Directory the plots will go in
    
    TString plotDirectory = TString(argv[2]);
        
    TFile *outputFile = new TFile(plotDirectory+"/outputMSVxParams.root","RECREATE");
    
    std::cout << "added file!, will be saved in directory: " << plotDirectory << std::endl;
    
    int isJZMC = 0; bool log_scale = false;
    TString inputFile = chain->GetFile()->GetName();
    if(inputFile.Contains("JZ") ){
    	log_scale = true;
    	if(inputFile.Contains("JZ0W") ) isJZMC = 1;
    	if(inputFile.Contains("JZ1W") ) isJZMC = 2;
    	if(inputFile.Contains("JZ2W") ) isJZMC = 3;
    	if(inputFile.Contains("JZ3W") ) isJZMC = 4;
    	if(inputFile.Contains("JZ4W") ) isJZMC = 5;
    	if(inputFile.Contains("JZ5W") ) isJZMC = 6;
    	if(inputFile.Contains("JZ6W") ) isJZMC = 7;
    	if(inputFile.Contains("JZ7W") ) isJZMC = 8;	
    	if(inputFile.Contains("JZ8W") ) isJZMC = 9;
    	if(inputFile.Contains("JZ9W") ) isJZMC = 10;
    	if(inputFile.Contains("JZ10W") ) isJZMC = 11;
    	if(inputFile.Contains("JZ11W") ) isJZMC = 12;
    	if(inputFile.Contains("JZ12W") ) isJZMC = 13;

    }
    std::cout << "Are 1D plots with log scale? " << log_scale << std::endl;
    //TTree *tree = (TTree*) f1->GetTree();
    
    
    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    
    //one set of histograms for barrel, one for endcaps,
    //one for the full detector together (nice for 2D maps)
    MSVxHistGroup* msvxHists = new MSVxHistGroup;
    msvxHists->initialize(true);
    
    setPrettyCanvasStyle();
    
    TH1::SetDefaultSumw2();
    
    isSignal = 0; if(inputFile.Contains("LLP") || inputFile.Contains("LongLived") ){ isSignal = 1;}
    
    timer.Start();
	
    msvxHists->fillFromTree(chain,isJZMC);
    std::cout << "filled hists!" << std::endl;
    
    PlottingUtils::plot("MSVertex_barrel_eta",plotDirectory ,"MS vertex #eta" , "Number of vertices",msvxHists->hMSVxBar->h_eta,
    		msvxHists->hMSVxBarGood->h_eta,msvxHists->hMSVxBarGoodMatched->h_eta, log_scale, 
    		"|#eta| < 0.8","|#eta| < 0.8, Good", "|#eta| < 0.8, Good, Matched");
    
    PlottingUtils::plot("MSVertex_eta_overlap",plotDirectory ,"MS vertex |#eta|" , "Number of vertices",msvxHists->hMSVxFull->h_eta_2MATCH, log_scale,"vertices from double-matched LLPs");
    
    TGraphAsymmErrors *g_etaOverlap = PlottingUtils::plotEfficiency("MSVertex_eta_overlap_eff", plotDirectory, "MS vertex |#eta|", "Fraction of vertices",
    		msvxHists->hMSVxFull->h_eta_2MATCH,msvxHists->hMSVxFull->h_eta_ABS,0.,3.);
    g_etaOverlap->Write();
    
    PlottingUtils::plot("MSVertex_barrel_phi",plotDirectory ,"MS vertex #phi" , "Number of vertices",msvxHists->hMSVxBar->h_phi,
    		msvxHists->hMSVxBarGood->h_phi,msvxHists->hMSVxBarGoodMatched->h_phi, log_scale, 
    		"|#eta| < 0.8","|#eta| < 0.8, Good", "|#eta| < 0.8, Good, Matched");
    
    PlottingUtils::plot("MSVertex_barrel_R",plotDirectory ,"MS vertex Lxy [m]" , "Number of vertices",msvxHists->hMSVxBar->h_R,
    		msvxHists->hMSVxBarGood->h_R,msvxHists->hMSVxBarGoodMatched->h_R, log_scale, 
    		"|#eta| < 0.8","|#eta| < 0.8, sGood", "|#eta| < 0.8, Good, Matched");
    
    PlottingUtils::plot("MSVertex_barrel_z",plotDirectory ,"MS vertex Lz [m]" , "Number of vertices",msvxHists->hMSVxBar->h_z,
    		msvxHists->hMSVxBarGood->h_z,msvxHists->hMSVxBarGoodMatched->h_z, log_scale, 
    		"|#eta| < 0.8","|#eta| < 0.8, Good", "|#eta| < 0.8, Good, Matched");
    
    PlottingUtils::plot("MSVertex_barrel_nMDT",plotDirectory ,"MS vertex nMDT hits" , "Number of vertices",msvxHists->hMSVxBar->h_nMDT,
        		msvxHists->hMSVxBarGood->h_nMDT,msvxHists->hMSVxBarGoodMatched->h_nMDT, log_scale, 
        		"|#eta| < 0.8","|#eta| < 0.8, Good", "|#eta| < 0.8, Good, Matched");
        
    PlottingUtils::plot("MSVertex_barrel_nRPC",plotDirectory ,"MS vertex nRPC hits" , "Number of vertices",msvxHists->hMSVxBar->h_nRPC,
        		msvxHists->hMSVxBarGood->h_nRPC,msvxHists->hMSVxBarGoodMatched->h_nRPC, log_scale, 
        		"|#eta| < 0.8","|#eta| < 0.8, Good", "|#eta| < 0.8, Good, Matched");
        
    PlottingUtils::plot("MSVertex_barrel_nTGC",plotDirectory ,"MS vertex nTGC hits" , "Number of vertices",msvxHists->hMSVxBar->h_nTGC,
        		msvxHists->hMSVxBarGood->h_nTGC,msvxHists->hMSVxBarGoodMatched->h_nTGC, log_scale, 
        		"|#eta| < 0.8","|#eta| < 0.8, Good", "|#eta| < 0.8, Good, Matched");
    
    PlottingUtils::plot("MSVertex_Endcap_eta",plotDirectory ,"MS vertex #eta" , "Number of vertices",msvxHists->hMSVxEC->h_eta,
    		msvxHists->hMSVxECGood->h_eta,msvxHists->hMSVxECGoodMatched->h_eta, log_scale, 
    		"1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good", "1.3 < |#eta| < 2.5, Good, Matched");
    
    PlottingUtils::plot("MSVertex_Endcap_phi",plotDirectory ,"MS vertex #phi" , "Number of vertices",msvxHists->hMSVxEC->h_phi,
    		msvxHists->hMSVxECGood->h_phi,msvxHists->hMSVxECGoodMatched->h_phi, log_scale, 
    		"1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good", "1.3 < |#eta| < 2.5, Good, Matched");
    
    PlottingUtils::plot("MSVertex_Endcap_R",plotDirectory ,"MS vertex Lxy [m]" , "Number of vertices",msvxHists->hMSVxEC->h_R,
    		msvxHists->hMSVxECGood->h_R,msvxHists->hMSVxECGoodMatched->h_R, log_scale, 
    		"1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good", "1.3 < |#eta| < 2.5, Good, Matched");
    
    PlottingUtils::plot("MSVertex_Endcap_z",plotDirectory ,"MS vertex Lz [m]" , "Number of vertices",msvxHists->hMSVxEC->h_z,
    		msvxHists->hMSVxECGood->h_z,msvxHists->hMSVxECGoodMatched->h_z, log_scale, 
    		"1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good", "1.3 < |#eta| < 2.5, Good, Matched");
    
    PlottingUtils::plot("MSVertex_Endcap_nMDT",plotDirectory ,"MS vertex nMDT hits" , "Number of vertices",msvxHists->hMSVxEC->h_nMDT,
        		msvxHists->hMSVxECGood->h_nMDT,msvxHists->hMSVxECGoodMatched->h_nMDT, log_scale, 
        		"1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good", "1.3 < |#eta| < 2.5, Good, Matched");
        
    PlottingUtils::plot("MSVertex_Endcap_nRPC",plotDirectory ,"MS vertex nRPC hits" , "Number of vertices",msvxHists->hMSVxEC->h_nRPC,
        		msvxHists->hMSVxECGood->h_nRPC,msvxHists->hMSVxECGoodMatched->h_nRPC, log_scale, 
        		"1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good", "1.3 < |#eta| < 2.5, Good, Matched");
        
    PlottingUtils::plot("MSVertex_Endcap_nTGC",plotDirectory ,"MS vertex nTGC hits" , "Number of vertices",msvxHists->hMSVxEC->h_nTGC,
        		msvxHists->hMSVxECGood->h_nTGC,msvxHists->hMSVxECGoodMatched->h_nTGC, log_scale, 
        		"1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good", "1.3 < |#eta| < 2.5, Good, Matched");
        
    PlottingUtils::plot2D("MSVertex_all_phieta",plotDirectory, "MS vertex #phi", "MS vertex #eta", msvxHists->hMSVxFull->h_phieta);
    PlottingUtils::plot2D("MSVertex_all_zR",plotDirectory,"MS vertex Lxy [m]", "MS vertex Lz [m]", msvxHists->hMSVxFull->h_zR);

    PlottingUtils::plot2D("MSVertex_Barrel_MDTvsRPC",plotDirectory,"MS vertex nRPC hits", "MS vertex nMDT hits", msvxHists->hMSVxBar->h_MDTvsTrig);
    PlottingUtils::plot2D("MSVertex_Endcap_MDTvsTGC",plotDirectory,"MS vertex nTGC hits", "MS vertex nMDT hits", msvxHists->hMSVxEC->h_MDTvsTrig);

    
    std::cout << "plotted hists!" << std::endl;

    outputFile->Write();
    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    
}
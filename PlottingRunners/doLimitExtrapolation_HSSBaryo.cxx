//
//  doLimitExtrapolation.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 03/09/16.
//
//

#include <stdio.h>

#include <TH2.h>
#include <TStyle.h>
#include <iostream>
#include "math.h"
#include <TMath.h>
#include <TRandom3.h>
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TEfficiency.h"
#include <sys/time.h>
#include <numeric>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <vector>
#include <string>
#include "TGraphAsymmErrors.h"
#include <fstream>
#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/HistTools.h"
#include "PlottingPackage/SampleDetails.h"
#include "PlottingPackage/ExtrapolationUtils.h"

using namespace ExtrapolationUtils;

#define PI 3.14159265
#define sq(x) ((x)*(x))

const double LUMI = 32864.+3212.96; //in pb-1
//18857.4+3193.68

//these are in metres?
const double LTSTEP = 0.05;
const double LTSTEP2 = 0.5;
const double LTSTEP3 = 0.005;

//trigger scaling
const double delta_MC = 13.262;
const double delta_DATA = 12.171;
const double sigma_MC = 2.084;
const double sigma_DATA = 2.9548;

//speed of light
const double c = 299.7924580;// in mm/ns
const double VERTEX_DR_CUT = 1.0; //(minimum delta R between two vertices; used to count expected signal)
typedef unsigned long long timestamp_t;
static timestamp_t
get_timestamp ()
{
  struct timeval now;
  gettimeofday (&now, NULL);
  return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}


int DetectorRegion(double eta, double lxy, double lz){
  // ** Legend
  // 1 = MS - BARREL
  // 2 = MS - ENDCAPS
  // 3 = ID or calorimeters - BARREL
  // 4 = ID or calorimeters - ENDCAPS
  // 5 = outside ATLAS - BARREL
  // 6 = outside ATLAS - ENDCAPS
  if      (TMath::Abs(eta) < 1.0 && lxy > 4000 && lxy < 7500) return 1;
  else if (TMath::Abs(eta) > 1.0 && TMath::Abs(eta) < 2.5 && lxy < 10000 && TMath::Abs(lz) > 6000 && TMath::Abs(lz) < 14000 ) return 2;
  else if (TMath::Abs(eta) < 1.0 && lxy < 4000 && TMath::Abs(lz) < 6000) return 3;
  else if (TMath::Abs(eta) > 1.0 && TMath::Abs(eta) < 3.2 && lxy < 4000 && TMath::Abs(lz) < 6000) return 4;
  else if (TMath::Abs(eta) < 1.0 && lxy > 7500) return 5;
  else if (TMath::Abs(eta) > 1.0 && ( (lxy > 10000 && TMath::Abs(lz) > 6000 && TMath::Abs(lz) < 14000) || TMath::Abs(lz) > 14000) ) return 6;
  else return -1;
}


//Systematic errors switch
const bool addSystematicErrors = true;

//Ratios switch
#define doRatios false
double LUMISYSERR = 0.0;

TH1D *h_BB_Decays, *h_BE_Decays, *h_EE_Decays, *h_1E_Decays, *h_1B_Decays;
TH1D *h_Expected_2MSVx;
TH1D *h_Expected_1MSVx;

timestamp_t t0;

using namespace std;
using namespace plotVertexParameters;

int nMSvx_BB,nMSvx_BE,nMSvx_EE;
int nMStrig_B,nMStrig_E;
int nMSVxMSVx;
int nBB_Decay, nBE_Decay, nEE_Decay, n1E_Decay, n1B_Decay;

int nBB_Trig, nBE_Trig, nEE_Trig;
int n1B_Trig, n1E_Trig;
int nBB_Trig_0vx, nBE_Trig_0vx, nEE_Trig_0vx;
int nBB_Trig_1vx, nBE_Trig_1vx, nEE_Trig_1vx;
int nBB_Trig_2vx, nBE_Trig_2vx, nEE_Trig_2vx;
int nBB_TrigA_0vx, nBE_TrigA_0vx, nEB_TrigA_0vx, nEE_TrigA_0vx, n1B_Trig_0vx, n1E_Trig_0vx;
int nBB_TrigA_1vx, nBE_TrigA_1vx,nEB_TrigA_1vx, nEE_TrigA_1vx, n1B_Trig_1vx, n1E_Trig_1vx;

//random number
TRandom3 rnd;


bool debug = false;
double nEvents = 0;


double triggerProb(float llp_beta, float llp_dist){

  double deltaT = llp_dist*(1/llp_beta - 1)/c;
  double eff_DATA = 0.5*TMath::Erfc((deltaT - delta_DATA)/(sqrt(2.0)*sigma_DATA));
  return eff_DATA;
}
double triggerProbMC(float llp_beta, float llp_dist){

  double deltaT = llp_dist*(1/llp_beta - 1)/c;
  double eff_MC = 0.5*TMath::Erfc((deltaT - delta_MC)/(sqrt(2.0)*sigma_MC));
  return eff_MC;
}
double triggerPassProb(std::vector<TVector3>& llp_loc, std::vector<double> *llp_beta, int& k, double &ctau){

  if(llp_loc.size() != llp_beta->size()){
    cout << "ERROR!: have " << llp_loc.size() << " llp decay locations, but " << llp_beta->size() << " 4-momenta" << endl;
    return -1;
  }

  //if(isBadTopology(llp_loc)) return -1;

  ///////////////////////////////////////////////////////
  //* Check if Trigger detects the v-pion *//

  double prob = rnd.Rndm();
  if(is1B_Event(llp_loc)){
    h_1B_Decays->Fill(ctau);
    if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k){
      n1B_Decay++;
    }
    int b_dec_index = -1;
    if(checkDecLoc(llp_loc[0]) == 1){
      b_dec_index = 0;
    } else{
      b_dec_index = 1;
    }
    bool isGoodTrigger(false);

    double probTrig = rnd.Rndm();
    double eff_DATA = triggerProb(llp_beta->at(b_dec_index),llp_loc[b_dec_index].Mag());

    if(probTrig < eff_DATA) isGoodTrigger=true;

    if(isGoodTrigger) {
      return prob;
    } else{
      return -1;
    }
  } else if(is1E_Event(llp_loc)){
    h_1E_Decays->Fill(ctau);
    if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k){
      n1E_Decay++;
    }
    int e_dec_index = -1;
    if(checkDecLoc(llp_loc[0]) == 2){
      e_dec_index = 0;
    } else{
      e_dec_index = 1;
    }
    bool isGoodTrigger(false);

    double probTrig = rnd.Rndm();
    double endcap_delT = llp_loc[e_dec_index].Mag()*(1.0/llp_beta->at(e_dec_index)-1.0)/c;

    if(endcap_delT < 25){
      return prob;
    } else{
      return -1;
    }
  } else if(isBB_Event(llp_loc)){
    h_BB_Decays->Fill(ctau);
    if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k){
      nBB_Decay++;
      if(nEvents < 10) cout << "SampleDetails::sim_ctau: " << SampleDetails::sim_ctau << " and k: " << k << " and ctau " << k*LTSTEP+0.0001 << endl;
    }
    //////////////////////////////////////////////////////////////
    //* Calculate the v-pion time delay wrt the bunch crossing *//
    bool isGoodTrigger(false);
    double smallest_delT = 999.;
    int smallest_delT_index = -1;
    for(int i_vp = 0; i_vp < llp_loc.size(); i_vp++){
      double tmp_delT = llp_loc[i_vp].Mag()*(1.0/llp_beta->at(i_vp)-1.0)/c;
      if(tmp_delT < smallest_delT){
	smallest_delT = tmp_delT;
	smallest_delT_index = i_vp;
      }
    }

    double probTrig = rnd.Rndm();
    double eff_DATA = triggerProb(llp_beta->at(smallest_delT_index),llp_loc[smallest_delT_index].Mag());

    if(probTrig < eff_DATA) isGoodTrigger=true;

    if(isGoodTrigger) {
      return prob;
    } else return -1;

  } else if(isBE_Event(llp_loc)){
    h_BE_Decays->Fill(ctau);
    if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k) nBE_Decay++;
    //need some sort of trigger scalaing here?!? naw, just ignore for now...

    //find which decay is in the barrel
    int b_dec_index = -1;
    int e_dec_index = -1;
    if(checkDecLoc(llp_loc[0]) == 1){
      b_dec_index = 0; e_dec_index = 1;
    } else{
      b_dec_index = 1; e_dec_index = 0;
    }
    bool isGoodTrigger(false);

    double endcap_delT = llp_loc[e_dec_index].Mag()*(1.0/llp_beta->at(e_dec_index)-1.0)/c;

    if(endcap_delT < 25){
      return prob;
    } else{
      double probTrig = rnd.Rndm();
      double eff_DATA = triggerProb(llp_beta->at(b_dec_index),llp_loc[b_dec_index].Mag());

      if(probTrig < eff_DATA) isGoodTrigger=true;

      if(isGoodTrigger) {
	return prob;
      } else return -1;

    }

  } else if(isEE_Event(llp_loc)){
    h_EE_Decays->Fill(ctau);
    if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k) nEE_Decay++;
    bool isGoodTrigger(false);
    double smallest_delT = 999.;
    int smallest_delT_index = -1;
    for(int i_vp = 0; i_vp < llp_loc.size(); i_vp++){
      double tmp_delT = llp_loc[i_vp].Mag()*(1.0/llp_beta->at(i_vp)-1.0)/c;
      if(tmp_delT < smallest_delT){
	smallest_delT = tmp_delT;
	smallest_delT_index = i_vp;
      }
    }
    if(smallest_delT < 25) isGoodTrigger=true;

    if(isGoodTrigger) {
      return prob;
    } else return -1;

  } else{
    return -1;
  }

  return -1;
}

double triggerPassProb(std::vector<TVector3>& llp_loc, std::vector<double> *llp_beta, int& k);
double triggerProbMC(float llp_beta, float llp_dist);
double triggerProb(float llp_beta, float llp_dist);
double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
  double delPhi = wrapPhi(phi1-phi2);
  double delEta = eta1-eta2;
  return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
  double delPhi = wrapPhi(phi1-phi2);
  double delEta = eta1-eta2;
  return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
  while (phi> TMath::Pi()) phi -= TMath::TwoPi();
  while (phi<-TMath::Pi()) phi += TMath::TwoPi();
  return phi;

}
int main(int argc, char **argv){
  if(addSystematicErrors){
    LUMISYSERR = 0.029; //from https://twiki.cern.ch/twiki/bin/view/Atlas/LuminosityForPhysics#2016_13_TeV_ICHEP_2016
  }
  nMSvx_BB=0;
  nMSvx_BE=0;
  nMSvx_EE=0;
  nMStrig_B=0;
  nMStrig_E=0;

  nBB_Decay=0;
  nBE_Decay=0;
  nEE_Decay=0;
  n1B_Decay=0;
  n1E_Decay=0;

  n1B_Trig=0;
  nBB_Trig=0;
  nBE_Trig=0;
  n1E_Trig=0;
  nEE_Trig=0;

  n1B_Trig_0vx=0;
  n1E_Trig_0vx=0;
  n1B_Trig_1vx=0;
  n1E_Trig_1vx=0;

  nBB_TrigA_0vx=0;
  nBE_TrigA_0vx=0;
  nEB_TrigA_0vx=0;
  nEE_TrigA_0vx=0;
  nBB_TrigA_1vx=0;
  nBE_TrigA_1vx=0;
  nEB_TrigA_1vx=0;
  nEE_TrigA_1vx=0;

  nBB_Trig_0vx=0;
  nBE_Trig_0vx=0;
  nEE_Trig_0vx=0;
  nBB_Trig_1vx=0;
  nBE_Trig_1vx=0;
  nEE_Trig_1vx=0;
  nBB_Trig_2vx=0;
  nBE_Trig_2vx=0;
  nEE_Trig_2vx=0;

  TChain *chain = new TChain("recoTree");

  std::cout << "running program!" << std::endl;
  std::cout << "Have " << argc << " arguments:" << std::endl;
  std::cout << "Running over sample: " << argv[1] << std::endl;
  std::cout << "Results aren't put in the folder: " << argv[2] << ". I'm ignoring it. haha. Instead, they're in signalMC/extrapolation/" << std::endl;
  std::cout << "Output File name is: " << argv[3] << std::endl;
  std::cout << "Considering region: " << argv[4] << " of the ABCD plane" << std::endl;

  InputFiles::AddFilesToChain(TString(argv[1]), chain);
  t0 = get_timestamp();

  /*
  const int nPoints=1000;
  Double_t xBinValues[1001];
  //LimitExtrapolation::make_x_axis(xBinValues);
  for(int j=0; j<200; j++){
    xBinValues[j] = LTSTEP3*j;
  }
  for(int j=200; j<400; j++){
    xBinValues[j] = LTSTEP*j - 9.;
  }
  for(int j=400; j<1001; j++){
    xBinValues[j] = LTSTEP2*j - 189.;
  }
  */

  const int nPoints=1000;
  Double_t xBinValues[1001];
  //LimitExtrapolation::make_x_axis(xBinValues);
  for(int j=0; j<200; j++){
    xBinValues[j] = LTSTEP3*j;
  }
  for(int j=200; j<400; j++){
    xBinValues[j] = LTSTEP*j - 9.;
  }
  for(int j=400; j<1001; j++){
    xBinValues[j] = LTSTEP2*j - 189.;
  }

  LLPVariables *toyLLP = new LLPVariables;
  toyLLP->setZeroVectors();
  std::cout << "running over: "  << chain->GetEntries()<< " events" << std::endl;
  toyLLP->setToyBranchAddresses(chain);
  bool lDebug=true;

  double BBTrigProb[20][20];
  double BBTrigProb_maxStat[20][20],      BBTrigProb_minStat[20][20];
  double BBTrigProb_maxSyst[20][20],      BBTrigProb_minSyst[20][20];

  double EETrigProb[20][20];
  double EETrigProb_maxStat[20][20],      EETrigProb_minStat[20][20];
  double EETrigProb_maxSyst[20][20],      EETrigProb_minSyst[20][20];

  double BETrigProb[20][20];
  double BETrigProb_maxStat[20][20],      BETrigProb_minStat[20][20];
  double BETrigProb_maxSyst[20][20],      BETrigProb_minSyst[20][20];

  double BBMSVxProb[20];
  double BBMSVxProb_maxSyst[20],BBMSVxProb_minSyst[20];
  double BBMSVxProb_maxStat[20],BBMSVxProb_minStat[20];
  double BEMSVxProb[20];
  double BEMSVxProb_maxSyst[20],BEMSVxProb_minSyst[20];
  double BEMSVxProb_maxStat[20],BEMSVxProb_minStat[20];
  double EBMSVxProb[20];
  double EBMSVxProb_maxSyst[20],EBMSVxProb_minSyst[20];
  double EBMSVxProb_maxStat[20],EBMSVxProb_minStat[20];
  double EEMSVxProb[20];
  double EEMSVxProb_maxSyst[20],EEMSVxProb_minSyst[20];
  double EEMSVxProb_maxStat[20],EEMSVxProb_minStat[20];

  double BTrigProb[20];       double ETrigProb[20];
  double BTrigProb_maxStat[20];       double ETrigProb_maxStat[20];
  double BTrigProb_minStat[20];       double ETrigProb_minStat[20];
  double BTrigProb_maxSyst[20];       double ETrigProb_maxSyst[20];
  double BTrigProb_minSyst[20];       double ETrigProb_minSyst[20];

  double BBMSVxProb_a[20];
  double BBMSVxProb_a_maxStat[20], BBMSVxProb_a_minStat[20];
  double BBMSVxProb_a_maxSyst[20], BBMSVxProb_a_minSyst[20];
  double BEMSVxProb_a[20];
  double BEMSVxProb_a_maxStat[20], BEMSVxProb_a_minStat[20];
  double BEMSVxProb_a_maxSyst[20], BEMSVxProb_a_minSyst[20];
  double EBMSVxProb_a[20];
  double EBMSVxProb_a_maxStat[20], EBMSVxProb_a_minStat[20];
  double EBMSVxProb_a_maxSyst[20], EBMSVxProb_a_minSyst[20];
  double EEMSVxProb_a[20];
  double EEMSVxProb_a_maxStat[20], EEMSVxProb_a_minStat[20];
  double EEMSVxProb_a_maxSyst[20], EEMSVxProb_a_minSyst[20];
  double BMSVxProb_a[20];
  double BMSVxProb_a_errHigh[20], BMSVxProb_a_errLow[20];
  double BMSVxProb_a_maxStat[20], BMSVxProb_a_minStat[20];
  double BMSVxProb_a_maxSyst[20], BMSVxProb_a_minSyst[20];
  double EMSVxProb_a[20];
  double EMSVxProb_a_errHigh[20], EMSVxProb_a_errLow[20];
  double EMSVxProb_a_maxStat[20], EMSVxProb_a_minStat[20];
  double EMSVxProb_a_maxSyst[20], EMSVxProb_a_minSyst[20];
    
  TString output="pp";

  TString amend="";

  //set parameters: masses, systematics...

  //string to enum?!?
  SampleDetails::setGlobalVariables(TString(argv[1]));

  std::cout << "Systematic Errors are: " << addSystematicErrors << " and have values (from SampleDetails): " << std::endl;
  std::cout << "Barrel RoI:" << SampleDetails::BTrigSysErr_up << ", " << SampleDetails::BTrigSysErr_down << ", Endcap RoI: " <<  SampleDetails::ETrigSysErr_up << ", " << SampleDetails::ETrigSysErr_down << std::endl;
  std::cout << "Barrel MSVx:" << SampleDetails::BVxSysErr_up << ", " << SampleDetails::BVxSysErr_down << ", Endcap MSVx: " << SampleDetails::EVxSysErr_up << ", " << SampleDetails::EVxSysErr_down << std::endl;
  std::cout << "Luminosity (not included in total error calculation at end!): " << LUMISYSERR << std::endl;
  std::cout << "Note that these are all relative errors!" << std::endl;
  std::cout << "Proper lifetime of this sample is: " << SampleDetails::sim_ctau << std::endl;

  TString inputDir = "../OutputPlots/signalMC/";
  TFile *fTrig = TFile::Open(inputDir + TString(argv[1]) + "/outputMSTrigEff.root");
  TFile *fMSVx = TFile::Open(inputDir + TString(argv[1]) + "/outputMSVxEff.root");

  TString dir="/home/calpigia/Exotics/NtuplesCristiano/out/";
  TString filename=TString(argv[1]);
  TFile *fLT = TFile::Open(dir+filename+"_LTEfficiency.root");
  
  cout << filename << endl;

  //////////////////////////////
  //* Histograms declaration *//
  //TFile * outputFile = new TFile(inputDir+"/extrapolation/"+TString(argv[3]),"RECREATE");
  TFile * outputFile = new TFile(inputDir+"/extrapolation/Files_NOLTeff/"+TString(argv[3]),"RECREATE");
  std::cout << "output ROOT file " << output << std::endl;
  TDirectory *d_Expected = outputFile->mkdir("ExpectedEvents");

  std::vector<TString> lifetime = {""};
  std::vector<TString> trigErr = {"","_maxStat","_minStat","_maxSyst","_minSyst","_maxTotal","_minTotal",};
  std::vector<TString> vxErr = {"","_maxTrigStat","_minTrigStat","_maxTrigSyst","_minTrigSyst","_maxVxStat","_minVxStat","_maxVxSyst","_minVxSyst","_maxTotal","_minTotal"};
  std::vector<TString> abcdErr = {"","b","e","_maxTrigStat","_minTrigStat","_maxTrigSyst","_minTrigSyst","_maxVxStat","_minVxStat","_maxVxSyst","_minVxSyst","_maxTotal","_minTotal"};
  std::vector<TString> abcdErrLong = {"","b","e", "_maxTrigStat","_minTrigStat","_maxTrigSyst","_minTrigSyst","_maxBVxStat","_minBVxStat","_maxBVxSyst","_minBVxSyst","_maxEVxStat",
				      "_minEVxStat","_maxEVxSyst","_minEVxSyst",
				      "_maxTotal","_minTotal"};
  std::vector<TString> vxBEErr = {"","_maxTrigStat","_minTrigStat","_maxTrigSyst","_minTrigSyst","_maxBVxStat","_minBVxStat","_maxBVxSyst","_minBVxSyst","_maxEVxStat","_minEVxStat","_maxEVxSyst","_minEVxSyst","_maxTotal","_minTotal"};

  HistTools *histTrig = new HistTools;
  HistTools *hist1Vx = new HistTools;
  HistTools *hist2Vx = new HistTools;

  histTrig->addHist("Expected_1BTrig", trigErr, nPoints, xBinValues);
  histTrig->addHist("Expected_1ETrig", trigErr, nPoints, xBinValues);
  histTrig->addHist("Expected_BBTrig", trigErr, nPoints, xBinValues);
  histTrig->addHist("Expected_BETrig", trigErr, nPoints, xBinValues);
  histTrig->addHist("Expected_EETrig", trigErr, nPoints, xBinValues);

  h_1B_Decays = new TH1D("n1B_Decays","",nPoints, xBinValues);
  h_1E_Decays = new TH1D("n1E_Decays","",nPoints, xBinValues);
  h_BB_Decays = new TH1D("nBB_Decays","",nPoints, xBinValues);
  h_BE_Decays = new TH1D("nBE_Decays","",nPoints, xBinValues);
  h_EE_Decays = new TH1D("nEE_Decays","",nPoints, xBinValues);

  hist1Vx->addHist("Expected_1BMSVx",abcdErr, nPoints, xBinValues);
  hist1Vx->addHist("Expected_1EMSVx",abcdErr, nPoints, xBinValues);
  hist1Vx->addHist("Expected_BB1MSVx",abcdErr, nPoints, xBinValues);
  hist1Vx->addHist("Expected_BE1MSVx",abcdErrLong, nPoints, xBinValues);
  hist1Vx->addHist("Expected_EB1MSVx",abcdErrLong, nPoints, xBinValues);
  hist1Vx->addHist("Expected_EE1MSVx",abcdErr, nPoints, xBinValues);

  hist1Vx->addHist("Expected_ABCD_BVx",trigErr, nPoints, xBinValues); // ????????????????????????
  hist1Vx->addHist("Expected_ABCD_EVx",trigErr, nPoints, xBinValues);
  hist1Vx->addHist("Expected_ABCD1MSVx",trigErr, nPoints, xBinValues);

  cout << "done histos " << endl;

  /////////////////////////////////////////////////////////////////////////////
  //* Expected number of events with at least two reconstructed MS vertices *//

  h_Expected_2MSVx = new TH1D("Expected_2MSVx","",nPoints, xBinValues);

  h_Expected_2MSVx->SetDirectory(d_Expected);

  hist2Vx->addHist("Expected_EEMSVx",vxErr, nPoints, xBinValues);
  hist2Vx->addHist("Expected_EBMSVx",vxBEErr, nPoints, xBinValues);
  hist2Vx->addHist("Expected_BEMSVx",vxBEErr, nPoints, xBinValues);
  hist2Vx->addHist("Expected_BBMSVx",vxErr, nPoints, xBinValues);

  hist2Vx->addHist("Expected_Tot2Vx", trigErr, nPoints, xBinValues); //trig err because all we need now is syst/stat up down.
  cout << "done ms ms histos" << endl;

  ////////////////////////////////////////////////////////////////////////////////
  //* Expected number of events with at least one reconstructed ID vertex *//

  h_Expected_1MSVx = new TH1D("Expected_1MSVx","",nPoints, xBinValues);

  ///////////////////////////////////////////////////////
  //* Get the trigger and reconstruction efficiencies *//


  TH1D *h_1BTrigN     = (TH1D*)fTrig->Get("MSTrig_1B_Lxy"); h_1BTrigN->Rebin(4);
  TH1D *h_1BTrigD     = (TH1D*)fTrig->Get("MSTrig_1B_Lxy_denom"); h_1BTrigD->Rebin(4);
  TGraphAsymmErrors *h_1BTrigEff = new TGraphAsymmErrors(h_1BTrigN,h_1BTrigD,"cl=0.683 b(1,1) mode");
  TH1D *h_1ETrigN     = (TH1D*)fTrig->Get("MSTrig_1E_Lz"); h_1ETrigN->Rebin(4);
  TH1D *h_1ETrigD     = (TH1D*)fTrig->Get("MSTrig_1E_Lz_denom"); h_1ETrigD->Rebin(4);
  TGraphAsymmErrors *h_1ETrigEff = new TGraphAsymmErrors(h_1ETrigN,h_1ETrigD,"cl=0.683 b(1,1) mode");
  std::cout << "got 1 trig eff " << std::endl;


  TH2D *h_BBTrigN     = (TH2D*)fTrig->Get("MSTrig_BB");
  TH2D *h_BBTrigD     = (TH2D*)fTrig->Get("MSTrig_BB_denom");
  TH2D *h_EETrigN     = (TH2D*)fTrig->Get("MSTrig_EE");
  TH2D *h_EETrigD     = (TH2D*)fTrig->Get("MSTrig_EE_denom");
  TH2D *h_BETrigN     = (TH2D*)fTrig->Get("MSTrig_BE");
  TH2D *h_BETrigD     = (TH2D*)fTrig->Get("MSTrig_BE_denom");


  TH1D *h_BBVxN   = (TH1D*)fMSVx->Get("MSVxTrig_BB_Lxy");
  TH1D *h_EEVxN   = (TH1D*)fMSVx->Get("MSVxTrig_EE_Lz");
  TH1D *h_BEVxN   = (TH1D*)fMSVx->Get("MSVxTrig_BE_Lxy");
  TH1D *h_EBVxN   = (TH1D*)fMSVx->Get("MSVxTrig_EB_Lz");
  TH1D *h_BBVxD   = (TH1D*)fMSVx->Get("MSVxTrig_BB_Lxy_denom");
  TH1D *h_EEVxD   = (TH1D*)fMSVx->Get("MSVxTrig_EE_Lz_denom");
  TH1D *h_BEVxD   = (TH1D*)fMSVx->Get("MSVxTrig_BE_Lxy_denom");
  TH1D *h_EBVxD   = (TH1D*)fMSVx->Get("MSVxTrig_EB_Lz_denom");

  TGraphAsymmErrors *h_BBVxEff = new TGraphAsymmErrors(h_BBVxN,h_BBVxD,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *h_BEVxEff = new TGraphAsymmErrors(h_BEVxN,h_BEVxD,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *h_EBVxEff = new TGraphAsymmErrors(h_EBVxN,h_EBVxD,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *h_EEVxEff = new TGraphAsymmErrors(h_EEVxN,h_EEVxD,"cl=0.683 b(1,1) mode");

  std::cout << "got 2msvx eff " << std::endl;

  //abcd 1vx plots
  TString regionABCD="_Reg";
  regionABCD+=argv[4];
  cout << "Running on ABCD region = " << regionABCD << endl;

  TH1D *h_1BVxN_A     = (TH1D*)fMSVx->Get("MSVxABCD_RegA_1B_Lxy");
  TH1D *h_1EVxN_A     = (TH1D*)fMSVx->Get("MSVxABCD_RegA_1E_Lz");
  TH1D *h_1BVxN_B     = (TH1D*)fMSVx->Get("MSVxABCD_RegB_1B_Lxy");
  TH1D *h_1EVxN_B     = (TH1D*)fMSVx->Get("MSVxABCD_RegB_1E_Lz");
  /*
  TH1D *h_BBVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD_BB_Lxy");
  TH1D *h_EEVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD_EE_Lz");
  TH1D *h_BEVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD_BE_Lxy");
  TH1D *h_EBVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD_EB_Lz");
  TH1D *h_1BVxN     = (TH1D*)fMSVx->Get("MSVxABCD_1B_Lxy");
  TH1D *h_1EVxN     = (TH1D*)fMSVx->Get("MSVxABCD_1E_Lz");
  */
  TH1D *h_BBVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD"+regionABCD+"_BB_Lxy");
  TH1D *h_EEVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD"+regionABCD+"_EE_Lz");
  TH1D *h_BEVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD"+regionABCD+"_BE_Lxy");
  TH1D *h_EBVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD"+regionABCD+"_EB_Lz");
  TH1D *h_1BVxN     = (TH1D*)fMSVx->Get("MSVxABCD"+regionABCD+"_1B_Lxy");
  TH1D *h_1EVxN     = (TH1D*)fMSVx->Get("MSVxABCD"+regionABCD+"_1E_Lz");
  
  TH1D *h_BBVxD_a   = (TH1D*)fMSVx->Get("MSVxABCD_BB_Lxy_denom");
  TH1D *h_EEVxD_a   = (TH1D*)fMSVx->Get("MSVxABCD_EE_Lz_denom");
  TH1D *h_BEVxD_a   = (TH1D*)fMSVx->Get("MSVxABCD_BE_Lxy_denom");
  TH1D *h_EBVxD_a   = (TH1D*)fMSVx->Get("MSVxABCD_EB_Lz_denom");
  TH1D *h_1BVxD     = (TH1D*)fMSVx->Get("MSVxABCD_1B_Lxy_denom");
  TH1D *h_1EVxD     = (TH1D*)fMSVx->Get("MSVxABCD_1E_Lz_denom");
  
  TGraphAsymmErrors *h_BBVxEff_a = new TGraphAsymmErrors(h_BBVxN_a,h_BBVxD_a,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *h_EEVxEff_a = new TGraphAsymmErrors(h_EEVxN_a,h_EEVxD_a,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *h_BEVxEff_a = new TGraphAsymmErrors(h_BEVxN_a,h_BEVxD_a,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *h_EBVxEff_a = new TGraphAsymmErrors(h_EBVxN_a,h_EBVxD_a,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *h_1BVxEff = new TGraphAsymmErrors(h_1BVxN,h_1BVxD,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *h_1EVxEff = new TGraphAsymmErrors(h_1EVxN,h_1EVxD,"cl=0.683 b(1,1) mode");

  double frac_barrel  = (h_1BVxN_A->Integral()+h_1BVxN_B->Integral())/(double)h_1BVxN->Integral();
  double frac_endcaps = (h_1EVxN_A->Integral()+h_1EVxN_B->Integral())/(double)h_1EVxN->Integral();
  if (regionABCD=="_RegC" || regionABCD=="_RegD"){
    frac_barrel=1-frac_barrel;
    frac_endcaps=1-frac_endcaps;
  }


  // ** LT
  //
  TString str_reg="";
  if (regionABCD=="_RegA" || regionABCD=="_RegC") str_reg = "RegAC";
  if (regionABCD=="_RegB" || regionABCD=="_RegD") str_reg = "RegBD";
  TH1D *h_LTN_B = (TH1D*)fLT->Get("num_"+str_reg+"_B");
  TH1D *h_LTN_E = (TH1D*)fLT->Get("num_"+str_reg+"_E");
  TH1D *h_LTD_B = (TH1D*)fLT->Get("den_"+str_reg+"_B");
  TH1D *h_LTD_E = (TH1D*)fLT->Get("den_"+str_reg+"_E");
  
  TGraphAsymmErrors *g_LTeff_b = new TGraphAsymmErrors(h_LTN_B,h_LTD_B,"cl=0.683 b(1,1) mode");
  TGraphAsymmErrors *g_LTeff_e = new TGraphAsymmErrors(h_LTN_E,h_LTD_E,"cl=0.683 b(1,1) mode");


  double BMSstepTRIG    = nPoints;
  double EMSstepTRIG    = nPoints;//(h_EETrigeff->GetBinCenter(h_EETrigeff->GetNbinsX())+h_EETrigeff->GetBinWidth(0)/2.)*nPoints/(h_EETrigeff->GetNbinsX()); 
  double BEMSstepTRIG_X = nPoints;//(h_BETrigeff->GetXaxis()->GetBinCenter(h_BETrigeff->GetNbinsX())+h_BETrigeff->GetXaxis()->GetBinWidth(0)/2.)*nPoints/(h_BETrigeff->GetNbinsX());
  double BEMSstepTRIG_Y = nPoints;//(h_BETrigeff->GetYaxis()->GetBinCenter(h_BETrigeff->GetNbinsY())+h_BETrigeff->GetYaxis()->GetBinWidth(0)/2.)*nPoints/(h_BETrigeff->GetNbinsY());

  double BBMSstep = (h_BBVxD->GetBinCenter(h_BBVxD->GetNbinsX())+h_BBVxD->GetBinWidth(0)/2.)*nPoints/(h_BBVxD->GetNbinsX());
  double BEMSstep = (h_BEVxD->GetBinCenter(h_BEVxD->GetNbinsX())+h_BEVxD->GetBinWidth(0)/2.)*nPoints/(h_BEVxD->GetNbinsX());
  double EBMSstep = (h_EBVxD->GetBinCenter(h_EBVxD->GetNbinsX())+h_EBVxD->GetBinWidth(0)/2.)*nPoints/(h_EBVxD->GetNbinsX());
  double EEMSstep = (h_EEVxD->GetBinCenter(h_EEVxD->GetNbinsX())+h_EEVxD->GetBinWidth(0)/2.)*nPoints/(h_EEVxD->GetNbinsX());

  int nBBTrigbins,nEETrigbins;
  int nBETrigbinsX,nBETrigbinsY;
  int nBBVXbins,nBEVXbins,nEBVXbins,nEEVXbins;

  nBBTrigbins     = h_BBTrigD->GetNbinsX();
  nBETrigbinsX    = h_BETrigD->GetNbinsX();
  nBETrigbinsY    = h_BETrigD->GetNbinsY();
  nEETrigbins     = h_EETrigD->GetNbinsX();

  nBBVXbins      = h_BBVxD->GetNbinsX();
  nBEVXbins      = h_BEVxD->GetNbinsX();
  nEBVXbins      = h_EBVxD->GetNbinsX();
  nEEVXbins      = h_EEVxD->GetNbinsX();

  cout << "got efficiencies " << endl;
  if (true) {
    cout << "==>> Pointers to efficiency histograms <<==" << endl;
    cout << "     Trigger_BB_Eff "     << h_BBTrigD << endl;
    cout << "     Trigger_BE_Eff "     << h_BETrigD << endl;
    cout << "     Trigger_EE_Eff "     << h_EETrigD << endl;
    cout << "     MSvx_BB_Trig "   << h_BBVxEff << endl;
    cout << "     MSvx_BE_Trig "   << h_BEVxEff << endl;
    cout << "     MSvx_EB_Trig "   << h_EBVxEff << endl;
    cout << "     MSvx_EE_Trig "   << h_EEVxEff << endl;

    cout << "" << endl;
  }


  // ** LT                                                                                                                                                                             
  //                                                                                                                                                                                   
  const int nBins_LT_B = h_LTD_B->GetNbinsX();
  double BMSLTProb[nBins_LT_B];
  double BMSLTProb_errHigh[nBins_LT_B], BMSLTProb_errLow[nBins_LT_B];
  double BMSLTProb_maxStat[nBins_LT_B], BMSLTProb_minStat[nBins_LT_B];
  double BMSLTProb_maxSyst[nBins_LT_B], BMSLTProb_minSyst[nBins_LT_B];
  for(int i=0; i<nBins_LT_B; ++i){
    if(h_LTD_B->GetBinContent(i+1) > 0){
      double bineff =  g_LTeff_b->Eval(h_LTD_B->GetBinCenter(i+1));
      BMSLTProb[i] = bineff;
      BMSLTProb_errHigh[i] = g_LTeff_b->GetErrorYhigh(i);
      BMSLTProb_errLow[i]  = g_LTeff_b->GetErrorYlow(i);
      BMSLTProb_maxStat[i] = bineff + g_LTeff_b->GetErrorYhigh(i);
      BMSLTProb_minStat[i] = bineff - g_LTeff_b->GetErrorYlow(i);
      BMSLTProb_maxSyst[i] = 0;
      BMSLTProb_minSyst[i] = 0;

      cout << " BMSLTProb[i] = " << BMSLTProb[i] << endl;
      cout << " BMSLTProb_maxStat[i] = " << BMSLTProb_maxStat[i] << endl;
      cout << " BMSLTProb_minStat[i] = " << BMSLTProb_minStat[i] << endl;
    }else{
      BMSLTProb[i] = 0;
      BMSLTProb_maxSyst[i] = 0;
      BMSLTProb_minSyst[i] = 0;
      BMSLTProb_maxStat[i] = 0;
      BMSLTProb_minStat[i] = 0;
    }
  } 
  const int nBins_LT_E = h_LTD_E->GetNbinsX();
  double EMSLTProb[nBins_LT_E];
  double EMSLTProb_errHigh[nBins_LT_E], EMSLTProb_errLow[nBins_LT_E];
  double EMSLTProb_maxStat[nBins_LT_E], EMSLTProb_minStat[nBins_LT_E];
  double EMSLTProb_maxSyst[nBins_LT_E], EMSLTProb_minSyst[nBins_LT_E];
  for(int i=0; i<nBins_LT_E; ++i){
    if(h_LTD_E->GetBinContent(i+1) > 0){
      double bineff =  g_LTeff_e->Eval(h_LTD_E->GetBinCenter(i+1));
      EMSLTProb[i] = bineff;
      EMSLTProb_errHigh[i] = g_LTeff_e->GetErrorYhigh(i);
      EMSLTProb_errLow[i]  = g_LTeff_e->GetErrorYlow(i);
      EMSLTProb_maxStat[i] = bineff + g_LTeff_e->GetErrorYhigh(i);
      EMSLTProb_minStat[i] = bineff - g_LTeff_e->GetErrorYlow(i);
      EMSLTProb_maxSyst[i] = 0;
      EMSLTProb_minSyst[i] = 0;

      cout << " EMSLTProb[i] = " << EMSLTProb[i] << endl;
      cout << " EMSLTProb_maxStat[i] = " << EMSLTProb_maxStat[i] << endl;
      cout << " EMSLTProb_minStat[i] = " << EMSLTProb_minStat[i] << endl;

    }else{
      EMSLTProb[i] = 0;
      EMSLTProb_maxSyst[i] = 0;
      EMSLTProb_minSyst[i] = 0;
      EMSLTProb_maxStat[i] = 0;
      EMSLTProb_minStat[i] = 0;
    }
  }


  for(int i=0; i<20; ++i) {
    // abcd ones
    if(i < nBBVXbins && h_1BTrigD->GetBinContent(i+1) > 0 ){
      double bineff = h_1BTrigEff->Eval(h_1BTrigEff->GetX()[i+1-4]);
      BTrigProb[i] = bineff;
      BTrigProb_maxStat[i] = bineff + h_1BTrigEff->GetErrorYhigh(i+1-4);
      BTrigProb_minStat[i] = bineff - h_1BTrigEff->GetErrorYlow(i+1-4);
      BTrigProb_maxSyst[i] = (1+SampleDetails::BTrigSysErr_up)*bineff;
      BTrigProb_minSyst[i] = (1-SampleDetails::BTrigSysErr_down)*bineff;
      //std::cout << "1B bin eff: " << bineff  << ", " << h_1BTrigN->GetBinContent(i+1)/h_1BTrigD->GetBinContent(i+1)<< std::endl;
    } else {
      BTrigProb[i] = 0;
      BTrigProb_maxSyst[i] = 0;
      BTrigProb_minSyst[i] = 0;
      BTrigProb_maxStat[i] = 0;
      BTrigProb_minStat[i] = 0;
    }
    if(i<nEEVXbins && (h_1ETrigD->GetBinContent(i+1) > 0)) {
      double bineff = h_1ETrigEff->Eval(h_1ETrigEff->GetX()[i+1-6]);
      ETrigProb[i] = bineff;
      ETrigProb_maxStat[i] = bineff + h_1ETrigEff->GetErrorYhigh(i+1-6);
      ETrigProb_minStat[i] = bineff - h_1ETrigEff->GetErrorYlow(i+1-6);
      ETrigProb_maxSyst[i] = (1+SampleDetails::ETrigSysErr_up)*bineff;
      ETrigProb_minSyst[i] = (1-SampleDetails::ETrigSysErr_down)*bineff;
      //std::cout << "1E bin eff: " << bineff  << ", " << h_1ETrigN->GetBinContent(i+1)/h_1ETrigD->GetBinContent(i+1) << std::endl;
    } else {
      ETrigProb[i] = 0;
      ETrigProb_maxSyst[i] = 0;
      ETrigProb_minSyst[i] = 0;
      ETrigProb_maxStat[i] = 0;
      ETrigProb_minStat[i] = 0;
    }

    if(i < nBBVXbins && h_1BVxD->GetBinContent(i+1) > 0 ){
      double bineff = h_1BVxEff->Eval(h_1BVxEff->GetX()[i+1-4]);
      BMSVxProb_a[i] = bineff;
      BMSVxProb_a_errHigh[i] = h_1BVxEff->GetErrorYhigh(i+1-4);
      BMSVxProb_a_errLow[i]  = h_1BVxEff->GetErrorYlow(i+1-4); 
      BMSVxProb_a_maxStat[i] = bineff + h_1BVxEff->GetErrorYhigh(i+1-4);
      BMSVxProb_a_minStat[i] = bineff - h_1BVxEff->GetErrorYlow(i+1-4);
      BMSVxProb_a_maxSyst[i] = (1+SampleDetails::BVxSysErr_up)*bineff;
      BMSVxProb_a_minSyst[i] = (1-SampleDetails::BVxSysErr_down)*bineff;
      //std::cout << "1B vx bin eff: " << bineff << ", " << h_1BVxN->GetBinContent(i+1)/h_1BVxD->GetBinContent(i+1) << std::endl;

    } else {
      BMSVxProb_a[i] = 0;
      BMSVxProb_a_maxSyst[i] = 0;
      BMSVxProb_a_minSyst[i] = 0;
      BMSVxProb_a_maxStat[i] = 0;
      BMSVxProb_a_minStat[i] = 0;
    }
    if(i<nEEVXbins && (h_1EVxD->GetBinContent(i+1) > 0)) {
      double bineff =  h_1EVxEff->Eval(h_1EVxEff->GetX()[i+1-6]);
      EMSVxProb_a[i] = bineff;
      EMSVxProb_a_errHigh[i] = h_1EVxEff->GetErrorYhigh(i+1-4);
      EMSVxProb_a_errLow[i]  = h_1EVxEff->GetErrorYlow(i+1-4); 
      EMSVxProb_a_maxStat[i] = bineff + h_1EVxEff->GetErrorYhigh(i+1-6);
      EMSVxProb_a_minStat[i] = bineff - h_1EVxEff->GetErrorYlow(i+1-6);
      EMSVxProb_a_maxSyst[i] = (1+SampleDetails::EVxSysErr_up)*bineff;
      EMSVxProb_a_minSyst[i] = (1-SampleDetails::EVxSysErr_down)*bineff;
      //std::cout << "1E vx bin eff: " << bineff << ", " << h_1EVxN->GetBinContent(i+1)/h_1EVxD->GetBinContent(i+1) << std::endl;

    } else {
      EMSVxProb_a[i] = 0;
      EMSVxProb_a_maxSyst[i] = 0;
      EMSVxProb_a_minSyst[i] = 0;
      EMSVxProb_a_maxStat[i] = 0;
      EMSVxProb_a_minStat[i] = 0;
    }
    if(i<nBBVXbins && h_BBVxD_a->GetBinContent(i+1) > 0 ) {
      double bineff = h_BBVxEff_a->Eval(h_BBVxEff_a->GetX()[i+1-4]);
      BBMSVxProb_a[i] = bineff;
      BBMSVxProb_a_maxStat[i] = bineff + h_BBVxEff_a->GetErrorYhigh(i+1-4);
      BBMSVxProb_a_minStat[i] = bineff - h_BBVxEff_a->GetErrorYlow(i+1-4);
      BBMSVxProb_a_maxSyst[i] = (1+SampleDetails::BVxSysErr_up)*bineff;
      BBMSVxProb_a_minSyst[i] = (1-SampleDetails::BVxSysErr_down)*bineff;
      //std::cout << "BB abcd vx bin eff: " << bineff << ", " << h_BBVxN_a->GetBinContent(i+1)/h_BBVxD_a->GetBinContent(i+1) << std::endl;

    } else {
      BBMSVxProb_a[i] = 0;
      BBMSVxProb_a_maxSyst[i] = 0;
      BBMSVxProb_a_minSyst[i] = 0;
      BBMSVxProb_a_maxStat[i] = 0;
      BBMSVxProb_a_minStat[i] = 0;
    }
    if(i<nBEVXbins && (h_BEVxD_a->GetBinContent(i+1) > 0) ) {
      double bineff = h_BEVxEff_a->Eval(h_BEVxEff_a->GetX()[i+1-4]);
      BEMSVxProb_a[i] = bineff;
      BEMSVxProb_a_maxStat[i] = bineff + h_BEVxEff_a->GetErrorYhigh(i+1-4);
      BEMSVxProb_a_minStat[i] = bineff - h_BEVxEff_a->GetErrorYlow(i+1-4);
      BEMSVxProb_a_maxSyst[i] = (1+SampleDetails::BVxSysErr_up)*bineff;
      BEMSVxProb_a_minSyst[i] = (1-SampleDetails::BVxSysErr_down)*bineff;
      //std::cout << "BE abcd vx bin eff: " << bineff << ", " << h_BEVxN_a->GetBinContent(i+1)/h_BEVxD_a->GetBinContent(i+1) << std::endl;

    } else {
      BEMSVxProb_a[i] = 0;
      BEMSVxProb_a_maxSyst[i] = 0;
      BEMSVxProb_a_minSyst[i] = 0;
      BEMSVxProb_a_maxStat[i] = 0;
      BEMSVxProb_a_minStat[i] = 0;
    }
    if(i<nEBVXbins && (h_EBVxD_a->GetBinContent(i+1) > 0) ) {
      double bineff =  h_EBVxEff_a->Eval(h_EBVxEff_a->GetX()[i+1-6]);
      EBMSVxProb_a[i] = bineff;
      EBMSVxProb_a_maxStat[i] = bineff + h_EBVxEff_a->GetErrorYhigh(i+1-6);
      EBMSVxProb_a_minStat[i] = bineff - h_EBVxEff_a->GetErrorYlow(i+1-6);
      EBMSVxProb_a_maxSyst[i] = (1+SampleDetails::EVxSysErr_up)*bineff;
      EBMSVxProb_a_minSyst[i] = (1-SampleDetails::EVxSysErr_down)*bineff;
      //std::cout << "EB abcd vx bin eff: " << bineff << ", " << h_EBVxN_a->GetBinContent(i+1)/h_EBVxD_a->GetBinContent(i+1) << std::endl;

    } else {
      EBMSVxProb_a[i] = 0;
      EBMSVxProb_a_maxSyst[i] = 0;
      EBMSVxProb_a_minSyst[i] = 0;
      EBMSVxProb_a_maxStat[i] = 0;
      EBMSVxProb_a_minStat[i] = 0;
    }
    if(i<nEEVXbins && (h_EEVxD_a->GetBinContent(i+1) > 0)) {
      double bineff =  h_EEVxEff_a->Eval(h_EEVxEff_a->GetX()[i+1-6]);
      EEMSVxProb_a[i] = bineff;
      EEMSVxProb_a_maxStat[i] = bineff + h_EEVxEff_a->GetErrorYhigh(i+1-6);
      EEMSVxProb_a_minStat[i] = bineff + h_EEVxEff_a->GetErrorYhigh(i+1-6);
      EEMSVxProb_a_maxSyst[i] = (1+SampleDetails::EVxSysErr_up)*bineff;
      EEMSVxProb_a_minSyst[i] = (1-SampleDetails::EVxSysErr_down)*bineff;
      //std::cout << "EE abcd vx bin eff: " << bineff << ", " << h_EEVxN_a->GetBinContent(i+1)/h_EEVxD_a->GetBinContent(i+1) << std::endl;

    } else {
      EEMSVxProb_a[i] = 0;
      EEMSVxProb_a_maxSyst[i] = 0;
      EEMSVxProb_a_minSyst[i] = 0;
      EEMSVxProb_a_maxStat[i] = 0;
      EEMSVxProb_a_minStat[i] = 0;
    }
    //non-abcd ones
    if(i<nBBVXbins && h_BBVxD->GetBinContent(i+1) > 0 ) {
      double bineff = h_BBVxEff->Eval(h_BBVxEff->GetX()[i+1-4]);
      BBMSVxProb[i] = bineff;
      BBMSVxProb_maxStat[i] = bineff + h_BBVxEff->GetErrorYhigh(i+1-4);
      BBMSVxProb_minStat[i] = bineff - h_BBVxEff->GetErrorYlow(i+1-4);
      BBMSVxProb_maxSyst[i] = (1+SampleDetails::BVxSysErr_up)*bineff;
      BBMSVxProb_minSyst[i] = (1-SampleDetails::BVxSysErr_down)*bineff;
      //std::cout << "BB vx bin eff: " << bineff << ", " << h_BBVxN->GetBinContent(i+1)/h_BBVxD->GetBinContent(i+1) << std::endl;

    } else {
      BBMSVxProb[i] = 0;
      BBMSVxProb_maxStat[i] = 0;
      BBMSVxProb_minStat[i] = 0;
      BBMSVxProb_maxSyst[i] = 0;
      BBMSVxProb_minSyst[i] = 0;
    }
    if(i<nBEVXbins && (h_BEVxD->GetBinContent(i+1) > 0) ) {
      double bineff = h_BEVxEff->Eval(h_BEVxEff->GetX()[i+1-4]);
      BEMSVxProb[i] = bineff;
      BEMSVxProb_maxStat[i] = bineff + h_BEVxEff->GetErrorYhigh(i+1-4);
      BEMSVxProb_minStat[i] = bineff - h_BEVxEff->GetErrorYlow(i+1-4);
      BEMSVxProb_maxSyst[i] = (1+SampleDetails::BVxSysErr_up)*bineff;
      BEMSVxProb_minSyst[i] = (1-SampleDetails::BVxSysErr_down)*bineff;
      //std::cout << "BE vx bin eff: " << bineff << ", " << h_BEVxN->GetBinContent(i+1)/h_BEVxD->GetBinContent(i+1) << std::endl;

    } else {
      BEMSVxProb[i] = 0;
      BEMSVxProb_maxStat[i] = 0;
      BEMSVxProb_minStat[i] = 0;
      BEMSVxProb_maxSyst[i] = 0;
      BEMSVxProb_minSyst[i] = 0;
    }
    if(i<nEBVXbins && (h_EBVxD->GetBinContent(i+1) > 0) ) {
      double bineff = h_EBVxEff->Eval(h_EBVxEff->GetX()[i+1-6]);
      EBMSVxProb[i] = bineff;
      EBMSVxProb_maxStat[i] = bineff + h_EBVxEff->GetErrorYhigh(i+1-6);
      EBMSVxProb_minStat[i] = bineff - h_EBVxEff->GetErrorYlow(i+1-6);
      EBMSVxProb_maxSyst[i] = (1+SampleDetails::EVxSysErr_up)*bineff;
      EBMSVxProb_minSyst[i] = (1-SampleDetails::EVxSysErr_down)*bineff;
      //std::cout << "EB vx bin eff: " << bineff << ", " << h_EBVxN->GetBinContent(i+1)/h_EBVxD->GetBinContent(i+1) << std::endl;

    } else {
      EBMSVxProb[i] = 0;
      EBMSVxProb_maxStat[i] = 0;
      EBMSVxProb_minStat[i] = 0;
      EBMSVxProb_maxSyst[i] = 0;
      EBMSVxProb_minSyst[i] = 0;
    }
    if(i<nEEVXbins && (h_EEVxD->GetBinContent(i+1) > 0)) {
      double bineff = h_EEVxEff->Eval(h_EEVxEff->GetX()[i+1-6]);
      EEMSVxProb[i] = bineff;
      EEMSVxProb_maxStat[i] = bineff + h_EEVxEff->GetErrorYhigh(i+1-6);
      EEMSVxProb_minStat[i] = bineff - h_EEVxEff->GetErrorYlow(i+1-6);
      EEMSVxProb_maxSyst[i] = (1+SampleDetails::EVxSysErr_up)*bineff;
      EEMSVxProb_minSyst[i] = (1-SampleDetails::EVxSysErr_down)*bineff;
      //std::cout << "EE vx bin eff: " << bineff << ", " << h_EEVxN->GetBinContent(i+1)/h_EEVxD->GetBinContent(i+1) << std::endl;

    } else {
      EEMSVxProb[i] = 0;
      EEMSVxProb_maxStat[i] = 0;
      EEMSVxProb_minStat[i] = 0;

      EEMSVxProb_maxSyst[i] = 0;
      EEMSVxProb_minSyst[i] = 0;
    }
    for(int j=0; j<20; j++){
	  
      if(j < nBBTrigbins && i < nBBTrigbins &&  (h_BBTrigD->GetBinContent(i+1,j+1)>0) ) {
	TH1D *hd = h_BBTrigD->ProjectionX("tgD_px",j+1,j+1);
	TH1D *hn = h_BBTrigN->ProjectionX("tgn_px",j+1,j+1);
	TGraphAsymmErrors *tg = new TGraphAsymmErrors(hn,hd,"cl=0.683 b(1,1) mode");
	double bbeff = tg->Eval(tg->GetX()[i+1-4-(j+1-4)]);
	BBTrigProb[i][j] = bbeff;
	//std::cout << "BB Trig eff: " << bbeff << std::endl;
	BBTrigProb_maxStat[i][j] = bbeff + tg->GetErrorYhigh(i+1-4-(j+1-4));
	BBTrigProb_minStat[i][j] = bbeff - tg->GetErrorYlow(i+1-4-(j+1-4));
	BBTrigProb_maxSyst[i][j] = (1+SampleDetails::BTrigSysErr_up)*bbeff;
	BBTrigProb_minSyst[i][j] = (1-SampleDetails::BTrigSysErr_down)*bbeff;
	//std::cout << "bb trig eff: " << bbeff << ", " << BBTrigProb_maxStat[i][j] <<", " << BBTrigProb_minStat[i][j] << " at " << i << ", " << j << std::endl;
	delete tg; delete hd; delete hn;
      } else {
	BBTrigProb[i][j] = 0;
	BBTrigProb_maxStat[i][j] = 0;
	BBTrigProb_minStat[i][j] = 0;
	BBTrigProb_maxSyst[i][j] = 0;
	BBTrigProb_minSyst[i][j] = 0;
      }
      if(j < nBETrigbinsY && i < nBETrigbinsX && (h_BETrigD->GetBinContent(i+1,j+1) > 0) ) {
	TH1D *hd = h_BETrigD->ProjectionX("tgD_px",j+1,j+1);
	TH1D *hn = h_BETrigN->ProjectionX("tgn_px",j+1,j+1);
	TGraphAsymmErrors *tg = new TGraphAsymmErrors(hn,hd,"cl=0.683 b(1,1) mode");
	double beeff = tg->Eval(tg->GetX()[i+1-6]);
	double beerrup = tg->GetErrorYhigh(i+1-6);
	double beerrlow = tg->GetErrorYhigh(i+1-6);

	BETrigProb[i][j] = beeff;
	BETrigProb_maxStat[i][j] = (1+beerrup)*beeff;
	BETrigProb_minStat[i][j] = (1-beerrlow)*beeff;
	BETrigProb_maxSyst[i][j] = (1+SampleDetails::BTrigSysErr_up)*beeff;
	BETrigProb_minSyst[i][j] = (1-SampleDetails::BTrigSysErr_down)*beeff;
	//cout << "be trig eff: " << beeff << ", " << BETrigProb_maxStat[i][j] <<", " << BETrigProb_minStat[i][j] << " at " << i << ", " << j << endl;

	delete tg; delete hd; delete hn;

      } else {
	BETrigProb[i][j] = 0;
	BETrigProb_maxStat[i][j] = 0;
	BETrigProb_minStat[i][j] = 0;
	BETrigProb_maxSyst[i][j] = 0;
	BETrigProb_minSyst[i][j] = 0;
      }
      if(j < nEETrigbins && i < nEETrigbins && (h_EETrigD->GetBinContent(i+1,j+1)>0)) {
	TH1D *hd = h_EETrigD->ProjectionX("tgD_px",j+1,j+1);
	TH1D *hn = h_EETrigN->ProjectionX("tgn_px",j+1,j+1);
	TGraphAsymmErrors *tg = new TGraphAsymmErrors(hn,hd,"cl=0.683 b(1,1) mode");
	double eeeff = tg->Eval(tg->GetX()[i+1-6 - (j+1-6) ]);
	double eeerrup = tg->GetErrorYhigh(i+1-6 - (j+1-6) );
	double eeerrlow = tg->GetErrorYhigh(i+1-6 - (j+1-6) );

	EETrigProb[i][j] = eeeff;
	EETrigProb_maxStat[i][j] = (1+eeerrup)*eeeff;
	EETrigProb_minStat[i][j] = (1-eeerrlow)*eeeff;
	EETrigProb_maxSyst[i][j] = (1+SampleDetails::ETrigSysErr_up)*eeeff;
	EETrigProb_minSyst[i][j] = (1-SampleDetails::ETrigSysErr_down)*eeeff;
	//cout << "ee trig eff: " << eeeff << ", " << eeerrup << ", " << eeerrlow << " at " << i << ", " << j << endl;

      } else {
	EETrigProb[i][j] = 0;
	EETrigProb_maxStat[i][j] = 0;
	EETrigProb_minStat[i][j] = 0;
	EETrigProb_maxSyst[i][j] = 0;
	EETrigProb_minSyst[i][j] = 0;
      }
    }

  }
  if(debug) std::cout << "filled efficiency arrays, going to loop through " << chain->GetEntries() << " events" << std::endl;
  for (int i_evt=0; i_evt < chain->GetEntries(); i_evt++)
    {
      /* if(i_evt % 1000 == 0){
         timer.Stop();
         std::cout << "event " << i_evt  << " at time " << timer.RealTime() << std::endl;
         timer.Start();
         }
      */
      chain->GetEntry(i_evt);
      if(debug) std::cout << "retrieved event " << i_evt << std::endl;

      toyLLP->SetAllVariables();

      nEvents+= 1.0;
      if(i_evt == 0) rnd.SetSeed(toyLLP->pT->at(0));
      if(i_evt % 10000 == 0) {
	Long64_t nEntries = chain->GetEntries();
	timestamp_t t1 = get_timestamp();
	double time1 = (t1 - t0) / 1000000.0L;
	// if(time1 > 0.0000031) cout << "time1 " << time1 << endl;
	cout << "nEvent: " << nEvents << "/" << nEntries  << " at time " << time1 << endl;
	t0 = t1;
      }

      double deltar_llps = -99;

      if(toyLLP->eta->size() == 2){
	deltar_llps = DeltaR(toyLLP->phi->at(0), toyLLP->phi->at(1), toyLLP->eta->at(0), toyLLP->eta->at(1));
      } else{
	cout << "there are: " << toyLLP->eta->size() << " llps in this event!" << endl;
	//nEventsWithout2LLPs++;
	continue;
      }

      //if(deltar_vpi < 2.0) continue; //only if doing 2vx search only!

      if(TMath::Abs(toyLLP->eta->at(0)) > 2.5 && TMath::Abs(toyLLP->eta->at(1)) > 2.5)
	continue; //abcd can use one up to 3.2 (end of calorimeters...)
      else  if(TMath::Abs(toyLLP->eta->at(0)) > 3.2 || TMath::Abs(toyLLP->eta->at(1)) > 3.2)
	continue;
      if(debug) std::cout << "topology eta check is ok! " << std::endl;
      ////////////////////////////////
      //* Varibales initialization *//

      int barreldec = 0;
      int endcapdec = 0;

      double R1=0.;
      double R2=0.;
      double z1=0.;
      double z2=0.;
      double eta1=0.;
      double eta2=0.;
      double ctau = -999;

      //k's for sim_ctaus: 250 - 384, 500 - 304, 800 - 284, 1200 - 200, 1500 - 180, 2000 - 148
      //need to divide these, these are for the other binning

      //mH400mS50: 0.7, 1.26 correspond to 280, 410

      for(int k=0; k<(nPoints+1); ++k) {
	//for(int k=300; k < 500; ++k){ //starting at k=200 doesn't work for mg1500, mg2000 - ctaus are shorter!
	/*
	if(k < 200 ) ctau = k*LTSTEP3 + 0.001;
	else if(k < 400) ctau = k*LTSTEP + 0.001 - 9.;
	else if(k < 1000) ctau = k*LTSTEP2 + 0.001 - 189.;
	*/
	if(k < 200 ) ctau = k*LTSTEP3 + 0.001;
	else if(k < 400) ctau = k*LTSTEP + 0.001 - 9.;
	else if(k < 1000) ctau = k*LTSTEP2 + 0.001 - 189.;

	int isBBTrig(0);
	int isBBTrig_maxStat(0),isBBTrig_minStat(0);
	int isBBTrig_maxSyst(0),isBBTrig_minSyst(0);
	int isBETrig(0);
	int isBETrig_maxStat(0),isBETrig_minStat(0);
	int isBETrig_maxSyst(0),isBETrig_minSyst(0);
	int isEETrig(0);
	int isEETrig_maxStat(0),isEETrig_minStat(0);
	int isEETrig_maxSyst(0),isEETrig_minSyst(0);

	int is1BTrig(0);
	int is1BTrig_maxStat(0),is1BTrig_minStat(0);
	int is1BTrig_maxSyst(0),is1BTrig_minSyst(0);
	int is1ETrig(0);
	int is1ETrig_maxStat(0),is1ETrig_minStat(0);
	int is1ETrig_maxSyst(0),is1ETrig_minSyst(0);


	int isBB1MSVx(0);
	int isBB1MSVx_maxTrigStat(0),isBB1MSVx_minTrigStat(0);
	int isBB1MSVx_maxTrigSyst(0),isBB1MSVx_minTrigSyst(0);
	int isBB1MSVx_maxSyst(0),isBB1MSVx_minSyst(0);
	int isBB1MSVx_maxStat(0),isBB1MSVx_minStat(0);

	int isBE1MSVx(0);
	int isBE1MSVx_maxTrigStat(0),isBE1MSVx_minTrigStat(0);
	int isBE1MSVx_maxTrigSyst(0),isBE1MSVx_minTrigSyst(0);
	int isBE1MSVx_maxSyst(0),isBE1MSVx_minSyst(0);
	int isBE1MSVx_maxStat(0),isBE1MSVx_minStat(0);

	int isEE1MSVx(0);
	int isEE1MSVx_maxTrigStat(0),isEE1MSVx_minTrigStat(0);
	int isEE1MSVx_maxTrigSyst(0),isEE1MSVx_minTrigSyst(0);
	int isEE1MSVx_maxSyst(0),isEE1MSVx_minSyst(0);
	int isEE1MSVx_maxStat(0),isEE1MSVx_minStat(0);

	int isEB1MSVx(0);
	int isEB1MSVx_maxTrigStat(0),isEB1MSVx_minTrigStat(0);
	int isEB1MSVx_maxTrigSyst(0),isEB1MSVx_minTrigSyst(0);
	int isEB1MSVx_maxSyst(0),isEB1MSVx_minSyst(0);
	int isEB1MSVx_maxStat(0),isEB1MSVx_minStat(0);

	int is1BMSVx(0);
	int is1BMSVx_maxTrigStat(0),is1BMSVx_minTrigStat(0);
	int is1BMSVx_maxTrigSyst(0),is1BMSVx_minTrigSyst(0);
	int is1BMSVx_maxSyst(0),is1BMSVx_minSyst(0);
	int is1BMSVx_maxStat(0),is1BMSVx_minStat(0);

	int is1EMSVx(0);
	int is1EMSVx_maxTrigStat(0),is1EMSVx_minTrigStat(0);
	int is1EMSVx_maxTrigSyst(0),is1EMSVx_minTrigSyst(0);
	int is1EMSVx_maxSyst(0),is1EMSVx_minSyst(0);
	int is1EMSVx_maxStat(0),is1EMSVx_minStat(0);

	//2 vertex search
	int isBBMSVx(0);
	int isBBMSVx_maxTrigStat(0),isBBMSVx_minTrigStat(0);
	int isBBMSVx_maxTrigSyst(0),isBBMSVx_minTrigSyst(0);
	int isBBMSVx_maxSyst(0),isBBMSVx_minSyst(0);
	int isBBMSVx_maxStat(0); int isBBMSVx_minStat(0);

	int isBEMSVx(0);
	int isBEMSVx_maxTrigStat(0),isBEMSVx_minTrigStat(0);
	int isBEMSVx_maxTrigSyst(0),isBEMSVx_minTrigSyst(0);
	int isBEMSVx_maxSyst(0), isBEMSVx_minSyst(0);
	int isBEMSVx_maxStat(0), isBEMSVx_minStat(0);


	int isEBMSVx(0);
	int isEBMSVx_maxTrigStat(0),isEBMSVx_minTrigStat(0);
	int isEBMSVx_maxTrigSyst(0),isEBMSVx_minTrigSyst(0);
	int isEBMSVx_maxSyst(0),isEBMSVx_minSyst(0);
	int isEBMSVx_maxStat(0); int isEBMSVx_minStat(0);


	int isEEMSVx_maxStat(0); int isEEMSVx_minStat(0);

	int isEEMSVx(0);
	int isEEMSVx_maxTrigStat(0),isEEMSVx_minTrigStat(0);
	int isEEMSVx_maxTrigSyst(0),isEEMSVx_minTrigSyst(0);
	int isEEMSVx_maxSyst(0),isEEMSVx_minSyst(0);

	int isPassedTrig(0),isPassedTrig_min(0),isPassedTrig_max(0);
	bool isGoodMSVx(false),isGoodMSVx_min(false),isGoodMSVx_max(false);
	int MSVxCombo(0),MSVxCombo_max(0),MSVxCombo_min(0);

	int nVertices(0);


	//if(ctau > 10) break;

	std::vector<TVector3> new_decay_pos;
	for(unsigned int i=0; i<toyLLP->eta->size(); ++i) {

	  ////////////////////////////////
	  //* Generating v-pion decays *//O

	  double bgct = toyLLP->bg->at(i)*ctau*1000.0; //in mm
	  double Lxyz = rnd.Exp(bgct);
	  TVector3 tmp_pos;
	  tmp_pos.SetMagThetaPhi(Lxyz,toyLLP->theta->at(i),toyLLP->phi->at(i));
	  new_decay_pos.push_back(tmp_pos);
	}

	double trigPassProb = triggerPassProb(new_decay_pos,toyLLP->beta,k,ctau);

	if(trigPassProb > 0){
	  if(is1B_Event(new_decay_pos)){

	    if(debug) std::cout << "1B event! " << std::endl;
	    int bMSindex=0;
	    if(checkDecLoc(new_decay_pos[0]) == 1){
	      bMSindex = int(new_decay_pos[0].Perp()/BBMSstep);
	    } else{
	      bMSindex = int(new_decay_pos[1].Perp()/BBMSstep);
	    }
	    if(trigPassProb < BTrigProb[bMSindex]){
	      is1BTrig++;
	      isPassedTrig++;
	      if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k) n1B_Trig++;
	    }
	    if(trigPassProb < BTrigProb_maxStat[bMSindex]) is1BTrig_maxStat++;
	    if(trigPassProb < BTrigProb_minStat[bMSindex]) is1BTrig_minStat++;
	    if(trigPassProb < BTrigProb_maxSyst[bMSindex]) is1BTrig_maxSyst++;
	    if(trigPassProb < BTrigProb_minSyst[bMSindex]) is1BTrig_minSyst++;
	  } else if(is1E_Event(new_decay_pos)){

	    if(debug) std::cout << "1E event! " << std::endl;
	    int eMSindex=0;
	    if(checkDecLoc(new_decay_pos[0]) == 2){
	      eMSindex = int(TMath::Abs(new_decay_pos[0].Z())/EEMSstep);
	    } else{
	      eMSindex = int(TMath::Abs(new_decay_pos[1].Z())/EEMSstep);
	    }
	    if(trigPassProb < ETrigProb[eMSindex]){
	      is1ETrig++;
	      isPassedTrig++;
	      if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k) n1E_Trig++;
	    }
	    if(trigPassProb < ETrigProb_maxStat[eMSindex]) is1ETrig_maxStat++;
	    if(trigPassProb < ETrigProb_minStat[eMSindex]) is1ETrig_minStat++;
	    if(trigPassProb < ETrigProb_maxSyst[eMSindex]) is1ETrig_maxSyst++;
	    if(trigPassProb < ETrigProb_minSyst[eMSindex]) is1ETrig_minSyst++;
	  } else if(isBB_Event(new_decay_pos)){
	    if(debug) std::cout << "BB event! " << std::endl;
	    vector<int> bbMSindex;
	    for(int j=0; j< new_decay_pos.size(); j++){
	      bbMSindex.push_back(int(new_decay_pos[j].Perp()/BMSstepTRIG));
	    }
	    std::sort(bbMSindex.begin(),bbMSindex.end());
	    if(trigPassProb < BBTrigProb[bbMSindex[1]][bbMSindex[0]]){
	      isBBTrig++;
	      isPassedTrig++;
	      if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k) nBB_Trig++;
	    }
	    if(trigPassProb < BBTrigProb_maxStat[bbMSindex[1]][bbMSindex[0]]) isBBTrig_maxStat++;
	    if(trigPassProb < BBTrigProb_minStat[bbMSindex[1]][bbMSindex[0]]) isBBTrig_minStat++;
	    if(trigPassProb < BBTrigProb_maxSyst[bbMSindex[1]][bbMSindex[0]]) isBBTrig_maxSyst++;
	    if(trigPassProb < BBTrigProb_minSyst[bbMSindex[1]][bbMSindex[0]]) isBBTrig_minSyst++;

	  } else if(isEE_Event(new_decay_pos)){
	    vector<int> eeMSindex;
	    if(debug) std::cout << "EE event! " << std::endl;
	    for(int j=0; j< new_decay_pos.size(); j++){
	      eeMSindex.push_back(int(TMath::Abs(new_decay_pos[j].Z())/EMSstepTRIG));
	    }
	    std::sort(eeMSindex.begin(),eeMSindex.end());
	    if(trigPassProb < EETrigProb[eeMSindex[1]][eeMSindex[0]]){
	      isEETrig++;
	      isPassedTrig++;
	      if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k)nEE_Trig++;
	    }
	    if(trigPassProb < EETrigProb_maxStat[eeMSindex[1]][eeMSindex[0]]) isEETrig_maxStat++;
	    if(trigPassProb < EETrigProb_minStat[eeMSindex[1]][eeMSindex[0]]) isEETrig_minStat++;
	    if(trigPassProb < EETrigProb_maxSyst[eeMSindex[1]][eeMSindex[0]]) isEETrig_maxSyst++;
	    if(trigPassProb < EETrigProb_minSyst[eeMSindex[1]][eeMSindex[0]]) isEETrig_minSyst++;


	  } else if(isBE_Event(new_decay_pos)){
	    if(debug) std::cout << "BE event! " << std::endl;
	    int beMSindex_b=0;
	    int beMSindex_e=0;
	    if(checkDecLoc(new_decay_pos[0]) == 1){
	      beMSindex_b = int(new_decay_pos[0].Perp()/BEMSstepTRIG_Y);
	      beMSindex_e = int(TMath::Abs(new_decay_pos[1].Z())/BEMSstepTRIG_X);
	    } else{
	      beMSindex_b = int(new_decay_pos[1].Perp()/BEMSstepTRIG_Y);
	      beMSindex_e = int(TMath::Abs(new_decay_pos[0].Z())/BEMSstepTRIG_X);
	    }

	    if(trigPassProb < BETrigProb[beMSindex_e][beMSindex_b]){
	      isBETrig++;
	      isPassedTrig++;
	      if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k)nBE_Trig++;
	    }
	    if(trigPassProb < BETrigProb_maxStat[beMSindex_e][beMSindex_b]) isBETrig_maxStat++;
	    if(trigPassProb < BETrigProb_minStat[beMSindex_e][beMSindex_b]) isBETrig_minStat++;
	    if(trigPassProb < BETrigProb_maxSyst[beMSindex_e][beMSindex_b]) isBETrig_maxSyst++;
	    if(trigPassProb < BETrigProb_minSyst[beMSindex_e][beMSindex_b]) isBETrig_minSyst++;
	  }
	} // end if trigPassProb > 0
	else{
	  continue;
	}
	/*timestamp_t t2 = get_timestamp();
	  double time2 = (t2 - t1) / 1000000.0L;
	  if(time2 > 0.0000031) cout << "time2 " << time2 << endl;*/

	////////////?WORKING HERE///////////////////

	if(is1B_Event(new_decay_pos)){
	  int msIndex = (checkDecLoc(new_decay_pos[0]) == 1) ? 0 : 1;
	  int BMSIndex  = int(new_decay_pos[msIndex].Perp()/BBMSstep);

	  int SecondLLPIndex = 1;                                                                                                             
          if (msIndex==1) SecondLLPIndex=0;                                                                                                 
	  int detReg = DetectorRegion(new_decay_pos[SecondLLPIndex].Eta(),new_decay_pos[SecondLLPIndex].Perp(),new_decay_pos[SecondLLPIndex].Z());
	  double LTeff = 0;
	  double LTeff_errHigh = 0;
	  double LTeff_errLow = 0;
	  if (detReg==1 || detReg==3 || detReg==5){
	    double pos = new_decay_pos[SecondLLPIndex].Perp()*0.001;
	    if (pos>h_LTD_B->GetBinLowEdge(h_LTD_B->GetNbinsX())+h_LTD_B->GetBinWidth(0) ) pos = h_LTD_B->GetBinCenter(h_LTD_B->GetNbinsX());
	    LTeff = BMSLTProb[h_LTD_B->FindBin(pos)-1];
	    LTeff_errHigh = BMSLTProb_errHigh[h_LTD_B->FindBin(pos)-1];
	    LTeff_errLow  = BMSLTProb_errLow[h_LTD_B->FindBin(pos)-1];
	  }else if (detReg==2 || detReg==4 || detReg==6){
	    double pos = fabs(new_decay_pos[SecondLLPIndex].Z()*0.001);
            if (pos>h_LTD_E->GetBinLowEdge(h_LTD_E->GetNbinsX())+h_LTD_E->GetBinWidth(0) ) pos = h_LTD_E->GetBinCenter(h_LTD_E->GetNbinsX());
	    LTeff = EMSLTProb[h_LTD_E->FindBin(pos)-1];
	    LTeff_errHigh = EMSLTProb_errHigh[h_LTD_E->FindBin(pos)-1];
	    LTeff_errLow  = EMSLTProb_errLow[h_LTD_E->FindBin(pos)-1];
	  }else{
	    LTeff=0;
	  }

	  double vxprob = rnd.Rndm();
	  
	  LTeff=1.0;
	  LTeff_errHigh=0.0;
	  LTeff_errLow=0.0;
	  frac_barrel=1.0;
	
	  double eff = BMSVxProb_a[BMSIndex]*LTeff*frac_barrel;
	      
	  if (is1BTrig){

	    if( vxprob < eff) is1BMSVx++;

	    double errHigh = sqrt( pow(BMSVxProb_a[BMSIndex]*LTeff_errHigh,2) + pow(BMSVxProb_a_errHigh[BMSIndex]*LTeff,2));
	    double errLow  = sqrt( pow(BMSVxProb_a[BMSIndex]*LTeff_errLow ,2) + pow(BMSVxProb_a_errLow[BMSIndex] *LTeff,2));
	    if(vxprob < (eff + errHigh)) is1BMSVx_maxStat++;
	    if(vxprob < (eff - errLow) ) is1BMSVx_minStat++;
		
	    double systHigh = (1+SampleDetails::BVxSysErr_up  )*eff;
	    double systLow  = (1-SampleDetails::BVxSysErr_down)*eff;
	    if(vxprob < systHigh) is1BMSVx_maxSyst++;
	    if(vxprob < systLow ) is1BMSVx_minSyst++;
	  }
	      
	  if(is1BTrig_maxSyst && vxprob < eff) is1BMSVx_maxTrigSyst++;
	  if(is1BTrig_minSyst && vxprob < eff) is1BMSVx_minTrigSyst++;
		
	  if(is1BTrig_maxStat && vxprob < eff) is1BMSVx_maxTrigStat++;
	  if(is1BTrig_minStat && vxprob < eff) is1BMSVx_minTrigStat++;
	      
	}
	else if(is1E_Event(new_decay_pos)){
	      
	  int msIndex= (checkDecLoc(new_decay_pos[0]) == 2) ? 0 : 1;
	  int EMSIndex = int(TMath::Abs(new_decay_pos[msIndex].Z())/EEMSstep);

	  int SecondLLPIndex = 1;
          if (msIndex==1) SecondLLPIndex=0;
          int detReg = DetectorRegion(new_decay_pos[SecondLLPIndex].Eta(),new_decay_pos[SecondLLPIndex].Perp(),new_decay_pos[SecondLLPIndex].Z());
          double LTeff = 0;
          double LTeff_errHigh = 0;
          double LTeff_errLow = 0;
          if (detReg==1 || detReg==3 || detReg==5){
            double pos = new_decay_pos[SecondLLPIndex].Perp()*0.001;
            if (pos>h_LTD_B->GetBinLowEdge(h_LTD_B->GetNbinsX())+h_LTD_B->GetBinWidth(0) ) pos = h_LTD_B->GetBinCenter(h_LTD_B->GetNbinsX());
            LTeff = BMSLTProb[h_LTD_B->FindBin(pos)-1];
	    LTeff_errHigh = BMSLTProb_errHigh[h_LTD_B->FindBin(pos)-1];
	    LTeff_errLow  = BMSLTProb_errLow[h_LTD_B->FindBin(pos)-1];
          }else if (detReg==2 || detReg==4 || detReg==6){
            double pos = fabs(new_decay_pos[SecondLLPIndex].Z()*0.001);
            if (pos>h_LTD_E->GetBinLowEdge(h_LTD_E->GetNbinsX())+h_LTD_E->GetBinWidth(0) ) pos = h_LTD_E->GetBinCenter(h_LTD_E->GetNbinsX());
            LTeff = EMSLTProb[h_LTD_E->FindBin(pos)-1];
	    LTeff_errHigh = EMSLTProb_errHigh[h_LTD_E->FindBin(pos)-1];
	    LTeff_errLow  = EMSLTProb_errLow[h_LTD_E->FindBin(pos)-1];
          }else{
	    LTeff=0;
	  }

	  double vxprob = rnd.Rndm();
	  
	  LTeff=1.0;
	  LTeff_errHigh=0.0;
	  LTeff_errLow=0.0;
	  frac_endcaps=1.0;	  

	  double eff = EMSVxProb_a[EMSIndex]*LTeff*frac_endcaps;

	  if (is1ETrig){
	    if(debug) std::cout << "1E trig event! " << std::endl;
	    
	    if( vxprob < eff) is1EMSVx++;

	    double errHigh = sqrt( pow(EMSVxProb_a[EMSIndex]*LTeff_errHigh,2) + pow(EMSVxProb_a_errHigh[EMSIndex]*LTeff,2));
	    double errLow  = sqrt( pow(EMSVxProb_a[EMSIndex]*LTeff_errLow ,2) + pow(EMSVxProb_a_errLow[EMSIndex] *LTeff,2));
	    if(vxprob < (eff + errHigh)) is1EMSVx_maxStat++;
	    if(vxprob < (eff - errLow) ) is1EMSVx_minStat++;
		
	    double systHigh = (1+SampleDetails::EVxSysErr_up  )*eff;
	    double systLow  = (1-SampleDetails::EVxSysErr_down)*eff;
	    if(vxprob < systHigh) is1EMSVx_maxSyst++;
	    if(vxprob < systLow ) is1EMSVx_minSyst++;
	  }
	      
	  if(is1ETrig_maxSyst && vxprob < eff) is1EMSVx_maxTrigSyst++;
	  if(is1ETrig_minSyst && vxprob < eff) is1EMSVx_minTrigSyst++;
		
	  if(is1ETrig_maxStat && vxprob < eff) is1EMSVx_maxTrigStat++;
	  if(is1ETrig_minStat && vxprob < eff) is1EMSVx_minTrigStat++;

	}


	//now, for events with 2 MS decays, run through each of the vpions separately!
	int firstDecayLeftVertex = 0;
	int firstDecayLeftVertex_maxTrigStat = 0;
	int firstDecayLeftVertex_minTrigStat = 0;
	int firstDecayLeftVertex_maxTrigSyst = 0;
	int firstDecayLeftVertex_minTrigSyst = 0;
	int firstDecayLeftVertex_maxEVxSyst = 0;
	int firstDecayLeftVertex_minEVxSyst = 0;
	int firstDecayLeftVertex_maxEVxStat = 0;
	int firstDecayLeftVertex_minEVxStat = 0;
	int firstDecayLeftVertex_maxBVxSyst = 0;
	int firstDecayLeftVertex_minBVxSyst = 0;
	int firstDecayLeftVertex_maxBVxStat = 0;
	int firstDecayLeftVertex_minBVxStat = 0;

	for(unsigned int i=0; i<new_decay_pos.size(); ++i) {

	  //CREATE R AND Z AGAIN, or USE TVECTOR3!
	  int BBMSindex = int(new_decay_pos[i].Perp()/BBMSstep);
	  int BEMSindex = int(new_decay_pos[i].Perp()/BEMSstep);

	  int EEMSindex = int(TMath::Abs(new_decay_pos[i].z())/EEMSstep);
	  int EBMSindex = int(TMath::Abs(new_decay_pos[i].z())/EBMSstep);

	  if(new_decay_pos[i].Perp() > 10000.) continue;

	  /////////////////////////////////////////////
	  //* Check if muon vertex is reconstructed *//

	  double vxprob = rnd.Rndm();

	  ///////////////////////////
	  //* First in the barrel *//

	  //if(eta<1.0 && BMSindex<nBTrigVXbins) {
	  bool testing = false;
	  if (isBBTrig) {

	    if( vxprob < BBMSVxProb_a[BBMSindex]) isBB1MSVx++;

	    if( vxprob < BBMSVxProb[BBMSindex]){
	      nVertices++;
	      isBBMSVx++;
	      MSVxCombo++;
	    }
	    if(vxprob < BBMSVxProb_maxStat[BBMSindex]) isBBMSVx_maxStat++;
	    if(vxprob < BBMSVxProb_minStat[BBMSindex]) isBBMSVx_minStat++;

	    if(vxprob < BBMSVxProb_maxSyst[BBMSindex]) isBBMSVx_maxSyst++;
	    if(vxprob < BBMSVxProb_minSyst[BBMSindex]) isBBMSVx_minSyst++;

	    if(vxprob < BBMSVxProb_a_maxStat[BBMSindex]) isBB1MSVx_maxStat++;
	    if(vxprob < BBMSVxProb_a_minStat[BBMSindex]) isBB1MSVx_minStat++;
		
	    if(vxprob < BBMSVxProb_a_maxSyst[BBMSindex]) isBB1MSVx_maxSyst++;
	    if(vxprob < BBMSVxProb_a_minSyst[BBMSindex]) isBB1MSVx_minSyst++;
	  }

	  if(isBBTrig_maxSyst && vxprob < BBMSVxProb[BBMSindex]) isBBMSVx_maxTrigSyst++;
	  if(isBBTrig_minSyst && vxprob < BBMSVxProb[BBMSindex]) isBBMSVx_minTrigSyst++;

	  if(isBBTrig_maxStat && vxprob < BBMSVxProb[BBMSindex]) isBBMSVx_maxTrigStat++;
	  if(isBBTrig_minStat && vxprob < BBMSVxProb[BBMSindex]) isBBMSVx_minTrigStat++;

	  if(isBBTrig_maxSyst && vxprob < BBMSVxProb_a[BBMSindex]) isBB1MSVx_maxTrigSyst++;   
	  if(isBBTrig_minSyst && vxprob < BBMSVxProb_a[BBMSindex]) isBB1MSVx_minTrigSyst++;
	
	  if(isBBTrig_maxStat && vxprob < BBMSVxProb_a[BBMSindex]) isBB1MSVx_maxTrigStat++;
	  if(isBBTrig_minStat && vxprob < BBMSVxProb_a[BBMSindex]) isBB1MSVx_minTrigStat++;
		 
	  if(isBETrig){
	    if(checkDecLoc(new_decay_pos[i]) == 1){
	      if(vxprob < BEMSVxProb[BEMSindex]) {
		nVertices++;
		isBEMSVx++;
		MSVxCombo++;
	      }  

	      if(vxprob < BEMSVxProb_a[BEMSindex]) isBE1MSVx++;

	      if(vxprob < BEMSVxProb_maxStat[BEMSindex]) isBEMSVx_maxStat++;
	      if(vxprob < BEMSVxProb_minStat[BEMSindex]) isBEMSVx_minStat++;

	      if(vxprob < BEMSVxProb_maxSyst[BEMSindex]) isBEMSVx_maxSyst++;
	      if(vxprob < BEMSVxProb_minSyst[BEMSindex]) isBEMSVx_minSyst++;

	      if(vxprob < BEMSVxProb_a_maxStat[BEMSindex]) isBE1MSVx_maxStat++;
	      if(vxprob < BEMSVxProb_a_minStat[BEMSindex]) isBE1MSVx_minStat++;

	      if(vxprob < BEMSVxProb_a_maxSyst[BEMSindex]) isBE1MSVx_maxSyst++;
	      if(vxprob < BEMSVxProb_a_minSyst[BEMSindex]) isBE1MSVx_minSyst++;
	    } else if(checkDecLoc(new_decay_pos[i]) == 2){

	      if(vxprob < EBMSVxProb[EBMSindex]) {
		nVertices++;
		isEBMSVx++;
		MSVxCombo++;
	      }
	      if(vxprob < EBMSVxProb_a[EBMSindex]) {
		isEB1MSVx++;
	      }
	      if(vxprob < EBMSVxProb_maxStat[EBMSindex]) isEBMSVx_maxStat++;
	      if(vxprob < EBMSVxProb_minStat[EBMSindex]) isEBMSVx_minStat++;

	      if(vxprob < EBMSVxProb_maxSyst[EBMSindex]) isEBMSVx_maxSyst++;
	      if(vxprob < EBMSVxProb_minSyst[EBMSindex]) isEBMSVx_minSyst++;

	      if(vxprob < EBMSVxProb_a_maxStat[EBMSindex]) isEB1MSVx_maxStat++;
	      if(vxprob < EBMSVxProb_a_minStat[EBMSindex]) isEB1MSVx_minStat++;

	      if(vxprob < EBMSVxProb_a_maxSyst[EBMSindex]) isEB1MSVx_maxSyst++;
	      if(vxprob < EBMSVxProb_a_minSyst[EBMSindex]) isEB1MSVx_minSyst++;
	    } //end if decay position was 2

	    if(i == 0 && (isEB1MSVx+isBE1MSVx) == 1) firstDecayLeftVertex = 1;     
	    if(i == 0 && (isEB1MSVx_maxStat+isBE1MSVx) == 1) firstDecayLeftVertex_maxEVxStat = 1;     
	    if(i == 0 && (isEB1MSVx_minStat+isBE1MSVx) == 1) firstDecayLeftVertex_minEVxStat = 1;     
	    if(i == 0 && (isEB1MSVx_maxSyst+isBE1MSVx) == 1) firstDecayLeftVertex_maxEVxSyst = 1;     
	    if(i == 0 && (isEB1MSVx_minSyst+isBE1MSVx) == 1) firstDecayLeftVertex_minEVxSyst = 1;     
	    if(i == 0 && (isEB1MSVx+isBE1MSVx_maxStat) == 1) firstDecayLeftVertex_maxBVxStat = 1;     
	    if(i == 0 && (isEB1MSVx+isBE1MSVx_minStat) == 1) firstDecayLeftVertex_minBVxStat = 1;     
	    if(i == 0 && (isEB1MSVx+isBE1MSVx_maxSyst) == 1) firstDecayLeftVertex_maxBVxSyst = 1;     
	    if(i == 0 && (isEB1MSVx+isBE1MSVx_minSyst) == 1) firstDecayLeftVertex_minBVxSyst = 1;     
	  }

	  if(isBETrig_maxSyst && checkDecLoc(new_decay_pos[i]) == 1 && vxprob < BEMSVxProb[BEMSindex]) isBEMSVx_maxTrigSyst++;
	  if(isBETrig_maxStat && checkDecLoc(new_decay_pos[i]) == 1 && vxprob < BEMSVxProb[BEMSindex]) isBEMSVx_maxTrigStat++;
	  if(isBETrig_minSyst && checkDecLoc(new_decay_pos[i]) == 1 && vxprob < BEMSVxProb[BEMSindex]) isBEMSVx_minTrigSyst++;
	  if(isBETrig_minStat && checkDecLoc(new_decay_pos[i]) == 1 && vxprob < BEMSVxProb[BEMSindex]) isBEMSVx_minTrigStat++;

	  if(isBETrig_maxSyst && checkDecLoc(new_decay_pos[i]) == 2 && vxprob < EBMSVxProb[EBMSindex]) isEBMSVx_maxTrigSyst++;
	  if(isBETrig_maxStat && checkDecLoc(new_decay_pos[i]) == 2 && vxprob < EBMSVxProb[EBMSindex]) isEBMSVx_maxTrigStat++;
	  if(isBETrig_minSyst && checkDecLoc(new_decay_pos[i]) == 2 && vxprob < EBMSVxProb[EBMSindex]) isEBMSVx_minTrigSyst++;
	  if(isBETrig_minStat && checkDecLoc(new_decay_pos[i]) == 2 && vxprob < EBMSVxProb[EBMSindex]) isEBMSVx_minTrigStat++;

	  if(isBETrig_maxStat){
	    if(checkDecLoc(new_decay_pos[i]) == 1){
	      if(vxprob < BEMSVxProb_a[BEMSindex]) isBE1MSVx_maxTrigStat++;

	    } else if(checkDecLoc(new_decay_pos[i]) == 2){
	      if(vxprob < EBMSVxProb_a[EBMSindex]) isEB1MSVx_maxTrigStat++;
	    } //end if decay position was 2

	    if(i == 0 && (isEB1MSVx_maxTrigStat+isBE1MSVx_maxTrigStat) == 1) firstDecayLeftVertex_maxTrigStat = 1;         

	  } //end if isBETrig_maxStat

	  if(isBETrig_minStat){
	    if(checkDecLoc(new_decay_pos[i]) == 1){
	      if(vxprob < BEMSVxProb_a[BEMSindex]) isBE1MSVx_minTrigStat++;

	    } else if(checkDecLoc(new_decay_pos[i]) == 2){
	      if(vxprob < EBMSVxProb_a[EBMSindex]) isEB1MSVx_minTrigStat++;
	    } //end if decay position was 2

	    if(i == 0 && (isEB1MSVx_minTrigStat+isBE1MSVx_minTrigStat) == 1) firstDecayLeftVertex_minTrigStat = 1;
  
	  } //end if isBETrig_minStat

	  if(isBETrig_maxSyst){
	    if(checkDecLoc(new_decay_pos[i]) == 1){
	      if(vxprob < BEMSVxProb_a[BEMSindex]) isBE1MSVx_maxTrigSyst++;

	    } else if(checkDecLoc(new_decay_pos[i]) == 2){
	      if(vxprob < EBMSVxProb_a[EBMSindex]) isEB1MSVx_maxTrigSyst++;
	    } //end if decay position was 2

	    if(i == 0 && (isEB1MSVx_maxTrigSyst+isBE1MSVx_maxTrigSyst) == 1) firstDecayLeftVertex_maxTrigSyst = 1;
  
	  } //end if isBETrig_maxSyst

	  if(isBETrig_minSyst){
	    if(checkDecLoc(new_decay_pos[i]) == 1){
	      if(vxprob < BEMSVxProb_a[BEMSindex]) isBE1MSVx_minTrigSyst++;

	    } else if(checkDecLoc(new_decay_pos[i]) == 2){
	      if(vxprob < EBMSVxProb_a[EBMSindex]) isEB1MSVx_minTrigSyst++;
	    } //end if decay position was 2

	    if(i == 0 && (isEB1MSVx_minTrigSyst+isBE1MSVx_minTrigSyst) == 1) firstDecayLeftVertex_minTrigSyst = 1;         

	  } //end if isBETrig_minSyst

	  if(isEETrig) {
	    if(vxprob < EEMSVxProb_a[EEMSindex]) isEE1MSVx++;
                     
	    if(vxprob < EEMSVxProb[EEMSindex]){
	      nVertices++;
	      isEEMSVx++;
	      MSVxCombo++;
	    }

	    if(vxprob < EEMSVxProb_maxStat[EEMSindex]) isEEMSVx_maxStat++;
	    if(vxprob < EEMSVxProb_minStat[EEMSindex]) isEEMSVx_minStat++;
	    if(vxprob < EEMSVxProb_maxSyst[EEMSindex]) isEEMSVx_maxSyst++;
	    if(vxprob < EEMSVxProb_minSyst[EEMSindex]) isEEMSVx_minSyst++;

	    if(vxprob < EEMSVxProb_a_maxStat[EEMSindex]) isEE1MSVx_maxStat++;
	    if(vxprob < EEMSVxProb_a_minStat[EEMSindex]) isEE1MSVx_minStat++;
                   
	    if(vxprob < EEMSVxProb_a_maxSyst[EEMSindex]) isEE1MSVx_maxSyst++;
	    if(vxprob < EEMSVxProb_a_minSyst[EEMSindex]) isEE1MSVx_minSyst++;                   
	  }
	  if(isEETrig_maxSyst && (vxprob < EEMSVxProb[EEMSindex])) isEEMSVx_maxTrigSyst++;
	  if(isEETrig_maxStat && (vxprob < EEMSVxProb[EEMSindex])) isEEMSVx_maxTrigStat++;
	  if(isEETrig_minSyst && (vxprob < EEMSVxProb[EEMSindex])) isEEMSVx_minTrigSyst++;
	  if(isEETrig_minStat && (vxprob < EEMSVxProb[EEMSindex])) isEEMSVx_minTrigStat++;

	  if(isEETrig_maxSyst && (vxprob < EEMSVxProb_a[EEMSindex])) isEE1MSVx_maxTrigSyst++;
	  if(isEETrig_maxStat && (vxprob < EEMSVxProb_a[EEMSindex])) isEE1MSVx_maxTrigStat++;           
	  if(isEETrig_minSyst && (vxprob < EEMSVxProb_a[EEMSindex])) isEE1MSVx_minTrigSyst++;
	  if(isEETrig_minStat && (vxprob < EEMSVxProb_a[EEMSindex])) isEE1MSVx_minTrigStat++;
              
	}
	if(debug) std::cout << "looped through particles to see if a vetex was found " << std::endl;
	/*   timestamp_t t3 = get_timestamp();
             double time3 = (t3 - t2) / 1000000.0L;
             if(time3 > 0.0000031) cout << "time3 " << time3 << endl;*/

	/////////////////////////////////////////////////////////
	//* Saving if trigger fired (both barrel and end-cap) *//

	if(is1BTrig) histTrig->fill("Expected_1BTrig",ctau);
	if(is1ETrig) histTrig->fill("Expected_1ETrig",ctau);

	if(isBBTrig)     histTrig->fill("Expected_BBTrig",ctau);
	if(isBBTrig_minStat){
	  histTrig->fill("Expected_BBTrig_minStat",ctau);
	}
	if(isBBTrig_maxStat){
	  histTrig->fill("Expected_BBTrig_maxStat",ctau);
	}
	if(isBBTrig_minSyst) histTrig->fill("Expected_BBTrig_minSyst",ctau);
	if(isBBTrig_maxSyst) histTrig->fill("Expected_BBTrig_maxSyst",ctau);

	if(isEETrig)     histTrig->fill("Expected_EETrig",ctau);
	if(isEETrig_minStat){
	  histTrig->fill("Expected_EETrig_minStat",ctau);
	}
	if(isEETrig_maxStat){
	  histTrig->fill("Expected_EETrig_maxStat",ctau);
	}
	if(isEETrig_minSyst) histTrig->fill("Expected_EETrig_minSyst",ctau);
	if(isEETrig_maxSyst) histTrig->fill("Expected_EETrig_maxSyst",ctau);

	if(isBETrig)     histTrig->fill("Expected_BETrig",ctau);
	if(isBETrig_minStat){
	  histTrig->fill("Expected_BETrig_minStat",ctau);
	}
	if(isBETrig_maxStat){
	  histTrig->fill("Expected_BETrig_maxStat",ctau);
	}
	if(isBETrig_minSyst) histTrig->fill("Expected_BETrig_minSyst",ctau);
	if(isBETrig_maxSyst) histTrig->fill("Expected_BETrig_maxSyst",ctau);


	////////////////////////////////////////////////////////////////////////////////////
	//* Saving if trigger fired and vertex was found (barrel and endcap separately) *//
	if(debug) std::cout << "saving ctau values" << std::endl;
	if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k){
	  //abcd method ones
	  if(is1BMSVx==0)  n1B_Trig_0vx++;
	  if(is1BMSVx==1)  n1B_Trig_1vx++;
	  if(is1EMSVx==0)  n1E_Trig_0vx++;
	  if(is1EMSVx==1)  n1E_Trig_1vx++;

	  if(isBB1MSVx==0)  nBB_TrigA_0vx++;
	  if(isBB1MSVx==1)  nBB_TrigA_1vx++;
	  if(isBE1MSVx==0 && isEB1MSVx == 0)  nBE_TrigA_0vx++;
	  if(isBE1MSVx==1 && isEB1MSVx ==0 )  nBE_TrigA_1vx++;

	  if(isBE1MSVx==0 && isEB1MSVx == 0)  nEB_TrigA_0vx++;
	  if(isEB1MSVx==1 && isBE1MSVx ==0 )  nEB_TrigA_1vx++;

	  if(isEE1MSVx==0)  nEE_TrigA_0vx++;
	  if(isEE1MSVx==1)  nEE_TrigA_1vx++;

	  if(deltar_llps > 1.0){
	    if(isBBMSVx==0)  nBB_Trig_0vx++;
	    if(isBBMSVx==1)  nBB_Trig_1vx++;
	    if(isBBMSVx==2)  nBB_Trig_2vx++;
	    if(isBEMSVx==0)  nBE_Trig_0vx++;
	    if((isBEMSVx==1&&isEBMSVx==0)||(isBEMSVx==0&&isEBMSVx==1))  nBE_Trig_1vx++;
	    if(isBEMSVx==1 && isEBMSVx==1)  nBE_Trig_2vx++;
	    if(isEEMSVx==0)  nEE_Trig_0vx++;
	    if(isEEMSVx==1)  nEE_Trig_1vx++;
	    if(isEEMSVx==2)  nEE_Trig_2vx++;
	  }
	}
	if(debug) std::cout << "filling histograms" << std::endl;
	if(isBB1MSVx==1)                             hist1Vx->fill("Expected_BB1MSVx",ctau);
	if(isBE1MSVx==1 && isEB1MSVx==0)             hist1Vx->fill("Expected_BE1MSVx",ctau);
	if(isBE1MSVx==0 && isEB1MSVx==1)             hist1Vx->fill("Expected_EB1MSVx",ctau);
	if(isEE1MSVx==1)                             hist1Vx->fill("Expected_EE1MSVx",ctau);
	if(is1BMSVx==1)                              hist1Vx->fill("Expected_1BMSVx",ctau);
	if(is1EMSVx==1)                              hist1Vx->fill("Expected_1EMSVx",ctau);

	if( isBB1MSVx==1 || isBE1MSVx==1 || isEE1MSVx==1 || is1BMSVx==1 ||  is1EMSVx==1 ) h_Expected_1MSVx->Fill(ctau);


	// ** Trig stat
	//
	if(isBB1MSVx_maxTrigStat==1)                             hist1Vx->fill("Expected_BB1MSVx_maxTrigStat",ctau);
	if(isBE1MSVx_maxTrigStat==1 && isEB1MSVx_maxTrigStat==0) hist1Vx->fill("Expected_BE1MSVx_maxTrigStat",ctau);
	if(is1BMSVx_maxTrigStat==1)                              hist1Vx->fill("Expected_1BMSVx_maxTrigStat",ctau);

	if(isEE1MSVx_maxTrigStat==1)                             hist1Vx->fill("Expected_EE1MSVx_maxTrigStat",ctau);
	if(isEB1MSVx_maxTrigStat==1 && isBE1MSVx_maxTrigStat==0) hist1Vx->fill("Expected_EB1MSVx_maxTrigStat",ctau);
	if(is1EMSVx_maxTrigStat==1)                              hist1Vx->fill("Expected_1EMSVx_maxTrigStat",ctau);

	if(isBB1MSVx_minTrigStat==1)                             hist1Vx->fill("Expected_BB1MSVx_minTrigStat",ctau);
	if(isBE1MSVx_minTrigStat==1 && isEB1MSVx_minTrigStat==0) hist1Vx->fill("Expected_BE1MSVx_minTrigStat",ctau);
	if(is1BMSVx_minTrigStat==1)                              hist1Vx->fill("Expected_1BMSVx_minTrigStat",ctau);
            
	if(isEB1MSVx_minTrigStat==1 && isBE1MSVx_minTrigStat==0) hist1Vx->fill("Expected_EB1MSVx_minTrigStat",ctau);
	if(isEE1MSVx_minTrigStat==1)                             hist1Vx->fill("Expected_EE1MSVx_minTrigStat",ctau);
	if(is1EMSVx_minTrigStat==1)                              hist1Vx->fill("Expected_1EMSVx_minTrigStat",ctau);


	// ** Trig Syst
	//
	if(isBB1MSVx_maxTrigSyst==1)                             hist1Vx->fill("Expected_BB1MSVx_maxTrigSyst",ctau);
	if(is1BMSVx_maxTrigSyst==1)                              hist1Vx->fill("Expected_1BMSVx_maxTrigSyst",ctau);
	if(isBE1MSVx_maxTrigSyst==1 && isEB1MSVx_maxTrigSyst==0) hist1Vx->fill("Expected_BE1MSVx_maxTrigSyst",ctau);
       
	if(isEB1MSVx_maxTrigSyst==1 && isBE1MSVx_maxTrigSyst==0) hist1Vx->fill("Expected_EB1MSVx_maxTrigSyst",ctau);
	if(isEE1MSVx_maxTrigSyst==1)                             hist1Vx->fill("Expected_EE1MSVx_maxTrigSyst",ctau);
	if(is1EMSVx_maxTrigSyst==1)                              hist1Vx->fill("Expected_1EMSVx_maxTrigSyst",ctau);

	if(isBB1MSVx_minTrigSyst==1)                             hist1Vx->fill("Expected_BB1MSVx_minTrigSyst",ctau);
	if(isBE1MSVx_minTrigSyst==1 && isEB1MSVx_minTrigSyst==0) hist1Vx->fill("Expected_BE1MSVx_minTrigSyst",ctau);
	if(is1BMSVx_minTrigSyst==1)                              hist1Vx->fill("Expected_1BMSVx_minTrigSyst",ctau);

	if(isEB1MSVx_minTrigSyst==1 && isBE1MSVx_minTrigSyst==0) hist1Vx->fill("Expected_EB1MSVx_minTrigSyst",ctau);
	if(isEE1MSVx_minTrigSyst==1)                             hist1Vx->fill("Expected_EE1MSVx_minTrigSyst",ctau);
	if(is1EMSVx_minTrigSyst==1)                              hist1Vx->fill("Expected_1EMSVx_minTrigSyst",ctau);


	// ** Vx stat
	//
	if(isBB1MSVx_maxStat==1)                            hist1Vx->fill("Expected_BB1MSVx_maxVxStat",ctau);
	if(isBE1MSVx_maxStat==1 && isEB1MSVx==0)            hist1Vx->fill("Expected_BE1MSVx_maxBVxStat",ctau);
	if(is1BMSVx_maxStat==1)                             hist1Vx->fill("Expected_1BMSVx_maxVxStat",ctau);
	if(isEB1MSVx_maxStat==0 && isBE1MSVx==1)            hist1Vx->fill("Expected_BE1MSVx_maxEVxStat",ctau);

	if(isEB1MSVx_maxStat==1 && isBE1MSVx==0)            hist1Vx->fill("Expected_EB1MSVx_maxEVxStat",ctau);
	if(isEE1MSVx_maxStat==1)                            hist1Vx->fill("Expected_EE1MSVx_maxVxStat",ctau);
	if(is1EMSVx_maxStat==1)                             hist1Vx->fill("Expected_1EMSVx_maxVxStat",ctau);
	if(isBE1MSVx_maxStat==0 && isEB1MSVx==1)            hist1Vx->fill("Expected_EB1MSVx_maxBVxStat",ctau);

	if(isBB1MSVx_minStat==1)                            hist1Vx->fill("Expected_BB1MSVx_minVxStat",ctau);
	if(isBE1MSVx_minStat==1 && isEB1MSVx==0)            hist1Vx->fill("Expected_BE1MSVx_minBVxStat",ctau);
	if(is1BMSVx_minStat==1)                             hist1Vx->fill("Expected_1BMSVx_minVxStat",ctau);
	if(isEB1MSVx_minStat==0 && isBE1MSVx==1)            hist1Vx->fill("Expected_BE1MSVx_minEVxStat",ctau);

	if(isEB1MSVx_minStat==1 && isBE1MSVx==0)            hist1Vx->fill("Expected_EB1MSVx_minEVxStat",ctau);
	if(isBE1MSVx_minStat==0 && isEB1MSVx==1)            hist1Vx->fill("Expected_EB1MSVx_minBVxStat",ctau);
	if(isEE1MSVx_minStat==1)                            hist1Vx->fill("Expected_EE1MSVx_minVxStat",ctau);
	if(is1EMSVx_minStat==1)                             hist1Vx->fill("Expected_1EMSVx_minVxStat",ctau);


	// ** Vx syst
	//
	if(isBB1MSVx_maxSyst==1)                            hist1Vx->fill("Expected_BB1MSVx_maxVxSyst",ctau);
	if(isBE1MSVx_maxSyst==1 && isEB1MSVx==0)            hist1Vx->fill("Expected_BE1MSVx_maxBVxSyst",ctau);
	if(is1BMSVx_maxSyst==1)                             hist1Vx->fill("Expected_1BMSVx_maxVxSyst",ctau);
	if(isEB1MSVx_maxSyst==0 && isBE1MSVx==1)            hist1Vx->fill("Expected_BE1MSVx_maxEVxSyst",ctau);

	if(isEB1MSVx_maxSyst==1 && isBE1MSVx==0)            hist1Vx->fill("Expected_EB1MSVx_maxEVxSyst",ctau);
	if(isBE1MSVx_maxSyst==0 && isEB1MSVx==1)            hist1Vx->fill("Expected_EB1MSVx_maxBVxSyst",ctau);
	if(isEE1MSVx_maxSyst==1)                            hist1Vx->fill("Expected_EE1MSVx_maxVxSyst",ctau);
	if(is1EMSVx_maxSyst==1)                             hist1Vx->fill("Expected_1EMSVx_maxVxSyst",ctau);
            
	if(isBB1MSVx_minSyst==1)                            hist1Vx->fill("Expected_BB1MSVx_minVxSyst",ctau);
	if(is1BMSVx_minSyst==1)                             hist1Vx->fill("Expected_1BMSVx_minVxSyst",ctau);
	if(isEB1MSVx_minSyst==0 && isBE1MSVx==1)            hist1Vx->fill("Expected_BE1MSVx_minEVxSyst",ctau);
	if(isBE1MSVx_minSyst==1 && isEB1MSVx==0)            hist1Vx->fill("Expected_BE1MSVx_minBVxSyst",ctau);
            
	if(isEB1MSVx_minSyst==1 && isBE1MSVx==0)            hist1Vx->fill("Expected_EB1MSVx_minEVxSyst",ctau);
	if(isBE1MSVx_minSyst==0 && isEB1MSVx==1)            hist1Vx->fill("Expected_EB1MSVx_minBVxSyst",ctau);
	if(isEE1MSVx_minSyst==1)                            hist1Vx->fill("Expected_EE1MSVx_minVxSyst",ctau);
	if(is1EMSVx_minSyst==1)                             hist1Vx->fill("Expected_1EMSVx_minVxSyst",ctau);


	if(deltar_llps > 1.0) {
	  if(isBBMSVx==2)                             hist2Vx->fill("Expected_BBMSVx",ctau);
	  if(isBEMSVx == 1 && isEBMSVx == 1)          hist2Vx->fill("Expected_BEMSVx",ctau);
	  if(isEEMSVx==2)                             hist2Vx->fill("Expected_EEMSVx",ctau);

	  if(isBBMSVx==2 || (isBEMSVx==1 && isEBMSVx==1) || isEEMSVx==2 ) h_Expected_2MSVx->Fill(ctau);
	  if( isBBMSVx==2 || (isBEMSVx==1 && isEBMSVx==1) || isEEMSVx==2 ) hist2Vx->fill("Expected_Tot2Vx",ctau);

	  // ** Trigger systematic
	  //
	  if(isBBMSVx_maxTrigSyst==2)                                      hist2Vx->fill("Expected_BBMSVx_maxTrigSyst",ctau);
	  if(isBEMSVx_maxTrigSyst== 1 && isEBMSVx_maxTrigSyst == 1)        hist2Vx->fill("Expected_BEMSVx_maxTrigSyst",ctau);
	  if(isEEMSVx_maxTrigSyst==2)                                      hist2Vx->fill("Expected_EEMSVx_maxTrigSyst",ctau);

	  if(isBBMSVx_minTrigSyst==2)                                      hist2Vx->fill("Expected_BBMSVx_minTrigSyst",ctau);
	  if(isBEMSVx_minTrigSyst==1 && isEBMSVx_minTrigSyst == 1)         hist2Vx->fill("Expected_BEMSVx_minTrigSyst",ctau);
	  if(isEEMSVx_minTrigSyst==2)                                      hist2Vx->fill("Expected_EEMSVx_minTrigSyst",ctau);

	  // ** Trigger statistics
	  //
	  if(isBBMSVx_maxTrigStat==2){
	    hist2Vx->fill("Expected_BBMSVx_maxTrigStat",ctau);
	  }
	  if(isBEMSVx_maxTrigStat==1 && isEBMSVx_maxTrigStat == 1){
	    hist2Vx->fill("Expected_BEMSVx_maxTrigStat",ctau);
	  }
	  if(isEEMSVx_maxTrigStat==2){
	    hist2Vx->fill("Expected_EEMSVx_maxTrigStat",ctau);
	  }

	  if(isBBMSVx_minTrigStat==2){
	    hist2Vx->fill("Expected_BBMSVx_minTrigStat",ctau);
	  }
	  if((isBEMSVx_minTrigStat==1 && isEBMSVx_minTrigStat == 1)){
	    hist2Vx->fill("Expected_BEMSVx_minTrigStat",ctau);
	  }
	  if(isEEMSVx_minTrigStat==2){
	    hist2Vx->fill("Expected_EEMSVx_minTrigStat",ctau);
	  }

	  // ** MS vertex syst
	  //
	  if(isBBMSVx_maxSyst==2)                             hist2Vx->fill("Expected_BBMSVx_maxVxSyst",ctau);
	  if(isEEMSVx_maxSyst==2)                             hist2Vx->fill("Expected_EEMSVx_maxVxSyst",ctau);

	  if(isBEMSVx_maxSyst==1 && isEBMSVx == 1)            hist2Vx->fill("Expected_BEMSVx_maxBVxSyst",ctau);
	  if(isBEMSVx == 1 && isEBMSVx_maxSyst == 1)          hist2Vx->fill("Expected_BEMSVx_maxEVxSyst",ctau);

	  if(isBBMSVx_minSyst==2)                             hist2Vx->fill("Expected_BBMSVx_minVxSyst",ctau);
	  if(isBEMSVx_minSyst == 1 && isEBMSVx == 1)          hist2Vx->fill("Expected_BEMSVx_minBVxSyst",ctau);
	  if(isBEMSVx == 1 && isEBMSVx_minSyst == 1)          hist2Vx->fill("Expected_BEMSVx_minEVxSyst",ctau);
	  if(isEEMSVx_minSyst==2)                             hist2Vx->fill("Expected_EEMSVx_minVxSyst",ctau);

	  // ** MS vertex stat
	  //
	  if(isBBMSVx_maxStat==2)  hist2Vx->fill("Expected_BBMSVx_maxVxStat",ctau);
	  if(isBBMSVx_minStat==2)  hist2Vx->fill("Expected_BBMSVx_minVxStat",ctau);

	  if(isEEMSVx_maxStat==2) hist2Vx->fill("Expected_EEMSVx_maxVxStat",ctau);
	  if(isEEMSVx_minStat==2) hist2Vx->fill("Expected_EEMSVx_minVxStat",ctau);


	  if(isBEMSVx_maxStat==1 && isEBMSVx == 1) hist2Vx->fill("Expected_BEMSVx_maxBVxStat",ctau);
	  if(isBEMSVx_minStat==1 && isEBMSVx == 1) hist2Vx->fill("Expected_BEMSVx_minBVxStat",ctau);

	  if(isBEMSVx == 1 && isEBMSVx_maxStat==1) hist2Vx->fill("Expected_BEMSVx_maxEVxStat",ctau);
	  if(isBEMSVx == 1 && isEBMSVx_minStat==1) hist2Vx->fill("Expected_BEMSVx_minEVxStat",ctau);

	  int testMSMS=0;

	  if((h_Expected_2MSVx->FindBin(SampleDetails::sim_ctau)-1) == k){

	    if((isBBMSVx==2 || (isBEMSVx==1 && isEBMSVx==1) || isEEMSVx==2) && isPassedTrig){
	      nMSVxMSVx++;
	      if(isBBMSVx == 2){
		nMSvx_BB++;
	      }else if(isBEMSVx == 1 && isEBMSVx == 1){
		nMSvx_BE++;
	      }else if(isEEMSVx == 2){
		nMSvx_EE++;
	      }
	    }
	  }
	}
      } //end loop through ctaus
        //return kTRUE;
      if(debug) std::cout << "done event" << std::endl;
      toyLLP->clearAllToyVectors();
    } //end loop through events?


  cout << "nBB, BE, EE decays"  << endl;
  cout << nBB_Decay << ", " << nBE_Decay << ", " << nEE_Decay << endl;

  cout << "nBB, BE, EE trig evts: " << nBB_Trig << ", " << nBE_Trig << ", " << nEE_Trig  << endl;
  cout << "nBB, BE, EE trig evts, 0vx: " << nBB_Trig_0vx << ", " << nBE_Trig_0vx << ", " << nEE_Trig_0vx << endl;
  cout << "nBB, BE, EE trig evts, 1vx: " << nBB_Trig_1vx << ", " << nBE_Trig_1vx << ", " << nEE_Trig_1vx << endl;
  cout << "nBB, BE, EE trig evts, 2vx: " << nBB_Trig_2vx << ", " << nBE_Trig_2vx << ", " << nEE_Trig_2vx  << endl;
  cout <<  "BB BE EE" << endl;
  cout << " " << nBB_Trig_2vx << " " << nBE_Trig_2vx << " " << nEE_Trig_2vx << endl;

  cout << "scale factor: " << LUMI*SampleDetails::mediatorXS/nEvents << endl;

  std::cout << " ************************************************** " << std::endl;
  std::cout << " ******************* ABCD RESULTS ***************** " << std::endl;

  std::cout << " n1B, n1E decays" <<std::endl;
  std::cout << n1B_Decay << " " << n1E_Decay << std::endl;
  std::cout << " n1B , n1E trig events: " << std::endl;
  std::cout << n1B_Trig << " " << n1E_Trig << std::endl;
  std::cout << " Trig + vx: 1B, 1E, BB, BE, EB, EE: " << std::endl;
  std::cout << n1B_Trig_1vx << " " << n1E_Trig_1vx << " " << nBB_TrigA_1vx << " ";
  std::cout << nBE_TrigA_1vx << " " << nEB_TrigA_1vx << " " <<nEE_TrigA_1vx << std::endl;
  std::cout << " ************************************************** " << std::endl;

  hist1Vx->scale(LUMI*SampleDetails::mediatorXS/nEvents);
  hist2Vx->scale(LUMI*SampleDetails::mediatorXS/nEvents);
  histTrig->scale(LUMI*SampleDetails::mediatorXS/nEvents);


  h_BB_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
  h_BE_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
  h_EE_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
  h_1B_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
  h_1E_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);

  h_Expected_1MSVx->Scale(LUMI*SampleDetails::mediatorXS/nEvents);

  h_Expected_2MSVx->Scale(LUMI*SampleDetails::mediatorXS/nEvents);


  cout << "         2 Vertex locations (BB,BE,EE) -- unscaled = ("
       << nMSvx_BB << ", " << nMSvx_BE << ", " << nMSvx_EE << ")" << endl;


  cout << "         2 Vertex locations (BB,BE,EE) -- scaled by lumi and x-sec = ("
       << nMSvx_BB*(LUMI*SampleDetails::mediatorXS/nEvents) << ", " << nMSvx_BE*(LUMI*SampleDetails::mediatorXS/nEvents) << ", " << nMSvx_EE*(LUMI*SampleDetails::mediatorXS/nEvents) << ")" << endl;

  // ** Calculate systematic errors!
  //
  double finestep = LTSTEP;
  double ctau=0;

  // for 2 vertex search
  vector<double> binBVx_min(2,0.0);
  vector<double> binBVx_max(2,0.0);
  vector<double> binEVx_min(2,0.0);
  vector<double> binEVx_max(2,0.0);
  vector<double> binBTrig_min(2,0.0);
  vector<double> binBTrig_max(2,0.0);
  vector<double> binETrig_min(1,0.0);
  vector<double> binETrig_max(1,0.0);

  vector<double> binStat_min(7,0.0);
  vector<double> binStat_max(7,0.0);

  vector<double> binBBMSVx_min(4,0.0);
  vector<double> binBBMSVx_max(4,0.0);
  vector<double> binEEMSVx_min(4,0.0);
  vector<double> binEEMSVx_max(4,0.0);
  vector<double> binBEMSVx_min(6,0.0);
  vector<double> binBEMSVx_max(6,0.0);

    
  // for 1 barrel vertex search
  //
  vector<double> bin1bStat_min(7,0.0);
  vector<double> bin1bStat_max(7,0.0);
  vector<double> bin1eStat_min(7,0.0);
  vector<double> bin1eStat_max(7,0.0);
    
  vector<double> binBTrigStat_max(3,0.0);
  vector<double> binBTrigStat_min(3,0.0);
  vector<double> binETrigStat_max(3,0.0);
  vector<double> binETrigStat_min(3,0.0);
  vector<double> binBVxStat_max(4,0.0);
  vector<double> binBVxStat_min(4,0.0);
  vector<double> binEVxStat_max(4,0.0);
  vector<double> binEVxStat_min(4,0.0);
  
  vector<double> binBTrigSyst_max(3,0.0);
  vector<double> binBTrigSyst_min(3,0.0);
  vector<double> binETrigSyst_max(3,0.0);
  vector<double> binETrigSyst_min(3,0.0);
  vector<double> binBVxSyst_max(4,0.0);
  vector<double> binBVxSyst_min(4,0.0);
  vector<double> binEVxSyst_max(4,0.0);
  vector<double> binEVxSyst_min(4,0.0);

  // ** For HSS
  //
  vector<double> bin1bStat_min_hss(7,0.0);
  vector<double> bin1bStat_max_hss(7,0.0);
  vector<double> bin1eStat_min_hss(7,0.0);
  vector<double> bin1eStat_max_hss(7,0.0);
    
  vector<double> binBTrigStat_max_hss(3,0.0);
  vector<double> binBTrigStat_min_hss(3,0.0);
  vector<double> binETrigStat_max_hss(3,0.0);
  vector<double> binETrigStat_min_hss(3,0.0);
  vector<double> binBVxStat_max_hss(4,0.0);
  vector<double> binBVxStat_min_hss(4,0.0);
  vector<double> binEVxStat_max_hss(4,0.0);
  vector<double> binEVxStat_min_hss(4,0.0);

  vector<double> binBTrigSyst_max_hss(3,0.0);
  vector<double> binBTrigSyst_min_hss(3,0.0);
  vector<double> binETrigSyst_max_hss(3,0.0);
  vector<double> binETrigSyst_min_hss(3,0.0);
  vector<double> binBVxSyst_max_hss(4,0.0);
  vector<double> binBVxSyst_min_hss(4,0.0);
  vector<double> binEVxSyst_max_hss(4,0.0);
  vector<double> binEVxSyst_min_hss(4,0.0);

    
  //  vector<double> binTotal_min_combined(6,0.0);
  //  vector<double> binTotal_max_combined(6,0.0);

  for(int i=1; i<(nPoints+1); ++i) {

    //ctau = (i-1)*finestep+0.0001;

    float BBMSVxVal = hist2Vx->getBinContent("Expected_BBMSVx", i);
    float BBtrigStatMax = hist2Vx->getBinContent("Expected_BBMSVx_maxTrigStat", i);
    float BBtrigStatMin = hist2Vx->getBinContent("Expected_BBMSVx_minTrigStat", i);
    float BBVxStatMax =   hist2Vx->getBinContent("Expected_BBMSVx_maxVxStat", i);
    float BBVxStatMin = hist2Vx->getBinContent("Expected_BBMSVx_minVxStat", i);

    binStat_max.at(0) = (BBtrigStatMax - BBMSVxVal);
    binStat_max.at(1) = (BBVxStatMax - BBMSVxVal);

    binStat_min.at(0) = (BBMSVxVal - BBtrigStatMin);
    binStat_min.at(1) = (BBMSVxVal - BBVxStatMin);

    float BEVXVal = hist2Vx->getBinContent("Expected_BEMSVx", i);
    float BEtrigStatMax =  hist2Vx->getBinContent("Expected_BEMSVx_maxTrigStat", i);
    float BEtrigStatMin = hist2Vx->getBinContent("Expected_BEMSVx_minTrigStat", i);
    float BEvxStatMax = hist2Vx->getBinContent("Expected_BEMSVx_maxBVxStat", i);
    float BEvxStatMin = hist2Vx->getBinContent("Expected_BEMSVx_minBVxStat", i);
    float EBvxStatMax =  hist2Vx->getBinContent("Expected_BEMSVx_maxEVxStat", i);
    float EBvxStatMin = hist2Vx->getBinContent("Expected_BEMSVx_minEVxStat", i);

    binStat_max.at(2) = (BEtrigStatMax - BEVXVal);
    binStat_max.at(3) = (BEvxStatMax - BEVXVal);
    binStat_max.at(4) = (EBvxStatMax - BEVXVal);

    binStat_min.at(2) = (BEVXVal - BEtrigStatMin);
    binStat_min.at(3) = (BEVXVal - BEvxStatMin);
    binStat_min.at(4) = (BEVXVal - EBvxStatMin);

    float EEvxVal = hist2Vx->getBinContent("Expected_EEMSVx", i);
    float EETrigStatMax = hist2Vx->getBinContent("Expected_EEMSVx_maxTrigStat", i);
    float EETrigStatMin = hist2Vx->getBinContent("Expected_EEMSVx_minTrigStat", i);
    float EEvxStatMax = hist2Vx->getBinContent("Expected_EEMSVx_maxVxStat", i);
    float EEvxStatMin = hist2Vx->getBinContent("Expected_EEMSVx_minVxStat", i);

    binStat_max.at(5) = (EETrigStatMax - EEvxVal);
    binStat_max.at(6) = (EEvxStatMax - EEvxVal);

    binStat_min.at(5) = (EEvxVal - EETrigStatMin);
    binStat_min.at(6) = (EEvxVal - EEvxStatMin);

        
    double binBBMSVx = 0.0;
    binBBMSVx = hist2Vx->getBinContent("Expected_BBMSVx", i);
        
    binBBMSVx_min.at(0) = (hist2Vx->getBinContent("Expected_BBMSVx_minTrigSyst", i));
    binBBMSVx_min.at(1) = (hist2Vx->getBinContent("Expected_BBMSVx_minTrigStat", i));
    binBBMSVx_min.at(2) = (hist2Vx->getBinContent("Expected_BBMSVx_minVxSyst", i));
    binBBMSVx_min.at(3) = (hist2Vx->getBinContent("Expected_BBMSVx_minVxStat", i));
        
    binBBMSVx_max.at(0) = (hist2Vx->getBinContent("Expected_BBMSVx_maxTrigSyst", i));
    binBBMSVx_max.at(1) = (hist2Vx->getBinContent("Expected_BBMSVx_maxTrigStat", i));
    binBBMSVx_max.at(2) = (hist2Vx->getBinContent("Expected_BBMSVx_maxVxSyst", i));
    binBBMSVx_max.at(3) = (hist2Vx->getBinContent("Expected_BBMSVx_maxVxStat", i));
        
        
    double binEEMSVx = 0.0;
    binEEMSVx = (hist2Vx->getBinContent("Expected_EEMSVx", i));
        
    binEEMSVx_min.at(0) = (hist2Vx->getBinContent("Expected_EEMSVx_minTrigSyst", i));
    binEEMSVx_min.at(1) = (hist2Vx->getBinContent("Expected_EEMSVx_minTrigStat", i));
    binEEMSVx_min.at(2) = (hist2Vx->getBinContent("Expected_EEMSVx_minVxSyst", i));
    binEEMSVx_min.at(3) = (hist2Vx->getBinContent("Expected_EEMSVx_minVxStat", i));
        
    binEEMSVx_max.at(0) = (hist2Vx->getBinContent("Expected_EEMSVx_maxTrigSyst", i));
    binEEMSVx_max.at(1) = (hist2Vx->getBinContent("Expected_EEMSVx_maxTrigStat", i));
    binEEMSVx_max.at(2) = (hist2Vx->getBinContent("Expected_EEMSVx_maxVxSyst", i));
    binEEMSVx_max.at(3) = (hist2Vx->getBinContent("Expected_EEMSVx_maxVxStat", i));
        
    for(int j=0; j< binEEMSVx_min.size(); j++){
      binEEMSVx_max.at(j) = binEEMSVx_max.at(j) -  binEEMSVx;
      binEEMSVx_min.at(j) = binEEMSVx - binEEMSVx_min.at(j);
      binBBMSVx_max.at(j) = binBBMSVx_max.at(j) -  binBBMSVx;
      binBBMSVx_min.at(j) = binBBMSVx - binBBMSVx_min.at(j);
    }
        
    binBVx_min.at(0) = binBBMSVx_min.at(2);
    binBVx_max.at(0) = binBBMSVx_max.at(2);
    binBTrig_min.at(0) = binBBMSVx_min.at(0);
    binBTrig_max.at(0) = binBBMSVx_max.at(0);
        
    binEVx_min.at(0) = binEEMSVx_min.at(2);
    binEVx_max.at(0) = binEEMSVx_max.at(2);
    binETrig_min.at(0) = binEEMSVx_min.at(0);
    binETrig_max.at(0) = binEEMSVx_max.at(0);
        
    /* binTotal_max_combined.at(0)+= binBBMSVx_max.at(0);
       binTotal_max_combined.at(1)+= binEEMSVx_max.at(0);
       binTotal_max_combined.at(4)+= binBBMSVx_max.at(2);
       binTotal_max_combined.at(5)+= binEEMSVx_max.at(2);
       binTotal_min_combined.at(0)+= binBBMSVx_min.at(0);
       binTotal_min_combined.at(1)+= binEEMSVx_min.at(0);
       binTotal_min_combined.at(4)+= binBBMSVx_max.at(2);
       binTotal_min_combined.at(5)+= binEEMSVx_min.at(2);*/
        
    double binBEMSVx = 0.0;
    binBEMSVx = hist2Vx->getBinContent("Expected_BEMSVx", i);
        
    binBEMSVx_min.at(0) = (hist2Vx->getBinContent("Expected_BEMSVx_minTrigSyst", i));
    binBEMSVx_min.at(1) = (hist2Vx->getBinContent("Expected_BEMSVx_minTrigStat", i));
    binBEMSVx_min.at(2) = (hist2Vx->getBinContent("Expected_BEMSVx_minBVxSyst", i));
    binBEMSVx_min.at(3) = (hist2Vx->getBinContent("Expected_BEMSVx_minBVxStat", i));
    binBEMSVx_min.at(4) = (hist2Vx->getBinContent("Expected_BEMSVx_minEVxSyst", i));
    binBEMSVx_min.at(5) = (hist2Vx->getBinContent("Expected_BEMSVx_minEVxStat", i));
        
    binBEMSVx_max.at(0) = (hist2Vx->getBinContent("Expected_BEMSVx_maxTrigSyst", i));
    binBEMSVx_max.at(1) = (hist2Vx->getBinContent("Expected_BEMSVx_maxTrigStat", i));
    binBEMSVx_max.at(2) = (hist2Vx->getBinContent("Expected_BEMSVx_maxBVxSyst", i));
    binBEMSVx_max.at(3) = (hist2Vx->getBinContent("Expected_BEMSVx_maxBVxStat", i));
    binBEMSVx_max.at(4) = (hist2Vx->getBinContent("Expected_BEMSVx_maxEVxSyst", i));
    binBEMSVx_max.at(5) = (hist2Vx->getBinContent("Expected_BEMSVx_maxEVxStat", i));
        
    for(int j=0; j< binBEMSVx_min.size(); j++){
      binBEMSVx_max.at(j) = binBEMSVx_max.at(j) -  binBEMSVx;
      binBEMSVx_min.at(j) = binBEMSVx - binBEMSVx_min.at(j);
    }
        
    binBVx_min.at(1) = binBEMSVx_min.at(2);
    binBVx_max.at(1) = binBEMSVx_max.at(2);
        
    binBTrig_min.at(1) = binBEMSVx_min.at(0);
    binBTrig_max.at(1) = binBEMSVx_max.at(0);
        
    binEVx_min.at(1) = binBEMSVx_min.at(4);
    binEVx_max.at(1) = binBEMSVx_max.at(4);
        
    double totBTrigsystMAX = std::accumulate(binBTrig_max.rbegin(),binBTrig_max.rend(),0.0);
    double totBTrigsystMIN = std::accumulate(binBTrig_min.rbegin(),binBTrig_min.rend(),0.0);
    double totETrigsystMAX = std::accumulate(binETrig_max.rbegin(),binETrig_max.rend(),0.0);
    double totETrigsystMIN = std::accumulate(binETrig_min.rbegin(),binETrig_min.rend(),0.0);
        
    double totBVXsystMAX = std::accumulate(binBVx_max.rbegin(),binBVx_max.rend(),0.0);
    double totBVXsystMIN = std::accumulate(binBVx_min.rbegin(),binBVx_min.rend(),0.0);
    double totEVXsystMAX = std::accumulate(binEVx_max.rbegin(),binEVx_max.rend(),0.0);
    double totEVXsystMIN = std::accumulate(binEVx_min.rbegin(),binEVx_min.rend(),0.0);
        
    double totSystMAX = sqrt(sq(totBTrigsystMAX) + sq(totETrigsystMAX) + sq(totBVXsystMAX) + sq(totEVXsystMAX));
    double totSystMIN = sqrt(sq(totBTrigsystMIN) + sq(totETrigsystMIN) + sq(totBVXsystMIN) + sq(totEVXsystMIN));
        
    double totStatMAX = sqrt(std::inner_product(binStat_max.begin(),binStat_max.end(),binStat_max.begin(),0.0));
    double totStatMIN = sqrt(std::inner_product(binStat_min.begin(),binStat_min.end(),binStat_min.begin(),0.0));
        
    double tmpMAXq = std::inner_product(binBTrig_max.begin(),binBTrig_max.end(),binBTrig_max.begin(),0.0);
    tmpMAXq += std::inner_product(binETrig_max.begin(),binETrig_max.end(),binETrig_max.begin(),0.0);
    tmpMAXq += std::inner_product(binEVx_max.begin(),binEVx_max.end(),binEVx_max.begin(),0.0);
    tmpMAXq += std::inner_product(binBVx_max.begin(),binBVx_max.end(),binBVx_max.begin(),0.0);
        
    double tmpMINq = std::inner_product(binBTrig_min.begin(),binBTrig_min.end(),binBTrig_min.begin(),0.0);
    tmpMINq += std::inner_product(binETrig_min.begin(),binETrig_min.end(),binETrig_min.begin(),0.0);
    tmpMINq += std::inner_product(binEVx_min.begin(),binEVx_min.end(),binEVx_min.begin(),0.0);
    tmpMINq += std::inner_product(binBVx_min.begin(),binBVx_min.end(),binBVx_min.begin(),0.0);
        
    double totSystMAXq = sqrt(tmpMAXq);
    double totSystMINq = sqrt(tmpMINq);
        
    double totBBErrorMAX = sqrt( std::inner_product(binBBMSVx_max.begin(),binBBMSVx_max.end(),binBBMSVx_max.begin(),0.0));
    double totBBErrorMIN = sqrt( std::inner_product(binBBMSVx_min.begin(),binBBMSVx_min.end(),binBBMSVx_min.begin(),0.0));
        
    double totEEErrorMAX = sqrt( std::inner_product(binEEMSVx_max.begin(),binEEMSVx_max.end(),binEEMSVx_max.begin(),0.0));
    double totEEErrorMIN = sqrt( std::inner_product(binEEMSVx_min.begin(),binEEMSVx_min.end(),binEEMSVx_min.begin(),0.0));
        
    double totBEErrorMAX = sqrt( std::inner_product(binBEMSVx_max.begin(),binBEMSVx_max.end(),binBEMSVx_max.begin(),0.0));
    double totBEErrorMIN = sqrt( std::inner_product(binBEMSVx_min.begin(),binBEMSVx_min.end(),binBEMSVx_min.begin(),0.0));
        
    double totErrorMAX = sqrt( sq(totStatMAX) + sq(totSystMAX));
    double totErrorMIN = sqrt( sq(totStatMIN) + sq(totSystMIN));
        
    double totErrorMAXq = sqrt( sq(totStatMAX) + sq(totSystMAXq));
    double totErrorMINq = sqrt( sq(totStatMIN) + sq(totSystMINq));
        
        
    hist2Vx->setBinContent("Expected_BBMSVx_maxTotal",i,binBBMSVx+totBBErrorMAX);
    hist2Vx->setBinContent("Expected_BBMSVx_minTotal",i,binBBMSVx-totBBErrorMIN);
    hist2Vx->setBinContent("Expected_EEMSVx_maxTotal",i,binEEMSVx+totEEErrorMAX);
    hist2Vx->setBinContent("Expected_EEMSVx_minTotal",i,binEEMSVx-totEEErrorMIN);
    hist2Vx->setBinContent("Expected_BEMSVx_maxTotal",i,binBEMSVx+totBEErrorMAX);
    hist2Vx->setBinContent("Expected_BEMSVx_minTotal",i,binBEMSVx-totBEErrorMIN);
        
    hist2Vx->setBinContent("Expected_Tot2Vx",i,binBBMSVx+binEEMSVx+binBEMSVx);
        
    hist2Vx->setBinContent("Expected_Tot2Vx_maxStat",i,binBBMSVx+binEEMSVx+binBEMSVx+totStatMAX);
    hist2Vx->setBinContent("Expected_Tot2Vx_maxSyst",i,binBBMSVx+binEEMSVx+binBEMSVx+totSystMAX);
    hist2Vx->setBinContent("Expected_Tot2Vx_maxTotal",i,binBBMSVx+binEEMSVx+binBEMSVx+totErrorMAX);
    hist2Vx->setBinContent("Expected_Tot2Vx_minStat",i,binBBMSVx+binEEMSVx+binBEMSVx-totStatMIN);
    hist2Vx->setBinContent("Expected_Tot2Vx_minSyst",i,binBBMSVx+binEEMSVx+binBEMSVx-totSystMIN);
    hist2Vx->setBinContent("Expected_Tot2Vx_minTotal",i,binBBMSVx+binEEMSVx+binBEMSVx-totErrorMIN);
        
    ///1vx search

    //stat
    float BB1MSVxVal  = hist1Vx->getBinContent("Expected_BB1MSVx", i);
    float BB1bMSVxVal = hist1Vx->getBinContent("Expected_BB1MSVxb", i);
    float BB1eMSVxVal = hist1Vx->getBinContent("Expected_BB1MSVxe", i);
        
    float BB1trigStatMax = hist1Vx->getBinContent("Expected_BB1MSVx_maxTrigStat", i);
    float BB1trigStatMin = hist1Vx->getBinContent("Expected_BB1MSVx_minTrigStat", i);
    float BB1VxStatMax   = hist1Vx->getBinContent("Expected_BB1MSVx_maxVxStat", i);
    float BB1VxStatMin   = hist1Vx->getBinContent("Expected_BB1MSVx_minVxStat", i);

    bin1bStat_max.at(0) = (BB1trigStatMax - BB1MSVxVal);
    bin1bStat_max.at(1) = (BB1VxStatMax - BB1MSVxVal);

    bin1bStat_min.at(0) = (BB1MSVxVal - BB1trigStatMin);
    bin1bStat_min.at(1) = (BB1MSVxVal - BB1VxStatMin);
        
    float BE1VXVal  = hist1Vx->getBinContent("Expected_BE1MSVx", i);
    float BE1bVXVal = hist1Vx->getBinContent("Expected_BE1MSVxb", i);
    float BE1eVXVal = hist1Vx->getBinContent("Expected_BE1MSVxe", i);
       
    float BE1trigStatMax =  hist1Vx->getBinContent("Expected_BE1MSVx_maxTrigStat", i);
    float BE1trigStatMin = hist1Vx->getBinContent("Expected_BE1MSVx_minTrigStat", i);
    float BE1bvxStatMax = hist1Vx->getBinContent("Expected_BE1MSVx_maxBVxStat", i);
    float BE1bvxStatMin = hist1Vx->getBinContent("Expected_BE1MSVx_minBVxStat", i);
    float BE1evxStatMax =  hist1Vx->getBinContent("Expected_BE1MSVx_maxEVxStat", i);
    float BE1evxStatMin = hist1Vx->getBinContent("Expected_BE1MSVx_minEVxStat", i);

    bin1bStat_max.at(2) = (BE1trigStatMax - BE1VXVal);
    bin1bStat_max.at(3) = (BE1bvxStatMax - BE1VXVal);
    bin1bStat_max.at(4) = (BE1evxStatMax - BE1VXVal);

    bin1bStat_min.at(2) = (BE1VXVal - BE1trigStatMin);
    bin1bStat_min.at(3) = (BE1VXVal - BE1bvxStatMin);
    bin1bStat_min.at(4) = (BE1VXVal - BE1evxStatMin);
                
    float B1MSVxVal = hist1Vx->getBinContent("Expected_1BMSVx", i);
    float B1bMSVxVal = hist1Vx->getBinContent("Expected_1BMSVxb", i);
    float B1eMSVxVal = hist1Vx->getBinContent("Expected_1BMSVxe", i);
        
    float B1trigStatMax = hist1Vx->getBinContent("Expected_1BMSVx_maxTrigStat", i);
    float B1trigStatMin = hist1Vx->getBinContent("Expected_1BMSVx_minTrigStat", i);
    float B1VxStatMax =   hist1Vx->getBinContent("Expected_1BMSVx_maxVxStat", i);
    float B1VxStatMin = hist1Vx->getBinContent("Expected_1BMSVx_minVxStat", i);

    bin1bStat_max.at(5) = (B1trigStatMax - B1MSVxVal);
    bin1bStat_max.at(6) = (B1VxStatMax - B1MSVxVal);

    bin1bStat_min.at(5) = (B1MSVxVal - B1trigStatMin);
    bin1bStat_min.at(6) = (B1MSVxVal - B1VxStatMin);
        
    float BB1trigSystMax = hist1Vx->getBinContent("Expected_BB1MSVx_maxTrigSyst", i);
    float BB1trigSystMin = hist1Vx->getBinContent("Expected_BB1MSVx_minTrigSyst", i);
    float BB1VxSystMax =   hist1Vx->getBinContent("Expected_BB1MSVx_maxVxSyst", i);
    float BB1VxSystMin = hist1Vx->getBinContent("Expected_BB1MSVx_minVxSyst", i);

    binBTrigSyst_max.at(0) = (BB1trigSystMax - BB1MSVxVal);
    binBVxSyst_max.at(0) = (BB1VxSystMax - BB1MSVxVal);

    binBTrigSyst_min.at(0) = (BB1MSVxVal - BB1trigSystMin);
    binBVxSyst_min.at(0) = (BB1MSVxVal - BB1VxSystMin);
        
    float BE1trigSystMax =  hist1Vx->getBinContent("Expected_BE1MSVx_maxTrigSyst", i);
    float BE1trigSystMin = hist1Vx->getBinContent("Expected_BE1MSVx_minTrigSyst", i);
    float BE1bvxSystMax = hist1Vx->getBinContent("Expected_BE1MSVx_maxBVxSyst", i);
    float BE1bvxSystMin = hist1Vx->getBinContent("Expected_BE1MSVx_minBVxSyst", i);
    float BE1evxSystMax =  hist1Vx->getBinContent("Expected_BE1MSVx_maxEVxSyst", i);
    float BE1evxSystMin = hist1Vx->getBinContent("Expected_BE1MSVx_minEVxSyst", i);

    binBTrigSyst_max.at(1) = (BE1trigSystMax - BE1VXVal);
    binBVxSyst_max.at(1) = (BE1bvxSystMax - BE1VXVal);
    binEVxSyst_max.at(0) = (BE1evxSystMax - BE1VXVal);

    binBTrigSyst_min.at(1) = (BE1VXVal - BE1trigSystMin);
    binBVxSyst_min.at(1) = (BE1VXVal - BE1bvxSystMin);
    binEVxSyst_min.at(0) = (BE1VXVal - BE1evxSystMin);
        
    float B1trigSystMax = hist1Vx->getBinContent("Expected_1BMSVx_maxTrigSyst", i);
    float B1trigSystMin = hist1Vx->getBinContent("Expected_1BMSVx_minTrigSyst", i);
    float B1VxSystMax =   hist1Vx->getBinContent("Expected_1BMSVx_maxVxSyst", i);
    float B1VxSystMin = hist1Vx->getBinContent("Expected_1BMSVx_minVxSyst", i);

    binBTrigSyst_max.at(2) = (B1trigSystMax - B1MSVxVal);
    binBVxSyst_max.at(2) = (B1VxSystMax - B1MSVxVal);

    binBTrigSyst_min.at(2) = (B1MSVxVal - B1trigSystMin);
    binBVxSyst_min.at(2) = (B1MSVxVal - B1VxSystMin);
    
    //end 1vx systs
        
    //endcap vertices:
    //stat
    float EE1MSVxVal =     hist1Vx->getBinContent("Expected_EE1MSVx", i);
    float EE1bMSVxVal =     hist1Vx->getBinContent("Expected_EE1MSVxb", i);
    float EE1eMSVxVal =     hist1Vx->getBinContent("Expected_EE1MSVxe", i);
        
    float EE1trigStatMax = hist1Vx->getBinContent("Expected_EE1MSVx_maxTrigStat", i);
    float EE1trigStatMin = hist1Vx->getBinContent("Expected_EE1MSVx_minTrigStat", i);
    float EE1VxStatMax =   hist1Vx->getBinContent("Expected_EE1MSVx_maxVxStat", i);
    float EE1VxStatMin =   hist1Vx->getBinContent("Expected_EE1MSVx_minVxStat", i);
        
    bin1eStat_max.at(0) = (EE1trigStatMax - EE1MSVxVal);
    bin1eStat_max.at(1) = (EE1VxStatMax - EE1MSVxVal);
        
    bin1eStat_min.at(0) = (EE1MSVxVal - EE1trigStatMin);
    bin1eStat_min.at(1) = (EE1MSVxVal - EE1VxStatMin);
                
    float EB1VXVal =       hist1Vx->getBinContent("Expected_EB1MSVx", i);
    float EB1bVXVal =       hist1Vx->getBinContent("Expected_EB1MSVxb", i);
    float EB1eVXVal =       hist1Vx->getBinContent("Expected_EB1MSVxe", i);

    float EB1trigStatMax = hist1Vx->getBinContent("Expected_EB1MSVx_maxTrigStat", i);
    float EB1trigStatMin = hist1Vx->getBinContent("Expected_EB1MSVx_minTrigStat", i);
    float EB1bvxStatMax =  hist1Vx->getBinContent("Expected_EB1MSVx_maxBVxStat", i);
    float EB1bvxStatMin =  hist1Vx->getBinContent("Expected_EB1MSVx_minBVxStat", i);
    float EB1evxStatMax =  hist1Vx->getBinContent("Expected_EB1MSVx_maxEVxStat", i);
    float EB1evxStatMin =  hist1Vx->getBinContent("Expected_EB1MSVx_minEVxStat", i);
        
    bin1eStat_max.at(2) = (EB1trigStatMax - EB1VXVal);
    bin1eStat_max.at(3) = (EB1bvxStatMax - EB1VXVal);
    bin1eStat_max.at(4) = (EB1evxStatMax - EB1VXVal);
        
    bin1eStat_min.at(2) = (EB1VXVal - EB1trigStatMin);
    bin1eStat_min.at(3) = (EB1VXVal - EB1bvxStatMin);
    bin1eStat_min.at(4) = (EB1VXVal - EB1evxStatMin);
        
    float E1MSVxVal =     hist1Vx->getBinContent("Expected_1EMSVx", i);
    float E1bMSVxVal =     hist1Vx->getBinContent("Expected_1EMSVxb", i);
    float E1eMSVxVal =     hist1Vx->getBinContent("Expected_1EMSVxe", i);
        
    float E1trigStatMax = hist1Vx->getBinContent("Expected_1EMSVx_maxTrigStat", i);
    float E1trigStatMin = hist1Vx->getBinContent("Expected_1EMSVx_minTrigStat", i);
    float E1VxStatMax =   hist1Vx->getBinContent("Expected_1EMSVx_maxVxStat", i);
    float E1VxStatMin =   hist1Vx->getBinContent("Expected_1EMSVx_minVxStat", i);
        
    bin1eStat_max.at(5) = (E1trigStatMax - E1MSVxVal);
    bin1eStat_max.at(6) = (E1VxStatMax - E1MSVxVal);
        
    bin1eStat_min.at(5) = (E1MSVxVal - E1trigStatMin);
    bin1eStat_min.at(6) = (E1MSVxVal - E1VxStatMin);
        
    float EE1trigSystMax = hist1Vx->getBinContent("Expected_EE1MSVx_maxTrigSyst", i);
    float EE1trigSystMin = hist1Vx->getBinContent("Expected_EE1MSVx_minTrigSyst", i);
    float EE1VxSystMax =   hist1Vx->getBinContent("Expected_EE1MSVx_maxVxSyst", i);
    float EE1VxSystMin =   hist1Vx->getBinContent("Expected_EE1MSVx_minVxSyst", i);
        
    binETrigSyst_max.at(0) = (EE1trigSystMax - EE1MSVxVal);
    binEVxSyst_max.at(1) = (EE1VxSystMax - EE1MSVxVal);
        
    binETrigSyst_min.at(0) = (EE1MSVxVal - EE1trigSystMin);
    binEVxSyst_min.at(1) = (EE1MSVxVal - EE1VxSystMin);

    float EB1trigSystMax = hist1Vx->getBinContent("Expected_EB1MSVx_maxTrigSyst", i);
    float EB1trigSystMin = hist1Vx->getBinContent("Expected_EB1MSVx_minTrigSyst", i);
    float EB1bvxSystMax =  hist1Vx->getBinContent("Expected_EB1MSVx_maxBVxSyst", i);
    float EB1bvxSystMin =  hist1Vx->getBinContent("Expected_EB1MSVx_minBVxSyst", i);
    float EB1evxSystMax =  hist1Vx->getBinContent("Expected_EB1MSVx_maxEVxSyst", i);
    float EB1evxSystMin =  hist1Vx->getBinContent("Expected_EB1MSVx_minEVxSyst", i);
        
    binETrigSyst_max.at(1) = (EB1trigSystMax - EB1VXVal);
    binBVxSyst_max.at(3) = (EB1bvxSystMax - EB1VXVal);
    binEVxSyst_max.at(2) = (EB1evxSystMax - EB1VXVal);
        
    binETrigSyst_min.at(1) = (EB1VXVal - EB1trigSystMin);
    binBVxSyst_min.at(3) = (EB1VXVal - EB1bvxSystMin);
    binEVxSyst_min.at(2) = (EB1VXVal - EB1evxSystMin);
        
    float E1trigSystMax = hist1Vx->getBinContent("Expected_1EMSVx_maxTrigSyst", i);
    float E1trigSystMin = hist1Vx->getBinContent("Expected_1EMSVx_minTrigSyst", i);
    float E1VxSystMax =   hist1Vx->getBinContent("Expected_1EMSVx_maxVxSyst", i);
    float E1VxSystMin = hist1Vx->getBinContent("Expected_1EMSVx_minVxSyst", i);
        
    binETrigSyst_max.at(2) = (E1trigSystMax - E1MSVxVal);
    binEVxSyst_max.at(3) = (E1VxSystMax - E1MSVxVal);
        
    binETrigSyst_min.at(2) = (E1MSVxVal - E1trigSystMin);
    binEVxSyst_min.at(3) = (E1MSVxVal - E1VxSystMin);
        
    //end 1vx systs
        
    //end 1vx stats

    //don't remember why stat vectors use std::accumulate...
    double tot1BTrigSystMAX = std::accumulate(binBTrigSyst_max.begin(), binBTrigSyst_max.end(),0.0);
    double tot1BTrigSystMIN = std::accumulate(binBTrigSyst_min.begin(), binBTrigSyst_min.end(),0.0);

    double tot1BTrigStatMAX = std::accumulate(binBTrigStat_max.begin(), binBTrigStat_max.end(),0.0);
    double tot1BTrigStatMIN = std::accumulate(binBTrigStat_min.begin(), binBTrigStat_min.end(),0.0);
        
    double tot1ETrigSystMAX = std::accumulate(binETrigSyst_max.begin(), binETrigSyst_max.end(),0.0);
    double tot1ETrigSystMIN = std::accumulate(binETrigSyst_min.begin(), binETrigSyst_min.end(),0.0);
        
    double tot1ETrigStatMAX = std::accumulate(binETrigStat_max.begin(), binETrigStat_max.end(),0.0);
    double tot1ETrigStatMIN = std::accumulate(binETrigStat_min.begin(), binETrigStat_min.end(),0.0);

    double tot1BVxSystMAX = std::accumulate(binBVxSyst_max.begin(), binBVxSyst_max.end(),0.0);
    double tot1BVxSystMIN = std::accumulate(binBVxSyst_min.begin(), binBVxSyst_min.end(),0.0);

    double tot1BVxBSystMAX = std::accumulate(binBVxSyst_max.begin(), binBVxSyst_max.end()-1,0.0);
    double tot1BVxBSystMIN = std::accumulate(binBVxSyst_min.begin(), binBVxSyst_min.end()-1,0.0);
        
    double tot1BVxESystMAX = binBVxSyst_max.at(binBVxSyst_max.size()-1);
    double tot1BVxESystMIN = binBVxSyst_min.at(binBVxSyst_min.size()-1);
       
        
    double tot1BVxStatMAX = std::accumulate(binBVxStat_max.begin(), binBVxStat_max.end(),0.0);
    double tot1BVxStatMIN = std::accumulate(binBVxStat_min.begin(), binBVxStat_min.end(),0.0);
        
    double tot1EVxSystMAX = std::accumulate(binEVxSyst_max.begin(), binEVxSyst_max.end(),0.0);
    double tot1EVxSystMIN = std::accumulate(binEVxSyst_min.begin(), binEVxSyst_min.end(),0.0);
       
    double tot1EVxESystMAX = std::accumulate(binEVxSyst_max.begin()+1, binEVxSyst_max.end(),0.0);
    double tot1EVxESystMIN = std::accumulate(binEVxSyst_min.begin()+1, binEVxSyst_min.end(),0.0);
        
    double tot1EVxBSystMAX = binEVxSyst_max.at(0);
    double tot1EVxBSystMIN = binEVxSyst_min.at(0);
        
    double tot1EVxStatMAX = std::accumulate(binEVxStat_max.begin(), binEVxStat_max.end(),0.0);
    double tot1EVxStatMIN = std::accumulate(binEVxStat_min.begin(), binEVxStat_min.end(),0.0);

    double tot1StatMAX = sqrt(std::inner_product(bin1bStat_max.begin(),bin1bStat_max.end(),bin1bStat_max.begin(),0.0) +
			      std::inner_product(bin1eStat_max.begin(),bin1eStat_max.end(),bin1eStat_max.begin(),0.0));
    double tot1StatMIN = sqrt(std::inner_product(bin1bStat_min.begin(),bin1bStat_min.end(),bin1bStat_min.begin(),0.0) + 
			      std::inner_product(bin1eStat_min.begin(),bin1eStat_min.end(),bin1eStat_min.begin(),0.0));

    double tot1BStatMAX = sqrt( std::inner_product(bin1bStat_max.begin(),bin1bStat_max.end(),bin1bStat_max.begin(),0.0));
    double tot1BStatMIN = sqrt( std::inner_product(bin1bStat_min.begin(),bin1bStat_min.end(),bin1bStat_min.begin(),0.0));

    double tot1EStatMAX = sqrt( std::inner_product(bin1eStat_max.begin(),bin1eStat_max.end(),bin1eStat_max.begin(),0.0));
    double tot1EStatMIN = sqrt( std::inner_product(bin1eStat_min.begin(),bin1eStat_min.end(),bin1eStat_min.begin(),0.0));

        
    double totStat1MAXq = sqrt( sq(tot1BTrigStatMAX) + sq(tot1BVxStatMAX) + sq(tot1EVxStatMAX));
    double totStat1MINq = sqrt( sq(tot1BTrigStatMIN) + sq(tot1BVxStatMIN) + sq(tot1EVxStatMIN));

    double totSyst1MAXq = sqrt( sq(tot1BTrigSystMAX) + sq(tot1ETrigSystMAX) + sq(tot1BVxSystMAX) + sq(tot1EVxSystMAX));
    double totSyst1MINq = sqrt( sq(tot1BTrigSystMIN) + sq(tot1ETrigSystMIN) + sq(tot1BVxSystMIN) + sq(tot1EVxSystMIN));

    double totBSyst1MAXq = sqrt( sq(tot1BTrigSystMAX) + sq(tot1BVxBSystMAX) + sq(tot1EVxBSystMAX));
    double totBSyst1MINq = sqrt( sq(tot1BTrigSystMIN) + sq(tot1BVxBSystMIN) + sq(tot1EVxBSystMIN));
    double totESyst1MAXq = sqrt( sq(tot1ETrigSystMAX) + sq(tot1BVxESystMAX) + sq(tot1EVxESystMAX));
    double totESyst1MINq = sqrt( sq(tot1ETrigSystMIN) + sq(tot1BVxESystMIN) + sq(tot1EVxESystMIN));

        
    double tot1bErrorMAXq = sqrt( sq(tot1BStatMAX) + sq(totBSyst1MAXq));
    double tot1bErrorMINq = sqrt( sq(tot1BStatMIN) + sq(totBSyst1MINq));
    double tot1eErrorMAXq = sqrt( sq(tot1EStatMAX) + sq(totESyst1MAXq));
    double tot1eErrorMINq = sqrt( sq(tot1EStatMIN) + sq(totESyst1MINq));
        
    double tot1ErrorMAXq = sqrt( sq(tot1StatMAX) + sq(totSyst1MAXq));
    double tot1ErrorMINq = sqrt( sq(tot1StatMIN) + sq(totSyst1MINq));

      //double bvx_bin = B1MSVxVal+BB1MSVxVal+BE1VXVal; // This takes into account also 2 vtx events
      double bvx_bin = B1MSVxVal; // Only 1 vtx events (so that we can combine results with 2 vtx analysis)
      hist1Vx->setBinContent("Expected_ABCD_BVx", i,bvx_bin);
        
      //double evx_bin = E1MSVxVal+EE1MSVxVal+EB1VXVal; // This takes into account also 2 vtx events
      double evx_bin = E1MSVxVal; // Only 1 vtx events (so that we can combine results with 2 vtx analysis)
      hist1Vx->setBinContent("Expected_ABCD_EVx", i,evx_bin);
        
      double onevx_bin = bvx_bin + evx_bin;
      hist1Vx->setBinContent("Expected_ABCD1MSVx", i,onevx_bin);
      hist1Vx->setBinContent("Expected_ABCD1MSVx_maxStat", i, onevx_bin+tot1StatMAX);
      hist1Vx->setBinContent("Expected_ABCD1MSVx_maxSyst", i, onevx_bin+totSyst1MAXq);
      hist1Vx->setBinContent("Expected_ABCD1MSVx_maxTotal", i, onevx_bin+tot1ErrorMAXq);
        
      hist1Vx->setBinContent("Expected_ABCD_BVx_maxStat", i, bvx_bin+tot1BStatMAX);
      hist1Vx->setBinContent("Expected_ABCD_BVx_maxSyst", i, bvx_bin+totBSyst1MAXq);
      hist1Vx->setBinContent("Expected_ABCD_BVx_maxTotal", i, bvx_bin+tot1bErrorMAXq);
      hist1Vx->setBinContent("Expected_ABCD_EVx_maxStat", i, evx_bin+tot1EStatMAX);
      hist1Vx->setBinContent("Expected_ABCD_EVx_maxSyst", i, evx_bin+totESyst1MAXq);
      hist1Vx->setBinContent("Expected_ABCD_EVx_maxTotal", i, evx_bin+tot1eErrorMAXq);

      if(onevx_bin > totStat1MINq){
      hist1Vx->setBinContent("Expected_ABCD1MSVx_minStat", i, onevx_bin-tot1StatMIN);
      } else {
      hist1Vx->setBinContent("Expected_ABCD1MSVx_minStat", i, 0);
      }
      if(onevx_bin > totSyst1MINq){
      hist1Vx->setBinContent("Expected_ABCD1MSVx_minSyst", i, onevx_bin-totSyst1MINq);
      } else {
      hist1Vx->setBinContent("Expected_ABCD1MSVx_minSyst", i, 0);
      }
      if(onevx_bin > tot1ErrorMINq){
      hist1Vx->setBinContent("Expected_ABCD1MSVx_minTotal", i, onevx_bin-tot1ErrorMINq);
      } else {
      hist1Vx->setBinContent("Expected_ABCD1MSVx_minTotal", i, 0);
      }
        
      if(bvx_bin > totStat1MINq){
      hist1Vx->setBinContent("Expected_ABCD_BVx_minStat", i, bvx_bin-tot1BStatMIN);
      } else {
      hist1Vx->setBinContent("Expected_ABCD_BVx_minStat", i, 0);
      }
      if(bvx_bin > totSyst1MINq){
      hist1Vx->setBinContent("Expected_ABCD_BVx_minSyst", i, bvx_bin-totBSyst1MINq);
      } else {
      hist1Vx->setBinContent("Expected_ABCD_BVx_minSyst", i, 0);
      }
      if(bvx_bin > tot1ErrorMINq){
      hist1Vx->setBinContent("Expected_ABCD_BVx_minTotal", i, bvx_bin-tot1bErrorMINq);
      } else {
      hist1Vx->setBinContent("Expected_ABCD_BVx_minTotal", i, 0);
      }
      if(evx_bin > totStat1MINq){
      hist1Vx->setBinContent("Expected_ABCD_EVx_minStat", i, evx_bin-tot1EStatMIN);
      } else {
      hist1Vx->setBinContent("Expected_ABCD_EVx_minStat", i, 0);
      }
      if(evx_bin > totSyst1MINq){
      hist1Vx->setBinContent("Expected_ABCD_EVx_minSyst", i, evx_bin-totESyst1MINq);
      } else {
      hist1Vx->setBinContent("Expected_ABCD_EVx_minSyst", i, 0);
      }
      if(evx_bin > tot1ErrorMINq){
      hist1Vx->setBinContent("Expected_ABCD_EVx_minTotal", i, evx_bin-tot1eErrorMINq);
      } else {
      hist1Vx->setBinContent("Expected_ABCD_EVx_minTotal", i, 0);
      }
    
  }
 
  outputFile->Write();

}


//  LocalWords:  Rndm

//
//  skimNtuples.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 05/03/17.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/CommonVariables.h"

#include "TFile.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

#include "PlottingPackage/skimmingVariableNames.h"

using namespace std;
using namespace plotVertexParameters;

TStopwatch timer;
int isSignal; bool isData;
double jetSliceWeights[13];
double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}

int main(int argc, char **argv){

    std::cout << "running ntuple skimming program!" << std::endl;

    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");

    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    std::cout << "Output file name : " << argv[3] << std::endl;

    isData = (TString(argv[1]).Contains("data") || TString(argv[1]).Contains("Data")) ? true : false;

    InputFiles::AddFilesToChain(TString(argv[1]), chain);

    //Directory the plots will go in

    TString outputDirectory = TString(argv[2]);

    TFile *outputFile = new TFile(outputDirectory+"/"+TString(argv[3]),"RECREATE");

    std::cout << "added file!, will be saved in directory: " << outputDirectory << std::endl;

    chain->SetBranchStatus("*",0);

    TString inputFile = chain->GetFile()->GetName();

    CommonVariables *commonVar = new CommonVariables;

    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain,true,false,false,false,false);
    std::cout << "got common variables" << std::endl;

    std::cout << "" << std::endl;
    std::cout << " Running over " << chain->GetEntries() << " events." << std::endl;
    std::cout << "" << std::endl;

    setJetSliceWeightsVxRerun(jetSliceWeights);

    //load the other file for side-by-side streaming
    TChain *vxChain = new TChain("recoTree");
    std::map<ULong64_t, int> msvxMap;
    ULong64_t eventNumber = 0;
    double newEventWeight=0;
    InputFiles::AddVertexRerunFiles(TString(argv[1]), vxChain);
    msvxMap = CreateMap(vxChain);
    vxChain->SetBranchStatus("*",0);
    vxChain->SetBranchAddress("EventNumber", &eventNumber);
    vxChain->SetBranchAddress("EventIterator",&eventIterator);
    vxChain->SetBranchAddress("EventWeight",&eventWeight);
    vxChain->SetBranchAddress("Run",&runNumber);
    vxChain->SetBranchAddress("nMDTHits",&nMDTs);
    vxChain->SetBranchAddress("nRPCHits",&nRPCs);
    vxChain->SetBranchAddress("nTGCHits",&nTGCs);
    vxChain->SetBranchAddress("mdt_chamber",&mdtChamber);
    vxChain->SetBranchAddress("mdt_tube",&mdtTube);
    vxChain->SetBranchAddress("mdt_layer",&mdtLayer);
    vxChain->SetBranchAddress("mdt_eta",&mdtEta);
    vxChain->SetBranchAddress("mdt_phi",&mdtPhi);
    vxChain->SetBranchAddress("mdt_R",&mdtR);
    vxChain->SetBranchAddress("mdt_z",&mdtz);
    vxChain->SetBranchAddress("mdtLoc_x",&mdtLoc_x);
    vxChain->SetBranchAddress("mdtLoc_y",&mdtLoc_y);
    vxChain->SetBranchAddress("mdtLoc_z",&mdtLoc_z);
    vxChain->SetBranchAddress("mdt_adc",&mdtAdc);
    vxChain->SetBranchAddress("mdt_tdc",&mdtTDC);
    vxChain->SetBranchAddress("mdt_status",&mdtStatus);
    vxChain->SetBranchAddress("mdt_radius",&mdtRadius);
    vxChain->SetBranchAddress("mdt_radErr",&mdtRadErr);
    /*vxChain->SetBranchAddress("MLCh_ML", &MLChMLInt);
     vxChain->SetBranchAddress("MLCh_Sector", &MLChSecInt);
     vxChain->SetBranchAddress("MLCh_Station", &MLChStInt);
     vxChain->SetBranchAddress("MLCh_nGoodHits", &MLCh_nGoodHits);
     vxChain->SetBranchAddress("MLCh_nHits", &MLCh_nHits);
     vxChain->SetBranchAddress("MLCh_nSeeds", &MLCh_nSeeds);
     vxChain->SetBranchAddress("MLCh_nSegs", &MLCh_nSegs);*/
    vxChain->SetBranchAddress("n3hitTrackletSeeds",&n3HitTrackletSeeds);
    vxChain->SetBranchAddress("TrackletSeed_slope",&TrackletSeeds_slope);
    vxChain->SetBranchAddress("TrackletSeed_int",&TrackletSeeds_int);
    vxChain->SetBranchAddress("TrackletSeed_mdt1_z",&TrackletSeeds_mdt1_z);
    vxChain->SetBranchAddress("TrackletSeed_mdt1_R",&TrackletSeeds_mdt1_R);
    vxChain->SetBranchAddress("TrackletSeed_mdt2_z",&TrackletSeeds_mdt2_z);
    vxChain->SetBranchAddress("TrackletSeed_mdt2_R",&TrackletSeeds_mdt2_R);
    vxChain->SetBranchAddress("TrackletSeed_mdt3_z",&TrackletSeeds_mdt3_z);
    vxChain->SetBranchAddress("TrackletSeed_mdt3_R",&TrackletSeeds_mdt3_R);
    vxChain->SetBranchAddress("nTrackletSegs",&nTrackletSegs);
    vxChain->SetBranchAddress("nTrackSegments",&nTrackSegments);
    vxChain->SetBranchAddress("nTracklets",&nTracklets);
    vxChain->SetBranchAddress("TrackletCounts",&nTracklet_Systs);
    vxChain->SetBranchAddress("TrackSeg_isGood",&TrackSegmentisGood);
    vxChain->SetBranchAddress("TrackSeg_station",&TrackSegmentStation);
    vxChain->SetBranchAddress("TrackSeg_deltaB",&TrackSegmentDeltaB);
    vxChain->SetBranchAddress("TrackSeg_deltaAlpha",&TrackSegmentDeltaAlpha);
    vxChain->SetBranchAddress("TrackSegment_eta",&TrackSegment_eta);
    vxChain->SetBranchAddress("TrackSegment_phi",&TrackSegment_phi);
    vxChain->SetBranchAddress("TrackSegment_R",&TrackSegment_R);
    vxChain->SetBranchAddress("TrackSegment_z",&TrackSegment_z);
    vxChain->SetBranchAddress("TSeg_tmp_chi2", &TSeg_tmp_chi2);
    vxChain->SetBranchAddress("TSeg_tmp_nodf", &TSeg_tmp_ndof);
    vxChain->SetBranchAddress("TSeg_tmp_chi2Prob", &TSeg_tmp_chi2Prob);
    vxChain->SetBranchAddress("MSeg_eta",&MSeg_eta);
    vxChain->SetBranchAddress("MSeg_phi",&MSeg_phi);
    vxChain->SetBranchAddress("MSeg_R",&MSeg_R);
    vxChain->SetBranchAddress("MSeg_x",&MSeg_x);
    vxChain->SetBranchAddress("MSeg_y",&MSeg_y);
    vxChain->SetBranchAddress("MSeg_z",&MSeg_z);
    vxChain->SetBranchAddress("MSeg_alpha",&MSeg_alpha);
    vxChain->SetBranchAddress("MSeg_chamber",&MSeg_chamber);
    vxChain->SetBranchAddress("MSegLoc_x",&MSegLoc_x);
    vxChain->SetBranchAddress("MSegLoc_y",&MSegLoc_y);
    vxChain->SetBranchAddress("MSegLoc_z",&MSegLoc_z);
    vxChain->SetBranchAddress("MSegLoc_alpha",&MSegLoc_theta);
    vxChain->SetBranchAddress("MSeg_chi2Prob",&MSeg_chi2Prob);
    vxChain->SetBranchAddress("Tracklet_eta",&Tracklet_eta);
    vxChain->SetBranchAddress("Tracklet_phi",&Tracklet_phi);
    vxChain->SetBranchAddress("Tracklet_R",&Tracklet_R);
    vxChain->SetBranchAddress("Tracklet_z",&Tracklet_z);
    vxChain->SetBranchAddress("Tracklet_nHitsOnTrack",&Tracklet_nHitsOnTrack);
    vxChain->SetBranchAddress("Tracklet_ml1_d12",&Tracklet_ml1_d12);
    vxChain->SetBranchAddress("Tracklet_ml1_d13",&Tracklet_ml1_d13);
    vxChain->SetBranchAddress("Tracklet_ml2_d12",&Tracklet_ml2_d12);
    vxChain->SetBranchAddress("Tracklet_ml2_d13",&Tracklet_ml2_d13);
    vxChain->SetBranchAddress("Tracklet_ml1_nHitsOnTrack",&Tracklet_ml1_nHitsOnTrack);
    vxChain->SetBranchAddress("Tracklet_ml2_nHitsOnTrack",&Tracklet_ml2_nHitsOnTrack);
    /* vxChain->SetBranchAddress("Tracklet_Hits_ml1_R",&Tracklet_Hits_ml1_R);
     vxChain->SetBranchAddress("Tracklet_Hits_ml1_z",&Tracklet_Hits_ml1_z);
     vxChain->SetBranchAddress("Tracklet_Hits_ml1_eta",&Tracklet_Hits_ml1_eta);
     vxChain->SetBranchAddress("Tracklet_Hits_ml1_phi",&Tracklet_Hits_ml1_phi);
     vxChain->SetBranchAddress("Tracklet_Hits_ml2_R",&Tracklet_Hits_ml2_R);
     vxChain->SetBranchAddress("Tracklet_Hits_ml2_z",&Tracklet_Hits_ml2_z);
     vxChain->SetBranchAddress("Tracklet_Hits_ml2_eta",&Tracklet_Hits_ml2_eta);
     vxChain->SetBranchAddress("Tracklet_Hits_ml2_phi",&Tracklet_Hits_ml2_phi);*/
    vxChain->SetBranchAddress("Tracklet_chamber",&Tracklet_chamber);
    vxChain->SetBranchAddress("Tracklet_inBarrel",&Tracklet_inBarrel);
    vxChain->SetBranchAddress("Tracklet_ML1Seg_x",&Tracklet_ML1Seg_pos_x);
    vxChain->SetBranchAddress("Tracklet_ML1Seg_y",&Tracklet_ML1Seg_pos_y);
    vxChain->SetBranchAddress("Tracklet_ML1Seg_z",&Tracklet_ML1Seg_pos_z);
    vxChain->SetBranchAddress("Tracklet_ML1Seg_alpha",&Tracklet_ML1Seg_alpha);
    vxChain->SetBranchAddress("Tracklet_ML2Seg_x",&Tracklet_ML2Seg_pos_x);
    vxChain->SetBranchAddress("Tracklet_ML2Seg_y",&Tracklet_ML2Seg_pos_y);
    vxChain->SetBranchAddress("Tracklet_ML2Seg_z",&Tracklet_ML2Seg_pos_z);
    vxChain->SetBranchAddress("Tracklet_ML2Seg_alpha",&Tracklet_ML2Seg_alpha);
    /*vxChain->SetBranchAddress("LLP_inMSBarrel",&LLP_inBarrel);
     vxChain->SetBranchAddress("LLP_inMSEndcap",&LLP_inEndcap);
     vxChain->SetBranchAddress("LLP_R",&LLP_R);
     vxChain->SetBranchAddress("LLP_z",&LLP_z);
     vxChain->SetBranchAddress("LLP_eta",&LLP_eta);
     vxChain->SetBranchAddress("LLP_phi",&LLP_phi);
     vxChain->SetBranchAddress("LLP_gamma",&LLP_gamma);
     vxChain->SetBranchAddress("LLP_nTrackSegments",&LLP_nTrackSegs);
     vxChain->SetBranchAddress("LLP_nTracklets",&LLP_nTracklets);
     vxChain->SetBranchAddress("LLP_nMDT",&LLP_nMDT);
     vxChain->SetBranchAddress("LLP_nRPC",&LLP_nRPC);
     vxChain->SetBranchAddress("LLP_nTGC",&LLP_nTGC);*/
    vxChain->SetBranchAddress("Muon_eta", &muon_eta);
    vxChain->SetBranchAddress("Muon_phi", &muon_phi);
    vxChain->SetBranchAddress("ProgressBar", &progressBar);
    vxChain->SetBranchAddress("ProgressBar_BVx", &progressBar_BVx);
    vxChain->SetBranchAddress("ProgressBar_EVx", &progressBar_EVx);
    vxChain->SetBranchAddress("Cluster_eta", &Cluster_eta);
    vxChain->SetBranchAddress("Cluster_phi", &Cluster_phi);
    vxChain->SetBranchAddress("Cluster_region", &Cluster_region);
    vxChain->SetBranchAddress("Cluster_sf", &Cluster_sf);
    vxChain->SetBranchAddress("Cluster_syst", &Cluster_syst);
    vxChain->SetBranchAddress("nClusters", &nClusters);
    vxChain->SetBranchAddress("nVertices", &nVertices);
    vxChain->SetBranchAddress("Vertex_indexLLP", &Vertex_indexLLP);
    vxChain->SetBranchAddress("Vertex_LLP_dR", &Vertex_LLP_dR);
    vxChain->SetBranchAddress("Vertex_eta", &VertexEta);
    vxChain->SetBranchAddress("Vertex_phi", &VertexPhi);
    vxChain->SetBranchAddress("Vertex_R", &Vertex_R);
    vxChain->SetBranchAddress("Vertex_z", &Vertex_z);
    vxChain->SetBranchAddress("Vertex_chi2prob", &Vertex_chi2prob);
    vxChain->SetBranchAddress("Vertex_nMDT", &Vertex_nMDT);
    vxChain->SetBranchAddress("Vertex_nRPC", &Vertex_nRPC);
    vxChain->SetBranchAddress("Vertex_nTGC", &Vertex_nTGC);
    vxChain->SetBranchAddress("Vertex_nTracklets", &Vertex_nTracklets);
    vxChain->SetBranchAddress("Vertex_nHighOccChambers", &Vertex_nHighOccChambers);
    vxChain->SetBranchAddress("Vertex_pass", &Vertex_pass);
    vxChain->SetBranchAddress("Vertex_region", &Vertex_region);
    vxChain->SetBranchAddress("Vertex_author", &Vertex_author);
    vxChain->SetBranchAddress("Vertex_sf", &Vertex_sf);
    vxChain->SetBranchAddress("Vertex_syst", &Vertex_syst);
    vxChain->SetBranchAddress("nGoodSegPairs", &nGoodSegmentPairs);
    vxChain->SetBranchAddress("nGoodSegPairsBarrel_Straight", &nGoodSegmentPairsSB);
    vxChain->SetBranchAddress("nGoodSegPairsBarrel_Curved", &nGoodSegmentPairsCB);
    vxChain->SetBranchAddress("nGoodSegPairsEndcap", &nGoodSegmentPairsE);
    vxChain->SetBranchAddress("nSeedsPerSegPair",&nSeedsPerSegPair);
    vxChain->SetBranchAddress("TrackletBestChi2",&TrackletFitBestChi2);

    //Create a new file + a clone of old tree in new file
    TTree *newtree = vxChain->CloneTree(0);
    newtree->Branch("newEventWeight",&newEventWeight);
    newtree->Branch("sum_msegA",&sum_mseg);
    newtree->Branch("lj_eta",&lj_eta);
    newtree->Branch("slj_eta",&slj_eta);
    newtree->Branch("lj_phi",&lj_phi);
    newtree->Branch("slj_phi",&slj_phi);
    newtree->Branch("lj_pt",&lj_pT);
    newtree->Branch("slj_pt",&slj_pT);
    newtree->Branch("lj_nmsegA",&lj_nmsegA);
    newtree->Branch("slj_nmsegA",&slj_nmsegA);
    newtree->Branch("lj_nmseg2",&lj_nmseg2);
    newtree->Branch("slj_nmseg2",&slj_nmseg2);
    newtree->Branch("lj_nmseg4",&lj_nmseg4);
    newtree->Branch("slj_nmseg4",&slj_nmseg4);
    newtree->Branch("lj_nmseg6",&lj_nmseg6);
    newtree->Branch("slj_nmseg6",&slj_nmseg6);
    newtree->Branch("lj_nmseg8",&lj_nmseg8);
    newtree->Branch("slj_nmseg8",&slj_nmseg8);
    newtree->Branch("lj_nmseg10",&lj_nmseg10);
    newtree->Branch("slj_nmseg10",&slj_nmseg10);

    timer.Start();
    int jetSlice = 99;

    for (int l=0;l<chain->GetEntries();l++)
    {


        if(l % 50000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }

        chain->GetEntry(l);
        if(!isData){
            if(   inputFile.Contains("JZ")    ){
                isSignal = 0;
                if(inputFile.Contains("JZ0W")) jetSlice = 0;
                if(inputFile.Contains("JZ1W")) jetSlice = 1;
                if(inputFile.Contains("JZ2W")) jetSlice = 2;
                if(inputFile.Contains("JZ3W")) jetSlice = 3;
                if(inputFile.Contains("JZ4W")) jetSlice = 4;
                if(inputFile.Contains("JZ5W")) jetSlice = 5;
                if(inputFile.Contains("JZ6W")) jetSlice = 6;
                if(inputFile.Contains("JZ7W")) jetSlice = 7;
                if(inputFile.Contains("JZ8W")) jetSlice = 8;
                if(inputFile.Contains("JZ9W")) jetSlice = 9;
                if(inputFile.Contains("JZ10W")) jetSlice = 10;
                if(inputFile.Contains("JZ11W")) jetSlice = 11;
                if(inputFile.Contains("JZ12W")) jetSlice = 12;
            }
            if( inputFile.Contains("LongLived") || inputFile.Contains("_LLP") || inputFile.Contains("Stealth") ){ isSignal = 1; }
            if( inputFile.Contains("data15_13TeV")){ isSignal=0; }
        }
        if(isSignal == 0 ) newEventWeight = jetSliceWeights[jetSlice] * commonVar->eventWeight;
        else if( isSignal == 1 ) newEventWeight = 1.0;
        else std::cout << "what on earth are you running on? Input file is " << inputFile << std::endl;
        if(l==0) std::cout << "isSignal: " << isSignal << std::endl;

        if(isData) newEventWeight = 1.0;

        if(!commonVar->passJ400){
            commonVar->clearAllVectors(false,true, false);
            continue; //dont want forward jets
        }

        if(!commonVar->isQualityEvent(false)) {
            commonVar->clearAllVectors(false, true ,false);
            continue;
        }
        if(!commonVar->hasGoodPV) {
            commonVar->clearAllVectors(false, true ,false);
            continue;
        }

        commonVar->setGoodJetEventFlags();
        if(commonVar->isBadEventJetLow){
            commonVar->clearAllVectors(false, true ,false);
            continue;
        }
        if(commonVar->Jet_eta->size() < 2){
            commonVar->clearAllVectors(false,true,false);
            continue;
        }

        commonVar->calcMHT();
        if(!commonVar->isGoodDijetEvent()){
            commonVar->clearAllVectors(false, true, false);
            continue;
        }

        if(TMath::Abs(commonVar->Jet_eta->at(0)) > 2.7 || TMath::Abs(commonVar->Jet_eta->at(1)) > 2.7){
            commonVar->clearAllVectors(false,true, false);
            continue; //dont want forward jets
        }

        int pt1 = 0; int pt2 = 0;
        if(commonVar->Jet_nAssocMSeg->at(0) > 20) pt1 = 1;
        if(commonVar->Jet_nAssocMSeg->at(1) > 20) pt2 = 1;

        if(pt1 == 0 && pt2 == 0){
            commonVar->clearAllVectors(false,true, false);
            continue; //only want events with a punch-through jet
        }

        //if(l < 1531522) continue;
        // not found

        if(msvxMap.find(commonVar->eventNumber) == msvxMap.end())
        {
            std::cout << "event " << commonVar->eventNumber << " not found :(" << std::endl;
            commonVar->clearAllVectors(false, true ,false);
            continue;
        }

        vxChain->GetEntry(msvxMap[commonVar->eventNumber]);

        lj_eta = commonVar->Jet_eta->at(0);
        slj_eta = commonVar->Jet_eta->at(1);

        lj_nmsegA = commonVar->Jet_nAssocMSeg->at(0);
        slj_nmsegA = commonVar->Jet_nAssocMSeg->at(1);

        for(int i=0;i<2; i++){ //look at leading, subleading jets
            std::vector<int> mseg_vec = {0,0,0,0,0};
            mseg_cone.push_back(mseg_vec);
            sum_mseg += commonVar->Jet_nAssocMSeg->at(i);
            for(int j=0; j < MSeg_eta->size(); j++){
                //dR squared...
                double dr2 = DeltaR2(MSeg_phi->at(j), commonVar->Jet_phi->at(i),MSeg_eta->at(j), commonVar->Jet_eta->at(i));
                if(dr2 < 0.04 ) mseg_cone.at(i).at(0)++;
                if(dr2 < 0.16 ) mseg_cone.at(i).at(1)++;
                if(dr2 < 0.36 ) mseg_cone.at(i).at(2)++;
                if(dr2 < 0.64 ) mseg_cone.at(i).at(3)++;
                if(dr2 < 1.00 ) mseg_cone.at(i).at(4)++;
            }
        }

        lj_phi = commonVar->Jet_phi->at(0);
        slj_phi = commonVar->Jet_phi->at(1);
        lj_pT = commonVar->Jet_pT->at(0);
        slj_pT = commonVar->Jet_pT->at(1);

        lj_nmseg2   = mseg_cone.at(0).at(0);
        lj_nmseg4   = mseg_cone.at(0).at(1);
        lj_nmseg6   = mseg_cone.at(0).at(2);
        lj_nmseg8   = mseg_cone.at(0).at(3);
        lj_nmseg10  = mseg_cone.at(0).at(4);

        slj_nmseg2  = mseg_cone.at(1).at(0);
        slj_nmseg4  = mseg_cone.at(1).at(1);
        slj_nmseg6  = mseg_cone.at(1).at(2);
        slj_nmseg8  = mseg_cone.at(1).at(3);
        slj_nmseg10 = mseg_cone.at(1).at(4);


        newtree->Fill();

        sum_mseg = 0;
        lj_nmsegA = 0;
        slj_nmsegA = 0;
        lj_nmseg2 = 0;
        slj_nmseg2 = 0;
        lj_nmseg4 = 0;
        slj_nmseg4 = 0;
        lj_nmseg6 = 0;
        slj_nmseg6 = 0;
        lj_nmseg8 = 0;
        slj_nmseg8 = 0;
        lj_nmseg10 = 0;
        slj_nmseg10 = 0;
        lj_phi = 0;
        slj_phi = 0;
        lj_pT = 0;
        slj_pT = 0;
        lj_eta = 0;
        lj_eta = 0;
        mseg_cone.clear();

        commonVar->clearAllVectors(false, true ,false);
        runNumber = 0;
        eventNumber = 0;
        eventWeight = 0;
        newEventWeight = 0;
        nMDTs=0;
        nRPCs=0;
        nTGCs=0;
        n3HitTrackletSeeds=0;
        mdtChamber->clear();
        mdtTube->clear();
        mdtLayer->clear();
        mdtR->clear();
        mdtz->clear();
        mdtEta->clear();
        mdtPhi->clear();
        mdtStatus->clear();
        mdtAdc->clear();
        mdtTDC->clear();
        mdtRadius->clear();
        mdtRadErr->clear();
        mdtLoc_x->clear();
        mdtLoc_y->clear();
        mdtLoc_z->clear();
        MLChStInt->clear();
        MLChSecInt->clear();
        MLChMLInt->clear();
        MLCh_nGoodHits->clear();
        MLCh_nHits->clear();
        MLCh_nSeeds->clear();
        MLCh_nSegs->clear();
        TrackletSeeds_slope->clear();
        TrackletSeeds_int->clear();
        TrackletSeeds_mdt1_z->clear();
        TrackletSeeds_mdt1_R->clear();
        TrackletSeeds_mdt2_z->clear();
        TrackletSeeds_mdt2_R->clear();
        TrackletSeeds_mdt3_z->clear();
        TrackletSeeds_mdt3_R->clear();
        nTrackletSegs=0;
        nTrackSegments=0;
        TrackSegmentisGood->clear();
        TrackSegmentStation->clear();
        TrackSegmentDeltaB->clear();
        TrackSegmentDeltaAlpha->clear();
        TrackSegment_eta->clear();
        TrackSegment_phi->clear();
        TrackSegment_R->clear();
        TrackSegment_z->clear();
        TSeg_tmp_ndof->clear();
        TSeg_tmp_chi2->clear();
        TSeg_tmp_chi2Prob->clear();

        MSegLoc_x->clear();
        MSegLoc_y->clear();
        MSegLoc_z->clear();
        MSegLoc_theta->clear();
        MSeg_eta->clear();
        MSeg_phi->clear();
        MSeg_R->clear();
        MSeg_x->clear();
        MSeg_y->clear();
        MSeg_z->clear();
        MSeg_chamber->clear();
        MSeg_alpha->clear();
        MSeg_chi2Prob->clear();
        Tracklet_eta->clear();
        Tracklet_phi->clear();
        Tracklet_R->clear();
        Tracklet_z->clear();
        Tracklet_ml1_d12->clear();
        Tracklet_ml1_d13->clear();
        Tracklet_ml2_d12->clear();
        Tracklet_ml2_d13->clear();
        Tracklet_ml1_nHitsOnTrack->clear();
        Tracklet_ml2_nHitsOnTrack->clear();
        Tracklet_nHitsOnTrack->clear();

        Tracklet_Hits_ml1_R->clear();
        Tracklet_Hits_ml1_z->clear();
        Tracklet_Hits_ml1_eta->clear();
        Tracklet_Hits_ml1_phi->clear();

        Tracklet_Hits_ml2_R->clear();
        Tracklet_Hits_ml2_z->clear();
        Tracklet_Hits_ml2_eta->clear();
        Tracklet_Hits_ml2_phi->clear();

        Tracklet_ML1Seg_pos_x->clear();
        Tracklet_ML1Seg_pos_y->clear();
        Tracklet_ML1Seg_pos_z->clear();

        Tracklet_ML1Seg_alpha->clear();
        Tracklet_chamber->clear();
        Tracklet_inBarrel->clear();
        LLP_inEndcap->clear();
        LLP_inBarrel->clear();
        muon_eta->clear();
        muon_phi->clear();
        LLP_R->clear();
        LLP_z->clear();
        LLP_eta->clear();
        LLP_phi->clear();
        LLP_gamma->clear();
        LLP_deltar->clear();
        LLP_nTrackSegs->clear();
        LLP_nTracklets->clear();
        LLP_nMDT->clear();
        LLP_nRPC->clear();
        LLP_nTGC->clear();
        nTracklets=0;
        nClusters=0;
        nVertices=0;
        Cluster_eta->clear();
        Cluster_phi->clear();
        Cluster_region->clear();
        Cluster_sf->clear();
        Cluster_syst->clear();
        Vertex_indexLLP->clear();
        Vertex_LLP_dR->clear();
        VertexEta->clear();
        VertexPhi->clear();
        Vertex_R->clear();
        Vertex_z->clear();
        Vertex_nMDT->clear();
        Vertex_nTGC->clear();
        Vertex_nRPC->clear();
        Vertex_nHighOccChambers->clear();
        Vertex_nTracklets->clear();
        Vertex_chi2prob->clear();
        Vertex_pass->clear();
        Vertex_region->clear();
        Vertex_author->clear();
        Vertex_sf->clear();
        Vertex_syst->clear();
        progressBar=0;
        progressBar_BVx=0;
        progressBar_EVx=0;
        nGoodSegmentPairs=0;
        nGoodSegmentPairsSB=0;
        nGoodSegmentPairsCB=0;
        TrackletFitBestChi2->clear();
        nSeedsPerSegPair->clear();
        nTracklet_Systs->clear();
    }

    std::cout << "finished all events" << std::endl;


    newtree->Print();
    newtree->AutoSave();
    //outputFile->Write();


    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

    delete outputFile;

}

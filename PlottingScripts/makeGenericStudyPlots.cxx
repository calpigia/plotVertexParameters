
void makeGenericStudyPlots(){
	//0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

	    TCanvas* c1 = new TCanvas("c1","c1",800,600);
		  SetAtlasStyle();

	//c1->SetRightMargin(0.05);
	//c1->SetTopMargin(0.07);
	//c1->SetLeftMargin(0.15);
	//c1->SetBottomMargin(0.15);
	TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
		pad1->SetTopMargin(0.075);
		pad1->SetBottomMargin(0.04);
		pad1->SetLeftMargin(0.14);
		pad1->SetLogy();
		pad1->Draw();
	
	  TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
	  pad2->SetTopMargin(0.05);
	  pad2->SetBottomMargin(0.40);
	  pad2->SetLeftMargin(0.14);
	  pad2->Draw();


	  
	//TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/dijetData/outputDataSearch.root");
	TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/dijetData/outputDataSearch_JetIso_unscaled.root");


	TString plotDir = "GenericAnalysis/ABCD/";
	

	 c1->cd();
	 pad1->cd();
	 pad1->SetLogy(0);
            gStyle->SetPadTickX(2);
            gStyle->SetPadTickY(2);

	TH1D *h_iso = (TH1D*)_file0->Get("pVx_vs_nJets_iso");
	TH1D *iso_d = (TH1D*)_file0->Get("nJets_iso");
	TH1D *h_noiso = (TH1D*)_file0->Get("pVx_vs_nJets_noiso");
	TH1D *noiso_d = (TH1D*)_file0->Get("nJets_noiso");

	//h_iso->SetBinContent(4,h_iso->GetBinContent(4) + h_iso->GetBinContent(5));
	//iso_d->SetBinContent(4,iso_d->GetBinContent(4) + iso_d->GetBinContent(5));

	h_iso->Divide(iso_d);
	h_noiso->Divide(noiso_d);

	h_noiso->GetXaxis()->SetTitle("Number of additional jets");
	h_noiso->GetYaxis()->SetTitle("P(MSVx)"); // / P(MSVx | 0 jets)");
	
	double iso_norm = h_iso->GetBinContent(1);
	double noiso_norm = h_noiso->GetBinContent(1);

	//h_iso->Scale(1./iso_norm);
	//h_noiso->Scale(1./noiso_norm);

	h_noiso->SetLineColor(kBlack);h_noiso->SetMarkerColor(kBlack);h_noiso->SetMarkerSize(1.2);h_noiso->SetLineWidth(2);h_noiso->SetMarkerStyle(20);
	h_iso->SetLineColor(kBlue+2);h_iso->SetMarkerColor(kBlue+2);h_iso->SetMarkerSize(1.2);h_iso->SetLineWidth(2);h_iso->SetMarkerStyle(4);	

	h_noiso->SetMinimum(0);h_noiso->SetMaximum(std::max(h_noiso->GetMaximum(),h_iso->GetMaximum())*1.3);
       h_noiso->GetYaxis()->SetLabelSize(0.070);
                  h_noiso->GetYaxis()->SetTitleSize(0.065);
                  h_noiso->GetYaxis()->SetTitleOffset(0.18);
                  h_noiso->GetXaxis()->SetTitleOffset(2.7);
                  h_noiso->GetXaxis()->SetLabelOffset(0.5);
                  h_noiso->GetYaxis()->SetTitleOffset(0.95);

        h_noiso->GetXaxis()->SetRangeUser(0,5);
	h_noiso->Draw();
	h_iso->Draw("SAME");
	
	c1->cd(); pad2->cd();

	  gStyle->SetPadTickX(1);
	  gStyle->SetPadTickY(1);
	  
	  TH1D *p1_new = (TH1D*)h_noiso->Clone("data_clone");
	  
	p1_new->Divide(h_iso);

	
	p1_new->SetMarkerStyle(8);
	  p1_new->SetMarkerSize(0.8);
	  p1_new->SetMarkerColor(kBlack);
	  p1_new->SetMinimum(0.0);
	  p1_new->SetMaximum(3.0);
	  //p1_new->GetXaxis()->SetTitle("to do");
	  p1_new->GetXaxis()->SetLabelFont(42);
	  p1_new->GetXaxis()->SetLabelSize(0.16);
	  p1_new->GetXaxis()->SetLabelOffset(0.05);
	  p1_new->GetXaxis()->SetTitleFont(42);
	  p1_new->GetXaxis()->SetTitleSize(0.14);
	  p1_new->GetXaxis()->SetTitleOffset(1.3);
	  p1_new->GetYaxis()->SetNdivisions(505);
	  p1_new->GetYaxis()->SetTitle("#font[42]{Noiso/Iso}");
	  p1_new->GetYaxis()->SetLabelFont(42);
	  p1_new->GetYaxis()->SetLabelSize(0.15);
	  p1_new->GetYaxis()->SetTitleFont(42);
	  p1_new->GetYaxis()->SetTitleSize(0.13);
	  p1_new->GetYaxis()->SetTitleOffset(0.44); 
	  p1_new->GetXaxis()->SetBinLabel(1,"0");
	  p1_new->GetXaxis()->SetBinLabel(2,"1");
	  p1_new->GetXaxis()->SetBinLabel(3,"2");
	  p1_new->GetXaxis()->SetBinLabel(4,"3");
	  p1_new->GetXaxis()->SetBinLabel(5,"4+");
	  p1_new->DrawCopy("eP");
	  
	  p1_new->Fit("pol0","Q");
	  			//std::cout << h_noiso->GetFunction("pol0")->GetParameter(0) << ", " << h_noiso->GetFunction("pol0")->GetParError(0) << std::endl;
	  		TString	fitVal = TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("pol0")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("pol0")->GetParError(0) );		
	  		
	  		std::cout << fitVal << std::endl; 
	  		pad2->SetLogy(0);

	  		//latex2.DrawLatex(.15,.98,title + ", Data/MC");
	  		
	  		 TLatex latex;
	  		 latex.SetNDC();
	  		 latex.SetTextColor(kBlack);
	  		 latex.SetTextSize(0.1);
	  		 latex.SetTextAlign(13);  //align at top
	  		 latex.DrawLatex(.25,.92,fitVal);
	c1->cd();
			 TLegend *leg = new TLegend(0.2,0.81,0.5,0.92);
			    leg->SetFillColor(0);
			    leg->SetFillStyle(0);
			    leg->SetBorderSize(0);
			    leg->SetTextSize(0.04);
			    	leg->AddEntry(h_noiso,"#font[42]{Non-isolated vertices}","l");
			    leg->AddEntry(h_iso,"#font[42]{Isolated vertices}","l");
			    leg->Draw();	  		 
	c1->Print(plotDir+"vertexRecoProbability_noRatio.pdf");
leg->Clear();
	for (TObject* keyAsObj : *(_file0->GetListOfKeys())){
    		auto key = dynamic_cast<TKey*>(keyAsObj);

  		pad1->cd(); 
  	    gStyle->SetPadTickX(2);
  	    gStyle->SetPadTickY(2);
    	TH1D *h_iso = (TH1D*) key->ReadObj();
		TString histName = TString(h_iso->GetName());
		if(!histName.Contains("_iso")) continue;
		Size_t histLength = histName.Length();
		TString noisoName( histName.Replace(histLength-3,3,"noiso") );
		TH1D *h_noiso = (TH1D*)_file0->Get(noisoName);
		std::cout << "Key name: " << key->GetName() << " Type: " << key->GetClassName() << ", entries data/mc: " << h_iso->GetEntries()  <<"/" <<h_noiso->GetEntries() <<  std::endl;


		if(h_iso->GetEntries() == 0 || h_noiso->GetEntries() == 0) continue;
		if(histName.Contains("abcd") ) continue; //this one is messed up right now
		
		pad1->SetLogy(0); h_noiso->SetMinimum(0);
		h_noiso->SetLineColor(kBlack);h_noiso->SetMarkerColor(kBlack);h_noiso->SetMarkerSize(1.2);h_noiso->SetLineWidth(2);h_noiso->SetMarkerStyle(20);
 		h_iso->SetLineColor(kBlue+2);h_iso->SetMarkerColor(kBlue+2);h_iso->SetMarkerSize(1.2);h_iso->SetLineWidth(2);h_iso->SetMarkerStyle(4);	
 		h_noiso->GetYaxis()->SetTitle("Number of MS vertices");
 		
		h_noiso->Scale(h_iso->Integral() / h_noiso->Integral() );
		
		if(histName.Contains("HT_")){h_noiso->GetXaxis()->SetTitle("Event H_{T} [GeV]"); h_noiso->GetYaxis()->SetTitle("Number of events");h_noiso->SetMinimum(0.8);pad1->SetLogy(1); h_noiso->GetXaxis()->SetRangeUser(000,1000); 	h_noiso->SetMinimum(0.1);}
		else if(histName.Contains("HTMiss_")){h_noiso->GetXaxis()->SetTitle("Event H_{T}^{miss} [GeV]"); h_noiso->GetYaxis()->SetTitle("Number of events");h_noiso->SetMinimum(0.8);pad1->SetLogy(1); h_noiso->GetXaxis()->SetRangeUser(000,500); 	h_noiso->SetMinimum(0.1);}
		else if(histName.Contains("Meff_")){h_noiso->GetXaxis()->SetTitle("Event M_{eff} [GeV]"); h_noiso->GetYaxis()->SetTitle("Number of events");h_noiso->SetMinimum(0.8);pad1->SetLogy(1); h_noiso->GetXaxis()->SetRangeUser(000,1000); 	h_noiso->SetMinimum(0.1);}
		else if(histName.Contains("nJets_")){h_noiso->SetMinimum(0);h_noiso->GetXaxis()->SetTitle("Number of jets"); h_noiso->GetYaxis()->SetTitle("Number of events");}
		else if(histName.Contains("Trig")){ h_noiso->GetXaxis()->SetTitle("#Delta R between RoI Cluster and MS Vertex"); h_noiso->SetMinimum(0.8);pad1->SetLogy(1); h_noiso->GetXaxis()->SetRangeUser(0,1); 	h_noiso->SetMinimum(0.1);}

		else if(histName.Contains("eta_")) h_noiso->GetXaxis()->SetTitle("MS vertex #eta");
		else if(histName.Contains("phi_")) h_noiso->GetXaxis()->SetTitle("MS vertex #phi");
		else if(histName.Contains("R_")) h_noiso->GetXaxis()->SetTitle("MS vertex L_{xy} [m]");
		else if(histName.Contains("z_")) h_noiso->GetXaxis()->SetTitle("MS vertex |L_{z}| [m]");
		else if(histName.Contains("nTrks")){ h_noiso->GetXaxis()->SetTitle("Number of MS Tracklets");	h_noiso->SetMinimum(0.1);pad1->SetLogy(1);	h_noiso->SetMinimum(0.8);}
		else if(histName.Contains("nMDT")){ h_noiso->GetXaxis()->SetTitle("Number of MDT hits");	h_noiso->SetMinimum(0.1);pad1->SetLogy(1); h_noiso->GetXaxis()->SetRangeUser(000,3000); 	h_noiso->SetMinimum(0.1);}
		else if(histName.Contains("nRPC")){ h_noiso->GetXaxis()->SetTitle("Number of RPC hits");	h_noiso->SetMinimum(0.1);pad1->SetLogy(1); h_noiso->GetXaxis()->SetRangeUser(000,3000); 	h_noiso->SetMinimum(0.1);}
		else if(histName.Contains("nTGC")){ h_noiso->GetXaxis()->SetTitle("Number of TGC hits");	h_noiso->SetMinimum(0.1);pad1->SetLogy(1); h_noiso->GetXaxis()->SetRangeUser(000,3000); 	h_noiso->SetMinimum(0.1);}
		

		double maxVal = h_iso->GetMaximum(); if(h_noiso->GetMaximum() > maxVal) maxVal = h_noiso->GetMaximum();
		h_noiso->SetMaximum(1.3*maxVal);
		h_noiso->SetTitle("");

		TString append = "";

		TString title = "";
		if(histName.Contains("full")) title = TString("|#eta| < 2.7") + append;
		
		  h_noiso->GetYaxis()->SetLabelSize(0.070);
		  h_noiso->GetYaxis()->SetTitleSize(0.065);
		  h_noiso->GetYaxis()->SetTitleOffset(0.18);
		  h_noiso->GetXaxis()->SetTitleOffset(2.7);
		  h_noiso->GetXaxis()->SetLabelOffset(0.5);
		  h_noiso->GetYaxis()->SetTitleOffset(0.95);
		  
		  
		    TH1 *h0_copy = (TH1D*)h_noiso->Clone("noiso_copy");
		    h_noiso->Draw("HISTO");
		    
		    h0_copy->SetLineColor(kBlack);
		    h0_copy->SetFillColor(kBlack);
		    h0_copy->SetFillStyle(3001);
		    h0_copy->SetMarkerSize(0);
		    h0_copy->Draw("E2 SAME");
		    
		    TH1 *h1_copy = (TH1D*)h_iso->Clone("iso_copy");
		    h1_copy->SetLineColor(kBlue);
		    h_iso->Draw("HISTO SAME");
		    
		    h1_copy->SetLineColor(kBlue);
		    h1_copy->SetFillColor(kBlue);
		    h1_copy->SetFillStyle(3001);
		    h1_copy->SetMarkerSize(0);
		    h1_copy->Draw("E2 SAME");
		
		 TLatex latex2;
		 latex2.SetNDC();
		 latex2.SetTextColor(kBlack);
		 latex2.SetTextSize(0.06);
		 latex2.SetTextAlign(31);  //align at bottom
		 latex2.DrawLatex(0.95,.94,title);
			 
		//c1->Print(plotDir+histName+".pdf");
		 TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
		   l.SetNDC();
		   l.SetTextFont(72);
		   l.SetTextSize(0.06);
		   l.SetTextColor(kBlack);
		   //l.DrawLatex(.72,.84,"ATLAS");
		     TLatex p; 
		     p.SetNDC();
		     p.SetTextSize(0.06);
		     p.SetTextFont(42);
		     p.SetTextColor(kBlack);
		    // p.DrawLatex(0.72+0.105,0.84,"Internal");

		     
		 c1->cd();
			
		 TLegend *leg = new TLegend(0.6,0.81,0.92,0.92);
		    leg->SetFillColor(0);
		    leg->SetFillColor(0);
		    leg->SetBorderSize(0);
		    leg->SetTextSize(0.04);
		    	leg->AddEntry(h0_copy,"#font[42]{Non-isolated vertices}","lpf");
		    leg->AddEntry(h1_copy,"#font[42]{Isolated vertices}","lpf");
		    leg->Draw();
		  pad2->cd();
		  gStyle->SetPadTickX(1);
		  gStyle->SetPadTickY(1);
		  
		  TH1D *p1_new = (TH1D*)h_noiso->Clone("data_clone");
		  
		p1_new->Divide(h_iso);

		
		p1_new->SetMarkerStyle(8);
		  p1_new->SetMarkerSize(0.8);
		  p1_new->SetMarkerColor(kBlack);
		  p1_new->SetMinimum(0.0);
		  p1_new->SetMaximum(5.0);
		  //p1_new->GetXaxis()->SetTitle("to do");
		  p1_new->GetXaxis()->SetLabelFont(42);
		  p1_new->GetXaxis()->SetLabelSize(0.16);
		  p1_new->GetXaxis()->SetLabelOffset(0.05);
		  p1_new->GetXaxis()->SetTitleFont(42);
		  p1_new->GetXaxis()->SetTitleSize(0.14);
		  p1_new->GetXaxis()->SetTitleOffset(1.3);
		  p1_new->GetYaxis()->SetNdivisions(505);
		  p1_new->GetYaxis()->SetTitle("#font[42]{Noiso/Iso}");
		  p1_new->GetYaxis()->SetLabelFont(42);
		  p1_new->GetYaxis()->SetLabelSize(0.15);
		  p1_new->GetYaxis()->SetTitleFont(42);
		  p1_new->GetYaxis()->SetTitleSize(0.13);
		  p1_new->GetYaxis()->SetTitleOffset(0.44); 
		  p1_new->DrawCopy("eP");
		  
		  TLine* line = new TLine();
		  line->DrawLine(p1_new->GetXaxis()->GetXmin(),1,p1_new->GetXaxis()->GetXmax(),1);
		
		TString fitVal = "";
		
		if (histName.Contains("Tracklet") && !histName.Contains("eta") && !histName.Contains("pT")){
		  TF1 *f1 = new TF1("fit1", "pol0", 0, 12);
		  p1_new->Fit("fit1","R");
		  p1_new->GetXaxis()->SetRangeUser(0,12);
		  fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
		  
		} else {
			p1_new->Fit("pol0","Q");
			//std::cout << h_noiso->GetFunction("pol0")->GetParameter(0) << ", " << h_noiso->GetFunction("pol0")->GetParError(0) << std::endl;
			fitVal = TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("pol0")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("pol0")->GetParError(0) );		
		}
		std::cout << fitVal << std::endl; 
		pad2->SetLogy(0);

		//latex2.DrawLatex(.15,.98,title + ", Data/MC");
		
		 TLatex latex;
		 latex.SetNDC();
		 latex.SetTextColor(kBlack);
		 latex.SetTextSize(0.1);
		 latex.SetTextAlign(13);  //align at top
		 latex.DrawLatex(.25,.92,fitVal);

		 

		 
		c1->Print(plotDir+"ratio_"+histName+".pdf");
		c1->cd();
		leg->Clear();
		leg->SetFillStyle(0);

	}


}

//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include "TMultiGraph.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLine.h"
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    int numberOfSlashes = 0; int tmpSlashNumber = 0;
    int indexOf2ndLastSlash = -1;
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            numberOfSlashes++;
        }
    }
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            tmpSlashNumber++;
        }
        if(tmpSlashNumber == (numberOfSlashes - 1)){
            indexOf2ndLastSlash = charN;
            break;
        }
    }
    //fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 29 - indexOf2ndLastSlash - 1);
    fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 22 - indexOf2ndLastSlash - 1);
    std::cout << "file name : " << fileName << std::endl;
    TString title = "";

    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nub#bar{b}";
        if(channel == "tatanu") channel = "#tau#tau#nu";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        //title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
	title = (TString) "#chi#rightarrow"+channel+", m_{#chi} = "+mchi+" GeV";
	//title = (TString) "m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{#Phi},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}
void makePlotWithLines(TFile *_sig[6],  TString type);
void makePlotWithLines(TFile *_sig[6],  TString type){
    bool add_ctau = false;    
    TString sigNames[6];
    int nSIG=4;
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<nSIG; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}
   
    Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};

    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};
    /*
    Int_t colorSig[16] = { kPink-3,kViolet-3,kBlue-3, kAzure+5, kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};
    if(nSIG < 4){
      colorSig[0] = kBlue - 3;
      colorSig[1] = kAzure + 5;
      colorSig[2] = kTeal - 6;
    }
    */
    Int_t colorSig[4] = {kViolet -3, kTeal - 6, kBlue -3, kRed};
    if(nSIG < 4){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
        colorSig[3] = kRed;
    }
    

    TString trignames[2];
    trignames[0] = "MSTrig_1B_Lxy";
    trignames[1] = "MSTrig_1E_Lz";
    
    std::cout << "running on : " << _sig[0]->GetName() << std::endl;

    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //vertex reco

    //trigger reco
    for(unsigned int i=0;i<2; i++){
        TGraphAsymmErrors* h_sig[30];
        TMultiGraph *mg = new TMultiGraph();

        //TLegend *legS = new TLegend(0.17,0.6,0.566,0.8);
	TLegend *legS = new TLegend(0.17,0.6,0.51,0.8);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);

        for(int j=0; j<nSIG; j++){
            TH1D *num = (TH1D*)_sig[j]->Get(trignames[i]);
            TH1D *den = (TH1D*)_sig[j]->Get(trignames[i]+"_denom");
            
            h_sig[j] = new TGraphAsymmErrors(num,den,"cl=0.683 b(1,1) mode");

            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]); h_sig[j]->SetLineColor(colorSig[j]); h_sig[j]->SetMarkerStyle(markerSig[j]);

            legS->AddEntry(h_sig[j],"#font[42]{"+sigNames[j]+"}","lp");
            mg->Add(h_sig[j]);

            if(j == nSIG-1){
                mg->Draw("AP");

                if(trignames[i].Contains("Lxy")){
                    mg->GetXaxis()->SetLimits(0,8.6);
                    if     (type == "bg")    mg->SetMaximum(0.65);
		    else if(type == "higgs") mg->SetMaximum(0.55);
		    else                     mg->SetMaximum(1);
                    mg->GetXaxis()->SetTitle("Long-lived particle  L_{xy} [m]");
                }
                else if(trignames[i].Contains("Lz")){
                    mg->GetXaxis()->SetLimits(0,15);
                    if     (type == "bg")    mg->SetMaximum(0.50);
		    else if(type == "higgs") mg->SetMaximum(0.35);
		    else                     mg->SetMaximum(1);
                    mg->GetXaxis()->SetTitle("Long-lived particle |L_{z}| [m]");
                }
                mg->GetYaxis()->SetTitle("Muon RoI Cluster trigger efficiency");

                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                y_val_label = 0.56;
                //latex.DrawLatex(.19,.91,"#font[72]{ATLAS}  Internal");
		latex.DrawLatex(.19,.91,"#font[72]{ATLAS}  Simulation");
		latex.DrawLatex(.19,.86,"Internal");
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.040);
                latex2.SetTextAlign(13);  //align at top
                if(trignames[i].Contains("Lxy")){
		  //latex2.DrawLatex(.19,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");}
		  latex2.DrawLatex(.19,y_val_label,"#font[42]{Barrel vertices}");}
                else {
		  //latex2.DrawLatex(.19,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");
		  latex2.DrawLatex(.19,y_val_label,"#font[42]{Endcap vertices}");
		}
		
                legS->Draw();
                TLatex detLabels;
                //detLabels.SetNDC();
                detLabels.SetTextColor(kBlack);
                detLabels.SetTextSize(0.03);
                detLabels.SetTextAlign(10);
                detLabels.SetTextAngle(90);
                if(i==0){
                    TLine *rpcs[6];
		    TLine *hcalEnd;
		    if (type == "higgs"){
		      detLabels.DrawLatex(3.85,0.45,"#font[42]{HCal end}");
		      detLabels.DrawLatex(7.75,0.45,"#font[42]{RPC1 S}");
		      detLabels.DrawLatex(6.72,0.45,"#font[42]{RPC1 L}");
		      detLabels.DrawLatex(8.27,0.45,"#font[42]{RPC2 S}");
		      detLabels.DrawLatex(7.36,0.45,"#font[42]{RPC2 L}");
		      rpcs[0]= new TLine(7.758,0,7.758,0.55);
		      rpcs[1]= new TLine(6.729,0,6.729,0.55);
		      rpcs[2]= new TLine(8.282,0,8.282,0.55);
		      rpcs[3]= new TLine(7.372,0,7.372,0.55);
		      rpcs[4]= new TLine(10.158,0,10.158,0.55);
		      rpcs[5]= new TLine(9.733,0,9.733,0.55);
		      hcalEnd = new TLine(3.865,0,3.865,0.55);
		      hcalEnd->SetLineColor(kGray);
		      hcalEnd->SetLineStyle(9);
		      hcalEnd->SetLineWidth(2);
		      hcalEnd->Draw();
		    }else if (type == "bg"){
		      detLabels.DrawLatex(3.85,0.50,"#font[42]{HCal end}");
		      detLabels.DrawLatex(7.75,0.50,"#font[42]{RPC1 S}");
		      detLabels.DrawLatex(6.72,0.50,"#font[42]{RPC1 L}");
		      detLabels.DrawLatex(8.27,0.50,"#font[42]{RPC2 S}");
		      detLabels.DrawLatex(7.36,0.50,"#font[42]{RPC2 L}");
		      rpcs[0]= new TLine(7.758,0,7.758,0.65);
		      rpcs[1]= new TLine(6.729,0,6.729,0.65);
		      rpcs[2]= new TLine(8.282,0,8.282,0.65);
		      rpcs[3]= new TLine(7.372,0,7.372,0.65);
		      rpcs[4]= new TLine(10.158,0,10.158,0.65);
		      rpcs[5]= new TLine(9.733,0,9.733,0.65);
		      hcalEnd = new TLine(3.865,0,3.865,0.65);
		      hcalEnd->SetLineColor(kGray);
		      hcalEnd->SetLineStyle(9);
		      hcalEnd->SetLineWidth(2);
		      hcalEnd->Draw();
		    }else{
		      //detLabels.DrawLatex(3.85,0.80,"#font[42]{HCal end}");
		      detLabels.DrawLatex(3.85,0.60,"#font[42]{HCal end}");
		      //detLabels.DrawLatex(3.85,0.10,"#font[42]{HCal end}");  // ** For Stealth SUSY
		      detLabels.DrawLatex(7.75,0.75,"#font[42]{RPC1 S}");
		      detLabels.DrawLatex(6.72,0.75,"#font[42]{RPC1 L}");
		      detLabels.DrawLatex(8.27,0.75,"#font[42]{RPC2 S}");
		      detLabels.DrawLatex(7.36,0.75,"#font[42]{RPC2 L}");
		      rpcs[0]= new TLine(7.758,0,7.758,1);
		      rpcs[1]= new TLine(6.729,0,6.729,1);
		      rpcs[2]= new TLine(8.282,0,8.282,1);
		      rpcs[3]= new TLine(7.372,0,7.372,1);
		      rpcs[4]= new TLine(10.158,0,10.158,1);
		      rpcs[5]= new TLine(9.733,0,9.733,1);
		      hcalEnd = new TLine(3.865,0,3.865,1);
		      hcalEnd->SetLineColor(kGray);
		      hcalEnd->SetLineStyle(9);
		      hcalEnd->SetLineWidth(2);
		      hcalEnd->Draw();
		    }

                    for(int line=0;line<4;line++){
                        rpcs[line]->SetLineColor(kGray+(line+2)/2);
                        rpcs[line]->SetLineStyle(2 + 5*(line%2));
                        rpcs[line]->SetLineWidth(2);

                        rpcs[line]->Draw();
                    }
                }else if(i==1){
                    TLine *rpcs[5];
		    TLine *hcalEnd;
		    if (type == "higgs"){
		      detLabels.DrawLatex(5.98,0.27,"#font[42]{HCal end}");
		      detLabels.DrawLatex(13.28,0.27,"#font[42]{TGC1}");
		      rpcs[0]= new TLine(13.3,0,13.3,0.35);
		      rpcs[1]= new TLine(14.04,0,14.04,0.35);
		      rpcs[2]= new TLine(14.34,0,14.34,0.35);
		      rpcs[3]= new TLine(14.54,0,14.54,0.35);
		      rpcs[4]= new TLine(14.81,0,14.81,0.35);
		      hcalEnd = new TLine(6,0,6,0.35);
		      hcalEnd->SetLineColor(kGray);
		      hcalEnd->SetLineStyle(9);
		      hcalEnd->SetLineWidth(2);
		      hcalEnd->Draw();
		    }else if (type == "bg"){
		      detLabels.DrawLatex(5.98,0.40,"#font[42]{HCal end}");
		      detLabels.DrawLatex(13.28,0.40,"#font[42]{TGC1}");
		      rpcs[0]= new TLine(13.3,0,13.3,0.50);
		      rpcs[1]= new TLine(14.04,0,14.04,0.50);
		      rpcs[2]= new TLine(14.34,0,14.34,0.50);
		      rpcs[3]= new TLine(14.54,0,14.54,0.50);
		      rpcs[4]= new TLine(14.81,0,14.81,0.50);
		      hcalEnd = new TLine(6,0,6,0.50);
		      hcalEnd->SetLineColor(kGray);
		      hcalEnd->SetLineStyle(9);
		      hcalEnd->SetLineWidth(2);
		      hcalEnd->Draw();
		    }else{
		      detLabels.DrawLatex(5.98,0.75,"#font[42]{HCal end}");
		      detLabels.DrawLatex(13.28,0.75,"#font[42]{TGC1}");
		      rpcs[0]= new TLine(13.3,0,13.3,1);
		      rpcs[1]= new TLine(14.04,0,14.04,1);
		      rpcs[2]= new TLine(14.34,0,14.34,1);
		      rpcs[3]= new TLine(14.54,0,14.54,1);
		      rpcs[4]= new TLine(14.81,0,14.81,1);
		      hcalEnd = new TLine(6,0,6,1);
		      hcalEnd->SetLineColor(kGray);
		      hcalEnd->SetLineStyle(9);
		      hcalEnd->SetLineWidth(2);
		      hcalEnd->Draw();
		    }

                    for(int line=0;line<1;line++){
                        rpcs[line]->SetLineColor(kGray+(line+2)/2);
                        rpcs[line]->SetLineStyle(2);
                        rpcs[line]->SetLineWidth(2);
                        if(line == 2 || line == 4) rpcs[line]->SetLineStyle(7);
                        rpcs[line]->Draw();
                    }
                }
                gPad->RedrawAxis();
                c->Print("../OutputPlots/notePlots/"+trignames[i]+"_"+type+".pdf");

                legS->Clear(); 
                mg->Clear();
            }

        }
    }
}
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type);
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type){
    bool add_ctau = false;
    TString sigNames[6];
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<nSIG; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}

    Int_t colorSig[16] = { kPink-3,kViolet-3,kBlue-3, kAzure+5, kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};

    if(nSIG < 4){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
    }
    Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};

    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};


    //names[2] = "MSTrig_1B_Lxy_eff";
    //names[3] = "MSTrig_1E_Lz_eff";

    TString trignames[4];
    trignames[0] = "MSTrig_1B_Lxy";
    //trignames[1] = "MSTrig_BID_Lxy_eff";
    trignames[1] = "MSTrig_1E_Lz";
    //trignames[3] = "MSTrig_EID_Lz_eff";



    //SetAtlasStyle();
    //gStyle->SetPalette(1);

    std::cout << "running on : " << _sig[0]->GetName() << std::endl;

    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //c->SetLogy();

    //trigger reco
    for(unsigned int i=0;i<2; i++){
        TGraphAsymmErrors* h_sig[30];
        TMultiGraph *mg = new TMultiGraph();

        //TLegend *legS = new TLegend(0.18,0.7,0.566,0.93);
	TLegend *legS = new TLegend(0.18,0.7,0.51,0.93);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);

        for(int j=0; j<nSIG; j++){
            TH1D *num = (TH1D*)_sig[j]->Get(trignames[i]);
            TH1D *den = (TH1D*)_sig[j]->Get(trignames[i]+"_denom");
	    /*
            if(type == "higgs"){
	      num->Rebin(2); den->Rebin(2);
            }
	    */
	    if( (type == "bg" || type == "higgs") && trignames[i].Contains("1E")){
	      num->Rebin(2); den->Rebin(2);
            }
            h_sig[j] = new TGraphAsymmErrors(num,den,"cl=0.683 b(1,1) mode");
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]); h_sig[j]->SetLineColor(colorSig[j]); h_sig[j]->SetMarkerStyle(markerSig[j]);

            legS->AddEntry(h_sig[j],"#font[42]{"+sigNames[j]+"}","lp");
            mg->Add(h_sig[j]);

            if(j == nSIG-1){
                mg->Draw("AP");

                if(trignames[i].Contains("Lxy")){
                    mg->GetXaxis()->SetLimits(0,8);
                    mg->SetMaximum(1);
                    if(type == "bg" || type == "higgs"){mg->SetMaximum(0.75);}
                    mg->GetXaxis()->SetTitle("Lxy [m]");
                }
                else if(trignames[i].Contains("Lz")){
                    mg->GetXaxis()->SetLimits(0,15);
                    if(type == "bg" || type == "higgs") mg->SetMaximum(0.55);
                    else                                mg->SetMaximum(1);  
                    
                    mg->GetXaxis()->SetTitle("|Lz| [m]");
                }
                mg->GetYaxis()->SetTitle("Muon RoI Cluster trigger efficiency");

                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                y_val_label = 0.86;
                latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(trignames[i].Contains("Lxy")){latex2.DrawLatex(.70,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");}
                else {latex2.DrawLatex(.65,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");}

                legS->Draw();
                gPad->RedrawAxis();
                c->Print("../OutputPlots/notePlots/"+trignames[i]+"_"+type+".pdf");

                legS->Clear();
                mg->Clear();
            }

        }
    }
}


int makeMSTrigEfficiencies_Note(){


    //gROOT->LoadMacro("AtlasUtils.C");
    //gROOT->LoadMacro("AtlasLabels.C");
    //gROOT->LoadMacro("AtlasStyle.C");

    bool add_ctau = false;

    //_bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
    //_bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
    TFile *_sig[6];
    //TString locn = "/Users/hrussell/Work/Run2Plots/signalMC/";
    TString locn = "../OutputPlots/signalMC/";
    /*
    _sig[0] = new TFile(locn+"mg250/outputMSTrigEff.root");
    _sig[1] = new TFile(locn+"mg500/outputMSTrigEff.root");
    _sig[2] = new TFile(locn+"mg800/outputMSTrigEff.root");
    _sig[3] = new TFile(locn+"mg1200/outputMSTrigEff.root");
    _sig[4] = new TFile(locn+"mg1500/outputMSTrigEff.root");
    _sig[5] = new TFile(locn+"mg2000/outputMSTrigEff.root");
    makePlotWithLines(_sig, "stealth");
    //makeCombinedPlot(_sig, 3, "stealthLow", forThesis);
    for(int i=0;i<6;i++){delete _sig[i];}
    */
    /*
    _sig[0] = new TFile(locn+"mg1200/outputMSEff.root");
    _sig[1] = new TFile(locn+"mg1500/outputMSEff.root");
    _sig[2] = new TFile(locn+"mg2000/outputMSEff.root");
    makeCombinedPlot(_sig, 3, "stealthHigh", forThesis);
    for(int i=0;i<3;i++){delete _sig[i];}
    */
    /*
    _sig[0] = new TFile(locn+"mg250/outputMSTrigEff_scaled.root");
    _sig[1] = new TFile(locn+"mg800/outputMSTrigEff_scaled.root");
    _sig[2] = new TFile(locn+"mg2000/outputMSTrigEff_scaled.root");
    makePlotWithLines(_sig, "detectorDetails");
    for(int i=0;i<3;i++){delete _sig[i];}
    */
    /*
    _sig[0] = new TFile(locn+"mg250/outputMSTrigEff_scaled.root");
    _sig[1] = new TFile(locn+"mg500/outputMSTrigEff_scaled.root");
    _sig[2] = new TFile(locn+"mg800/outputMSTrigEff_scaled.root");
    _sig[3] = new TFile(locn+"mg1200/outputMSTrigEff_scaled.root");
    _sig[4] = new TFile(locn+"mg1500/outputMSTrigEff_scaled.root");
    _sig[5] = new TFile(locn+"mg2000/outputMSTrigEff_scaled.root");    
    makeCombinedPlot(_sig, 6, "stealthMixed");
    for(int i=0;i<6;i++){delete _sig[i];}
    */
    /*
    _sig[0] = new TFile(locn+"mH125mS5lt5/outputMSTrigEff.root");
    _sig[1] = new TFile(locn+"mH125mS8lt5/outputMSTrigEff.root");
    _sig[2] = new TFile(locn+"mH125mS15lt5/outputMSTrigEff.root");
    _sig[3] = new TFile(locn+"mH125mS25lt5/outputMSTrigEff.root");
    _sig[4] = new TFile(locn+"mH125mS40lt5/outputMSTrigEff.root");
    makeCombinedPlot(_sig, 5, "higgs");
    for(int i=0;i<5;i++){delete _sig[i];}
    */
    /*    
    _sig[0] = new TFile(locn+"mH100mS8lt5/outputMSTrigEff.root");
    _sig[1] = new TFile(locn+"mH200mS25lt5/outputMSTrigEff.root");
    _sig[2] = new TFile(locn+"mH400mS50lt5/outputMSTrigEff.root");
    _sig[3] = new TFile(locn+"mH600mS50lt5/outputMSTrigEff.root");
    _sig[4] = new TFile(locn+"mH1000mS150lt5/outputMSTrigEff.root");
    _sig[5] = new TFile(locn+"mH1000mS400lt5/outputMSTrigEff.root");
    makePlotWithLines(_sig, "scalar");
    for(int i=0;i<6;i++){delete _sig[i];}
    */
    /*    
    _sig[0] = new TFile(locn+"HChiChi_nubb_mH125mChi10/outputMSTrigEff.root");
    _sig[1] = new TFile(locn+"HChiChi_nubb_mH125mChi30/outputMSTrigEff.root");
    _sig[2] = new TFile(locn+"HChiChi_nubb_mH125mChi50/outputMSTrigEff.root");
    _sig[3] = new TFile(locn+"HChiChi_cbs_mH125mChi10/outputMSTrigEff.root");
    _sig[4] = new TFile(locn+"HChiChi_cbs_mH125mChi30/outputMSTrigEff.root");
    _sig[5] = new TFile(locn+"HChiChi_cbs_mH125mChi50/outputMSTrigEff.root");
    makeCombinedPlot(_sig, 6, "bg");
    for(int i=0;i<6;i++){delete _sig[i];}
    */
    /*        
    _sig[0] = new TFile(locn+"HChiChi_nubb_mH125mChi10/outputMSTrigEff.root");
    _sig[1] = new TFile(locn+"HChiChi_nubb_mH125mChi50/outputMSTrigEff.root");
    _sig[2] = new TFile(locn+"HChiChi_nubb_mH125mChi100/outputMSTrigEff.root");
    _sig[3] = new TFile(locn+"HChiChi_cbs_mH125mChi10/outputMSTrigEff.root");
    _sig[4] = new TFile(locn+"HChiChi_cbs_mH125mChi50/outputMSTrigEff.root");
    _sig[5] = new TFile(locn+"HChiChi_cbs_mH125mChi100/outputMSTrigEff.root");
    makePlotWithLines(_sig,"bg");
    for(int i=0;i<6;i++){delete _sig[i];}
    */
        
    _sig[0] = new TFile(locn+"mg500/outputMSTrigEff.root");
    _sig[1] = new TFile(locn+"mg1500/outputMSTrigEff.root");
    _sig[2] = new TFile(locn+"HChiChi_cbs_mH125mChi10/outputMSTrigEff.root");
    _sig[3] = new TFile(locn+"HChiChi_cbs_mH125mChi100/outputMSTrigEff.root");
    //_sig[4] = new TFile(locn+"mH125mS5lt5/outputMSTrigEff.root");
    //_sig[5] = new TFile(locn+"mH125mS40lt5/outputMSTrigEff.root");
    makePlotWithLines(_sig, "stealthMixed");
    for(int i=0;i<4;i++){delete _sig[i];}
    
    /*
    _sig[0] = new TFile(locn+"mH125mS5lt5/outputMSTrigEff.root");
    _sig[1] = new TFile(locn+"mH125mS8lt5/outputMSTrigEff.root");
    _sig[2] = new TFile(locn+"mH125mS15lt5/outputMSTrigEff.root");
    _sig[3] = new TFile(locn+"mH125mS25lt5/outputMSTrigEff.root");
    _sig[4] = new TFile(locn+"mH125mS40lt5/outputMSTrigEff.root");
    makePlotWithLines(_sig, "higgs");
    for(int i=0;i<5;i++){delete _sig[i];}
    */

    return 314;
    
}

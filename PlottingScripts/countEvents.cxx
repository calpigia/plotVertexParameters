#include "../PlottingTools/PlottingPackage/InputFiles.h"

void countEvents(){
    std::vector<TString> benchmark = {"mH125mS5lt"};//{"mH125mS5lt","mH125mS8lt","mH125mS15lt","mH125mS25lt","mH125mS40lt","mH125mS55lt","mH200mS8lt", "mH200mS25lt","mH200mS50lt", "mH400mS50lt", "mH400mS100lt", "mH600mS50lt", "mH600mS150lt", "mH1000mS50lt", "mH1000mS150lt", "mH1000mS400lt"};
    std::vector<double> nev5, nev9;
    for(int i=0; i<benchmark.size();i++){
	TChain *ch5 = new TChain("recoTree");
	InputFiles::AddFilesToChain(benchmark.at(i)+"5",ch5);
	TChain *ch9 = new TChain("recoTree");
	InputFiles::AddFilesToChain(benchmark.at(i)+"9",ch9);
	TH1D* h5 = new TH1D("h5","h5",1,0.5,1.5);
	ch5->Draw("1.0 >> h5","pileupEventWeight");
	
	TH1D* h9 = new TH1D("h9","h9",1,0.5,1.5);
	ch9->Draw("1.0 >> h9","pileupEventWeight");
	nev5.push_back(h5->Integral());
	nev9.push_back(h9->Integral());
	delete h5; delete h9; delete ch5; delete ch9;
    }
    std::cout << "std::vector<double> nEvents_5m = {";
    for(int i=0;i<nev5.size(); i++){
	std::cout << nev5.at(i) << ", ";
    }
    std::cout << "}" << std::endl;
    std::cout << "std::vector<double> nEvents_9m = {";
    for(int i=0;i<nev9.size(); i++){
	std::cout << nev9.at(i) << ", ";
    }
    std::cout << "}" << std::endl;
        std::vector<TString> samples = {"mg250", "mg500", "mg800" ,"mg1200", "mg1500", "mg2000"};
   std::vector<double> nevs;
   for(int i=0; i<samples.size();i++){
        TChain *ch5 = new TChain("recoTree");
        InputFiles::AddFilesToChain(samples.at(i),ch5);
        TH1D* h5 = new TH1D("h5","h5",1,0.5,1.5);
        ch5->Draw("1.0 >> h5","pileupEventWeight");
        nevs.push_back(h5->Integral());
        delete h5; delete ch5;
    }
    std::cout << "std::vector<double> nEvents_Stealth = {";
    for(int i=0;i<nevs.size(); i++){
        std::cout << nevs.at(i) << ", ";
    }
    std::cout << "}" << std::endl;

}

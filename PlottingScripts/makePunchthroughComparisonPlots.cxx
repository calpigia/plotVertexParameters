void plot(TChain *dCh, TChain *jzCh, TString pS, TString criteria, vector<double> axisVars,int nBins, TString xLab, TString yLab, TString outName, TString plotDir);
void plot(TChain *dCh, TChain *jzCh, TString pS, TString criteria, vector<double> axisVars,int nBins, TString xLab, TString yLab, TString outName, TString plotDir){

    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    SetAtlasStyle();

    //c1->SetRightMargin(0.05);
    //c1->SetTopMargin(0.07);
    //c1->SetLeftMargin(0.15);
    //c1->SetBottomMargin(0.15);
    TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
    pad1->SetTopMargin(0.075);
    pad1->SetBottomMargin(0.04);
    pad1->SetLeftMargin(0.14);
    pad1->SetLogy(0);
    pad1->Draw();

    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.40);
    pad2->SetLeftMargin(0.14);
    pad2->Draw();
    pad1->cd(); 
    gStyle->SetPadTickX(2);
    gStyle->SetPadTickY(2);
    
    TH1D *h_MC = new TH1D("mc","mc",nBins, axisVars[0], axisVars[1]);
    TH1D *h_Data = new TH1D("data","data",nBins, axisVars[0], axisVars[1]);

    dCh->Draw(pS+">> data", "1./1608.*("+criteria+")");
    jzCh->Draw(pS+">> mc", "newEventWeight./213990157.*("+criteria+")");
    
    h_Data->SetLineColor(kBlack);h_Data->SetMarkerColor(kBlack);h_Data->SetMarkerSize(1.2);h_Data->SetLineWidth(2);h_Data->SetMarkerStyle(20);
    h_MC->SetLineColor(kBlue+2);h_MC->SetMarkerColor(kBlue+2);h_MC->SetMarkerSize(1.2);h_MC->SetLineWidth(2);h_MC->SetMarkerStyle(4);   

    h_Data->GetYaxis()->SetTitle("Fraction of events");

    double maxVal = h_MC->GetMaximum(); if(h_Data->GetMaximum() > maxVal) maxVal = h_Data->GetMaximum();

    h_Data->SetMaximum(1.3*maxVal);
    h_Data->SetTitle("");
    TString title = "";

    h_Data->GetYaxis()->SetLabelSize(0.070);
    h_Data->GetYaxis()->SetTitleSize(0.065);
    h_Data->GetYaxis()->SetTitleOffset(0.18);
    h_Data->GetXaxis()->SetTitleOffset(2.7);
    h_Data->GetXaxis()->SetLabelOffset(0.5);
    h_Data->GetYaxis()->SetTitleOffset(0.95);

    h_Data->Draw();
    h_MC->Draw("SAME");

    TLegend *leg = new TLegend(0.65,0.75,0.85,0.9);
    leg->SetFillColor(0);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.05);
    leg->AddEntry(h_MC,"Pythia multi-jet MC","lp");
    leg->AddEntry(h_Data,"run 304008","lp");
    leg->Draw();

    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.05);
    latex2.SetTextAlign(33);
    latex2.DrawLatex(0.9,0.75,"Punch-through jet events");
    
    TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
    l.SetNDC();
    l.SetTextFont(72);
    l.SetTextSize(0.06);
    l.SetTextColor(kBlack);
    l.DrawLatex(.4,.84,"ATLAS");
    TLatex p; 
    p.SetNDC();
    p.SetTextSize(0.06);
    p.SetTextFont(42);
    p.SetTextColor(kBlack);
    p.DrawLatex(0.4+0.105,0.84,"Internal");

    c1->cd();

    pad2->cd();
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);

    TH1D *p1_new = (TH1D*)h_Data->Clone("data_clone");

    p1_new->Divide(h_MC);

    p1_new->SetMarkerStyle(8);
    p1_new->SetMarkerSize(0.8);
    p1_new->SetMarkerColor(kBlack);
    p1_new->SetMinimum(0.0);
    p1_new->SetMaximum(2.0);
    //p1_new->GetXaxis()->SetTitle("to do");
    p1_new->GetXaxis()->SetLabelFont(42);
    p1_new->GetXaxis()->SetLabelSize(0.16);
    p1_new->GetXaxis()->SetLabelOffset(0.05);
    p1_new->GetXaxis()->SetTitleFont(42);
    p1_new->GetXaxis()->SetTitleSize(0.14);
    p1_new->GetXaxis()->SetTitleOffset(1.3);
    p1_new->GetYaxis()->SetNdivisions(505);
    p1_new->GetYaxis()->SetTitle("#font[42]{Data/MC}");
    p1_new->GetYaxis()->SetLabelFont(42);
    p1_new->GetYaxis()->SetLabelSize(0.15);
    p1_new->GetYaxis()->SetTitleFont(42);
    p1_new->GetYaxis()->SetTitleSize(0.13);
    p1_new->GetYaxis()->SetTitleOffset(0.44); 
    p1_new->DrawCopy("eP");

    TLine* line = new TLine();
    line->DrawLine(p1_new->GetXaxis()->GetXmin(),1,p1_new->GetXaxis()->GetXmax(),1);

    TString fitVal = "";
    TString fitVal2 = "";
    double minFit =0;// (histName.Contains("eta1")) ? 1500 : 1200; 

    p1_new->Fit("pol0","Q");
    fitVal = TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("pol0")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("pol0")->GetParError(0) );      

    std::cout << fitVal << std::endl; 
    pad2->SetLogy(0);

    //latex2.DrawLatex(.15,.98,title + ", Data/MC");

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.1);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatex(.25,.92, fitVal);
    //latex.DrawLatex(.25,.82, fitVal2);

    c1->Print(plotDir+"ratio_"+outName+".pdf");

}
void makePunchthroughComparisonPlots(){
    //0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

    TChain *dCh = new TChain("recoTree"); dCh->Add("/LLPData/Outputs/MSVertex_Rerun_JZXW/vertexRerun_data_304008.root");
    TChain *jzCh = new TChain("recoTree");jzCh->Add("/LLPData/Outputs/MSVertex_Rerun_JZXW/testOutput_JZ*W.root");

    TString plotDir = "../OutputPlots/PunchthroughComparison/";
    
    std::vector<TString> vars = {"mdt_eta"}; std::vector<TString> cuts = {"mdt_adc > 50 && mdt_status != 1"}; 
    std::vector<int> nbins = {120}; std::vector<std::vector<double>> axes = {{-3.0,3.0}};

    for(int i=0;i<vars.size();i++){
        plot(dCh, jzCh, vars.at(i), cuts.at(i), axes.at(i), nbins.at(i),"MDT hit #eta", "<Hits per event>","mdt_eta_perEvent", plotDir );
        //plot(dCh, jzCh, vars.at(i), cuts.at(i), {-1.0,1.0}, 100 ,"MDT hit #eta", "<Hits per event>","mdt_eta_perEvent", plotDir );

    }
    
}

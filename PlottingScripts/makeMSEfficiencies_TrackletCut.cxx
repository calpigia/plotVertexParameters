//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include "TMultiGraph.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    int numberOfSlashes = 0; int tmpSlashNumber = 0;
    int indexOf2ndLastSlash = -1;
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            numberOfSlashes++;
        }
    }
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            tmpSlashNumber++;
        }
        if(tmpSlashNumber == (numberOfSlashes - 1)){
            indexOf2ndLastSlash = charN;
            break;
        }
    }
    if(fileName.Contains("unscaled")) fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 29 - indexOf2ndLastSlash - 1);
    else fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 20 - indexOf2ndLastSlash - 1);
    std::cout << "file name : " << fileName << std::endl;
	TString title = "";

	if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
		return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
		TString lt = "";
		TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
		TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
		if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
		if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
		else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
		return title;
	} else if(fileName.Contains("mg")){
		TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
		title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
	}
	return title;
}
void makeCombinedPlot(TFile *_sig[5], int nSIG, TString type, bool forThesis);
void makeCombinedPlot(TFile *_sig[5], int nSIG, TString type, bool forThesis = false){
bool add_ctau = false;    
    TString sigNames[6];
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<nSIG; i++){ 
        sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);
        if(i%2 == 1){
            sigNames[i] = sigNames[i] + ", scaled";
        }
        
    }
    
    Int_t colorSig[16] = {kBlue-3,kBlue-3,kAzure+5, kAzure+5, kTeal-6,kTeal-6, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};
    
    if(nSIG < 4){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
    }
    Int_t markerSig[16] = {20,24,22,26,21,25,28,30,26,27,24,25,26,27,24,25};
    
    TString names[12];
    names[0] = "MSVx_Barrel_Lxy_eff";
    names[1] = "MSVx_Endcap_Lz_eff";

    
    //names[2] = "MSTrig_1B_Lxy_eff";
    //names[3] = "MSTrig_1E_Lz_eff";
    
    names[2] = "MSVx_Barrel_pT_A_eff";
    names[3] = "MSVx_Barrel_pT_B_eff";
    names[4] = "MSVx_Endcap_pT_A_eff";
    names[5] = "MSVx_Endcap_pT_B_eff";
    
    
    //SetAtlasStyle();
    //gStyle->SetPalette(1);
    
    std::cout << "running on : " << _sig[0]->GetName() << std::endl;
    
    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //c->SetLogy();
    
    //vertex reco
    for(unsigned int i=0;i<2; i++){
        
        std::cout << "plot: " << names[i] << std::endl;
        
        TH1D* h_sig[30];
        
        TLegend *legS = new TLegend(0.18,0.78,0.566,0.93);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);
        
        for(int j=0; j<nSIG; j++){
            h_sig[j] = (TH1D*) _sig[j]->Get(names[i]);
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]);
            h_sig[j]->SetLineColor(colorSig[j]);
            h_sig[j]->SetMarkerStyle(markerSig[j]);
            
            if(names[i].Contains("Lxy")){
                h_sig[j]->GetXaxis()->SetTitle("Lxy [m]");
                h_sig[j]->GetYaxis()->SetRangeUser(0,0.6);
                h_sig[j]->GetXaxis()->SetRangeUser(0,8);
            }
            else if(names[i].Contains("Lz")){ h_sig[j]->GetXaxis()->SetTitle("|Lz| [m]");
                h_sig[j]->GetYaxis()->SetRangeUser(0,1);}
            else if(names[i].Contains("pT")){
                h_sig[j]->GetYaxis()->SetRangeUser(0,1);
                h_sig[j]->GetXaxis()->SetRangeUser(0,1200);
                h_sig[j]->GetXaxis()->SetTitle("p_{T} [GeV]");
            }
            h_sig[j]->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
            
            legS->AddEntry(h_sig[j],sigNames[j],"lp");

            if(j == 0){ h_sig[j]->Draw("");}
            else h_sig[j]->Draw("SAME");
            
            if(j == nSIG-1){
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                if(!forThesis){
                    y_val_label = 0.86;
                    latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");
                }
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("Lxy")){latex2.DrawLatex(.70,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");}
                else if(names[i].Contains("Lz") ){latex2.DrawLatex(.65,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");}
                else if(names[i].Contains("Barrel_pT_A")){latex2.DrawLatex(.70,y_val_label,"#splitline{#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}}{3 < L_{xy} < 3.8 m}");}
                else if(names[i].Contains("Barrel_pT_B")){latex2.DrawLatex(.70,y_val_label,"#splitline{#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}}{3.8 < L_{xy} < 8 m}");}
                else if(names[i].Contains("Endcap_pT_A") ){latex2.DrawLatex(.65,y_val_label,"#splitline{#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}}{5 < L_{z} < 6 m}");}
                else if(names[i].Contains("Endcap_pT_B") ){latex2.DrawLatex(.65,y_val_label,"#splitline{#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}}{6 < L_{z} < 15 m}");}
                
                legS->Draw();
                gPad->RedrawAxis();
                
                c->Print("../OutputPlots/thesisPlots/"+names[i]+"_"+type+"_sfEffects.pdf");
                legS->Clear();
            }
            
        }
    }
}


int makeMSEfficiencies_TrackletCut(bool forThesis = false){


	//gROOT->LoadMacro("AtlasUtils.C");
	//gROOT->LoadMacro("AtlasLabels.C");
	//gROOT->LoadMacro("AtlasStyle.C");

	bool add_ctau = false;
    
	//_bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
	//_bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
	TFile *_sig[4];
    TString locn = "../OutputPlots/signalMC/";
    
    _sig[1] = new TFile(locn+"mg250/outputMSVxEff.root");
    _sig[0] = new TFile(locn+"mg250/outputMSVxEff_unscaled.root");
    //_sig[3] = new TFile(locn+"mg800/outputMSVxEff.root");
    //_sig[2] = new TFile(locn+"mg800/outputMSVxEff_unscaled.root");
    _sig[3] = new TFile(locn+"mg2000/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"mg2000/outputMSVxEff_unscaled.root");
    
    makeCombinedPlot(_sig, 4, "stealth", forThesis);
    /*
    makeCombinedPlot(_sig, 3, "stealthLow", forThesis);
    _sig[0] = new TFile(locn+"mg1200/outputMSEff.root");
    _sig[1] = new TFile(locn+"mg1500/outputMSEff.root");
    _sig[2] = new TFile(locn+"mg2000/outputMSEff.root");
    makeCombinedPlot(_sig, 3, "stealthHigh", forThesis);

    
    if(!forThesis){
    _sig[0] = new TFile("mH125mS8lt5/outputMSEff.root");
    _sig[1] = new TFile("mH125mS15lt5/outputMSEff.root");
    _sig[2] = new TFile("mH125mS25lt5/outputMSEff.root");
    _sig[3] = new TFile("mH125mS40lt5/outputMSEff.root");
    _sig[4] = new TFile("mH125mS55lt5/outputMSEff.root");
    
    makeCombinedPlot(_sig, 5, "higgs");
    
    _sig[0] = new TFile("mH100mS8lt5/outputMSEff.root");
    _sig[1] = new TFile("mH200mS25lt5/outputMSEff.root");
    _sig[2] = new TFile("mH400mS50lt5/outputMSEff.root");
    _sig[3] = new TFile("mH600mS50lt5/outputMSEff.root");
    _sig[4] = new TFile("mH600mS150lt5/outputMSEff.root");
    
    makeCombinedPlot(_sig, 5, "scalar");
    
    _sig[0] = new TFile("HChiChi_cbs_mH125mChi100/outputMSEff.root");
    _sig[1] = new TFile("HChiChi_lcb_mH125mChi30/outputMSEff.root");
    _sig[2] = new TFile("HChiChi_nubb_mH125mChi30/outputMSEff.root");
    _sig[3] = new TFile("WChiChi_ltb_mChi1000/outputMSEff.root");
    _sig[4] = new TFile("WChiChi_tbs_mChi1500/outputMSEff.root");
    
    makeCombinedPlot(_sig, 5, "bg");
    }*/
	return 314;

}

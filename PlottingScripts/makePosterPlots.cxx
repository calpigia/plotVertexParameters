void makePosterPlots(){
	SetAtlasStyle();
        TCanvas* c1 = new TCanvas("c_1","c_1",800,600);
	c1->SetLogy(1);
	c1->SetRightMargin(0.08);
  TGaxis::SetMaxDigits(2);  
	TFile* f[4];
	f[0] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/bkgdJZ/JZAll/outputDataSearch_goodJetEventFlags.root");
	f[1] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/mg250/outputDataSearch_goodJetEventFlags.root");
	f[2] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/mg800/outputDataSearch_goodJetEventFlags.root");
	f[3] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/mg1500/outputDataSearch_goodJetEventFlags.root");

 	TH1D* h[4][4];
    
    Int_t colorSig[4] = {kGray, kBlue-3, kViolet-6, kTeal-6};
	TLegend *leg = new TLegend(0.6,0.73,.92,0.91);
	leg->SetFillStyle(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.03);
TString label[4] = {"","m_{#tilde{g}} = 250 GeV","m_{#tilde{g}} = 800 GeV","m_{#tilde{g}} = 1500 GeV"};

	for(int i=0;i<4;i++){
		h[0][i] = (TH1D*)f[i]->Get("e1_MSVxIso_2j150_b");
		h[1][i] = (TH1D*)f[i]->Get("e1_MSVxHits_2j150_b");
		h[2][i] = (TH1D*)f[i]->Get("e1_MSVxIso_2j250_ec");
		h[3][i] = (TH1D*)f[i]->Get("e1_MSVxHits_2j250_ec");
		h[1][i]->Rebin(4); h[3][i]->Rebin(4);
		for(int j=0;j<4;j++){ 
			h[j][i]->Scale(1./h[j][i]->Integral());
			h[j][i]->SetLineColor(colorSig[i]);
			h[j][i]->SetLineStyle(i+1);
			h[j][i]->GetYaxis()->SetTitle("Fraction of vertices");
		}
		if(i==0){ 
			for(int j=0;j<4;j++){ h[j][i]->SetFillColor(kGray); h[j][i]->GetYaxis()->SetRangeUser(0.0001,1);}
			h[0][i]->GetXaxis()->SetTitle("min[#DeltaR(jet,vx), #DeltaR(track, vx)]");
			h[0][i]->GetXaxis()->SetRangeUser(0,3);
			h[2][i]->GetXaxis()->SetTitle("min[#DeltaR(jet,vx), #DeltaR(track, vx)]");
			h[2][i]->GetXaxis()->SetRangeUser(0,3);
        		leg->AddEntry(h[0][i],"QCD Multi-jets","f");
			h[0][i]->Draw("hist");
		}

		else{
		 h[0][i]->Draw("HIST SAME");
        	leg->AddEntry(h[0][i],label[i],"l");}
	}
	leg->Draw();
	gPad->RedrawAxis();
	ATLASLabel(0.19,0.88,"Internal", kBlack);
    
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    //latex.SetTextSize(0.04);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatex(.19,.87,"Simulation");
    
	c1->Print("../OutputPlots/notePlots/e1_MSVxIso_2j150_b.pdf");

	h[3][0]->GetXaxis()->SetTitle("Number of associated TGC + MDT hits");
	h[3][0]->GetXaxis()->SetRangeUser(0,10000);
	h[3][0]->GetYaxis()->SetRangeUser(0.0001,2);

	h[1][0]->GetXaxis()->SetTitle("Number of associated RPC + MDT hits");
	h[1][0]->GetXaxis()->SetRangeUser(0,10000);
	h[1][0]->GetYaxis()->SetRangeUser(0.0001,2);
	h[1][0]->Draw("hist");
	for(int i=1;i<4;i++) h[1][i]->Draw("hist same");
	leg->Draw();
	gPad->RedrawAxis();
	ATLASLabel(0.19,0.88,"Internal", kBlack);
    	latex.DrawLatex(.19,.87,"Simulation");

	c1->Print("../OutputPlots/notePlots/e1_MSVxHits_2j150_b.pdf");


	h[2][0]->Draw("hist");
	for(int i=1;i<4;i++) h[2][i]->Draw("hist same");
	leg->Draw();
	gPad->RedrawAxis();
	ATLASLabel(0.19,0.88,"Internal", kBlack);
    	latex.DrawLatex(.19,.87,"Simulation");

	c1->Print("../OutputPlots/notePlots/e1_MSVxIso_2j250_ec.pdf");

	h[3][0]->Draw("hist");
	for(int i=1;i<4;i++) h[3][i]->Draw("hist same");
	leg->Draw();
	gPad->RedrawAxis();
	ATLASLabel(0.19,0.88,"Internal", kBlack);
    	latex.DrawLatex(.19,.87,"Simulation");

	c1->Print("../OutputPlots/notePlots/e1_MSVxHits_2j250_ec.pdf");
}

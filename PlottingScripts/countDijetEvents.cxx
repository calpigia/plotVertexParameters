void countDijetEvents(){
        double lumi_in_nb = 18857.4*1000. + 3193.68*1000.; //2016 MD2 + 2015 full repro. Needs to be the same as the prw!
	double jetSliceWeights[13];
        jetSliceWeights[0] = 1.0240     * 78420000.      * lumi_in_nb /100000.; //( Filter eff. ) * ( Cross-section [nb] ) * ( desired lumi [nb^-1] ) / ( Num Events )
        jetSliceWeights[1] = 0.00067198 * 78420000.      * lumi_in_nb /100000.; //JZ1W
        jetSliceWeights[2] = 0.00033264 * 2433400.       * lumi_in_nb /1982189.;
        jetSliceWeights[3] = 0.00031953 * 26454.         * lumi_in_nb /7884500.;
        jetSliceWeights[4] = 0.00053009 * 254.64         * lumi_in_nb /7979800.;
        jetSliceWeights[5] = 0.00092325 * 4.5536         * lumi_in_nb /7977600.;
        jetSliceWeights[6] = 0.00094016 * 0.25752        * lumi_in_nb /1893400.;
        jetSliceWeights[7] = 0.00039282 * 0.016214       * lumi_in_nb /1770200.; //JZ7W
        jetSliceWeights[8] = 0.010162   * 0.00062505     * lumi_in_nb /1743200.; //JZ8W
        jetSliceWeights[9] = 0.012054   * 0.00001964     * lumi_in_nb /1813200.; //JZ9W
        jetSliceWeights[10] = 0.0058935 * 0.0000011961   * lumi_in_nb /1996000.; //JZ10W
        jetSliceWeights[11] = 0.0027015 * 0.00000004226  * lumi_in_nb /1993200.; //JZ11W
        jetSliceWeights[12] = 0.00042502* 0.000000001037 * lumi_in_nb /1974600.; //JZ12W
        
	TChain *ch[11];
	for(int i=0;i<11;i++){ch[i] = new TChain("recoTree");}
	 ch[0]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v13/user.hrussell.mc15_13TeV.361022.Pythia8EvtGen_jetjet_JZ2W.EXOT15_DVAna.r7725_dv13_hist/*.root*");
	 ch[1]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v13/user.hrussell.mc15_13TeV.361023.Pythia8EvtGen_jetjet_JZ3W.EXOT15_DVAna.r7725_dv13_hist/*.root*");
	 ch[2]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361024.Pythia8EvtGen_jetjet_JZ4W.EXOT15_DVAna.r7725_dv14_hist/*.root*");
	 ch[3]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361025.Pythia8EvtGen_jetjet_JZ5W.EXOT15_DVAna.r7725_dv14_hist/*.root*");
	 ch[4]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361026.Pythia8EvtGen_jetjet_JZ6W.EXOT15_DVAna.r7725_dv14_hist/*.root*");
	 ch[5]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361027.Pythia8EvtGen_jetjet_JZ7W.EXOT15_DVAna.r7725_dv14_hist/*.root*");
	 ch[6]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361028.Pythia8EvtGen_jetjet_JZ8W.EXOT15_DVAna.r7772_dv14_hist/*.root*");
	 ch[7]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361029.Pythia8EvtGen_jetjet_JZ9W.EXOT15_DVAna.r7772_dv14_hist/*.root*");
	 ch[8]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361030.Pythia8EvtGen_jetjet_JZ10W.EXOT15_DVAna.r7772_dv14_hist/*.root*");
	 ch[9]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361031.Pythia8EvtGen_jetjet_JZ11W.EXOT15_DVAna.r7772_dv14_hist/*.root*");
	 ch[10]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.361032.Pythia8EvtGen_jetjet_JZ12W.EXOT15_DVAna.r7772_dv14_hist/*.root*");

	double nVertices = 0;
	for(int i=0;i<11;i++){
		TH1D* htmp = new TH1D("htemp","htemp",50,-3,3);
		int N = ch[i]->Draw("MSVertex_eta>>htemp","eventWeight*pileupEventWeight*(MSVertex_indexTrigRoI[0] >= 0 && MSVertex_eta@.size() == 1 && MSVertex_passesHitThresholds && CalibJet_isGoodStand[0] && CalibJet_pT[0] > 50.)",""); 
		std::cout << "JZ" + to_string(i+2)+"W has: "<< N << " vertices matching a trigger in " << ch[i]->GetEntries() << " entries." << std::endl;
		std::cout << "This converts to: " << htmp->Integral()*jetSliceWeights[i+2] << " vertices in 2015+2016 data." << std::endl;
		nVertices +=  htmp->Integral()*jetSliceWeights[i+2];
		delete htmp;	
	}
	std::cout << "Total number of vertices predicted in 2015+2016 dataset: " << nVertices << std::endl;

}


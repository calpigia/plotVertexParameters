#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>

double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-12) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }

    Double_t arg = 1.0 - ( x - peak ) * tail / width;

    if (arg < 1.e-12) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);

    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )

    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );

    return TMath::Exp(exponent) ;
}
double novosibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}
void ToysComparisonLTefficiency(){

  std::vector<TString> dir = {"","Files_NOLTeff/"};  
  
  TString benchmark;
  benchmark = "mH125mS5lt5";

    const int nSamples=2;
    //int colors[nSamples] = {kViolet+7, kAzure+7, kTeal, kSpring-2, kOrange-3, kRed-4};
    int colors[nSamples] = {kAzure+7, kRed-4};

    TFile* tfile[nSamples];
    TH1D* thist[nSamples];
    TH1D* thist2[nSamples];
    TH1D *thist_up[nSamples];
    TH1D *thist2_up[nSamples];
    TH1D *thist_down[nSamples];
    TH1D *thist2_down[nSamples];
    TF1* fn_up[nSamples];
    TF1* fn_down[nSamples];
    TF1* fn2_up[nSamples];
    TF1* fn2_down[nSamples];
    TF1* fn[nSamples];
    TF1* fn2[nSamples];
    TGraph* fnb[nSamples];
    TGraphAsymmErrors* g_err1[nSamples];
    TGraphAsymmErrors* g_err2[nSamples];
    double maxVals[nSamples] = {0,0};
    char name1[10];
    double maxValGraph=-99;
    for(int i=0; i<nSamples; i++){

      TString add_str="";
      add_str="_regA";

      tfile[i] = TFile::Open("../OutputPlots/signalMC/extrapolation/"+dir.at(i)+benchmark+add_str+".root");
    
        thist[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx");
        thist_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_maxTotal");
        thist_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_minTotal");
        thist2[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx");
        thist2_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_maxTotal");
        thist2_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_minTotal");

        sprintf(name1,"fn_%d",i);
        fn[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn[i]->SetParameter(0,thist[i]->GetMaximum());
        fn[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
        thist[i]->Fit( fn[i], "WRV" );

        sprintf(name1,"fn_up_%d",i);
        fn_up[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn_up[i]->SetParameter(0,thist_up[i]->GetMaximum());
        fn_up[i]->SetParameter(1,-0.3); //fn_up->SetParLimits(1,-1,2);
        fn_up[i]->SetParameter(2,0.4); //fn_up->SetParLimits(2,0,2);
        fn_up[i]->SetParameter(3,-0.2); //fn_up->SetParLimits(3,0,20.0);
        thist_up[i]->Fit( fn_up[i], "WRV" );
        sprintf(name1,"fn_down_%d",i);
        fn_down[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn_down[i]->SetParameter(0,thist_down[i]->GetMaximum());
        fn_down[i]->SetParameter(1,-0.3); //fn_down->SetParLimits(1,-1,2);
        fn_down[i]->SetParameter(2,0.4); //fn_down->SetParLimits(2,0,2);
        fn_down[i]->SetParameter(3,-0.2); //fn_down->SetParLimits(3,0,20.0);
        thist_down[i]->Fit( fn_down[i], "WRV" );

        sprintf(name1,"fn2_%d",i);
        fn2[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn2[i]->SetParameter(0,thist2[i]->GetMaximum());
        fn2[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn2[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn2[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
        thist2[i]->Fit( fn2[i], "WRV" );

        sprintf(name1,"fn2_up_%d",i);
        fn2_up[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn2_up[i]->SetParameter(0,thist2_up[i]->GetMaximum());
        fn2_up[i]->SetParameter(1,-0.3); //fn_up->SetParLimits(1,-1,2);
        fn2_up[i]->SetParameter(2,0.4); //fn_up->SetParLimits(2,0,2);
        fn2_up[i]->SetParameter(3,-0.2); //fn_up->SetParLimits(3,0,20.0);
        thist2_up[i]->Fit( fn2_up[i], "WRV" );
        sprintf(name1,"fn2_down_%d",i);
        fn2_down[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn2_down[i]->SetParameter(0,thist2_down[i]->GetMaximum());
        fn2_down[i]->SetParameter(1,-0.3); //fn_down->SetParLimits(1,-1,2);
        fn2_down[i]->SetParameter(2,0.4); //fn_down->SetParLimits(2,0,2);
        fn2_down[i]->SetParameter(3,-0.2); //fn_down->SetParLimits(3,0,20.0);
        thist2_down[i]->Fit( fn2_down[i], "WRV" );
        

        Double_t ctau[1000];
        Double_t eff[1000];
        Double_t eff1[1000];
        Double_t errUp[1000];
        Double_t errDown[1000];
        Double_t errUp1[1000];
        Double_t errDown1[1000];
        int ntot = 0;
        TGraph *g_res = new TGraph(thist[i]);
        
        for (Int_t j = 0; j < 1000; j++) {
            Double_t life, nev, nev_up, nev_down, nev1_up, nev1_down;
            g_res->GetPoint(j, life, nev);
            nev = fn[i]->Eval(life);
            nev_up = fn_up[i]->Eval(life) - fn[i]->Eval(life);
            nev_down = fn[i]->Eval(life) - fn_down[i]->Eval(life);
            nev1_up = fn_up[i]->Eval(life)-fn[i]->Eval(life);
            nev1_down = fn[i]->Eval(life)-fn_down[i]->Eval(life);

            if (nev == 0) {
                std::cout << "Continue because nev= " << nev << std::endl;
                continue;
            }
            if(nev > maxVals[i]) maxVals[i] = nev;
	    if (nev>maxValGraph) maxValGraph=nev;
            eff[ntot] = nev;
            eff1[ntot] = fn[i]->Eval(life);
            errUp[ntot] = nev_up;
            errDown[ntot] = nev_down;
            errUp1[ntot] = nev1_up;
            errDown1[ntot] = nev1_down;
            ctau[ntot] = life;
            ntot++;
        }
        fnb[i] = new TGraph(ntot, ctau,eff);
        fnb[i]->SetLineColor(colors[i]);
        fnb[i]->SetLineStyle(i+1);
        fnb[i]->SetLineWidth(3);
        g_err3[i] = new TGraphAsymmErrors(ntot, ctau, eff);
        g_err1[i] = new TGraphAsymmErrors(ntot, ctau, eff1);
        for(int j=0;j<ntot; j++){
            g_err3[i]->SetPointEYlow(j, errDown[j]);
            g_err3[i]->SetPointEYhigh(j, errUp[j]);
            g_err1[i]->SetPointEYlow(j, errDown1[j]);
            g_err1[i]->SetPointEYhigh(j, errUp1[j]);
        }
        g_err2[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err3[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err2[i]->SetLineWidth(0);
        g_err3[i]->SetLineWidth(0);
    }

    
 

    /*
    fnb[0]->Draw("AL same");
    fnb[0]->GetYaxis()->SetTitleOffset(1.6);
    fnb[0]->GetYaxis()->SetRangeUser(0,1.3*maxValGraph);
    fnb[0]->GetXaxis()->SetLimits(0.03,300);
    fnb[0]->GetXaxis()->SetTitle("Scalar proper lifetime (c*#tau) [m]");
    fnb[0]->GetYaxis()->SetTitle("Expected events in 36.1 fb^{-1}/BR[H#rightarrowss]");
    fnb[0]->Draw("AL same");
    g_err[0]->Draw("c3 SAME");

    c1->Update();
    for(int i=1; i<nSamples; i++){
        g_err[i]->Draw("c3 SAME");
        fnb[i]->Draw("LSAME");
    }
    TLegend *leg = new TLegend(0.66,0.66,0.99,0.91);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.04);
    leg->AddEntry(fn1b[0],"LT eff","l");
    leg->AddEntry(fn1b[1],"No LT eff","l");
    leg->Draw();
    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.05);
    latex2.SetTextAlign(13);  //align at top
    latex2.DrawLatex(.2,.92,"#font[72]{ATLAS}  Internal");

    c1->Print("../OutputPlots/signalMC/extrapolation/GlobalEfficiencies_Combined_ABCD_HSS.pdf");
    */

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.045);
    latex.SetTextAlign(13);  //align at top                                                                                                                                                   

    TLine line1; line1.SetLineWidth(1);
    TLine line2; line2.SetLineWidth(1); line2.SetLineColor(kGreen+1);

    TCanvas *c1 = new TCanvas("c1","c1",800,600);    
    c1->SetLogx();

    for(int i=0;i<nSamples;i++){
      fn[i]->SetLineColor(kRed+1);
      fn2[i]->SetLineColor(kBlue-3);
      g_err1[i]->SetFillColorAlpha(kRed+1,0.2);
      g_err2[i]->SetFillColorAlpha(kBlue-3,0.2);
      fn[i]->SetLineStyle(2); fn2[i]->SetLineStyle(7);
      fn[i]->SetLineWidth(3); fn2[i]->SetLineWidth(3);

      fn[i]->GetYaxis()->SetRangeUser(0,1.5*maxVals[i]);
      fn[i]->Draw("AL");
      fn[i]->GetYaxis()->SetTitleOffset(1.6);
      fn[i]->GetXaxis()->SetLimits(0.03,300);
      fn[i]->GetXaxis()->SetTitle("Scalar proper lifetime (c*#tau) [m]");
      fn[i]->GetYaxis()->SetLimits(0,1.5*maxVals[i]);
      fn[i]->GetYaxis()->SetTitle("Expected events in 36.1 fb^{-1}/BR[H#rightarrowss]");


      fn[i]->Draw("AL same");
      c1->Update();
      g_err1[i]->Draw("c3 same");
      fn[i]->Draw("LSAME");
      g_err2[i]->Draw("c3 same");
      fn2[i]->Draw("LSAME");

      TLegend *leg2 = new TLegend(0.58,0.76,0.95,0.91);
      leg2->SetFillStyle(0);
      leg2->SetBorderSize(0);
      leg2->AddEntry(fn[i],"Barrel MS vertex ","l");
      leg2->AddEntry(fn2[i],"Endcap MS vertex","l");
      leg2->AddEntry(fn3b[i],"Sum","l");
      leg2->Draw();
      //latex.DrawLatex(0.2,0.85,"m_{H},m_{s} = [125,"+scalarmass.at(i)+"] GeV");
      //latex2.DrawLatex(.2,.92,"#font[72]{ATLAS}  Internal");
     
      c1->Print("../OutputPlots/signalMC/extrapolation/GlobalEfficiency_ABCDChannel_"+benchmark+".pdf");
    }

    /*
    TCanvas *c2 = new TCanvas("c2","c2",800,600);    
    c2->SetLogx();

    for(int i=0;i<nSamples;i++){
      fn[i]->SetLineColor(kRed+1);
      fn2[i]->SetLineColor(kBlue-3);
      fn3b[i]->SetLineColor(kViolet-6);
      g_err3[i]->SetFillColorAlpha(kViolet-6,0.2);
      g_err1[i]->SetFillColorAlpha(kRed+1,0.2);
      g_err2[i]->SetFillColorAlpha(kBlue-3,0.2);
      fn[i]->SetLineStyle(2); fn2[i]->SetLineStyle(7);
      fn[i]->SetLineWidth(3); fn2[i]->SetLineWidth(3);
      fn3b[i]->SetLineStyle(1);

      //maxVals[0] = 0.006/1.5;                                                                                                                                                             
      fn3b[i]->GetYaxis()->SetRangeUser(0,1.5*maxVals[i]);
      fn3b[i]->Draw("AL");
      fn3b[i]->GetYaxis()->SetTitleOffset(1.6);
      fn3b[i]->GetXaxis()->SetLimits(0.03,300);
      fn3b[i]->GetXaxis()->SetTitle("Scalar proper lifetime (c*#tau) [m]");
      fn3b[i]->GetYaxis()->SetLimits(0,1.5*maxVals[i]);
      fn3b[i]->GetYaxis()->SetTitle("Expected events in 36.1 fb^{-1}/BR[H#rightarrowss]");

      fn3b[i]->Draw("AL same");
      g_err3[i]->Draw("c3 same");
      fn3b[i]->Draw("L SAME");
      c1->Update();
      g_err1[i]->Draw("c3 same");
      fn[i]->Draw("LSAME");
      g_err2[i]->Draw("c3 same");
      fn2[i]->Draw("LSAME");

      TLegend *leg2 = new TLegend(0.58,0.76,0.95,0.91);
      leg2->SetFillStyle(0);
      leg2->SetBorderSize(0);
      leg2->AddEntry(fn[i],"Barrel MS vertex ","l");
      leg2->AddEntry(fn2[i],"Endcap MS vertex","l");
      leg2->AddEntry(fn3b[i],"Sum","l");
      leg2->Draw();
      //latex.DrawLatex(0.2,0.85,"m_{H},m_{s} = [125,"+scalarmass.at(i)+"] GeV");
      //latex2.DrawLatex(.2,.92,"#font[72]{ATLAS}  Internal");
     
      c1->Print("../OutputPlots/signalMC/extrapolation/GlobalEfficiency_ABCDChannel_"+benchmark+".pdf");
    }
    */
    return;
}

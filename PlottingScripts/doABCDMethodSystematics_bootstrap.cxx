#include "../PlottingTools/PlottingPackage/SampleDetails.h"
#include <stdio.h>

#include "Riostream.h"
#include <iostream>
#include "TTree.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include <utility>
#include "TFile.h"
#include "TLine.h"
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"
#include "TRandom3.h"
#include <numeric>
#include "TBox.h"

TFile *_file0;
TRandom3 rnd;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    std::cout << "file name : " << fileName << std::endl;
    TString title = "";
    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}
void makeProjection(std::string axis, TH2D* hist){
    if( !(axis == "x"  || axis == "y") ){
        std::cout << "requested projection of: " << axis << " is not an axis... must be x or y" << std::endl;
        return;
    }
    
    TH1D* hP = axis=="x" ? (TH1D*)hist->ProjectionX() : (TH1D*)hist->ProjectionY();
    hP->Draw();
    return;
    
}
std::pair<double,double> getBootstrapOutput(std::vector<double> outputs){
    
    double sum = std::accumulate(outputs.begin(), outputs.end(), 0.0);
    double mean = sum / outputs.size();
    
    std::vector<double> diff(outputs.size());
    std::transform(outputs.begin(), outputs.end(), diff.begin(),
                   std::bind2nd(std::minus<double>(), mean));
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / outputs.size());
    
    return std::make_pair(mean, stdev);
    
}
void fillVectors(TFile *&file, std::vector<double> &xAxis, std::vector<double> &yAxis, int type){
    TTree *t = (TTree*)file->Get("abcdTree");
    Int_t nEntries = t->GetEntries();
    double mindR = 0; int nHits = 0; int reg = 0;
    t->SetBranchAddress("min_dR", &mindR);
    t->SetBranchAddress("nHits", &nHits);
    t->SetBranchAddress("region", &reg);
    
    for(unsigned int i=0; i < nEntries; i++){
        t->GetEntry(i);
        if(reg == type){
            xAxis.push_back(mindR);
            yAxis.push_back(nHits);
        }
    }
}

double runBootstrap(std::vector<double> &xAxis, std::vector<double> &yAxis, std::vector<double> xBounds, std::vector<double> yBounds, int type){
    
    std::vector<double> abcds = {0,0,0,0};
    
    for(unsigned int i=0; i< xAxis.size(); i++){
        int copies = rnd.Poisson(1.0);
        if(xAxis.at(i) > xBounds.at(1) && yAxis.at(i) > yBounds.at(1)){
            abcds.at(0) += double(copies);
        } else if(xAxis.at(i) < xBounds.at(0) && yAxis.at(i) > yBounds.at(1)){
            abcds.at(1) += double(copies);
        } else if(xAxis.at(i) > xBounds.at(1) && yAxis.at(i) < yBounds.at(0)){
            abcds.at(2) += double(copies);
        } else if(xAxis.at(i) < xBounds.at(0) && yAxis.at(i) < yBounds.at(0)){
            abcds.at(3) += double(copies);
        }
    }
    //    std::cout << "abcd: ";
    //    for(int i=0;i<4;i++) std::cout << abcds.at(i) << "  ";
    double bcd = abcds.at(1)*abcds.at(2)/abcds.at(3);
    //    std::cout << " bcd: " << bcd << std::endl;
    if(type == 0){
        return (abcds.at(0) - bcd)/abcds.at(0);
    } else{
        return bcd;
    }
}

void doABCDMethodSystematics_bootstrap(TString outDir = "/Users/hrussell/Work/Run2Plots/GenericAnalysis/ABCD/Data2016Full_v3/", TString sampleName = "", bool blind = true, std::vector<double> xMinBound = {0,0}, std::vector<double> yMinBound = {0,0}, std::vector<double> xBound = {0.3,0.4}, std::vector<double> yBound = {2000,2500}){
    double yRes = 100;
    double xRes = 0.1;
    SetAtlasStyle();
    TString expName = "e1";
    bool isData = (sampleName == "")? true : false;
    double nEvents, prodXS, lumi, scaling;
    TString label = "";
    if(!isData){
        label = convertFiletoLabel(sampleName);
        std::cout << "sample is: " << sampleName << std::endl;
        SampleDetails::setGlobalVariables(sampleName);
        nEvents = SampleDetails::nEventsInSample;
        prodXS = SampleDetails::mediatorXS;
        lumi =  32864.+3212.96;
        scaling = lumi*prodXS/nEvents;
        std::cout << "Applying scaling of " << scaling << std::endl;
    } else {
        std::cout << "Sample is data. Hope you blinded correctly!" << std::endl;
        scaling = 1.0;
    }
    
    TCanvas *c1 = new TCanvas("c1","c1",800,600);
    gStyle->SetPalette(1);
    c1->SetLogz(1);
    c1->SetRightMargin(0.13);
    
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;
    
    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    
    std::vector<TString> reg = {"_b","_ec"};
    std::vector<std::vector<TString>> histNames = {{"1j50_1j150","2j150"},{"1j100_1j250","2j250"}};
    std::vector<std::vector<TString>>  histLabels = {{"#splitline{E_{T,1} > 150 GeV,}{150 GeV > E_{T,2} > 50 GeV}", "#splitline{E_{T,1} > 150 GeV,}{E_{T,2} > 150 GeV}"},{"#splitline{E_{T,1} > 250 GeV,}{250 GeV > E_{T,2} > 100 GeV}", "#splitline{E_{T,1} > 250 GeV,}{E_{T,2} > 250 GeV}"}};
    
    TH1D* h_bs[4];
    h_bs[0] = new TH1D("barrelVR","barrelVR",300,20,80);
    h_bs[1] = new TH1D("barrelSR","barrelSR",200,0,40);
    h_bs[2] = new TH1D("endcapVR","endcapVR",200,0,40);
    h_bs[3] = new TH1D("endcapSR","endcapSR",200,0,40);
    
    TH1D* h_bs_diff[6];
    h_bs_diff[0] = new TH1D("barrelVR_diff","barrelVR_diff",200,-1,1);
    h_bs_diff[1] = new TH1D("endcapSR_diff","endcapVR_diff",200,-3,1);
    h_bs_diff[2] = new TH1D("barrelVR_diff_1s","barrelVR_diff_1s",200,-1,1);
    h_bs_diff[3] = new TH1D("endcapSR_diff_1s","endcapVR_diff_1s",200,-3,1);
    h_bs_diff[4] = new TH1D("barrelVR_diff_2s","barrelVR_diff_2s",200,-1,1);
    h_bs_diff[5] = new TH1D("endcapSR_diff_2s","endcapVR_diff_2s",200,-3,1);
    
    for(int i=0;i<6;i++){
        h_bs_diff[i]->Sumw2(); h_bs_diff[i]->GetXaxis()->SetTitle("[A - B*C/D]/A");
        h_bs_diff[i]->GetYaxis()->SetTitle("Number of toys");
    }
    std::vector<double> stdev;
    
    for(int i_reg=0;i_reg < 2; i_reg++){
        
        for(int i_cr = 0; i_cr < 2; i_cr++){
            int region = 2*(i_reg) + 1 + i_cr;
            
            std::vector<double> xAxis; std::vector<double> yAxis;
            
            if(i_cr == 0 || i_cr == 1){
                h_bs[region-1]->Sumw2(); h_bs[region-1]->GetXaxis()->SetTitle("B*C/D");
                h_bs[region-1]->GetYaxis()->SetTitle("Number of toys");
                
                fillVectors(_file0,xAxis,yAxis,region);
                std::vector<double> outputs, outputs_2s, outputs_1s;
                std::vector<double> outputs_aexp;
                
                for(int i_bs=0;i_bs<100000;i_bs++){
                    if(i_bs%10000 == 0) std::cout << "on iteration: " << i_bs << std::endl;
                    double bs = 0; double bs_1s = 0;double bs_2s = 0; double bs_aexp = 0;
                    if(i_cr == 0){
                        if(i_reg == 0) bs = runBootstrap(xAxis,yAxis,{0.3,0.3},{2000,2000},0);
                        else bs = runBootstrap(xAxis,yAxis,{0.4,0.4},{2500,2500},0);
                        if(i_reg == 0) bs_1s = runBootstrap(xAxis,yAxis,{0.2,0.3},{1900,2000},0);
                        else bs_1s = runBootstrap(xAxis,yAxis,{0.3,0.4},{2400,2500},0);
                        if(i_reg == 0) bs_2s = runBootstrap(xAxis,yAxis,{0.1,0.3},{1800,2000},0);
                        else bs_2s = runBootstrap(xAxis,yAxis,{0.2,0.4},{2300,2500},0);
                    }
                    
                    if(i_reg == 0) bs_aexp = runBootstrap(xAxis,yAxis,{0.3,0.3},{2000,2000},1);
                    else bs_aexp = runBootstrap(xAxis,yAxis,{0.4,0.4},{2500,2500},1);
                    
                    if(i_cr == 0){
                        if(bs > -999999999){
                        outputs.push_back(bs);
                        h_bs_diff[i_reg]->Fill(bs);
                        }
                        if(bs_1s > -99999999){
                            outputs_1s.push_back(bs_1s);
                            h_bs_diff[i_reg+2]->Fill(bs_1s);
                        }
                        if(bs_2s > -99999999){
                            outputs_2s.push_back(bs_2s);
                            h_bs_diff[i_reg+4]->Fill(bs_2s);
                        }
                    }
                    outputs_aexp.push_back(bs_aexp);
                    h_bs[region-1]->Fill(bs_aexp);
                    
                }
                if(i_cr == 0){
                    std::pair<double, double> result = getBootstrapOutput(outputs);
                    std::pair<double, double> result_1s = getBootstrapOutput(outputs_1s);
                    std::pair<double, double> result_2s = getBootstrapOutput(outputs_2s);
                    std::cout << "========= NO gap removed ========" << std::endl;
                    std::cout << "[A - B*C/D]/A mean/stdev: " << result.first << ", standard deviation: " << result.second << std::endl;
                    std::cout << "========= 1 sigma gap removed ========" << std::endl;
                    std::cout << "[A - B*C/D]/A mean/stdev: " << result_1s.first << ", standard deviation: " << result_1s.second << std::endl;
                    std::cout << "========= 2 sigma gap removed ========" << std::endl;
                    std::cout << "[A - B*C/D]/A mean/stdev: " << result_2s.first << ", standard deviation: " << result_2s.second << std::endl;
                    stdev.push_back(result.second);
                    stdev.push_back(result_1s.second);
                    stdev.push_back(result_2s.second);
                }
                std::pair<double, double> result_aexp = getBootstrapOutput(outputs_aexp);
                std::cout << "A_exp mean/stdev: " << result_aexp.first << ", standard deviation: " << result_aexp.second << std::endl;
                
            }
            
            TString srName = "you're in the signal region!";
            TH2D *h2 = nullptr;
            if(i_cr !=1){
                if(i_reg == 0) srName= expName+"_ClosestdR_vs_nHits_"+histNames.at(i_reg).at(i_cr+1)+"_low"+reg.at(i_reg);
                else if(i_reg == 1) srName= expName+"_ClosestdR_vs_nHits_"+histNames.at(i_reg).at(i_cr+1)+reg.at(i_reg);
                h2 = (TH2D*)_file0->Get(srName);
                //h2->Scale(scaling);
                h2->SetDirectory(0);
            }
            //std::cout << "signal region: " << srName << std::endl;
            
            TString histname = expName+"_ClosestdR_vs_nHits_"+histNames.at(i_reg).at(i_cr)+reg.at(i_reg);
            //std::cout << "looking at hist: " << histname << std::endl;
            TH2D *h = (TH2D*)_file0->Get(histname);
            h->SetDirectory(0);
            //h->Scale(scaling);
            if(i_cr != 1) h->Add(h2,-1.);
            
            double corr = h->GetCorrelationFactor();
            //std::cout << "correlation: " << h->GetCorrelationFactor() << std::endl;
            
            double errorA = 0;
            double errorB = 0;
            double errorC = 0;
            double errorD = 0;
            double errorBCD = 0;
            
            h->GetXaxis()->SetTitle("min(#Delta R(closest jet), #Delta R(closest track))");
            
            TBox *box1;
            // ** Define the sigma bands for the systematic
            TBox *band_1sigma_X;
            TBox *band_2sigma_X;
            TBox *band_1sigma_Y;
            TBox *band_2sigma_Y;
            
            
            h->GetYaxis()->SetTitle("vertex(nMDT + nTrig hits)");
            
            for(int i_gap=1;i_gap < 3;i_gap++){
            //std::cout << "ABCD Method with lines at: " << xBound<< ", " << yBound << std::endl;
            int xMin = h->GetXaxis()->FindBin(xMinBound.at(i_reg) + 0.001);
            int xMax_a = h->GetXaxis()->FindBin(xBound.at(i_reg) - xRes*double(i_gap) - 0.001);
            int xMax_b = h->GetXaxis()->FindBin(xBound.at(i_reg) - 0.001);
            
            int yMin = h->GetYaxis()->FindBin(yMinBound.at(i_reg) + 0.001);
            int yMax_a = h->GetYaxis()->FindBin(yBound.at(i_reg) - yRes*double(i_gap)   - 0.001);
            int yMax_b = h->GetYaxis()->FindBin(yBound.at(i_reg)  - 0.001);
            
            std::cout << xMin << " " << xMax_a << " " << xMax_b << " " << yMin << " " << yMax_a << " " << yMax_b << std::endl;
            
            std::cout << "ABCD Method with gap between: [" << h->GetXaxis()->GetBinLowEdge(xMax_a+1) << ", " << h->GetXaxis()->GetBinLowEdge(xMax_b+1) << "], [" << h->GetYaxis()->GetBinLowEdge(yMax_a+1) << ", " <<h->GetYaxis()->GetBinLowEdge(yMax_b+1) << "]" << std::endl;
            std::cout << "total number of events in histogram: " << h->Integral()*scaling << std::endl;
            double valA = h->IntegralAndError(xMax_b+1,h->GetXaxis()->GetNbins()+1,yMax_b+1,h->GetYaxis()->GetNbins()+1,errorA);
            double valB = h->IntegralAndError(xMin,xMax_a,yMax_b+1,h->GetYaxis()->GetNbins()+1,errorB);
            double valC = h->IntegralAndError(xMax_b+1,h->GetXaxis()->GetNbins()+1,yMin,yMax_a,errorC);
            double valD = h->IntegralAndError(xMin,xMax_a,yMin,yMax_a,errorD);
            errorA = sqrt(valA);
            errorB = sqrt(valB);
            errorC = sqrt(valC);
            
            double BCD = valB*valC/valD;
            errorBCD = sqrt(1/valB + 1/valC + 1/valD) * BCD;
            
            if((i_cr == 1)&& blind){
                std::cout <<  histNames.at(i_reg).at(i_cr) << " & blind & $" << BCD*scaling << " \\pm " << errorBCD*scaling << "$ & $";
                
                box1 = new TBox(xBound.at(i_reg), yBound.at(i_reg), 5, 10000);
                box1->SetFillColor(kBlue-10);
            } else{
                std::cout << histNames.at(i_reg).at(i_cr) << " & $"<<valA*scaling << " \\pm " << errorA*scaling << "$ & $" << BCD*scaling << " \\pm " << errorBCD*scaling << "$ & $";
            }
            std::cout << valB*scaling << " \\pm " << errorB*scaling << "$ & $";
            std::cout << valC*scaling << " \\pm " << errorC*scaling << "$ & $";
            std::cout << valD*scaling << " \\pm " << errorD*scaling << "$ \\\\ " << std::endl;
            }
            //h->Scale(1./389000.*1.4891*22.1);
            h->GetXaxis()->SetRangeUser(0,5);
            h->SetMinimum(0.8);
            h->Scale(scaling);
            h->Draw("COLZ");
            
            if((i_cr == 1) && blind) box1->Draw();
            else if(i_cr == 0){
                if(i_reg == 0){
                    // ** Define the sigma bands for the systematic
                    band_1sigma_X = new TBox(0.2,0,0.3,10000);
                    band_1sigma_X->SetFillColor(kRed);
                    band_1sigma_X->SetFillStyle(3001);
                    band_1sigma_X->SetLineWidth(0);
                    band_2sigma_X = new TBox(0.1,0.0,0.3,10000);
                    band_2sigma_X->SetFillColor(kRed);
                    band_2sigma_X->SetFillStyle(3344);
                    band_2sigma_X->SetLineWidth(0);
                    band_1sigma_Y = new TBox(0.0,1900,5,2000);
                    band_1sigma_Y->SetFillColor(kRed);
                    band_1sigma_Y->SetFillStyle(3001);
                    band_1sigma_Y->SetLineWidth(0);
                    band_2sigma_Y = new TBox(0.0,1800,5,2000);
                    band_2sigma_Y->SetFillColor(kRed);
                    band_2sigma_Y->SetFillStyle(3344);
                    band_2sigma_Y->SetLineWidth(0);
                    band_1sigma_X->Draw();
                    
                    band_2sigma_X->Draw();
                    band_2sigma_Y->Draw();
                    band_1sigma_Y->Draw();
                }else if (i_reg == 1){
                    band_1sigma_X = new TBox(0.3,0.0,0.4,10000);
                    band_1sigma_X->SetFillColor(kRed);
                    band_1sigma_X->SetFillStyle(3001);
                    band_1sigma_X->SetLineWidth(0);
                    band_2sigma_X = new TBox(0.2,0.0,0.4,10000);
                    band_2sigma_X->SetFillColor(kRed);
                    band_2sigma_X->SetFillStyle(3344);
                    band_2sigma_X->SetLineWidth(0);
                    band_1sigma_Y = new TBox(0.0,2400,5,2500);
                    band_1sigma_Y->SetFillColor(kRed);
                    band_1sigma_Y->SetFillStyle(3001);
                    band_1sigma_Y->SetLineWidth(0);
                    band_2sigma_Y = new TBox(0.0,2300,5,2500);
                    band_2sigma_Y->SetFillColor(kRed);
                    band_2sigma_Y->SetFillStyle(3344);
                    band_2sigma_Y->SetLineWidth(0);
                    band_1sigma_X->Draw();
                    band_2sigma_X->Draw();
                    band_1sigma_Y->Draw();
                    band_2sigma_Y->Draw();
                }
            }
            ATLASLabel(0.185,0.89,"Internal");
            
            
            TLatex latexr;
            latexr.SetNDC();
            latexr.SetTextColor(kBlack);
            latexr.SetTextSize(0.04);
            latexr.SetTextAlign(13);  //align at top
            if(reg.at(i_reg) == "_b") latexr.DrawLatex(.57,.87,"Barrel vertices");
            else latexr.DrawLatex(.57,.87,"Endcap vertices");
            if(isData) latexr.DrawLatex(.57,.92,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}");
            else{
                latexr.DrawLatex(0.57,0.92,label);
                latexr.SetTextSize(0.05);
                latexr.DrawLatex(0.19,0.863,"Simulation");
            }
            TLatex latex2;
            latex2.SetNDC();
            latex2.SetTextColor(kBlack);
            latex2.SetTextSize(0.035);
            latex2.SetTextAlign(13);  //align at top
            latex2.DrawLatex(.6,.82,histLabels.at(i_reg).at(i_cr));
            TLatex latex3;
            latex3.SetNDC();
            latex3.SetTextColor(kBlack);
            latex3.SetTextSize(0.035);
            latex3.SetTextAlign(13);  //align at top
            latex3.DrawLatex(.6,.7,TString("correlation: "+std::to_string(corr)) );
            
            c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_"+histNames.at(i_reg).at(i_cr)+reg.at(i_reg)+".pdf");
            
            
            h = nullptr;
            h2 = nullptr;
            xAxis.clear();
            yAxis.clear();
            if(i_cr==0){
                delete band_1sigma_X;
                delete band_1sigma_Y;
                delete band_2sigma_X;
                delete band_2sigma_Y;
            }
        }
    }
    c1->SetRightMargin(0.05);
    
    std::vector<std::string> bcdnames = {"bVR","bSR","eVR","eSR"};
    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.035);
    latex2.SetTextAlign(13);  //align at top
    std::vector<TString> hist_labs =  {"Barrel validation region","Endcap validation region","Barrel validation region, 1#sigma","Endcap validation region, 1#sigma","Barrel validation region, 2#sigma","Endcap validation region, 2#sigma"};
    std::vector<std::string> out_names = {"nom","nom","1s","1s","2s","2s"};
    for(int i=0;i<6;i++){
        h_bs_diff[i]->Draw();
        ATLASLabel(0.2 ,0.87,"Internal");
        latex2.DrawLatex(0.2,.82,hist_labs.at(i));
        latex2.DrawLatex(0.2, 0.75,"#sigma: "+ TString(std::to_string(stdev.at(i))));
        c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_diff_"+bcdnames.at( ((i%2 == 0)?0:2) )+"_"+out_names.at(i)+".pdf");
    }
    
    h_bs[0]->Draw();
    ATLASLabel(0.65,0.87,"Internal");
    latex2.DrawLatex(.65,.82,"Barrel validation region");
    c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_bcd_"+bcdnames.at(0)+".pdf");
    
    h_bs[1]->Draw();
    ATLASLabel(0.65,0.87,"Internal");
    latex2.DrawLatex(.65,.82,"Barrel signal region");
    c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_bcd_"+bcdnames.at(1)+".pdf");
    
    h_bs[2]->Draw();
    ATLASLabel(0.65,0.87,"Internal");
    latex2.DrawLatex(.65,.82,"Endcaps validation region");
    c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_bcd_"+bcdnames.at(2)+".pdf");
    h_bs[3]->Draw();
    ATLASLabel(0.65,0.87,"Internal");
    latex2.DrawLatex(.65,.82,"Endcaps signal region");
    c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_bcd_"+bcdnames.at(3)+".pdf");
    
}

int main(){
    _file0 = TFile::Open("/Users/hrussell/Work/Run2Plots/GenericAnalysis/ABCD/Data2016Full_v3/outputDataSearch_allData_v3.root");
    doABCDMethodSystematics_bootstrap();
}

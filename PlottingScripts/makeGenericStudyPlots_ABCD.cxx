
void makeGenericStudyPlots_ABCD(){
	//0.7 < |η| < 1.0 or 1.5 < |η| < 1.7


	SetAtlasStyle();

	TCanvas* c1 = new TCanvas("c1","c1",800,600);
	c1 = (TCanvas*) gROOT->FindObject("c1");
	c1->cd();
	
	c1->SetRightMargin(0.05);
	c1->SetTopMargin(0.04);
	c1->SetLeftMargin(0.15);
	c1->SetBottomMargin(0.15);

	//TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/dijetData/outputDataSearch.root");
	TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/data/outputDataSearch_noTrigIso_unscaled.root");
	TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/signalMC/mg800/outputDataSearch_noTrigIso_unscaled.root");
	TFile *_file2 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/signalMC/mg250/outputDataSearch_noTrigIso_unscaled.root");

	TString plotDir = "GenericAnalysis/ABCD/";

	c1->SetLogy(1);
	TString type[5]; type[0] = "eventHT"; type[1]="eventMeff"; type[2] = "eventHTMiss"; type[3] = "MSVxEta";type[4] = "MSVxPhi";
	TString loc[2]; loc[0] = "b"; loc[1] = "ec";
	TString regn[2];regn[0] = "e1"; regn[1]="e1t"; std::cout << "regn[0]: " << regn[0] << ", regn[1]: " << regn[1] << std::endl;
	TString jetreg[4]; jetreg[0] = "1j50_"; jetreg[1] = "2j50_"; jetreg[2] = "1j50_1j150_"; jetreg[3] = "2j150_";
	for(int i=0; i<2;i++){
		for(int j=0;j<5;j++){
			for(int k=0;k<2;k++){
			    for(int l=0; l<4;l++){
			std::cout << "hist: "  << regn[i] <<"_"<<type[j]<<"_isoSR_"<<loc[k] << std::endl;

	TH1D *hAsig2 = (TH1D*)_file1->Get(regn[i]+"_"+type[j]+"_iso_"+jetreg[l]+loc[k]);
	TH1D *hAsig = (TH1D*)_file2->Get(regn[i]+"_"+type[j]+"_iso_"+jetreg[l]+loc[k]);
	TH1D *hA = (TH1D*)_file0->Get(regn[i]+"_"+type[j]+"_iso_"+jetreg[l]+loc[k]);
	TH1D *hB = (TH1D*)_file0->Get(regn[i]+"_"+type[j]+"_noiso_"+jetreg[l]+loc[k]);


	hAsig2->Scale(1.4891*3.34*1000./357000.);
	hAsig->Scale(1190.35*3.34*1000./396000.);
	
	//hC->Add(hCb);
	//hD->Add(hDb);
	
	if(j<2){ hAsig->Rebin(2); hA->Rebin(2);hB->Rebin(2);} //hC->Rebin(2); hD->Rebin(2);}
	
	hAsig->GetXaxis()->SetTitle(type[j](5,type[j].Sizeof() - 5)+" [GeV]");
	if(j == 3 ) hAsig->GetXaxis()->SetTitle("#eta");
	else if(j==4 ) hAsig->GetXaxis()->SetTitle("#phi [radians]");

	hAsig->GetYaxis()->SetTitle("Number of events"); // / P(MSVx | 0 jets)");


	hAsig2->SetLineWidth(0);hAsig2->SetFillColorAlpha(kAzure-9,0.75);
	hAsig->SetLineWidth(0);hAsig->SetFillColorAlpha(kOrange-9,0.35);
	hA->SetLineColor(kBlack);hA->SetMarkerColor(kBlack);hA->SetMarkerSize(1.2);hA->SetLineWidth(2);hA->SetMarkerStyle(20);
	hB->SetLineColor(kBlue+2);hB->SetMarkerColor(kBlue+2);hB->SetMarkerSize(1.2);hB->SetLineWidth(2);hB->SetMarkerStyle(22);	
	//hC->SetLineColor(kRed+2);hC->SetMarkerColor(kRed+2);hC->SetMarkerSize(1.2);hC->SetLineWidth(2);hC->SetMarkerStyle(24);	
	//hD->SetLineColor(kGreen+2);hD->SetMarkerColor(kGreen+2);hD->SetMarkerSize(1.2);hD->SetLineWidth(2);hD->SetMarkerStyle(26);	

	double maxAsig = hAsig->GetMaximum()*2.;
	hAsig->SetMinimum(0.8);
	hAsig->SetMaximum(std::max(hAsig->GetMaximum()*2,hA->GetMaximum())*2); 
	hAsig->SetMaximum(std::max(hAsig->GetMaximum()*2,hB->GetMaximum())*2); 
	//hAsig->SetMaximum(std::max(hAsig->GetMaximum()*2,hC->GetMaximum())*2);
	//hAsig->SetMaximum(std::max(hAsig->GetMaximum()*2,hD->GetMaximum())*2);
	
	/*hA->GetYaxis()->SetLabelSize(0.070);
	hA->GetYaxis()->SetTitleSize(0.065);
	hA->GetYaxis()->SetTitleOffset(0.18);
	hA->GetXaxis()->SetTitleOffset(2.7);
	hA->GetXaxis()->SetLabelOffset(0.5);
	hA->GetYaxis()->SetTitleOffset(0.95);
*/
	hAsig->GetXaxis()->SetRangeUser(0,2000);
	if(j==2) 	hAsig->GetXaxis()->SetRangeUser(0,1000);

	hAsig->Draw("HIST");
	hAsig2->Draw("HIST SAME");
	hA->Draw("SAME");
	hB->Draw("SAME");
	//hC->Draw("SAME");
	//hD->Draw("SAME");

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.045);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatex(.17,.92,"#font[72]{ATLAS}  Internal");

    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.035);
    latex2.SetTextAlign(13);  //align at top
    latex2.DrawLatex(.18,.87,"#sqrt{s} = 13 TeV, ~3.2 fb^{-1}");
    
    TLegend *leg = new TLegend(0.43,0.8,0.95,0.93);
	leg->SetFillColor(0);
	leg->SetFillStyle(0);
	leg->SetBorderSize(0);
	leg->SetTextSize(0.04);
	leg->SetNColumns(2);
	leg->AddEntry(hAsig,"#font[42]{m_{#tilde{g}}=250 GeV}","f");
	leg->AddEntry(hAsig2,"#font[42]{m_{#tilde{g}}=800 GeV}","f");
	leg->AddEntry(hA,"#font[42]{Iso}","lp");
	leg->AddEntry(hB,"#font[42]{Noniso}","lp");
	//leg->AddEntry(hC,"#font[42]{Iso CR}","lp");
	//leg->AddEntry(hD,"#font[42]{Noniso CR}","lp");
	leg->Draw();	
	
	c1->Print(plotDir+"new/"+regn[i]+"_"+type[j]+"_"+loc[k]+"_"+jetreg[l]+"separate.pdf");
	if(j > 2) continue;
	leg->Clear();
	
	/*
	hB->Multiply(hC); hB->Divide(hD);
	
	hAsig->GetXaxis()->SetRangeUser(0,1000);
	if(j==2) 	hAsig->GetXaxis()->SetRangeUser(0,500);
	hAsig->SetMinimum(0.1);
	hAsig->SetMaximum(std::max(maxAsig*5., hB->GetMaximum()*5.)); 
	hAsig->SetMaximum(std::max(hAsig->GetMaximum()*5, hA->GetMaximum()*5.)); 

	hAsig->Draw("HIST");
	hAsig2->Draw("HIST SAME");

	hA->Draw("SAME");

	hB->Draw("SAME");

    latex.DrawLatex(.17,.92,"#font[72]{ATLAS}  Internal");
    latex2.DrawLatex(.18,.87,"#sqrt{s} = 13 TeV, ~3.2 fb^{-1}");

	leg->SetNColumns(2);
	leg->AddEntry(hAsig,"#font[42]{m_{#tilde{g}}=250 GeV}","f");
	leg->AddEntry(hAsig2,"#font[42]{m_{#tilde{g}}=800 GeV}","f");
	leg->AddEntry(hA,"#font[42]{Iso SR}","lp");
	leg->AddEntry(hB,"#font[42]{B*C/D (bkg)}","lp");
	leg->Draw();	
	
	c1->Print(plotDir+regn[i]+"_"+type[j]+"_"+loc[k]+"_ABCD.pdf");
	*/
	leg->Clear();
	
		}
		}
		}
	}
	/*
	
	TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
	pad1->SetTopMargin(0.075);
	pad1->SetBottomMargin(0.04);
	pad1->SetLeftMargin(0.14);
	pad1->SetLogy();
	pad1->Draw();

	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
	pad2->SetTopMargin(0.05);
	pad2->SetBottomMargin(0.40);
	pad2->SetLeftMargin(0.14);
	pad2->Draw();
	
	c1->cd(); pad2->cd();

	gStyle->SetPadTickX(1);
	gStyle->SetPadTickY(1);

	TH1D *p1_new = (TH1D*)hA->Clone("data_clone");

	p1_new->Divide(hB);


	p1_new->SetMarkerStyle(8);
	p1_new->SetMarkerSize(0.8);
	p1_new->SetMarkerColor(kBlack);
	p1_new->SetMinimum(0.0);
	p1_new->SetMaximum(3.0);
	//p1_new->GetXaxis()->SetTitle("to do");
	p1_new->GetXaxis()->SetLabelFont(42);
	p1_new->GetXaxis()->SetLabelSize(0.16);
	p1_new->GetXaxis()->SetLabelOffset(0.05);
	p1_new->GetXaxis()->SetTitleFont(42);
	p1_new->GetXaxis()->SetTitleSize(0.14);
	p1_new->GetXaxis()->SetTitleOffset(1.3);
	p1_new->GetYaxis()->SetNdivisions(505);
	p1_new->GetYaxis()->SetTitle("#font[42]{Noiso/Iso}");
	p1_new->GetYaxis()->SetLabelFont(42);
	p1_new->GetYaxis()->SetLabelSize(0.15);
	p1_new->GetYaxis()->SetTitleFont(42);
	p1_new->GetYaxis()->SetTitleSize(0.13);
	p1_new->GetYaxis()->SetTitleOffset(0.44); 
	p1_new->GetXaxis()->SetBinLabel(1,"0");
	p1_new->GetXaxis()->SetBinLabel(2,"1");
	p1_new->GetXaxis()->SetBinLabel(3,"2");
	p1_new->GetXaxis()->SetBinLabel(4,"3");
	p1_new->GetXaxis()->SetBinLabel(5,"4+");
	p1_new->DrawCopy("eP");

	p1_new->Fit("pol0","Q");
	//std::cout << hA->GetFunction("pol0")->GetParameter(0) << ", " << hA->GetFunction("pol0")->GetParError(0) << std::endl;
	TString	fitVal = TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("pol0")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("pol0")->GetParError(0) );		

	std::cout << fitVal << std::endl; 
	pad2->SetLogy(0);

	//latex2.DrawLatex(.15,.98,title + ", Data/MC");

	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(kBlack);
	latex.SetTextSize(0.1);
	latex.SetTextAlign(13);  //align at top
	latex.DrawLatex(.25,.92,fitVal);
	c1->cd();
	TLegend *leg = new TLegend(0.2,0.81,0.5,0.92);
	leg->SetFillColor(0);
	leg->SetFillStyle(0);
	leg->SetBorderSize(0);
	leg->SetTextSize(0.04);
	leg->AddEntry(hA,"#font[42]{Non-isolated vertices}","l");
	leg->AddEntry(hB,"#font[42]{Isolated vertices}","l");
	leg->Draw();	  		 
	c1->Print(plotDir+"vertexRecoProbability_noRatio.pdf");
	leg->Clear();
	for (TObject* keyAsObj : *(_file0->GetListOfKeys())){
		auto key = dynamic_cast<TKey*>(keyAsObj);

		pad1->cd(); 
		gStyle->SetPadTickX(2);
		gStyle->SetPadTickY(2);
		TH1D *hB = (TH1D*) key->ReadObj();
		TString histName = TString(hB->GetName());
		if(!histName.Contains("_iso")) continue;
		Size_t histLength = histName.Length();
		TString noisoName( histName.Replace(histLength-3,3,"noiso") );
		TH1D *hA = (TH1D*)_file0->Get(noisoName);
		std::cout << "Key name: " << key->GetName() << " Type: " << key->GetClassName() << ", entries data/mc: " << hB->GetEntries()  <<"/" <<hA->GetEntries() <<  std::endl;


		if(hB->GetEntries() == 0 || hA->GetEntries() == 0) continue;
		if(histName.Contains("abcd") ) continue; //this one is messed up right now

		pad1->SetLogy(0); hA->SetMinimum(0);
		hA->SetLineColor(kBlack);hA->SetMarkerColor(kBlack);hA->SetMarkerSize(1.2);hA->SetLineWidth(2);hA->SetMarkerStyle(20);
		hB->SetLineColor(kBlue+2);hB->SetMarkerColor(kBlue+2);hB->SetMarkerSize(1.2);hB->SetLineWidth(2);hB->SetMarkerStyle(4);	
		hA->GetYaxis()->SetTitle("Number of MS vertices");

		hA->Scale(hB->Integral() / hA->Integral() );

		if(histName.Contains("HT_")){hA->GetXaxis()->SetTitle("Event H_{T} [GeV]"); hA->GetYaxis()->SetTitle("Number of events");hA->SetMinimum(0.8);pad1->SetLogy(1); hA->GetXaxis()->SetRangeUser(000,1000); 	hA->SetMinimum(0.1);}
		else if(histName.Contains("HTMiss_")){hA->GetXaxis()->SetTitle("Event H_{T}^{miss} [GeV]"); hA->GetYaxis()->SetTitle("Number of events");hA->SetMinimum(0.8);pad1->SetLogy(1); hA->GetXaxis()->SetRangeUser(000,500); 	hA->SetMinimum(0.1);}
		else if(histName.Contains("Meff_")){hA->GetXaxis()->SetTitle("Event M_{eff} [GeV]"); hA->GetYaxis()->SetTitle("Number of events");hA->SetMinimum(0.8);pad1->SetLogy(1); hA->GetXaxis()->SetRangeUser(000,1000); 	hA->SetMinimum(0.1);}
		else if(histName.Contains("nJets_")){hA->SetMinimum(0);hA->GetXaxis()->SetTitle("Number of jets"); hA->GetYaxis()->SetTitle("Number of events");}
		else if(histName.Contains("Trig")){ hA->GetXaxis()->SetTitle("#Delta R between RoI Cluster and MS Vertex"); hA->SetMinimum(0.8);pad1->SetLogy(1); hA->GetXaxis()->SetRangeUser(0,1); 	hA->SetMinimum(0.1);}

		else if(histName.Contains("eta_")) hA->GetXaxis()->SetTitle("MS vertex #eta");
		else if(histName.Contains("phi_")) hA->GetXaxis()->SetTitle("MS vertex #phi");
		else if(histName.Contains("R_")) hA->GetXaxis()->SetTitle("MS vertex L_{xy} [m]");
		else if(histName.Contains("z_")) hA->GetXaxis()->SetTitle("MS vertex |L_{z}| [m]");
		else if(histName.Contains("nTrks")){ hA->GetXaxis()->SetTitle("Number of MS Tracklets");	hA->SetMinimum(0.1);pad1->SetLogy(1);	hA->SetMinimum(0.8);}
		else if(histName.Contains("nMDT")){ hA->GetXaxis()->SetTitle("Number of MDT hits");	hA->SetMinimum(0.1);pad1->SetLogy(1); hA->GetXaxis()->SetRangeUser(000,3000); 	hA->SetMinimum(0.1);}
		else if(histName.Contains("nRPC")){ hA->GetXaxis()->SetTitle("Number of RPC hits");	hA->SetMinimum(0.1);pad1->SetLogy(1); hA->GetXaxis()->SetRangeUser(000,3000); 	hA->SetMinimum(0.1);}
		else if(histName.Contains("nTGC")){ hA->GetXaxis()->SetTitle("Number of TGC hits");	hA->SetMinimum(0.1);pad1->SetLogy(1); hA->GetXaxis()->SetRangeUser(000,3000); 	hA->SetMinimum(0.1);}


		double maxVal = hB->GetMaximum(); if(hA->GetMaximum() > maxVal) maxVal = hA->GetMaximum();
		hA->SetMaximum(1.3*maxVal);
		hA->SetTitle("");

		TString append = "";

		TString title = "";
		if(histName.Contains("full")) title = TString("|#eta| < 2.7") + append;

		hA->GetYaxis()->SetLabelSize(0.070);
		hA->GetYaxis()->SetTitleSize(0.065);
		hA->GetYaxis()->SetTitleOffset(0.18);
		hA->GetXaxis()->SetTitleOffset(2.7);
		hA->GetXaxis()->SetLabelOffset(0.5);
		hA->GetYaxis()->SetTitleOffset(0.95);


		TH1 *h0_copy = (TH1D*)hA->Clone("noiso_copy");
		hA->Draw("HISTO");

		h0_copy->SetLineColor(kBlack);
		h0_copy->SetFillColor(kBlack);
		h0_copy->SetFillStyle(3001);
		h0_copy->SetMarkerSize(0);
		h0_copy->Draw("E2 SAME");

		TH1 *h1_copy = (TH1D*)hB->Clone("iso_copy");
		h1_copy->SetLineColor(kBlue);
		hB->Draw("HISTO SAME");

		h1_copy->SetLineColor(kBlue);
		h1_copy->SetFillColor(kBlue);
		h1_copy->SetFillStyle(3001);
		h1_copy->SetMarkerSize(0);
		h1_copy->Draw("E2 SAME");

		TLatex latex2;
		latex2.SetNDC();
		latex2.SetTextColor(kBlack);
		latex2.SetTextSize(0.06);
		latex2.SetTextAlign(31);  //align at bottom
		latex2.DrawLatex(0.95,.94,title);

		//c1->Print(plotDir+histName+".pdf");
		TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
		l.SetNDC();
		l.SetTextFont(72);
		l.SetTextSize(0.06);
		l.SetTextColor(kBlack);
		//l.DrawLatex(.72,.84,"ATLAS");
		TLatex p; 
		p.SetNDC();
		p.SetTextSize(0.06);
		p.SetTextFont(42);
		p.SetTextColor(kBlack);
		// p.DrawLatex(0.72+0.105,0.84,"Internal");


		c1->cd();

		TLegend *leg = new TLegend(0.6,0.81,0.92,0.92);
		leg->SetFillColor(0);
		leg->SetFillColor(0);
		leg->SetBorderSize(0);
		leg->SetTextSize(0.04);
		leg->AddEntry(h0_copy,"#font[42]{Non-isolated vertices}","lpf");
		leg->AddEntry(h1_copy,"#font[42]{Isolated vertices}","lpf");
		leg->Draw();
		pad2->cd();
		gStyle->SetPadTickX(1);
		gStyle->SetPadTickY(1);

		TH1D *p1_new = (TH1D*)hA->Clone("data_clone");

		p1_new->Divide(hB);


		p1_new->SetMarkerStyle(8);
		p1_new->SetMarkerSize(0.8);
		p1_new->SetMarkerColor(kBlack);
		p1_new->SetMinimum(0.0);
		p1_new->SetMaximum(5.0);
		//p1_new->GetXaxis()->SetTitle("to do");
		p1_new->GetXaxis()->SetLabelFont(42);
		p1_new->GetXaxis()->SetLabelSize(0.16);
		p1_new->GetXaxis()->SetLabelOffset(0.05);
		p1_new->GetXaxis()->SetTitleFont(42);
		p1_new->GetXaxis()->SetTitleSize(0.14);
		p1_new->GetXaxis()->SetTitleOffset(1.3);
		p1_new->GetYaxis()->SetNdivisions(505);
		p1_new->GetYaxis()->SetTitle("#font[42]{Noiso/Iso}");
		p1_new->GetYaxis()->SetLabelFont(42);
		p1_new->GetYaxis()->SetLabelSize(0.15);
		p1_new->GetYaxis()->SetTitleFont(42);
		p1_new->GetYaxis()->SetTitleSize(0.13);
		p1_new->GetYaxis()->SetTitleOffset(0.44); 
		p1_new->DrawCopy("eP");

		TLine* line = new TLine();
		line->DrawLine(p1_new->GetXaxis()->GetXmin(),1,p1_new->GetXaxis()->GetXmax(),1);

		TString fitVal = "";

		if (histName.Contains("Tracklet") && !histName.Contains("eta") && !histName.Contains("pT")){
			TF1 *f1 = new TF1("fit1", "pol0", 0, 12);
			p1_new->Fit("fit1","R");
			p1_new->GetXaxis()->SetRangeUser(0,12);
			fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );

		} else {
			p1_new->Fit("pol0","Q");
			//std::cout << hA->GetFunction("pol0")->GetParameter(0) << ", " << hA->GetFunction("pol0")->GetParError(0) << std::endl;
			fitVal = TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("pol0")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("pol0")->GetParError(0) );		
		}
		std::cout << fitVal << std::endl; 
		pad2->SetLogy(0);

		//latex2.DrawLatex(.15,.98,title + ", Data/MC");

		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(kBlack);
		latex.SetTextSize(0.1);
		latex.SetTextAlign(13);  //align at top
		latex.DrawLatex(.25,.92,fitVal);




		c1->Print(plotDir+"ratio_"+histName+".pdf");
		c1->cd();
		leg->Clear();
		leg->SetFillStyle(0);

	}*/


}

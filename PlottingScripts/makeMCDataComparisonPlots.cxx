
void makeMCDataComparisonPlots(){
    //0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    SetAtlasStyle();

    //c1->SetRightMargin(0.05);
    //c1->SetTopMargin(0.07);
    //c1->SetLeftMargin(0.15);
    //c1->SetBottomMargin(0.15);
    TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
    pad1->SetTopMargin(0.075);
    pad1->SetBottomMargin(0.04);
    pad1->SetLeftMargin(0.14);
    pad1->SetLogy();
    pad1->Draw();

    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.40);
    pad2->SetLeftMargin(0.14);
    pad2->Draw();



    //TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/SystematicsOutputs/JZ3W_OverlayStudies/JZ3WOverlay/outputMSSystematics_dv16.root");
    //TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/SystematicsOutputs/JZ3W_OverlayStudies/JZ3WStandard/outputMSSystematics_dv14.root");
    //TString plotDir = "../OutputPlots/SystematicsOutputs/JZ3W_OverlayStudies/";
    // TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/SystematicsOutputs/JZ3W_OverlayStudies/JZ3WOverlay/outputMSSystematics_dv16.root");
    //TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/SystematicsOutputs/JZ3W_OverlayStudies/JZ3WStandard/outputMSSystematics_dv16.root");
    TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/data/outputDataSearch_goodJetEventFlags.root");
    TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/bkgdJZ/JZAll/outputDataSearch_goodJetEventFlags.root");
    TString plotDir = "../OutputPlots/notePlots/DataMCComparison/";
    
    for (TObject* keyAsObj : *(_file1->GetListOfKeys())){
        auto key = dynamic_cast<TKey*>(keyAsObj);
        if((TString)key->GetClassName() == "TH2D") continue;
        pad1->cd(); 
        gStyle->SetPadTickX(2);
        gStyle->SetPadTickY(2);
        TH1D *h_MC = (TH1D*) key->ReadObj();
        TString histName = TString(h_MC->GetName());

        if(!histName.Contains("1j50_1j150_b") && !histName.Contains("1j100_1j250_ec") && !histName.Contains("2j150_b") && !histName.Contains("2j250_ec"))continue; 
        
        //if(!histName.Contains("full")) continue;
        TH1D *h_Data = (TH1D*)_file0->Get(histName);
        std::cout << "Key name: " << key->GetName() << " Type: " << key->GetClassName() << ", entries data/MC: " << h_Data->GetEntries()  <<"/" <<h_MC->GetEntries() <<  std::endl;
        //if(!histName.Contains("pMSVx") || !histName.Contains("pTrig")) continue;

        //if(histName.Contains("MuonRoI") || histName.Contains("pTrig")) continue;
        if(h_MC->GetEntries() == 0 || h_Data->GetEntries() == 0) continue;
        if(histName.Contains("HT_") || histName.Contains("Meff")) {h_MC->Rebin(10); h_Data->Rebin(10);}
        if(histName.Contains("Hits")){h_MC->Rebin(4); h_Data->Rebin(4);}
        pad1->SetLogy(1); h_Data->SetMinimum(0.01);
        h_Data->SetLineColor(kBlack);h_Data->SetMarkerColor(kBlack);h_Data->SetMarkerSize(1.2);h_Data->SetLineWidth(2);h_Data->SetMarkerStyle(20);
        h_MC->SetLineColor(kBlue+2);h_MC->SetMarkerColor(kBlue+2);h_MC->SetMarkerSize(1.2);h_MC->SetLineWidth(2);h_MC->SetMarkerStyle(4);	
        h_Data->GetYaxis()->SetTitle("Number of events");

        h_MC->Scale(1.636); //because jz weighting is for MD2 still
        
        double maxVal = h_MC->GetMaximum(); if(h_Data->GetMaximum() > maxVal) maxVal = h_Data->GetMaximum();
        h_Data->SetMaximum(1.3*maxVal);
        h_Data->SetTitle("");

        if(histName.Contains("HT_")){h_Data->GetXaxis()->SetTitle("Event H_{T} [GeV]");}
        else if(histName.Contains("HTMiss")){h_Data->GetXaxis()->SetTitle("Event H_{T}^{miss} [GeV]");}
        else if(histName.Contains("Meff")){h_Data->GetXaxis()->SetTitle("Event M_{eff} [GeV]");}
        else if(histName.Contains("Hits")){h_Data->GetXaxis()->SetTitle("vertex(nMDT + nTrig hits)");}
        else if(histName.Contains("Iso")){h_Data->GetXaxis()->SetRangeUser(0,4);h_Data->GetXaxis()->SetTitle("min(#Delta R(closest jet), #Delta R(closest track))");}
        
        TString append = "";


        TString title = "";
        
        std::vector<std::vector<TString>>  histLabels = {{"#splitline{E_{T,1} > 150 GeV,}{150 GeV > E_{T,2} > 50 GeV}", "#splitline{E_{T,1} > 150 GeV,}{E_{T,2} > 150 GeV}"},{"#splitline{E_{T,1} > 250 GeV,}{250 GeV > E_{T,2} > 100 GeV}", "#splitline{E_{T,1} > 250 GeV,}{E_{T,2} > 250 GeV}"}};

          
        
        if(histName.Contains("1j50_1j150")) title = histLabels.at(0).at(0) + append;
        else if(histName.Contains("2j150")) title = histLabels.at(0).at(1) + append;
        else if(histName.Contains("1j100_1j250")) title = histLabels.at(1).at(0) + append;
        else if(histName.Contains("2j250")) title = histLabels.at(1).at(1) + append;


        h_Data->GetYaxis()->SetLabelSize(0.070);
        h_Data->GetYaxis()->SetTitleSize(0.065);
        h_Data->GetYaxis()->SetTitleOffset(0.18);
        h_Data->GetXaxis()->SetTitleOffset(2.7);
        h_Data->GetXaxis()->SetLabelOffset(0.5);
        h_Data->GetYaxis()->SetTitleOffset(0.95);

        h_Data->Draw();
        h_MC->Draw("SAME");

        TLegend *leg = new TLegend(0.65,0.75,0.85,0.9);
        leg->SetFillColor(0);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.05);
        leg->AddEntry(h_MC,"Pythia multi-jet MC","lp");
        leg->AddEntry(h_Data,"Data","lp");
        leg->Draw();

        TLatex latex2;
        latex2.SetNDC();
        latex2.SetTextColor(kBlack);
        latex2.SetTextSize(0.05);
        latex2.SetTextAlign(33);
        latex2.DrawLatex(0.9,0.75,title);
        if(histName.Contains("2j250_ec")) append = "Endcap SR"; 
        else if(histName.Contains("2j150_b")) append = "Barrel SR"; 
        else if(histName.Contains("1j100_2j250_ec")) append = "Endcap VR"; 
        else if(histName.Contains("1j50_1j150_b")) append = "Barrel VR"; 
        
        latex2.DrawLatex(0.9,0.6,append);
        
        //c1->Print(plotDir+histName+".pdf");
        TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
        l.SetNDC();
        l.SetTextFont(72);
        l.SetTextSize(0.06);
        l.SetTextColor(kBlack);
        l.DrawLatex(.4,.84,"ATLAS");
        TLatex p; 
        p.SetNDC();
        p.SetTextSize(0.06);
        p.SetTextFont(42);
        p.SetTextColor(kBlack);
         p.DrawLatex(0.4+0.105,0.84,"Internal");
        
        c1->cd();

        pad2->cd();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);

        TH1D *p1_new = (TH1D*)h_Data->Clone("data_clone");

        p1_new->Divide(h_MC);


        p1_new->SetMarkerStyle(8);
        p1_new->SetMarkerSize(0.8);
        p1_new->SetMarkerColor(kBlack);
        p1_new->SetMinimum(0.0);
        p1_new->SetMaximum(2.0);
        //p1_new->GetXaxis()->SetTitle("to do");
        p1_new->GetXaxis()->SetLabelFont(42);
        p1_new->GetXaxis()->SetLabelSize(0.16);
        p1_new->GetXaxis()->SetLabelOffset(0.05);
        p1_new->GetXaxis()->SetTitleFont(42);
        p1_new->GetXaxis()->SetTitleSize(0.14);
        p1_new->GetXaxis()->SetTitleOffset(1.3);
        p1_new->GetYaxis()->SetNdivisions(505);
        p1_new->GetYaxis()->SetTitle("#font[42]{Data/MC}");
        p1_new->GetYaxis()->SetLabelFont(42);
        p1_new->GetYaxis()->SetLabelSize(0.15);
        p1_new->GetYaxis()->SetTitleFont(42);
        p1_new->GetYaxis()->SetTitleSize(0.13);
        p1_new->GetYaxis()->SetTitleOffset(0.44); 
        p1_new->DrawCopy("eP");

        TLine* line = new TLine();
        line->DrawLine(p1_new->GetXaxis()->GetXmin(),1,p1_new->GetXaxis()->GetXmax(),1);

        TString fitVal = "";
        TString fitVal2 = "";
        double minFit =0;// (histName.Contains("eta1")) ? 1500 : 1200; 
        if (histName.Contains("Tracklet") && !histName.Contains("eta") && !histName.Contains("pT") && !histName.Contains("nMSeg")){
            TF1 *f1 = new TF1("fit1", "pol0", 0, 12);
            p1_new->Fit("fit1","R");
            p1_new->GetXaxis()->SetRangeUser(0,12);
            fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
            //fitVal2 =  TString("slope: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(1) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(1) );

        } else if(histName.Contains("MDT")){
            TF1 *f1 = new TF1("fit1", "pol0", 300, 3000);
            p1_new->Fit("fit1","R");
            p1_new->GetXaxis()->SetRangeUser(300,3000); 
            fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
            //fitVal2 =  TString("slope: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(1) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(1) );
        } else if(histName.Contains("_pT_")){
            TF1 *f1 = new TF1("fit1", "pol0", minFit, 5000);
            p1_new->Fit("fit1","R");
            p1_new->GetXaxis()->SetRangeUser(0,5000); 
            fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
            //fitVal2 =  TString("slope: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(1) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(1) );
        }  else {
            p1_new->Fit("pol0","Q");
            //std::cout << h_Data->GetFunction("pol0")->GetParameter(0) << ", " << h_Data->GetFunction("pol0")->GetParError(0) << std::endl;
            //std::cout << h_Data->GetFunction("pol0")->GetParameter(0) << ", " << h_Data->GetFunction("pol0")->GetParError(0) << std::endl;
            fitVal = TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("pol0")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("pol0")->GetParError(0) );		
            //fitVal2 = TString("slope: " ) + TString(std::to_string( p1_new->GetFunction("pol0")->GetParameter(1) ) ) + "+/-" + std::to_string( p1_new->GetFunction("pol0")->GetParError(1) );		

        }
        std::cout << fitVal << std::endl; 
        pad2->SetLogy(0);

        //latex2.DrawLatex(.15,.98,title + ", Data/MC");

        TLatex latex;
        latex.SetNDC();
        latex.SetTextColor(kBlack);
        latex.SetTextSize(0.1);
        latex.SetTextAlign(13);  //align at top
        latex.DrawLatex(.25,.92, fitVal);
        //latex.DrawLatex(.25,.82, fitVal2);

        c1->Print(plotDir+"ratio_"+histName+".pdf");
        c1->Print(plotDir+"ratio_"+histName+".png");
    }


}

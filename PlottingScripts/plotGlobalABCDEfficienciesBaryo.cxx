#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>

double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-12) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }

    Double_t arg = 1.0 - ( x - peak ) * tail / width;

    if (arg < 1.e-12) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);

    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )

    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );

    return TMath::Exp(exponent) ;
}
double novosibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}

TString DecChannel(TString benchmark){
  TString string;
  if (benchmark.Contains("nubb"))   string = "#chi #rightarrow #nubb";
  if (benchmark.Contains("cbs"))    string = "#chi #rightarrow cbs";
  if (benchmark.Contains("lcb"))    string = "#chi #rightarrow lcb";
  if (benchmark.Contains("tatanu")) string = "#chi #rightarrow #tau#tau#nu";
  return string;
}

void plotGlobalABCDEfficienciesBaryo(){
 
    // ** For Baryogenesis
    /*
    const int nSamples=4;
    std::vector<TString> benchmark = {"HChiChi_nubb_mH125mChi10"  ,"HChiChi_nubb_mH125mChi30"  ,"HChiChi_nubb_mH125mChi50"  ,"HChiChi_nubb_mH125mChi100"};
    //std::vector<TString> benchmark = {"HChiChi_cbs_mH125mChi10"   ,"HChiChi_cbs_mH125mChi30"   ,"HChiChi_cbs_mH125mChi50"   ,"HChiChi_cbs_mH125mChi100"};
    //std::vector<TString> benchmark = {"HChiChi_lcb_mH125mChi10"   ,"HChiChi_lcb_mH125mChi30"   ,"HChiChi_lcb_mH125mChi50"   ,"HChiChi_lcb_mH125mChi100"};
    //std::vector<TString> benchmark = {"HChiChi_tatanu_mH125mChi10","HChiChi_tatanu_mH125mChi30","HChiChi_tatanu_mH125mChi50","HChiChi_tatanu_mH125mChi100"};
    TString globalLabel = {"nubb"};
    //TString globalLabel = {"cbs"};
    //TString globalLabel = {"lcb"};
    //TString globalLabel = {"tatanu"};

    std::vector<TString> scalarmass = {"10","30","50","100"};
    int colors[nSamples] = {kViolet+7, kAzure+7, kTeal, kSpring-2};
    double propLT[nSamples]={920,2750,5550,3500};
    */
    /*
    double nEvents_B[nSamples]={4332,3695,1845,12162};
    double nEvents_E[nSamples]={8683,5273,3322,18235};
    */
    /*
    double nEvents_B[nSamples]={4280,3663,1860,12139};
    double nEvents_E[nSamples]={8561,5206,3318,17809};
    */
    /*
    double nEvents_B[nSamples]={3324,2643,1050,8125};
    double nEvents_E[nSamples]={7514,4872,2780,14390};
    */
    /*
    double nEvents_B[nSamples]={2749,2256 ,973, 8069};
    double nEvents_E[nSamples]={6719,4092,2441,14080};
    */
    /*
    // No bkg flags
    double nEvents_B[nSamples]={2793,2330,1003, 8464};
    double nEvents_E[nSamples]={6911,4198,2517,14662};
    */
    
    const int nSamples=3;
    std::vector<TString> benchmark = {"HChiChi_nubb_mH125mChi10"  ,"HChiChi_nubb_mH125mChi30"  ,"HChiChi_nubb_mH125mChi50"};
    //std::vector<TString> benchmark = {"HChiChi_cbs_mH125mChi10"   ,"HChiChi_cbs_mH125mChi30"   ,"HChiChi_cbs_mH125mChi50"};
    //std::vector<TString> benchmark = {"HChiChi_lcb_mH125mChi10"   ,"HChiChi_lcb_mH125mChi30"   ,"HChiChi_lcb_mH125mChi50"};
    TString globalLabel = {"nubb"};
    //TString globalLabel = {"cbs"};
    //TString globalLabel = {"lcb"};
    //
    std::vector<TString> scalarmass = {"10","30","50"};
    int colors[nSamples] = {kViolet+7, kAzure+7, kTeal};
    double propLT[nSamples]={920,2750,5550};

    // No bkg flags
    double nEvents_B[nSamples]={2793,2330,1003};
    double nEvents_E[nSamples]={6911,4198,2517};

    double scaling =  32864. + 3212.96; //lumi from doLimitExtrapolation, because prodXSs are set to 1.0 in SampleDetails.h and scaling is LUMI*SampleDetails::mediatorXS/nEvents
    //can plot these ones as global number of expected events/BR[H->ss] 

    TCanvas *c1 = new TCanvas("c1","c1",800,600);

    TFile* tfile[nSamples];
    TH1D* thist[nSamples];
    TH1D* thist2[nSamples];
    TH1D* thist3[nSamples];
    TH1D *thist_up[nSamples];
    TH1D *thist2_up[nSamples];
    TH1D *thist3_up[nSamples];
    TH1D *thist_down[nSamples];
    TH1D *thist2_down[nSamples];
    TH1D *thist3_down[nSamples];
    TF1* fn_up[nSamples];
    TF1* fn_down[nSamples];
    TF1* fn3_up[nSamples];
    TF1* fn2_up[nSamples];
    TF1* fn2_down[nSamples];
    TF1* fn3_down[nSamples];
    TF1* fn[nSamples];
    TF1* fn2[nSamples];
    TF1* fn3[nSamples];
    TGraph* fnb[nSamples];
    TGraph* fn2b[nSamples];
    TGraph* fn3b[nSamples];
    TGraphAsymmErrors* g_err1[nSamples];
    TGraphAsymmErrors* g_err2[nSamples];
    TGraphAsymmErrors* g_err3[nSamples];

    //double maxVals[nSamples] = {0,0,0,0};
    double maxVals[nSamples] = {0.0074,0.07,0.0023};
    double maxValGraph=-99;
    char name1[10];
    for(int i=0; i<nSamples; i++){

      TString add_str="";
      add_str="_regA";

      //tfile[i] = TFile::Open("../OutputPlots/signalMC/extrapolation/"+benchmark.at(i)+add_str+".root");
      tfile[i] = TFile::Open("../OutputPlots/signalMC/extrapolation/Files_NOLTeff/"+benchmark.at(i)+add_str+".root");
        
        thist[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx");
	thist_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_maxStat");
        thist_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_minStat");
        //thist_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_maxTotal");
        //thist_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_minTotal");
        thist2[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx");
        thist2_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_maxStat");
        thist2_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_minStat");
        //thist2_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_maxTotal");
        //thist2_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_minTotal");
        thist3[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx");
        thist3_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_maxStat");
        thist3_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_minStat");
	//thist3_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_maxTotal");
        //thist3_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_minTotal");
        /*
        thist[i]->Scale(1./scalings.at(i));
        thist_up[i]->Scale(1./scalings.at(i));
        thist_down[i]->Scale(1./scalings.at(i));
        thist2[i]->Scale(1./scalings.at(i));
        thist3[i]->Scale(1./scalings.at(i));
        thist2_up[i]->Scale(1./scalings.at(i));
        thist3_up[i]->Scale(1./scalings.at(i));
        thist2_down[i]->Scale(1./scalings.at(i));
        thist3_down[i]->Scale(1./scalings.at(i));
        */
	double p0=-0.3;
	double p1=0.4;
	double p2=-0.2;
	if (benchmark.at(i).Contains("tatanu") && i==2){
	  p0=0.47;
	  p1=0.4;
	  p2=0.15;
	}
        sprintf(name1,"fn_%d",i);
	fn[i] = new TF1(name1,novosibirsk,0.03,1000,4);
	cout << "qui" << thist[i] <<  endl;
        fn[i]->SetParameter(0,thist[i]->GetMaximum());
        fn[i]->SetParameter(1,p0); //fn->SetParLimits(1,-1,2);
        fn[i]->SetParameter(2,p1); //fn->SetParLimits(2,0,2);
        fn[i]->SetParameter(3,p2); //fn->SetParLimits(3,0,20.0);
        thist[i]->Fit( fn[i], "N WRV" );
        sprintf(name1,"fn_up_%d",i);
	fn_up[i] = new TF1(name1,novosibirsk,0.03,1000,4);
        fn_up[i]->SetParameter(0,thist_up[i]->GetMaximum());
        fn_up[i]->SetParameter(1,p0); //fn_up->SetParLimits(1,-1,2);
        fn_up[i]->SetParameter(2,p1); //fn_up->SetParLimits(2,0,2);
        fn_up[i]->SetParameter(3,p2); //fn_up->SetParLimits(3,0,20.0);
        thist_up[i]->Fit( fn_up[i], "N WRV" );
        sprintf(name1,"fn_down_%d",i);
	fn_down[i] = new TF1(name1,novosibirsk,0.03,1000,4);
        fn_down[i]->SetParameter(0,thist_down[i]->GetMaximum());
        fn_down[i]->SetParameter(1,p0); //fn_down->SetParLimits(1,-1,2);
        fn_down[i]->SetParameter(2,p1); //fn_down->SetParLimits(2,0,2);
        fn_down[i]->SetParameter(3,p2); //fn_down->SetParLimits(3,0,20.0);
        thist_down[i]->Fit( fn_down[i], "N WRV" );

        sprintf(name1,"fn2_%d",i);
	fn2[i] = new TF1(name1,novosibirsk,0.03,1000,4);
        fn2[i]->SetParameter(0,thist2[i]->GetMaximum());
        fn2[i]->SetParameter(1,p0); //fn->SetParLimits(1,-1,2);
        fn2[i]->SetParameter(2,p1); //fn->SetParLimits(2,0,2);
        fn2[i]->SetParameter(3,p2); //fn->SetParLimits(3,0,20.0);
        thist2[i]->Fit( fn2[i], "N WRV" );

        sprintf(name1,"fn2_up_%d",i);
	fn2_up[i] = new TF1(name1,novosibirsk,0.03,1000,4);
        fn2_up[i]->SetParameter(0,thist2_up[i]->GetMaximum());
        fn2_up[i]->SetParameter(1,p0); //fn_up->SetParLimits(1,-1,2);
        fn2_up[i]->SetParameter(2,p1); //fn_up->SetParLimits(2,0,2);
        fn2_up[i]->SetParameter(3,p2); //fn_up->SetParLimits(3,0,20.0);
        thist2_up[i]->Fit( fn2_up[i], "N WRV" );
        sprintf(name1,"fn2_down_%d",i);
	fn2_down[i] = new TF1(name1,novosibirsk,0.03,1000,4);
        fn2_down[i]->SetParameter(0,thist2_down[i]->GetMaximum());
        fn2_down[i]->SetParameter(1,p0); //fn_down->SetParLimits(1,-1,2);
        fn2_down[i]->SetParameter(2,p1); //fn_down->SetParLimits(2,0,2);
        fn2_down[i]->SetParameter(3,p2); //fn_down->SetParLimits(3,0,20.0);
        thist2_down[i]->Fit( fn2_down[i], "N WRV" );

        sprintf(name1,"fn3_%d",i);
	fn3[i] = new TF1(name1,novosibirsk,0.03,1000,4);
        fn3[i]->SetParameter(0,thist3[i]->GetMaximum());
        fn3[i]->SetParameter(1,p0); //fn->SetParLimits(1,-1,2);
        fn3[i]->SetParameter(2,p1); //fn->SetParLimits(2,0,2);
        fn3[i]->SetParameter(3,p2); //fn->SetParLimits(3,0,20.0);
        thist3[i]->Fit( fn3[i], "N WRV" );

        sprintf(name1,"fn3_up_%d",i);
	fn3_up[i] = new TF1(name1,novosibirsk,0.03,1000,4);
        fn3_up[i]->SetParameter(0,thist3_up[i]->GetMaximum());
        fn3_up[i]->SetParameter(1,p0); //fn_up->SetParLimits(1,-1,2);
        fn3_up[i]->SetParameter(2,p1); //fn_up->SetParLimits(2,0,2);
        fn3_up[i]->SetParameter(3,p2); //fn_up->SetParLimits(3,0,20.0);
        thist3_up[i]->Fit( fn3_up[i], "N WRV" );
	thist3_up[i]->Fit( fn3_up[i], "N WRV" );
	thist3_up[i]->Fit( fn3_up[i], "N WRV" );
        sprintf(name1,"fn3_down_%d",i);
	fn3_down[i] = new TF1(name1,novosibirsk,0.03,1000,4);
        fn3_down[i]->SetParameter(0,thist3_down[i]->GetMaximum());
        fn3_down[i]->SetParameter(1,p0); //fn_down->SetParLimits(1,-1,2);
        fn3_down[i]->SetParameter(2,p1); //fn_down->SetParLimits(2,0,2);
        fn3_down[i]->SetParameter(3,p2); //fn_down->SetParLimits(3,0,20.0);
        thist3_down[i]->Fit( fn3_down[i], "N WRV" );
        thist3_down[i]->Fit( fn3_down[i], "N WRV" );
        thist3_down[i]->Fit( fn3_down[i], "N WRV" );


	const int nPoints=1000;
        Double_t ctau[nPoints];
        Double_t eff[nPoints];
        Double_t eff1[nPoints];
        Double_t eff2[nPoints];
        Double_t errUp[nPoints];
        Double_t errDown[nPoints];
        Double_t errUp1[nPoints];
        Double_t errDown1[nPoints];
        Double_t errUp2[nPoints];
        Double_t errDown2[nPoints];
        int ntot = 0;
        TGraph *g_res = new TGraph(thist[i]);
        
        for (Int_t j = 0; j < 1000; j++) {
            Double_t life, nev, nev_up, nev_down, nev1_up, nev1_down, nev2_up, nev2_down;
            g_res->GetPoint(j, life, nev);
            nev = fn3[i]->Eval(life);
            nev_up = fn3_up[i]->Eval(life) - fn3[i]->Eval(life);
            nev_down = fn3[i]->Eval(life) - fn3_down[i]->Eval(life);
            nev1_up = fn_up[i]->Eval(life)-fn[i]->Eval(life);
            nev1_down = fn[i]->Eval(life)-fn_down[i]->Eval(life);
            nev2_up = fn2_up[i]->Eval(life)-fn2[i]->Eval(life);
            nev2_down = fn2[i]->Eval(life)-fn2_down[i]->Eval(life);
            if (nev == 0) {
                std::cout << "Continue because nev= " << nev << std::endl;
                continue;
            }
	    /*
            if(nev > maxVals[i]) maxVals[i] = nev;
	    if (nev>maxValGraph) maxValGraph=nev;
            eff[ntot] = nev;
            eff1[ntot] = fn[i]->Eval(life);
            eff2[ntot] = fn2[i]->Eval(life);
            errUp[ntot] = nev_up;
            errDown[ntot] = nev_down;
            errUp1[ntot] = nev1_up;
            errDown1[ntot] = nev1_down;
            errUp2[ntot] = nev2_up;
            errDown2[ntot] = nev2_down;
            ctau[ntot] = life;
            ntot++;
	    */
            if(nev/scaling > maxVals[i]) maxVals[i] = nev/scaling;
	    if (nev/scaling>maxValGraph) maxValGraph=nev/scaling;
            eff[ntot] = nev/scaling;
            eff1[ntot] = fn[i]->Eval(life)/scaling;
            eff2[ntot] = fn2[i]->Eval(life)/scaling;
            errUp[ntot] = nev_up/scaling;
            errDown[ntot] = nev_down/scaling;
            errUp1[ntot] = nev1_up/scaling;
            errDown1[ntot] = nev1_down/scaling;
            errUp2[ntot] = nev2_up/scaling;
            errDown2[ntot] = nev2_down/scaling;
            ctau[ntot] = life;
            ntot++;
        }
        fnb[i]  = new TGraph(ntot, ctau,eff1);
	fn2b[i] = new TGraph(ntot, ctau,eff2);
        fn3b[i] = new TGraph(ntot, ctau,eff);
        fn3b[i]->SetLineColor(colors[i]);
        fn3b[i]->SetLineStyle(i+1);
        fn3b[i]->SetLineWidth(3);
        g_err3[i] = new TGraphAsymmErrors(ntot, ctau, eff);
        g_err1[i] = new TGraphAsymmErrors(ntot, ctau, eff1);
        g_err2[i] = new TGraphAsymmErrors(ntot, ctau, eff2);
        for(int j=0;j<ntot; j++){
            g_err3[i]->SetPointEYlow(j, errDown[j]);
            g_err3[i]->SetPointEYhigh(j, errUp[j]);
            g_err1[i]->SetPointEYlow(j, errDown1[j]);
            g_err1[i]->SetPointEYhigh(j, errUp1[j]);
            g_err2[i]->SetPointEYlow(j, errDown2[j]);
            g_err2[i]->SetPointEYhigh(j, errUp2[j]);
        }
        g_err1[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err2[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err3[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err1[i]->SetLineWidth(0);
        g_err2[i]->SetLineWidth(0);
        g_err3[i]->SetLineWidth(0);
    }
    //fn3b[3]->SetLineColor(98);
    c1->SetLogx();
    //fn3b[0]->Draw("AL");
    fn3b[0]->GetYaxis()->SetTitleOffset(1.6);
    fn3b[0]->GetYaxis()->SetRangeUser(0,1.3*maxValGraph);
    fn3b[0]->GetXaxis()->SetLimits(0.03,300);
    //fn3b[0]->GetXaxis()->SetLimits(0.03,1000);
    fn3b[0]->GetXaxis()->SetTitle("#chi proper lifetime (c*#tau) [m]");
    fn3b[0]->GetYaxis()->SetTitle("Global efficiency");
    fn3b[0]->Draw("AL");
    g_err3[0]->Draw("c3 SAME");

    c1->Update();
    for(int i=1; i<nSamples; i++){
        g_err3[i]->Draw("c3 SAME");
        fn3b[i]->Draw("LSAME");
    }

    TLegend *leg = new TLegend(0.65,0.70,0.99,0.91);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.04);
    leg->AddEntry(fn3b[0],"m_{#chi} = 10 GeV","l");
    leg->AddEntry(fn3b[1],"m_{#chi} = 30 GeV","l");
    leg->AddEntry(fn3b[2],"m_{#chi} = 50 GeV","l");
    //leg->AddEntry(fn3b[3],"m_{#chi} = 100 GeV","l");
    leg->Draw();

    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.05);
    latex2.SetTextAlign(13);  //align at top
    latex2.DrawLatex(.2,.92,"#font[72]{ATLAS}  Internal");
    latex2.DrawLatex(.2,.86,DecChannel(benchmark.at(0)));

    c1->Print("../OutputPlots/signalMC/extrapolation/GlobalEfficiencies_Combined_ABCD_Baryogenesis_"+globalLabel+".pdf");


    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.045);
    latex.SetTextAlign(13);  //align at top

    TLine line1; line1.SetLineWidth(1);
    TLine line2; line2.SetLineWidth(1);
    TLine line3; line3.SetLineWidth(1);


    for(int i=0;i<nSamples;i++){
        fnb[i]->SetLineColor(kRed+1);
        fn2b[i]->SetLineColor(kBlue-3);
        fn3b[i]->SetLineColor(kViolet-6);
        g_err3[i]->SetFillColorAlpha(kViolet-6,0.2);
        g_err1[i]->SetFillColorAlpha(kRed+1,0.2);
        g_err2[i]->SetFillColorAlpha(kBlue-3,0.2);
        fnb[i]->SetLineStyle(2); fn2b[i]->SetLineStyle(7);
        fnb[i]->SetLineWidth(3); fn2b[i]->SetLineWidth(3);
        fn3b[i]->SetLineStyle(1);

        //maxVals[0] = 0.006/1.5;
        //fn3b[i]->GetYaxis()->SetRangeUser(0,1.4*maxVals[i]);
	fn3b[i]->GetYaxis()->SetRangeUser(0,maxVals[i]);
        fn3b[i]->Draw("AL");
        fn3b[i]->GetYaxis()->SetTitleOffset(1.6);
        fn3b[i]->GetXaxis()->SetLimits(0.03,300);
	//fn3b[i]->GetXaxis()->SetLimits(0.03,1000);
	fn3b[i]->GetXaxis()->SetTitle("#chi proper lifetime (c*#tau) [m]");
        //fn3b[i]->GetYaxis()->SetRangeUser(0,1.5*maxVals[i]);
	fn3b[i]->GetYaxis()->SetRangeUser(0,maxVals[i]);
        fn3b[i]->GetYaxis()->SetTitle("Global efficiency");

        fn3b[i]->Draw("AL");
        g_err3[i]->Draw("c3 same");
        fn3b[i]->Draw("L SAME");
        c1->Update();
        g_err1[i]->Draw("c3 same");
        fnb[i]->Draw("LSAME");
        g_err2[i]->Draw("c3 same");
        fn2b[i]->Draw("LSAME");
        TLegend *leg2 = new TLegend(0.58,0.76,0.95,0.91);
        leg2->SetFillStyle(0);
        leg2->SetBorderSize(0);
        leg2->AddEntry(fnb[i],"Barrel MS vertex ","l");
        leg2->AddEntry(fn2b[i],"Endcap MS vertex","l");
        leg2->AddEntry(fn3b[i],"Sum","l");
        leg2->Draw();
	latex.DrawLatex(.2,.80,"m_{H},m_{#chi} = [125,"+scalarmass.at(i)+"] GeV");
	latex.DrawLatex(.2,.86,DecChannel(benchmark.at(0)));
        latex2.DrawLatex(.2,.92,"#font[72]{ATLAS}  Internal");
	propLT[i]/=1000; 
	//line1.DrawLine(propLT[i],0,propLT[i],nEvents_B[i]+nEvents_E[i]+100);
	//line2.DrawLine(0,nEvents_B[i],propLT[i]+0.3,nEvents_B[i]);
	//line3.DrawLine(0,nEvents_E[i],propLT[i]+0.3,nEvents_E[i]);
        c1->Print("../OutputPlots/signalMC/extrapolation/GlobalEfficiency_ABCDChannel_"+benchmark.at(i)+".pdf");

    }

    TFile *fOut = new TFile("test.root","RECREATE");
    for(int i=0;i<nSamples;i++){
      g_err3[i]->Write();
      fn3b[i]->Write();
    }
    fOut->Write();
    fOut->Close();

    return;
}

//  LocalWords:  scalings

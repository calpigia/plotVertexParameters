//
//  makeTriggerStudyPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 01/10/16.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include "TMultiGraph.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLine.h"
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    int numberOfSlashes = 0; int tmpSlashNumber = 0;
    int indexOf2ndLastSlash = -1;
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            numberOfSlashes++;
        }
    }
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            tmpSlashNumber++;
        }
        if(tmpSlashNumber == (numberOfSlashes - 1)){
            indexOf2ndLastSlash = charN;
            break;
        }
    }
    fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 36 - indexOf2ndLastSlash - 1);
    //fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 22 - indexOf2ndLastSlash - 1);
    std::cout << "file name : " << fileName << std::endl;
    TString title = "";
    
    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type, bool forThesis);
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type, bool forThesis = false){
    bool add_ctau = false;
    TString sigNames[6];
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=1; i<nSIG; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}
    sigNames[0] = "SM multi-jets";
    //Int_t colorSig[16] = { kPink-3,kViolet-3,kBlue-3, kAzure+5, kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};
    Int_t colorSig[16] = { kViolet-3,kAzure+5,kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5};
    if(nSIG < 4){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
    }
    //Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};
    Int_t markerSig[16] = {24,26,25,27,28,30,26,27,24,25,26,27,24,25,26,27};
    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};
    
    TString names[12];
    names[0] = "LLP_trig_dR_B";
    names[1] = "LLP_trig_dR_E";
    names[2] = "Trig_jet_dR_B";
    names[3] = "Trig_jet_dR_E";
    names[4] = "Trig_trk_dR_B";
    names[5] = "Trig_trk_dR_E";
    
    //SetAtlasStyle();
    //gStyle->SetPalette(1);
    
    std::cout << "running on : " << _sig[0]->GetName() << std::endl;
    
    //TCanvas* c = new TCanvas("c_1","c_1",800,600);
    TCanvas* c = new TCanvas("c_1","c_1",700,600);
    //c->SetLogy();
    
    //vertex reco
    for(unsigned int i=0;i<6; i++){
        
        std::cout << "plot: " << names[i] << std::endl;
        
        TH1D* h_sig[4];
        

        TLegend *legS = new TLegend(0.6,0.7,0.96,0.93);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);
        double maxYVal = 0;
        for(int j=0; j<nSIG; j++){
	    if(i < 2 && j==0) continue;
            h_sig[j] = (TH1D*) _sig[j]->Get(names[i]);
            if(!h_sig[j] ) continue;
  	    h_sig[j]->Rebin(4);
            if(j==0){
                h_sig[j]->SetFillColor(kGray+1);
                h_sig[j]->SetLineColor(kGray+1);
            }else{
                h_sig[j]->SetMarkerColor(colorSig[j-1]);
                h_sig[j]->SetLineColor(colorSig[j-1]);
                h_sig[j]->SetMarkerStyle(markerSig[j-1]);
            }
            if( (j== 0 && i > 1) || i < 2) h_sig[j]->Scale(1./h_sig[j]->Integral());
	    else h_sig[j]->Scale(10./h_sig[j]->Integral());

            if(h_sig[j]->GetMaximum() > maxYVal){ maxYVal = h_sig[j]->GetMaximum(); }
            
            if(names[i].Contains("LLP")){
                h_sig[j]->GetXaxis()->SetTitle("#Delta R_{min}(LLP, muon RoI cluster)");
                h_sig[j]->GetXaxis()->SetRangeUser(0,1);
            }
            else if(names[i].Contains("jet")){ 
		h_sig[j]->GetXaxis()->SetTitle("#Delta R_{min}(muon RoI cluster, jet)");
                h_sig[j]->GetXaxis()->SetRangeUser(0,3);}
            else if(names[i].Contains("trk")){
                h_sig[j]->GetXaxis()->SetRangeUser(0,3);
                h_sig[j]->GetXaxis()->SetTitle("#Delta R_{min}(muon RoI cluster, track)");
            }
            h_sig[j]->GetYaxis()->SetTitle("Fraction of muon RoI clusters");
            

            if(j > 0 && i > 1) legS->AddEntry(h_sig[j],"("+sigNames[j]+")x10","lp");
            else if (j==0) legS->AddEntry(h_sig[j],sigNames[j], "f");
            else legS->AddEntry(h_sig[j], sigNames[j], "lp");

            if(j == 0){ h_sig[j]->Draw("HIST");}
            else if(i < 2 && j == 1) h_sig[j]->Draw("");
	    else h_sig[j]->Draw("SAME");
            
	    h_sig[j]->SetMaximum(maxYVal*1.5);	
	    h_sig[j]->SetMinimum(0);

            if(j == nSIG-1){
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                if(!forThesis){
                    y_val_label = 0.86;
                    latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");
                }
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("_B")){latex2.DrawLatex(.18,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 1.0)}");}
                else if(names[i].Contains("_E") ){latex2.DrawLatex(.18,y_val_label,"#font[52]{Endcaps }#font[42]{(1.0 < |#eta| < 2.5)}");}
                
                legS->Draw();
                gPad->RedrawAxis();
                
                c->Print("../OutputPlots/thesisPlots/"+names[i]+"_"+type+".pdf");
                c->Print("../OutputPlots/thesisPlots/"+names[i]+"_"+type+".png");
                c->Print("../OutputPlots/thesisPlots/"+names[i]+"_"+type+".root");
                legS->Clear();
            }
            
        }
    }
}


int makeTriggerStudyPlots(bool forThesis = false){
    
    
    //gROOT->LoadMacro("AtlasUtils.C");
    //gROOT->LoadMacro("AtlasLabels.C");
    //gROOT->LoadMacro("AtlasStyle.C");
    
    bool add_ctau = false;
    
    //_bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
    //_bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
    TFile *_sig[7];
/*    TString locn = "/Users/hrussell/Work/Run2Plots/signalMC/";
    
     _sig[1] = new TFile(locn+"mg250/outputMSEff.root");
     //_sig[1] = new TFile(locn+"mg500/outputMSEff.root");
     _sig[2] = new TFile(locn+"mg800/outputMSEff.root");
     //_sig[3] = new TFile(locn+"mg1200/outputMSEff.root");
     //_sig[4] = new TFile(locn+"mg1500/outputMSEff.root");
     _sig[3] = new TFile(locn+"mg2000/outputMSEff.root");
     _sig[0] = new TFile(locn+"bkgdJZ/JZAll/outputMSEff_forTrigParams.root")
     
     makeCombinedPlot(_sig, 4, "stealth", forThesis);

    }*/
    
    TString locn = "../OutputPlots/signalMC/";
    
     _sig[1] = new TFile(locn+"mg250/outputMSTrigEff_40TrackletCut.root");
     //_sig[1] = new TFile(locn+"mg500/outputMSEff.root");
     _sig[2] = new TFile(locn+"mg800/outputMSTrigEff_40TrackletCut.root");
     //_sig[3] = new TFile(locn+"mg1200/outputMSEff.root");
     //_sig[4] = new TFile(locn+"mg1500/outputMSEff.root");
     _sig[3] = new TFile(locn+"mg2000/outputMSTrigEff_40TrackletCut.root");
     _sig[0] = new TFile("../OutputPlots/bkgdJZ/JZAll/outputMSEff_forTrigParams.root");
     
     makeCombinedPlot(_sig, 4, "stealth", forThesis);

    return 314;
    
}

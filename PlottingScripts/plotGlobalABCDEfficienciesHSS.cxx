#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>

double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-12) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }

    Double_t arg = 1.0 - ( x - peak ) * tail / width;

    if (arg < 1.e-12) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);

    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )

    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );

    return TMath::Exp(exponent) ;
}
double novosibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}
void plotGlobalABCDEfficienciesHSS(){
  
    std::vector<TString> benchmark = {"mH125mS5lt5", "mH125mS8lt5", "mH125mS15lt5", "mH125mS25lt5", "mH125mS40lt5", "mH125mS55lt5"};
    const int nSamples=6;
    std::vector<TString> scalarmass = {"5","8","15","25","40","55"};
    int colors[nSamples] = {kViolet+7, kAzure+7, kTeal, kSpring-2, kOrange-3, kRed-4};
    double propLT[nSamples]={127,200,580,760,1180,1540};
    /*
    double nEvents_B[nSamples]={1254,1610,6126,6036,3664,881};
    double nEvents_E[nSamples]={6687,7351,13803,13789,10266,3966};
    */
    /*
    double nEvents_B[nSamples]={1218,1583, 6062, 5966, 3580,975};
    double nEvents_E[nSamples]={6627,7280,13656,13638,10082,4096};
    */
    /*
    double nEvents_B[nSamples]={1108,1249, 4546, 4542, 2705,367};
    double nEvents_E[nSamples]={6163,6900,12364,12328,8966,2586};
    */
    /*
    double nEvents_B[nSamples]={ 914,1106, 4146, 4352,2662, 368};
    double nEvents_E[nSamples]={5718,6435,11719,11912,8703,2473};
    */
    double nEvents_B[nSamples]={1001,1210,4467, 4717, 2914,0};
    double nEvents_E[nSamples]={6163,6965,12543,12865,9408,0};

    double propLT_9m[nSamples]={228,375,710,1210,1900,2730};
    double nEvents_B_9m[nSamples]={1329,1841, 4776, 6201,4033,0};
    double nEvents_E_9m[nSamples]={6461,6693,12104,12711,9746,0};

    TCanvas *c1 = new TCanvas("c1","c1",800,600);

    TFile* tfile[nSamples];
    TH1D* thist[nSamples];
    TH1D* thist2[nSamples];
    TH1D* thist3[nSamples];
    TH1D *thist_up[nSamples];
    TH1D *thist2_up[nSamples];
    TH1D *thist3_up[nSamples];
    TH1D *thist_down[nSamples];
    TH1D *thist2_down[nSamples];
    TH1D *thist3_down[nSamples];
    TF1* fn_up[nSamples];
    TF1* fn_down[nSamples];
    TF1* fn3_up[nSamples];
    TF1* fn2_up[nSamples];
    TF1* fn2_down[nSamples];
    TF1* fn3_down[nSamples];
    TF1* fn[nSamples];
    TF1* fn2[nSamples];
    TF1* fn3[nSamples];
    TGraph* fn3b[nSamples];
    TGraphAsymmErrors* g_err1[nSamples];
    TGraphAsymmErrors* g_err2[nSamples];
    TGraphAsymmErrors* g_err3[nSamples];
    //double maxVals[nSamples] = {12000,14500,25000,32000,24000,10000};
    double maxVals[nSamples] = {0,0,0,0,0,0};
    char name1[10];
    double maxValGraph=-99;
    for(int i=0; i<5; i++){

      TString add_str="";
      add_str="_regA";

      tfile[i] = TFile::Open("../OutputPlots/signalMC/extrapolation/"+benchmark.at(i)+add_str+".root");
      //tfile[i] = TFile::Open("../OutputPlots/signalMC/extrapolation/Files_NOLTeff/"+benchmark.at(i)+add_str+".root");
      /*
        thist[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx");
	//thist_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_maxStat");
        //thist_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_minStat");
        thist_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_maxTotal");
        thist_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_BVx_minTotal");
        thist2[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx");
	//thist2_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_maxStat");
        //thist2_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_minStat");
        thist2_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_maxTotal");
        thist2_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD_EVx_minTotal");
        thist3[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx");
	//thist3_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_maxStat");
        //thist3_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_minStat");
        thist3_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_maxTotal");
        thist3_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_minTotal");
      */
	thist[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx");
        //thist_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxStat");
        //thist_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minStat");
	thist_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxTotal");
	thist_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minTotal");
        thist2[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx");
        thist2_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxTotal");
        thist2_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minTotal");
        thist3[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx");
        thist3_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxTotal");
        thist3_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minTotal");
	
        /*
        thist[i]->Scale(1./scalings.at(i));
        thist_up[i]->Scale(1./scalings.at(i));
        thist_down[i]->Scale(1./scalings.at(i));
        thist2[i]->Scale(1./scalings.at(i));
        thist3[i]->Scale(1./scalings.at(i));
        thist2_up[i]->Scale(1./scalings.at(i));
        thist3_up[i]->Scale(1./scalings.at(i));
        thist2_down[i]->Scale(1./scalings.at(i));
        thist3_down[i]->Scale(1./scalings.at(i));
        */
        sprintf(name1,"fn_%d",i);
        fn[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn[i]->SetParameter(0,thist[i]->GetMaximum());
        fn[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
        thist[i]->Fit( fn[i], "WRV" );

        sprintf(name1,"fn_up_%d",i);
        fn_up[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn_up[i]->SetParameter(0,thist_up[i]->GetMaximum());
        fn_up[i]->SetParameter(1,-0.3); //fn_up->SetParLimits(1,-1,2);
        fn_up[i]->SetParameter(2,0.4); //fn_up->SetParLimits(2,0,2);
        fn_up[i]->SetParameter(3,-0.2); //fn_up->SetParLimits(3,0,20.0);
        thist_up[i]->Fit( fn_up[i], "WRV" );
        sprintf(name1,"fn_down_%d",i);
        fn_down[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn_down[i]->SetParameter(0,thist_down[i]->GetMaximum());
        fn_down[i]->SetParameter(1,-0.3); //fn_down->SetParLimits(1,-1,2);
        fn_down[i]->SetParameter(2,0.4); //fn_down->SetParLimits(2,0,2);
        fn_down[i]->SetParameter(3,-0.2); //fn_down->SetParLimits(3,0,20.0);
        thist_down[i]->Fit( fn_down[i], "WRV" );

        sprintf(name1,"fn2_%d",i);
        fn2[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn2[i]->SetParameter(0,thist2[i]->GetMaximum());
        fn2[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn2[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn2[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
        thist2[i]->Fit( fn2[i], "WRV" );

        sprintf(name1,"fn2_up_%d",i);
        fn2_up[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn2_up[i]->SetParameter(0,thist2_up[i]->GetMaximum());
        fn2_up[i]->SetParameter(1,-0.3); //fn_up->SetParLimits(1,-1,2);
        fn2_up[i]->SetParameter(2,0.4); //fn_up->SetParLimits(2,0,2);
        fn2_up[i]->SetParameter(3,-0.2); //fn_up->SetParLimits(3,0,20.0);
        thist2_up[i]->Fit( fn2_up[i], "WRV" );
        sprintf(name1,"fn2_down_%d",i);
        fn2_down[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn2_down[i]->SetParameter(0,thist2_down[i]->GetMaximum());
        fn2_down[i]->SetParameter(1,-0.3); //fn_down->SetParLimits(1,-1,2);
        fn2_down[i]->SetParameter(2,0.4); //fn_down->SetParLimits(2,0,2);
        fn2_down[i]->SetParameter(3,-0.2); //fn_down->SetParLimits(3,0,20.0);
        thist2_down[i]->Fit( fn2_down[i], "WRV" );

        sprintf(name1,"fn3_%d",i);
        fn3[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn3[i]->SetParameter(0,thist3[i]->GetMaximum());
        fn3[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn3[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn3[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
        thist3[i]->Fit( fn3[i], "WRV" );

        sprintf(name1,"fn3_up_%d",i);
        fn3_up[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn3_up[i]->SetParameter(0,thist3_up[i]->GetMaximum());
        fn3_up[i]->SetParameter(1,-0.3); //fn_up->SetParLimits(1,-1,2);
        fn3_up[i]->SetParameter(2,0.4); //fn_up->SetParLimits(2,0,2);
        fn3_up[i]->SetParameter(3,-0.2); //fn_up->SetParLimits(3,0,20.0);
        thist3_up[i]->Fit( fn3_up[i], "WRV" );
        sprintf(name1,"fn3_down_%d",i);
        fn3_down[i] = new TF1(name1,novosibirsk,0.03,300,4);
        fn3_down[i]->SetParameter(0,thist3_down[i]->GetMaximum());
        fn3_down[i]->SetParameter(1,-0.3); //fn_down->SetParLimits(1,-1,2);
        fn3_down[i]->SetParameter(2,0.4); //fn_down->SetParLimits(2,0,2);
        fn3_down[i]->SetParameter(3,-0.2); //fn_down->SetParLimits(3,0,20.0);
        thist3_down[i]->Fit( fn3_down[i], "WRV" );

        

        Double_t ctau[1000];
        Double_t eff[1000];
        Double_t eff1[1000];
        Double_t eff2[1000];
        Double_t errUp[1000];
        Double_t errDown[1000];
        Double_t errUp1[1000];
        Double_t errDown1[1000];
        Double_t errUp2[1000];
        Double_t errDown2[1000];
        int ntot = 0;
        TGraph *g_res = new TGraph(thist[i]);
        
        for (Int_t j = 0; j < 1000; j++) {
            Double_t life, nev, nev_up, nev_down, nev1_up, nev1_down, nev2_up, nev2_down;
            g_res->GetPoint(j, life, nev);
            nev = fn3[i]->Eval(life);
            nev_up = fn3_up[i]->Eval(life) - fn3[i]->Eval(life);
            nev_down = fn3[i]->Eval(life) - fn3_down[i]->Eval(life);
            nev1_up = fn_up[i]->Eval(life)-fn[i]->Eval(life);
            nev1_down = fn[i]->Eval(life)-fn_down[i]->Eval(life);
            nev2_up = fn2_up[i]->Eval(life)-fn2[i]->Eval(life);
            nev2_down = fn2[i]->Eval(life)-fn2_down[i]->Eval(life);

            if (nev == 0) {
                std::cout << "Continue because nev= " << nev << std::endl;
                continue;
            }
            if(nev > maxVals[i]) maxVals[i] = nev;
	    if (nev>maxValGraph) maxValGraph=nev;
            eff[ntot] = nev*0.93;
            eff1[ntot] = fn[i]->Eval(life);
            eff2[ntot] = fn2[i]->Eval(life);
            errUp[ntot] = nev_up*0.82;
            errDown[ntot] = nev_down*0.82;
            errUp1[ntot] = nev1_up;
            errDown1[ntot] = nev1_down;
            errUp2[ntot] = nev2_up;
            errDown2[ntot] = nev2_down;
            ctau[ntot] = life;
            ntot++;
	    if (life>1.97 && life<2.02){
	      //cout << "benchmark = " << benchmark.at(i) << "-->  NEvt = " << nev << " + " << nev_up << " - " << nev_down << endl;
	      cout << "benchmark = " << benchmark.at(i) << "-->  NEvt = " << nev*0.93 << " + " << nev_up*0.82 << " - " << nev_down*0.82 << endl;
	    }
        }
        fn3b[i] = new TGraph(ntot, ctau,eff);
        fn3b[i]->SetLineColor(colors[i]);
        fn3b[i]->SetLineStyle(i+1);
        fn3b[i]->SetLineWidth(3);
        g_err3[i] = new TGraphAsymmErrors(ntot, ctau, eff);
        g_err1[i] = new TGraphAsymmErrors(ntot, ctau, eff1);
        g_err2[i] = new TGraphAsymmErrors(ntot, ctau, eff2);
        for(int j=0;j<ntot; j++){
            g_err3[i]->SetPointEYlow(j, errDown[j]);
            g_err3[i]->SetPointEYhigh(j, errUp[j]);
            g_err1[i]->SetPointEYlow(j, errDown1[j]);
            g_err1[i]->SetPointEYhigh(j, errUp1[j]);
            g_err2[i]->SetPointEYlow(j, errDown2[j]);
            g_err2[i]->SetPointEYhigh(j, errUp2[j]);
        }
        g_err1[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err2[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err3[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err1[i]->SetLineWidth(0);
        g_err2[i]->SetLineWidth(0);
        g_err3[i]->SetLineWidth(0);
    }
    
    //fn3b[5]->SetLineColor(98);
    c1->SetLogx();
    fn3b[0]->SetFillColorAlpha(colors[0], 0.15);
    fn3b[0]->Draw("AL same");
    fn3b[0]->GetYaxis()->SetTitleOffset(1.6);
    fn3b[0]->GetYaxis()->SetRangeUser(0,1.3*maxValGraph);
    fn3b[0]->GetXaxis()->SetLimits(0.03,300);
    fn3b[0]->GetXaxis()->SetTitle("Scalar proper lifetime (c*#tau) [m]");
    fn3b[0]->GetYaxis()->SetTitle("Expected events in 36.1 fb^{-1}/BR[H#rightarrowss]");
    fn3b[0]->Draw("AL same");
    g_err3[0]->Draw("c3 SAME");

    c1->Update();
    //for(int i=1; i<nSamples; i++){
    for(int i=1; i<5; i++){
        g_err3[i]->Draw("c3 SAME");
        fn3b[i]->SetFillColorAlpha(colors[i], 0.15);
        fn3b[i]->Draw("LSAME");
    }
   

    TLegend *leg = new TLegend(0.66,0.66,0.99,0.91);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.04);
    leg->AddEntry(fn3b[0],"m_{s} = 5 GeV","fl");
    leg->AddEntry(fn3b[1],"m_{s} = 8 GeV","fl");
    leg->AddEntry(fn3b[2],"m_{s} = 15 GeV","fl");
    leg->AddEntry(fn3b[3],"m_{s} = 25 GeV","fl");
    leg->AddEntry(fn3b[4],"m_{s} = 40 GeV","fl");
    //leg->AddEntry(fn3b[5],"m_{s} = 55 GeV","l");
    leg->Draw();

    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.05);
    latex2.SetTextAlign(13);  //align at top
    latex2.DrawLatex(.19,.92,"#font[72]{ATLAS}  Internal");
    latex2.DrawLatex(.19,.87,"#font[42]{m_{H} = 125 GeV}");
    latex2.DrawLatex(.19,.81,"#font[42]{One vertex channel}");
    //latex2.DrawLatex(.19,.81,"#font[42]{Two vertex channel}");

    c1->Print("../OutputPlots/signalMC/extrapolation/GlobalEfficiencies_Combined_ABCD_HSS.pdf");


    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.045);
    latex.SetTextAlign(13);  //align at top

    TLine line1; line1.SetLineWidth(1);
    TLine line2; line2.SetLineWidth(1); line2.SetLineColor(kGreen+1);

    for(int i=0;i<5;i++){
        fn[i]->SetLineColor(kRed+1);
        fn2[i]->SetLineColor(kBlue-3);
        fn3b[i]->SetLineColor(kViolet-6);
        g_err3[i]->SetFillColorAlpha(kViolet-6,0.2);
        g_err1[i]->SetFillColorAlpha(kRed+1,0.2);
        g_err2[i]->SetFillColorAlpha(kBlue-3,0.2);
        fn[i]->SetLineStyle(2); fn2[i]->SetLineStyle(7);
        fn[i]->SetLineWidth(3); fn2[i]->SetLineWidth(3);
        fn3b[i]->SetLineStyle(1);

        //maxVals[0] = 0.006/1.5;
        fn3b[i]->GetYaxis()->SetRangeUser(0,1.5*maxVals[i]);
	//fn3b[i]->GetYaxis()->SetRangeUser(0,maxVals[i]);
        fn3b[i]->Draw("AL");
        fn3b[i]->GetYaxis()->SetTitleOffset(1.6);
        fn3b[i]->GetXaxis()->SetLimits(0.03,300);
	fn3b[i]->GetXaxis()->SetTitle("Scalar proper lifetime (c*#tau) [m]");
        fn3b[i]->GetYaxis()->SetLimits(0,1.5*maxVals[i]);
        fn3b[i]->GetYaxis()->SetTitle("Expected events in 36.1 fb^{-1}/BR[H#rightarrowss]");

        fn3b[i]->Draw("AL same");
        g_err3[i]->Draw("c3 same");
        fn3b[i]->Draw("L SAME");
        c1->Update();
        g_err1[i]->Draw("c3 same");
        fn[i]->Draw("LSAME");
        g_err2[i]->Draw("c3 same");
        fn2[i]->Draw("LSAME");
     
        TLegend *leg2 = new TLegend(0.58,0.76,0.95,0.91);
        leg2->SetFillStyle(0);
        leg2->SetBorderSize(0);
        leg2->AddEntry(fn[i],"Barrel MS vertex ","l");
        leg2->AddEntry(fn2[i],"Endcap MS vertex","l");
        leg2->AddEntry(fn3b[i],"Sum","l");
        leg2->Draw();
	latex.DrawLatex(0.2,0.85,"m_{H},m_{s} = [125,"+scalarmass.at(i)+"] GeV");
        latex2.DrawLatex(.2,.92,"#font[72]{ATLAS}  Internal");
	propLT[i]/=1000; 
	//line1.DrawLine(propLT[i],0,propLT[i],nEvents_B[i]+nEvents_E[i]+100);
	//line1.DrawLine(0,nEvents_B[i],propLT[i]+0.3,nEvents_B[i]);
	//line1.DrawLine(0,nEvents_E[i],propLT[i]+0.3,nEvents_E[i]);
	propLT_9m[i]/=1000; 
	//line2.DrawLine(propLT_9m[i],0,propLT_9m[i],nEvents_B_9m[i]+nEvents_E_9m[i]+100);
	//line2.DrawLine(0,nEvents_B_9m[i],propLT_9m[i]+0.3,nEvents_B_9m[i]);
	//line2.DrawLine(0,nEvents_E_9m[i],propLT_9m[i]+0.3,nEvents_E_9m[i]);
        c1->Print("../OutputPlots/signalMC/extrapolation/GlobalEfficiency_ABCDChannel_"+benchmark.at(i)+".pdf");

    }


    return;
}

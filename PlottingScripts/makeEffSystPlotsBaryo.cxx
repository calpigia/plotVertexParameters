void makeEffSystPlotsBaryo(){
    SetAtlasStyle();
    
    std::vector<TString> samples ={"HChiChi_nubb_mH125mChi10","HChiChi_nubb_mH125mChi30","HChiChi_nubb_mH125mChi50","HChiChi_nubb_mH125mChi100",
				   "HChiChi_lcb_mH125mChi10","HChiChi_lcb_mH125mChi30","HChiChi_lcb_mH125mChi50","HChiChi_lcb_mH125mChi100",
				   "HChiChi_cbs_mH125mChi10","HChiChi_cbs_mH125mChi30","HChiChi_cbs_mH125mChi50","HChiChi_cbs_mH125mChi100",
				   "HChiChi_tatanu_mH125mChi10","HChiChi_tatanu_mH125mChi30","HChiChi_tatanu_mH125mChi50","HChiChi_tatanu_mH125mChi100"
    };
    
    std::vector<TString> benchmark;
    std::vector<string> massLine;
    TString model = "baryogenesis";
    if(model == "baryogenesis"){
      benchmark = {"HChiChi_nubb_mH125mChi10","HChiChi_nubb_mH125mChi30","HChiChi_nubb_mH125mChi50","HChiChi_nubb_mH125mChi100",
		   "HChiChi_lcb_mH125mChi10","HChiChi_lcb_mH125mChi30","HChiChi_lcb_mH125mChi50","HChiChi_lcb_mH125mChi100",
		   "HChiChi_cbs_mH125mChi10","HChiChi_cbs_mH125mChi30","HChiChi_cbs_mH125mChi50","HChiChi_cbs_mH125mChi100",
		   "HChiChi_tatanu_mH125mChi10","HChiChi_tatanu_mH125mChi30","HChiChi_tatanu_mH125mChi50","HChiChi_tatanu_mH125mChi100"};
      massLine = {"10","30","50","100","10","30","50","100","10","30","50","100","10","30","50","100"};
    }
    ofstream file;
    file.open("SystematicsTables_"+model+"_v2BgkFlags.tex",ios::out);
    

    std::vector<std::pair<double, double> > msvx_b;
    std::vector<std::pair<double, double> > msvx_e;
    std::vector<std::pair<double, double> > mstrig_b;
    std::vector<std::pair<double, double> > mstrig_e;
    
    std::vector<std::pair<double, double> > trigsf_b;
    std::vector<std::pair<double, double> > trigsf_e;
    std::vector<std::pair<double, double> > ttmp;
    
    std::pair<double, double> tmp;
    
    for(unsigned int i=0; i<samples.size(); i++){
        msvx_b.push_back(plotPURatios_test(samples.at(i),"MSVx_Barrel_Lxy",true));
        msvx_e.push_back(plotPURatios_test(samples.at(i),"MSVx_Endcap_Lz",true));
        mstrig_b.push_back(plotPURatios_test(samples.at(i),"MSTrig_1B_Lxy",true));
        mstrig_e.push_back(plotPURatios_test(samples.at(i),"MSTrig_1E_Lz",true));
        tmp = plotPURatios_test(samples.at(i),"MSVx_Barrel_Lxy",false);
        tmp = plotPURatios_test(samples.at(i),"MSVx_Endcap_Lz",false);
        tmp = plotPURatios_test(samples.at(i),"MSTrig_1B_Lxy",false);
        tmp = plotPURatios_test(samples.at(i),"MSTrig_1E_Lz",false);
        
    }
    
    
    for(int i=0; i<samples.size(); i++){
        
        msvx_b.push_back(plotPDFRatios_test_oneBin(samples.at(i),"MSVx_Barrel_Lxy"));
        msvx_e.push_back(plotPDFRatios_test_oneBin(samples.at(i),"MSVx_Endcap_Lz"));
        mstrig_b.push_back(plotPDFRatios_test_oneBin(samples.at(i),"MSTrig_1B_Lxy"));
        mstrig_e.push_back(plotPDFRatios_test_oneBin(samples.at(i),"MSTrig_1E_Lz"));
        plotPDFRatios_test(samples.at(i),"MSVx_Barrel_Lxy");
        plotPDFRatios_test(samples.at(i),"MSVx_Endcap_Lz");
        plotPDFRatios_test(samples.at(i),"MSTrig_1B_Lxy");
        plotPDFRatios_test(samples.at(i),"MSTrig_1E_Lz");
        
    }
    
    
    
    for(int i=0; i<samples.size(); i++){
        
        findScaleFactors(samples.at(i),"MSTrig");
        ttmp = findScaleFactors_oneBin(samples.at(i),"MSTrig");
        trigsf_b.push_back(ttmp.at(0));
        trigsf_e.push_back(ttmp.at(1));
    }
    
    std::cout << "PileupSystematics " << std::endl;
    for(unsigned int i=0; i <samples.size(); i++){
        std::cout << samples.at(i) << " " <<  msvx_b.at(i).first << " " << msvx_e.at(i).first << " " << mstrig_b.at(i).first << " " << mstrig_e.at(i).first << std::endl;
        std::cout << samples.at(i) << " " <<  msvx_b.at(i).second << " " << msvx_e.at(i).second << " " << mstrig_b.at(i).second << " " << mstrig_e.at(i).second << std::endl;
    }
    std::cout << "PDFSystematics " << std::endl;
    for(unsigned int i=samples.size(); i <msvx_b.size(); i++){
        std::cout << samples.at(i-samples.size()) << " " << msvx_b.at(i).first << " " << msvx_e.at(i).first << " " << mstrig_b.at(i).first << " " << mstrig_e.at(i).first << std::endl;
        std::cout << samples.at(i-samples.size()) << " " << msvx_b.at(i).second << " " << msvx_e.at(i).second << " " << mstrig_b.at(i).second << " " << mstrig_e.at(i).second << std::endl;
    }
    std::cout << "Trigger scale factor systematics " << std::endl;
    for(unsigned int i=0; i <trigsf_b.size(); i++){
        std::cout << samples.at(i) << " " <<  trigsf_b.at(i).first  << " " << trigsf_e.at(i).first << std::endl;
        std::cout << samples.at(i) << " " <<  trigsf_b.at(i).second << " "<< trigsf_e.at(i).second << std::endl;
    }
    
    

    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    if(model == "baryogenesis")file << "\\begin{tabular}{cc|cccc}" << endl;
    
    file << "\\toprule" << endl;
    file << "Channel & {$m_{\\chi}$} & Trigger reconstruction & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;
    
    double sc1 = 1000.; double sc2 = 10.;
    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt(pow(trigsf_b.at(i).first,2) + pow( mstrig_b.at(i).first,2) + pow(mstrig_b.at(i+samples.size()).first,2));
        double totdown = TMath::Sqrt(pow(trigsf_b.at(i).second,2) + pow( mstrig_b.at(i).second,2) + pow(mstrig_b.at(i+samples.size()).second,2));
        std::vector<double> uncertainties = {trigsf_b.at(i).first, trigsf_b.at(i).second, mstrig_b.at(i).first, mstrig_b.at(i).second, mstrig_b.at(i+samples.size()).first,mstrig_b.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\pm " << unints.at(4) << "\\%$ & ";
        } else{
            file << "+" << unints.at(4) << ", -" << unints.at(5) << "\\% & ";
        }
        if(unints.at(6) == unints.at(7)){
            file << "$\\mathbf{\\pm " << unints.at(6) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(6) << ", -" << unints.at(7) << "\\% } \\\\ " << endl;
        }
    }
    
    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{MS barrel trigger systematics.}\\label{tab:MSBTrigSyst_}" << endl;
    file << "\\end{table}" << endl;
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;
    
    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    if( model == "baryogenesis")file << "\\begin{tabular}{cc|cccc}" << endl;
    
    file << "\\toprule" << endl;
    file << "Channel & {$m_{\\chi}$} & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;
    
    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt( pow( msvx_b.at(i).first,2) + pow(msvx_b.at(i+samples.size()).first,2));
        double totdown = TMath::Sqrt(pow( msvx_b.at(i).second,2) + pow(msvx_b.at(i+samples.size()).second,2));
        std::vector<double> uncertainties = {msvx_b.at(i).first, msvx_b.at(i).second, msvx_b.at(i+samples.size()).first,msvx_b.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
         if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
        else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\mathbf{\\pm " << unints.at(4) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(4) << ", -" << unints.at(5) << "\\% } \\\\ " << endl;
        }
    }
    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{MS barrel vertex systematics.}\\label{tab:MSBVxSyst_}" << endl;
    file << "\\end{table}" << endl;
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;
    
    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    if( model == "baryogenesis")file << "\\begin{tabular}{cc|cccc}" << endl;
    
    file << "\\toprule" << endl;
    file << "Channel & {$m_{\\chi}$} & Trigger reconstruction & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;
    
    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt(pow(trigsf_e.at(i).first,2) + pow( mstrig_e.at(i).first,2) + pow(mstrig_e.at(i+samples.size()).first,2));
        double totdown = TMath::Sqrt(pow(trigsf_e.at(i).second,2) + pow( mstrig_e.at(i).second,2) + pow(mstrig_e.at(i+samples.size()).second,2));
        std::vector<double> uncertainties = {trigsf_e.at(i).first, trigsf_e.at(i).second, mstrig_e.at(i).first, mstrig_e.at(i).second, mstrig_e.at(i+samples.size()).first,mstrig_e.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\pm " << unints.at(4) << "\\%$ & ";
        } else{
            file << "+" << unints.at(4) << ", -" << unints.at(5) << "\\% & ";
        }
        if(unints.at(6) == unints.at(7)){
            file << "$\\mathbf{\\pm " << unints.at(6) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(6) << ", -" << unints.at(7) << "\\% } \\\\ " << endl;
        }
    }
    
    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{MS endcap trigger systematics.}\\label{tab:MSETrigSyst_}" << endl;
    file << "\\end{table}" << endl;
    
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;
    
    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    if( model == "baryogenesis")file << "\\begin{tabular}{cc|cccc}" << endl;
    
    file << "\\toprule" << endl;
    file << "Chennel & {$m_{\\chi}$} & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;
    
    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt( pow( msvx_e.at(i).first,2) + pow(msvx_e.at(i+samples.size()).first,2));
        double totdown = TMath::Sqrt(pow( msvx_e.at(i).second,2) + pow(msvx_e.at(i+samples.size()).second,2));
        std::vector<double> uncertainties = {msvx_e.at(i).first, msvx_e.at(i).second, msvx_e.at(i+samples.size()).first,msvx_e.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
         if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
        else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\mathbf{\\pm " << unints.at(4) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(4) << ", -" << unints.at(5) << "\\% } \\\\ " << endl;
        }
    }
    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{MS endcap vertex systematics.}\\label{tab:MSEVxSyst_}" << endl;
    file << "\\end{table}" << endl;
    
}

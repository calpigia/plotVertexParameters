void makeToyWorkflowPlots(TString sample, TString inDir, TString outDir){
    
    TFile *fMSVx = TFile::Open(inDir+"signalMC/mg"+sample+"/outputMSVxEff.root");
    TFile *fTrig = TFile::Open(inDir+"signalMC/mg"+sample+"/outputMSTrigEff_40TrackletCut.root");
    
    SetAtlasStyle();
    
    Double_t stops[9] = { 0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000};
    Double_t red[9]   = { 0.2082, 0.0592, 0.0780, 0.0232, 0.1802, 0.5301, 0.8186, 0.9956, 0.9764};
    Double_t green[9] = { 0.1664, 0.3599, 0.5041, 0.6419, 0.7178, 0.7492, 0.7328, 0.7862, 0.9832};
    Double_t blue[9]  = { 0.5293, 0.8684, 0.8385, 0.7914, 0.6425, 0.4662, 0.3499, 0.1968, 0.0539};
    TColor::CreateGradientColorTable(9, stops, red, green, blue, 255, 1);
    
    gStyle->SetPalette(kBird);
    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    c1->SetRightMargin(0.13);

    gStyle->SetPaintTextFormat("1.2f");

    
    TH2D *h_BLLPJetN = (TH2D*)fMSVx->Get("LLPJetEff2_pT_Lxy");
    TH2D *h_BLLPJetD = (TH2D*)fMSVx->Get("LLPJetEff2_pT_Lxy_denom");
    TH2D *h_ELLPJetN = (TH2D*)fMSVx->Get("LLPJetEff2_pz_Lz");
    TH2D *h_ELLPJetD = (TH2D*)fMSVx->Get("LLPJetEff2_pz_Lz_denom");
    
    h_BLLPJetN->Divide(h_BLLPJetD);
    h_BLLPJetN->GetXaxis()->SetTitle("LLP L_{xy} [m]");
    h_BLLPJetN->GetYaxis()->SetTitle("LLP p_{T} [GeV]");
    h_BLLPJetN->SetMarkerSize(1.5);
    h_BLLPJetN->SetMaximum(1);
    h_BLLPJetN->Draw("COLZ text0");
    
    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.035);
    latex2.SetTextAlign(13);  //align at top
    latex2.DrawLatex(.18,0.98,"#font[42]{Barrel LLPs}");
    c1->Print(outDir+"/LLPJet_Barrel_2D_mg800.pdf");

    h_ELLPJetN->Divide(h_ELLPJetD);
    h_ELLPJetN->GetXaxis()->SetTitle("LLP L_{z} [m]");
    h_ELLPJetN->GetYaxis()->SetTitle("LLP p_{z} [GeV]");
    h_ELLPJetN->SetMarkerSize(1);
    h_ELLPJetN->SetMaximum(1);
    h_ELLPJetN->Draw("COLZ text0");
    
    latex2.DrawLatex(.18,0.98,"#font[42]{Endcap LLPs}");
    c1->Print(outDir+"/LLPJet_Endcap_2D_mg800.pdf");
    
    TH2D *h_BBTrigN     = (TH2D*)fTrig->Get("MSTrig_BB");
    TH2D *h_BBTrigD     = (TH2D*)fTrig->Get("MSTrig_BB_denom");
    
    h_BBTrigN->Divide(h_BBTrigD);
    h_BBTrigN->GetXaxis()->SetTitle("Farther LLP L_{xy} [m]");
    h_BBTrigN->GetYaxis()->SetTitle("Closer LLP L_{xy} [m]");
    h_BBTrigN->GetXaxis()->SetRangeUser(3,8);
    h_BBTrigN->GetYaxis()->SetRangeUser(3,8);
    h_BBTrigN->SetMarkerSize(2.5);
    h_BBTrigN->SetMaximum(1);
    h_BBTrigN->Draw("COLZ text0");
    
    latex2.DrawLatex(.18,0.9,"#font[42]{Barrel-Barrel decay topology}");
    //latex2.DrawLatex(.18,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    //latex2.DrawLatex(.18,y_val_label,"#font[52]{Endcaps }#font[42]{(1.0 < |#eta| < 2.5)}");
    
    c1->Print(outDir+"/MSTrig_BB_2D_mg800.pdf");

    TH2D *h_BETrigN     = (TH2D*)fTrig->Get("MSTrig_BE");
    TH2D *h_BETrigD     = (TH2D*)fTrig->Get("MSTrig_BE_denom");
    
    h_BETrigN->Divide(h_BETrigD);
    gStyle->SetPaintTextFormat("1.2f");
    h_BETrigN->GetXaxis()->SetTitle("Endcap LLP L_{z} [m]");
    h_BETrigN->GetYaxis()->SetTitle("Barrel LLP L_{xy} [m]");
    h_BETrigN->GetXaxis()->SetRangeUser(5,15);
    h_BETrigN->GetYaxis()->SetRangeUser(3,8);
    h_BETrigN->SetMarkerSize(1.5);
    h_BETrigN->SetMaximum(1);
    h_BETrigN->Draw("COLZ text0");
    
    latex2.DrawLatex(.18,0.98,"#font[42]{Barrel-Endcap decay topology}");
    //latex2.DrawLatex(.18,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    //latex2.DrawLatex(.18,y_val_label,"#font[52]{Endcaps }#font[42]{(1.0 < |#eta| < 2.5)}");
    
    c1->Print(outDir+"/MSTrig_BE_2D_mg800.pdf");

    
    TH2D *h_EETrigN     = (TH2D*)fTrig->Get("MSTrig_EE");
    TH2D *h_EETrigD     = (TH2D*)fTrig->Get("MSTrig_EE_denom");
    
    h_EETrigN->Divide(h_EETrigD);
    gStyle->SetPaintTextFormat("1.2f");
    h_EETrigN->GetXaxis()->SetTitle("Farther LLP L_{z} [m]");
    h_EETrigN->GetYaxis()->SetTitle("Closer LLP L_{z} [m]");
    h_EETrigN->GetXaxis()->SetRangeUser(5,15);
    h_EETrigN->GetYaxis()->SetRangeUser(5,13);
    h_EETrigN->SetMarkerSize(1.5);
    h_EETrigN->SetMaximum(1);
    h_EETrigN->Draw("COLZ text0");
    
    latex2.DrawLatex(.18,0.9,"#font[42]{Endcap-Endcap decay topology}");
    //latex2.DrawLatex(.18,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    //latex2.DrawLatex(.18,y_val_label,"#font[52]{Endcaps }#font[42]{(1.0 < |#eta| < 2.5)}");
    
    c1->Print(outDir+"/MSTrig_EE_2D_mg800.pdf");

    
    c1->SetRightMargin(0.05);
    
    TH1D *h_1BTrigN     = (TH1D*)fTrig->Get("MSTrig_1B_Lxy");
    TH1D *h_1BTrigD     = (TH1D*)fTrig->Get("MSTrig_1B_Lxy_denom");
    TGraphAsymmErrors *h_1BTrigEff = new TGraphAsymmErrors(h_1BTrigN,h_1BTrigD,"cl=0.683 b(1,1) mode");
    h_1BTrigEff->GetXaxis()->SetTitle("LLP L_{xy} [m]");
    h_1BTrigEff->Draw("AP");
    h_1BTrigEff->GetXaxis()->SetLimits(3,8);
    h_1BTrigEff->GetYaxis()->SetRangeUser(0,1.0);
    h_1BTrigEff->Draw("AP");
    gPad->Update();
    h_1BTrigEff->GetYaxis()->SetTitle("Muon RoI Cluster trigger efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{1 MS (Barrel) decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    c1->Print(outDir+"/MSTrig_1B_Lxy_mg800.pdf");

    
    TH1D *h_1ETrigN     = (TH1D*)fTrig->Get("MSTrig_1E_Lz");
    TH1D *h_1ETrigD     = (TH1D*)fTrig->Get("MSTrig_1E_Lz_denom");
    TGraphAsymmErrors *h_1ETrigEff = new TGraphAsymmErrors(h_1ETrigN,h_1ETrigD,"cl=0.683 b(1,1) mode");
    h_1ETrigEff->GetXaxis()->SetTitle("LLP L_{z} [m]");
    h_1ETrigEff->Draw("AP");
    h_1ETrigEff->GetXaxis()->SetLimits(5,15);
    h_1ETrigEff->GetYaxis()->SetRangeUser(0,1);
    h_1ETrigEff->Draw("AP");
    gPad->Update();
    h_1ETrigEff->GetYaxis()->SetTitle("Muon RoI Cluster trigger efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{1 MS (Endcaps) decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    c1->Print(outDir+"/MSTrig_1E_Lz_mg800.pdf");
     
    TH1D *h_BBVxN   = (TH1D*)fMSVx->Get("MSVxTrig_BB_Lxy");
    TH1D *h_BBVxD   = (TH1D*)fMSVx->Get("MSVxTrig_BB_Lxy_denom");
    TGraphAsymmErrors *h_BBVxEff = new TGraphAsymmErrors(h_BBVxN,h_BBVxD,"cl=0.683 b(1,1) mode");
    h_BBVxEff->GetXaxis()->SetTitle("LLP L_{xy} [m]");
    h_BBVxEff->GetXaxis()->SetRangeUser(3,8);
    h_BBVxEff->GetYaxis()->SetRangeUser(0,0.2);
    h_BBVxEff->Draw("AP");
    h_BBVxEff->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{Barrel-Barrel decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    
    c1->Print(outDir+"/MSVx_BB_Lxy_mg800.pdf");
    
    TH1D *h_BEVxN   = (TH1D*)fMSVx->Get("MSVxTrig_BE_Lxy");
    TH1D *h_BEVxD   = (TH1D*)fMSVx->Get("MSVxTrig_BE_Lxy_denom");
    
    TGraphAsymmErrors *h_BEVxEff = new TGraphAsymmErrors(h_BEVxN,h_BEVxD,"cl=0.683 b(1,1) mode");
    h_BEVxEff->GetXaxis()->SetTitle("LLP L_{xy} [m]");
    h_BEVxEff->GetXaxis()->SetRangeUser(3,8);
    h_BEVxEff->GetYaxis()->SetRangeUser(0,0.2);
    h_BEVxEff->Draw("AP");
    h_BEVxEff->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{Barrel-Endcap decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    
    c1->Print(outDir+"/MSVx_BE_Lxy_mg800.pdf");
    
    TH1D *h_EBVxN   = (TH1D*)fMSVx->Get("MSVxTrig_EB_Lz");
    TH1D *h_EBVxD   = (TH1D*)fMSVx->Get("MSVxTrig_EB_Lz_denom");
    
    TGraphAsymmErrors *h_EBVxEff = new TGraphAsymmErrors(h_EBVxN,h_EBVxD,"cl=0.683 b(1,1) mode");
    h_EBVxEff->GetXaxis()->SetTitle("LLP L_{z} [m]");
    h_EBVxEff->Draw("AP");
    h_EBVxEff->GetXaxis()->SetRangeUser(5,15);
    h_EBVxEff->GetYaxis()->SetRangeUser(0,1);
    h_EBVxEff->Draw("AP");
    gPad->Update();
    h_EBVxEff->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{Barrel-Endcap decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    
    c1->Print(outDir+"/MSVx_EB_Lz_mg800.pdf");
    
    TH1D *h_EEVxN   = (TH1D*)fMSVx->Get("MSVxTrig_EE_Lz");
    TH1D *h_EEVxD   = (TH1D*)fMSVx->Get("MSVxTrig_EE_Lz_denom");
    
    TGraphAsymmErrors *h_EEVxEff = new TGraphAsymmErrors(h_EEVxN,h_EEVxD,"cl=0.683 b(1,1) mode");
    h_EEVxEff->GetXaxis()->SetTitle("LLP L_{z} [m]");
    h_EEVxEff->Draw("AP");
    h_EEVxEff->GetXaxis()->SetRangeUser(5,15);
    h_EEVxEff->GetYaxis()->SetRangeUser(0,1);
    h_EEVxEff->Draw("AP");
    gPad->Update();
    h_EEVxEff->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{Endcap-Endcap decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    
    c1->Print(outDir+"/MSVx_EE_Lz_mg800.pdf");

    TH1D *h_BBVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD_BB_Lxy");
    TH1D *h_EEVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD_EE_Lz");
    TH1D *h_BEVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD_BE_Lxy");
    TH1D *h_EBVxN_a   = (TH1D*)fMSVx->Get("MSVxABCD_EB_Lz");
    TH1D *h_1BVxN     = (TH1D*)fMSVx->Get("MSVxABCD_1B_Lxy");
    TH1D *h_1EVxN     = (TH1D*)fMSVx->Get("MSVxABCD_1E_Lz");

    TH1D *h_BBVxD_a   = (TH1D*)fMSVx->Get("MSVxABCD_BB_Lxy_denom");
    TH1D *h_EEVxD_a   = (TH1D*)fMSVx->Get("MSVxABCD_EE_Lz_denom");
    TH1D *h_BEVxD_a   = (TH1D*)fMSVx->Get("MSVxABCD_BE_Lxy_denom");
    TH1D *h_EBVxD_a   = (TH1D*)fMSVx->Get("MSVxABCD_EB_Lz_denom");
    TH1D *h_1BVxD     = (TH1D*)fMSVx->Get("MSVxABCD_1B_Lxy_denom");
    TH1D *h_1EVxD     = (TH1D*)fMSVx->Get("MSVxABCD_1E_Lz_denom");

    TGraphAsymmErrors *h_BBVxEff_a = new TGraphAsymmErrors(h_BBVxN_a,h_BBVxD_a,"cl=0.683 b(1,1) mode");
    TGraphAsymmErrors *h_EEVxEff_a = new TGraphAsymmErrors(h_EEVxN_a,h_EEVxD_a,"cl=0.683 b(1,1) mode");
    TGraphAsymmErrors *h_BEVxEff_a = new TGraphAsymmErrors(h_BEVxN_a,h_BEVxD_a,"cl=0.683 b(1,1) mode");
    TGraphAsymmErrors *h_EBVxEff_a = new TGraphAsymmErrors(h_EBVxN_a,h_EBVxD_a,"cl=0.683 b(1,1) mode");
    TGraphAsymmErrors *h_1BVxEff = new TGraphAsymmErrors(h_1BVxN,h_1BVxD,"cl=0.683 b(1,1) mode");
    TGraphAsymmErrors *h_1EVxEff = new TGraphAsymmErrors(h_1EVxN,h_1EVxD,"cl=0.683 b(1,1) mode");

    h_1BVxEff->GetXaxis()->SetTitle("LLP L_{xy} [m]");
    h_1BVxEff->GetXaxis()->SetRangeUser(3,8);
    h_1BVxEff->GetYaxis()->SetRangeUser(0,0.2);
    h_1BVxEff->Draw("AP");
    h_1BVxEff->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{1 MS (Barrel) decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    c1->Print(outDir+"/MSVxABCD_1B_Lxy_mg800.pdf");
    
    h_BBVxEff_a->GetXaxis()->SetTitle("LLP L_{xy} [m]");
    h_BBVxEff_a->GetXaxis()->SetRangeUser(3,8);
    h_BBVxEff_a->GetYaxis()->SetRangeUser(0,0.2);
    h_BBVxEff_a->Draw("AP");
    h_BBVxEff_a->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{Barrel-Barrel decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    c1->Print(outDir+"/MSVxABCD_BB_Lxy_mg800.pdf");
    
    h_BEVxEff_a->GetXaxis()->SetTitle("LLP L_{xy} [m]");
    h_BEVxEff_a->GetXaxis()->SetRangeUser(3,8);
    h_BEVxEff_a->GetYaxis()->SetRangeUser(0,0.2);
    h_BEVxEff_a->Draw("AP");
    h_BEVxEff_a->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{Barrel-Endcap decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    c1->Print(outDir+"/MSVxABCD_BE_Lxy_mg800.pdf");

    h_1EVxEff->GetXaxis()->SetTitle("LLP L_{z} [m]");
    h_1EVxEff->Draw("AP");
    h_1EVxEff->GetXaxis()->SetRangeUser(5,15);
    h_1EVxEff->GetYaxis()->SetRangeUser(0,1);
    h_1EVxEff->Draw("AP");
    gPad->Update();
    h_1EVxEff->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{1 MS (Endcap) decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    c1->Print(outDir+"/MSVxABCD_1E_Lz_mg800.pdf");
    
    h_EBVxEff_a->Draw("AP");
    h_EBVxEff_a->GetXaxis()->SetTitle("LLP L_{z} [m]");
    h_EBVxEff_a->GetXaxis()->SetRangeUser(5,15);
    h_EBVxEff_a->GetYaxis()->SetRangeUser(0,1);
    h_EBVxEff_a->Draw("AP");
    gPad->Update();
    h_EBVxEff_a->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{Barrel-Endcap decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    c1->Print(outDir+"/MSVxABCD_EB_Lz_mg800.pdf");
    
    h_EEVxEff_a->Draw("AP");
    h_EEVxEff_a->GetXaxis()->SetTitle("LLP L_{z} [m]");
    h_EEVxEff_a->GetXaxis()->SetRangeUser(5,15);
    h_EEVxEff_a->GetYaxis()->SetRangeUser(0,1);
    h_EEVxEff_a->Draw("AP");
    gPad->Update();
    h_EEVxEff_a->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
    latex2.DrawLatex(.6,0.9,"#font[42]{Endcap-Endcap decay topology}");
    latex2.DrawLatex(.6,0.86,"#font[42]{Events with #leq 40 MS tracklets}");
    latex2.DrawLatex(.6,0.82,"#font[42]{Passing trigger}");
    c1->Print(outDir+"/MSVxABCD_EE_Lz_mg800.pdf");

    
}
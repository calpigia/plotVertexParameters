#include "../PlottingTools/PlottingPackage/InputFiles.h"
#include "TString.h"
#include "TFile.h"
void calculateJetSystematics(TString sample);
void calculateJetSystematics(TString sample){
	TString trees[12];
	trees[0] = "recoTree"; 
	trees[1] = "JET_EtaIntercalibration_NonClosure__1up"; trees[2] = "JET_EtaIntercalibration_NonClosure__1down";
	trees[3] = "JET_GroupedNP_1__1up"; trees[4] = "JET_GroupedNP_1__1down";
	trees[5] = "JET_GroupedNP_2__1up"; trees[6] = "JET_GroupedNP_2__1down";
	trees[7] = "JET_GroupedNP_3__1up"; trees[8] = "JET_GroupedNP_3__1down";
	trees[9] = "JET_JER_SINGLE_NP__1up"; 
	trees[10] = "JET_EMF__1up"; trees[11] = "JET_EMF__1down";

	TChain *ch[12];
	for(int i=0;i<12;i++){
		ch[i] = new TChain(trees[i]);
    		InputFiles::AddFilesToChain(sample, ch[i]);
	}
	
   	TCanvas* c1 = new TCanvas("c1","c1",800,600);
	ch[0]->Draw("MSVertex_isGood");
	for(int i=1; i < 12; i++){ ch[i]->Draw("MSVertex_isGood","","SAME");}
}

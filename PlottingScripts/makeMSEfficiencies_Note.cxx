//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include "TMultiGraph.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLine.h"
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    int numberOfSlashes = 0; int tmpSlashNumber = 0;
    int indexOf2ndLastSlash = -1;
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            numberOfSlashes++;
        }
    }
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            tmpSlashNumber++;
        }
        if(tmpSlashNumber == (numberOfSlashes - 1)){
            indexOf2ndLastSlash = charN;
            break;
        }
    }
    fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 20 - indexOf2ndLastSlash - 1);
    std::cout << "file name : " << fileName << std::endl;
    TString title = "";

    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nub#bar{b}";
        if(channel == "tatanu") channel = "#tau#tau#nu";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        //title = (TString) "h #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
	title = (TString) "#chi#rightarrow"+channel+", m_{#chi} = "+mchi+" GeV";
	//title = (TString) "m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{#Phi},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}
void makePlotWithLines(TFile *_sig[6],  TString type);
void makePlotWithLines(TFile *_sig[6],  TString type){
    bool add_ctau = false;    
    TString sigNames[6];
    int nSIG=6;
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<nSIG; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}

    Int_t colorSig[16] = { kPink-3,kViolet-3,kBlue-3, kAzure+5, kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};
    /*
    Int_t colorSig[4] = {kViolet -3, kTeal - 6, kBlue -3, kRed};
    if(nSIG < 4){
      colorSig[0] = kBlue - 3;
      colorSig[1] = kAzure + 5;
      colorSig[2] = kTeal - 6;
      colorSig[3] = kRed;
    }
    */
    Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};

    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};
    /*
    Int_t markerSig[6] = {24,25,26,27,28,29};
    Int_t colorSig[6] = {kViolet -3, kTeal - 6, kBlue -3, kRed, kMagenta, kCyan+1};
    if(nSIG < 6){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
        colorSig[3] = kRed;
        colorSig[4] = kMagenta;
        colorSig[5] = kCyan+1;
    }
    */

    TString names[2];
    
    names[0] = "MSVx_Barrel_Lxy";
    names[1] = "MSVx_Endcap_Lz";
    /*
    names[0] = "MSVxABCD_1B_Lxy";
    names[1] = "MSVxABCD_1E_Lz";
    */
    std::cout << "running on : " << _sig[0]->GetName() << std::endl;

    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //vertex reco
    for(unsigned int i=0;i<2; i++){

        std::cout << "plot: " << names[i] << std::endl;
        TGraphAsymmErrors* h_sig[30];
        TMultiGraph *mg = new TMultiGraph();

        //TLegend *legS = new TLegend(0.17,0.6,0.566,0.8);
	TLegend *legS = new TLegend(0.17,0.6,0.52,0.8);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);

        for(int j=0; j<nSIG; j++){
            TH1D *num = (TH1D*)_sig[j]->Get(names[i]);
            TH1D *den = (TH1D*)_sig[j]->Get(names[i]+"_denom");
            if(type == "higgs" && names[i].Contains("Lz")){
                num->Rebin(2); den->Rebin(2);
            }
            h_sig[j] = new TGraphAsymmErrors(num,den,"cl=0.683 b(1,1) mode");
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]); h_sig[j]->SetLineColor(colorSig[j]); h_sig[j]->SetMarkerStyle(markerSig[j]);

            legS->AddEntry(h_sig[j],"#font[42]{"+sigNames[j]+"}","lp");
            mg->Add(h_sig[j]);

            if(j == nSIG-1){
                mg->Draw("AP");

                if(names[i].Contains("Lxy")){
                    mg->GetXaxis()->SetTitle("Long-lived particle L_{xy} [m]");
		    
		    if (names[0]=="MSVx_Barrel_Lxy" || names[0]=="MSVx_Endcap_Lz"){	      
		      if (type == "bg" || type == "higgs") mg->GetYaxis()->SetRangeUser(0,0.7);
		      else                                 mg->GetYaxis()->SetRangeUser(0,0.7);
		    }else if (names[0]=="MSVxABCD_1B_Lxy" || names[0]=="MSVxABCD_1E_Lz"){
		      if (type == "bg" || type == "higgs") mg->GetYaxis()->SetRangeUser(0,0.5);
		      else                                 mg->GetYaxis()->SetRangeUser(0,0.5);
		    }
                    mg->GetXaxis()->SetLimits(0,8.6);
                }
                else if(names[i].Contains("Lz")){
                    mg->GetXaxis()->SetTitle("Long-lived particle |L_{z}| [m]");
		    
		    if (names[0]=="MSVx_Barrel_Lxy" || names[0]=="MSVx_Endcap_Lz"){
		      mg->GetYaxis()->SetRangeUser(0,1);
		    }else if (names[0]=="MSVxABCD_1B_Lxy" || names[0]=="MSVxABCD_1E_Lz"){
		      mg->GetYaxis()->SetRangeUser(0,0.8);
		    }
                    mg->GetXaxis()->SetLimits(0,15);
                }
                mg->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");


                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                y_val_label = 0.56;
                latex.DrawLatex(.19,.91,"#font[72]{ATLAS}  Simulation");
		latex.DrawLatex(.19,.86,"Internal");
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.040);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("Lxy")){
		  //latex2.DrawLatex(.19,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");
		  latex2.DrawLatex(.19,y_val_label,"#font[42]{Barrel vertices}");
                }
                else if(names[i].Contains("Lz") ){
		  //latex2.DrawLatex(.19,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");
		  latex2.DrawLatex(.19,y_val_label,"#font[42]{Endcap vertices}");
                }
		if (names[0]=="MSVx_Barrel_Lxy" || names[0]=="MSVx_Endcap_Lz"){
		  latex2.DrawLatex(.19,0.49,"#font[42]{2MSVx strategy}");
		}else if (names[0]=="MSVxABCD_1B_Lxy" || names[0]=="MSVxABCD_1E_Lz"){
		  latex2.DrawLatex(.19,0.49,"#font[42]{1MSVx+AO strategies}");
		}

                legS->Draw();
                TLatex detLabels;
                //detLabels.SetNDC();
                detLabels.SetTextColor(kBlack);
                detLabels.SetTextSize(0.03);
                detLabels.SetTextAlign(10);
                detLabels.SetTextAngle(90);

                if(i==0){
                    //barrel lines
		    TLine *hcalEnd;	
		    if (names[0]=="MSVx_Barrel_Lxy" || names[0]=="MSVx_Endcap_Lz"){    
		      hcalEnd = new TLine(3.865,0,3.865,0.7);
		    }else if (names[0]=="MSVxABCD_1B_Lxy" || names[0]=="MSVxABCD_1E_Lz"){
		      hcalEnd = new TLine(3.865,0,3.865,0.5);  // ** For ABCD plots
		    }
                    hcalEnd->SetLineColor(kGray);
                    hcalEnd->SetLineStyle(9);
                    hcalEnd->SetLineWidth(2);
                    hcalEnd->Draw();
                    TLine *mdts[6];
		     
		    if (names[0]=="MSVx_Barrel_Lxy" || names[0]=="MSVx_Endcap_Lz"){
		      mdts[0]= new TLine(4.383,0,4.383,0.7);
		      mdts[1]= new TLine(4.718,0,4.718,0.7);
		      mdts[2]= new TLine(7.888,0,7.888,0.7);
		      mdts[3]= new TLine(6.861,0,6.861,0.7);
		      mdts[4]= new TLine(10.275,0,10.275,0.7);
		      mdts[5]= new TLine(9.222,0,9.222,0.7);
		      
		      detLabels.DrawLatex(3.85,0.55,"#font[42]{HCal end}");
		      detLabels.DrawLatex(4.37,0.55,"#font[42]{MDT1 S}");
		      detLabels.DrawLatex(4.7,0.55,"#font[42]{MDT1 L}");
		      detLabels.DrawLatex(7.87,0.55,"#font[42]{MDT2 S}");
		      detLabels.DrawLatex(6.85,0.55,"#font[42]{MDT2 L}");
		      //detLabels.DrawLatex(10.26,0.46,"#font[42]{MDT3 S}");
		      //detLabels.DrawLatex(9.21,0.46,"#font[42]{MDT3 L}");
		    }else if (names[0]=="MSVxABCD_1B_Lxy" || names[0]=="MSVxABCD_1E_Lz"){
		      mdts[0]= new TLine(4.383,0,4.383,0.5);  // ** For ABCD plots 
		      mdts[1]= new TLine(4.718,0,4.718,0.5);
		      mdts[2]= new TLine(7.888,0,7.888,0.5);
		      mdts[3]= new TLine(6.861,0,6.861,0.5);
		      mdts[4]= new TLine(10.275,0,10.275,0.5);
		      mdts[5]= new TLine(9.222,0,9.222,0.5);
		      
		      detLabels.DrawLatex(3.85,0.35,"#font[42]{HCal end}");
		      detLabels.DrawLatex(4.37,0.35,"#font[42]{MDT1 S}");
		      detLabels.DrawLatex(4.7,0.35,"#font[42]{MDT1 L}");
		      detLabels.DrawLatex(7.87,0.35,"#font[42]{MDT2 S}");
		      detLabels.DrawLatex(6.85,0.35,"#font[42]{MDT2 L}");
		      //detLabels.DrawLatex(10.26,0.46,"#font[42]{MDT3 S}");
		      //detLabels.DrawLatex(9.21,0.46,"#font[42]{MDT3 L}");
		    }
                    for(int line=0;line<4;line++){
                        mdts[line]->SetLineColor(kGray+(line+2)/2);
                        mdts[line]->SetLineStyle(2 + 5*(line%2));
                        mdts[line]->SetLineWidth(2);

                        mdts[line]->Draw();
                    }
                }else if(i==1){
                    //endcap lines
		    
		    TLine *hcalEnd;
		    if (names[0]=="MSVx_Barrel_Lxy" || names[0]=="MSVx_Endcap_Lz"){
		      hcalEnd = new TLine(6.0,0,6.0,1);
		    }else if (names[0]=="MSVxABCD_1B_Lxy" || names[0]=="MSVxABCD_1E_Lz"){
		      hcalEnd = new TLine(6.0,0,6.0,0.8);  // ** For ABCD plots 
		    }
                    hcalEnd->SetLineColor(kGray);
                    hcalEnd->SetLineStyle(9);
                    hcalEnd->SetLineWidth(2);
                    hcalEnd->Draw();
                    TLine *mdts[6];
		    
		    if (names[0]=="MSVx_Barrel_Lxy" || names[0]=="MSVx_Endcap_Lz"){
		      mdts[0]= new TLine(7.023,0,7.023,1);
		      mdts[1]= new TLine(7.409,0,7.409,1);
		      mdts[2]= new TLine(13.265,0,13.265,1);
		      mdts[3]= new TLine(13.660,0,13.660,1);
		      mdts[4]= new TLine(21.260,0,21.260,1);
		      mdts[5]= new TLine(21.680,0,21.680,1);
		      detLabels.DrawLatex(5.98,0.75,"#font[42]{HCal end}");
		      detLabels.DrawLatex(7.01,0.75,"#font[42]{MDT1 S}");
		      detLabels.DrawLatex(7.39,0.75,"#font[42]{MDT1 L}");
		      detLabels.DrawLatex(13.25,0.75,"#font[42]{MDT2 S}");
		      detLabels.DrawLatex(13.65,0.75,"#font[42]{MDT2 L}");
		      //detLabels.DrawLatex(21.25,0.75,"#font[42]{MDT3 S}");
		      //detLabels.DrawLatex(21.67,0.75,"#font[42]{MDT3 L}");
		    }else if (names[0]=="MSVxABCD_1B_Lxy" || names[0]=="MSVxABCD_1E_Lz"){
		      mdts[0]= new TLine(7.023,0,7.023,0.8);  // ** For ABCD plots 
		      mdts[1]= new TLine(7.409,0,7.409,0.8);
		      mdts[2]= new TLine(13.265,0,13.265,0.8);
		      mdts[3]= new TLine(13.660,0,13.660,0.8);
		      mdts[4]= new TLine(21.260,0,21.260,0.8);
		      mdts[5]= new TLine(21.680,0,21.680,0.8);
		      detLabels.DrawLatex(5.98,0.55,"#font[42]{HCal end}");
		      detLabels.DrawLatex(7.01,0.55,"#font[42]{MDT1 S}");
		      detLabels.DrawLatex(7.39,0.55,"#font[42]{MDT1 L}");
		      detLabels.DrawLatex(13.25,0.55,"#font[42]{MDT2 S}");
		      detLabels.DrawLatex(13.65,0.55,"#font[42]{MDT2 L}");
		      //detLabels.DrawLatex(21.25,0.75,"#font[42]{MDT3 S}");
		      //detLabels.DrawLatex(21.67,0.75,"#font[42]{MDT3 L}");
		    }
                    for(int line=0;line<4;line++){
                        mdts[line]->SetLineColor(kGray+(line+2)/2);
                        mdts[line]->SetLineStyle(2 + 5*(line%2));
                        mdts[line]->SetLineWidth(2);

                        mdts[line]->Draw();
                    }
                }

                gPad->RedrawAxis();

                c->Print("../OutputPlots/notePlots/"+names[i]+"_eff_"+type+".pdf");
                legS->Clear();
                mg->Clear();

            }

        }
    }
}
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type);
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type){
    bool add_ctau = false;
    TString sigNames[6];
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<nSIG; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}

    Int_t colorSig[16] = { kPink-3,kViolet-3,kBlue-3, kAzure+5, kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};

    if(nSIG <= 4){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
	colorSig[3] = kRed; 
    }
    Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};

    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};

    TString names[12];
    names[0] = "MSVx_Barrel_Lxy";
    names[1] = "MSVx_Endcap_Lz";

    names[2] = "MSVx_Barrel_pT_A";
    names[3] = "MSVx_Barrel_pT_B";
    names[4] = "MSVx_Endcap_pT_A";
    names[5] = "MSVx_Endcap_pT_B";

    std::cout << "running on : " << _sig[0]->GetName() << std::endl;

    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //vertex reco
    for(unsigned int i=0;i<6; i++){

        std::cout << "plot: " << names[i] << std::endl;
        TGraphAsymmErrors* h_sig[30];
        TMultiGraph *mg = new TMultiGraph();
        double xvalA = 0.18; double xvalB = 0.566;
        if(names[i].Contains("pT") || names[i].Contains("pZ")){ xvalA = 0.68; xvalB = 0.93;}
        TLegend *legS = new TLegend(xvalA,0.7,xvalB,0.93);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);

        for(int j=0; j<nSIG; j++){
            TH1D *num = (TH1D*)_sig[j]->Get(names[i]);
            TH1D *den = (TH1D*)_sig[j]->Get(names[i]+"_denom");
            if(type == "higgs" && names[i].Contains("Lz")){
                num->Rebin(2); den->Rebin(2);
            }
            h_sig[j] = new TGraphAsymmErrors(num,den,"cl=0.683 b(1,1) mode");
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]); h_sig[j]->SetLineColor(colorSig[j]); h_sig[j]->SetMarkerStyle(markerSig[j]);

	    legS->AddEntry(h_sig[j],"#font[42]{"+sigNames[j]+"}","lp");
            mg->Add(h_sig[j]);

            if(j == nSIG-1){
                mg->Draw("AP");

                if(names[i].Contains("Lxy")){
                    mg->GetXaxis()->SetTitle("Long-lived particle L_{xy} [m]");
                    if (type == "bg" || type == "higgs") mg->GetYaxis()->SetRangeUser(0,0.7);
                    else                                 mg->GetYaxis()->SetRangeUser(0,0.7);
                    mg->GetXaxis()->SetLimits(0,8.6);
                }
                else if(names[i].Contains("Lz")){
                    mg->GetXaxis()->SetTitle("Long-lived particle |L_{z}| [m]");
                    mg->GetYaxis()->SetRangeUser(0,1);
                    mg->GetXaxis()->SetLimits(0,15);
                } else if(names[i].Contains("pT")){
                    mg->GetXaxis()->SetTitle("Long-lived particle p_{T} [GeV]");
                    mg->GetXaxis()->SetLimits(0,1200);
                } 
                mg->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");

                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                y_val_label = 0.86;
                if(names[i].Contains("L")) latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");
                else latex.DrawLatex(.18,.91,"#font[72]{ATLAS}  Internal");
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("Lxy")){latex2.DrawLatex(.70,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");}
                else if(names[i].Contains("Lz") ){latex2.DrawLatex(.65,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");}
                else if(names[i].Contains("Barrel_pT_A")){latex2.DrawLatex(.2,y_val_label,"#splitline{#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}}{3 < L_{xy} < 3.8 m}");}
                else if(names[i].Contains("Barrel_pT_B")){latex2.DrawLatex(.2,y_val_label,"#splitline{#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}}{3.8 < L_{xy} < 8 m}");}
                else if(names[i].Contains("Endcap_pT_A") ){latex2.DrawLatex(.2,y_val_label,"#splitline{#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}}{5 < L_{z} < 6 m}");}
                else if(names[i].Contains("Endcap_pT_B") ){latex2.DrawLatex(.2,y_val_label,"#splitline{#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}}{6 < L_{z} < 15 m}");}

                legS->Draw();
                gPad->RedrawAxis();

                c->Print("../OutputPlots/notePlots/"+names[i]+"_eff_"+type+".pdf");
                legS->Clear();
                mg->Clear();
            }

        }
    }
}


int makeMSEfficiencies_Note(bool forThesis = false){


    //gROOT->LoadMacro("AtlasUtils.C");
    //gROOT->LoadMacro("AtlasLabels.C");
    //gROOT->LoadMacro("AtlasStyle.C");

    bool add_ctau = false;

    //_bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
    //_bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
    TFile *_sig[6];
    //TString locn = "/Users/hrussell/Work/Run2Plots/signalMC/";
    TString locn = "../OutputPlots/signalMC/";

    /*
    _sig[0] = new TFile(locn+"mg250/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"mg500/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"mg800/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"mg1200/outputMSVxEff.root");
    _sig[4] = new TFile(locn+"mg1500/outputMSVxEff.root");
    _sig[5] = new TFile(locn+"mg2000/outputMSVxEff.root");
    //makeCombinedPlot(_sig, 6, "stealth");
    makePlotWithLines(_sig,"stealth");
    for(int i=0;i<6;i++){delete _sig[i];}
    */
    /*
    _sig[0] = new TFile(locn+"mg1200/outputMSEff.root");
    _sig[1] = new TFile(locn+"mg1500/outputMSEff.root");
    _sig[2] = new TFile(locn+"mg2000/outputMSEff.root");
    makeCombinedPlot(_sig, 3, "stealthHigh");
    for(int i=0;i<3;i++){delete _sig[i];}
    */
    /*
    _sig[0] = new TFile(locn+"mg250/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"mg800/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"mg2000/outputMSVxEff.root");
    makePlotWithLines(_sig, "detectorDetails");
    for(int i=0;i<3;i++){delete _sig[i];}
    */
    /*
    _sig[0] = new TFile(locn+"mg250/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"mg500/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"mg800/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"mg1200/outputMSVxEff.root");
    _sig[4] = new TFile(locn+"mg1500/outputMSVxEff.root");
    _sig[5] = new TFile(locn+"mg2000/outputMSVxEff.root");
    makeCombinedPlot(_sig, 6, "stealthMixed");
    */
    /*
    _sig[0] = new TFile(locn+"mH125mS5lt5/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"mH125mS8lt5/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"mH125mS15lt5/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"mH125mS25lt5/outputMSVxEff.root");
    _sig[4] = new TFile(locn+"mH125mS40lt5/outputMSVxEff.root");
    makePlotWithLines(_sig, "higgs");
    for(int i=0;i<5;i++){delete _sig[i];}
    */
    /*
    _sig[0] = new TFile(locn+"mH100mS8lt5/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"mH200mS25lt5/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"mH400mS50lt5/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"mH600mS50lt5/outputMSVxEff.root");
    _sig[4] = new TFile(locn+"mH1000mS150lt5/outputMSVxEff.root");
    _sig[5] = new TFile(locn+"mH1000mS400lt5/outputMSVxEff.root");
    makePlotWithLines(_sig,"scalar");
    for(int i=0;i<6;i++){delete _sig[i];}
    */
    /*    
    _sig[0] = new TFile(locn+"HChiChi_nubb_mH125mChi10/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"HChiChi_nubb_mH125mChi30/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"HChiChi_nubb_mH125mChi50/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"HChiChi_cbs_mH125mChi10/outputMSVxEff.root");
    _sig[4] = new TFile(locn+"HChiChi_cbs_mH125mChi30/outputMSVxEff.root");
    _sig[5] = new TFile(locn+"HChiChi_cbs_mH125mChi50/outputMSVxEff.root");
    makePlotWithLines(_sig, "bg");
    for(int i=0;i<6;i++){delete _sig[i];}
    */
    /*    
    _sig[0] = new TFile(locn+"HChiChi_nubb_mH125mChi10/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"HChiChi_nubb_mH125mChi50/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"HChiChi_nubb_mH125mChi100/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"HChiChi_cbs_mH125mChi10/outputMSVxEff.root");
    _sig[4] = new TFile(locn+"HChiChi_cbs_mH125mChi50/outputMSVxEff.root");
    _sig[5] = new TFile(locn+"HChiChi_cbs_mH125mChi100/outputMSVxEff.root");
    makePlotWithLines(_sig,"bg");
    for(int i=0;i<6;i++){delete _sig[i];}
    */
    
    _sig[0] = new TFile(locn+"HChiChi_cbs_mH125mChi10/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"HChiChi_cbs_mH125mChi50/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"HChiChi_cbs_mH125mChi100/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"HChiChi_tatanu_mH125mChi10/outputMSVxEff.root");
    _sig[4] = new TFile(locn+"HChiChi_tatanu_mH125mChi50/outputMSVxEff.root");
    _sig[5] = new TFile(locn+"HChiChi_tatanu_mH125mChi100/outputMSVxEff.root");
    makePlotWithLines(_sig,"bg");
    for(int i=0;i<6;i++){delete _sig[i];}
   

    /*    
    _sig[0] = new TFile(locn+"mg500/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"mg1500/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"HChiChi_cbs_mH125mChi10/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"HChiChi_cbs_mH125mChi100/outputMSVxEff.root");
    //_sig[4] = new TFile(locn+"mH100mS8lt5/outputMSVxEff.root");
    //_sig[5] = new TFile(locn+"mH1000mS50lt5/outputMSVxEff.root");
    makePlotWithLines(_sig, "stealthMixed");
    for(int i=0;i<4;i++){delete _sig[i];}
    */

    return 314;

}

//
//  MathUtils.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef plotVertexParameters_MathUtils_h
#define plotVertexParameters_MathUtils_h

#include "TMath.h"

namespace MathUtils{
    
    double wrapPhi(double phi){
        while (phi> TMath::Pi()) phi -= TMath::TwoPi();
        while (phi<-TMath::Pi()) phi += TMath::TwoPi();
        return phi;
        
    }
    double DeltaR2(double phi1, double phi2, double eta1, double eta2){
        double delPhi = wrapPhi(phi1-phi2);
        double delEta = eta1-eta2;
        return(delPhi*delPhi+delEta*delEta);
    }
    double DeltaR(double phi1, double phi2, double eta1, double eta2){
        double delPhi = wrapPhi(phi1-phi2);
        double delEta = eta1-eta2;
        return sqrt(delPhi*delPhi+delEta*delEta);
    }
};

#endif

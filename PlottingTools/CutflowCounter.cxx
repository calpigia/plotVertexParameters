//
//  CutflowCounter.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 03/09/16.
//
//

#include <PlottingPackage/CutflowCounter.h>

// For the creation of a CutflowCounter object that will store both trigger quantities
// and the offline parameters of matched LLPs for identification of Muon RoI Clusters.

CutflowCounter::CutflowCounter()
: m_nVariables(0)//  : eta(-99), phi(-99),
{}

void CutflowCounter::initialize(){
    m_counters = 0;
}

void CutflowCounter::addVariable(std::string variable_name){
    double counter = 0;
    std::make_pair<std::string, double> new_variable = std::make_pair(variable_name, counter);
    m_counters->push_back(new_variable);
    
    m_nVariables++;
}

bool CutflowCounter::addEvent(std::string criteria_passed, double event_weight){
    for(unsigned int i_var = 0; i_var < m_nVariables; i_var++){
        if(m_counters->at(i_var).first == criteria_passed){
            m_counters->at(i_var).second += event_weight;
            return true;
        }
    }
    return false;
}

void CutflowCounter::reset()
{
    m_nVariables = 0;
    m_counters->clear();
}

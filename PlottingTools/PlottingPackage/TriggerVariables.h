//
//  TriggerVariables.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef plotVertexParameters_TriggerVariables_h
#define plotVertexParameters_TriggerVariables_h
#include <vector>
#include "TChain.h"
#include "TRandom3.h"

//struct for RoI cluster
typedef struct lvl1_muclu_roi{
    lvl1_muclu_roi()
    {
      eta = -99;
      phi = -99;
      nRoI = -1;
      nTrks = 0;
      nJets = 0;
    }
    ~lvl1_muclu_roi()
    {
    }
    double eta;
    double phi;
    int nRoI;
    std::vector<int> inClu;
    int nJets;
    int nTrks;
} lvl1_muclu_roi;


struct TriggerVariables {
    
public:
    TRandom3 rnd;

    std::vector<double> *eta;
    std::vector<double> *phi;
    std::vector<int>    *nRoI;
    std::vector<int>    *nJet;
    std::vector<int>    *nTrk;
    std::vector<int>    *indexLLP;
    std::vector<double> *LLP_dR;
    std::vector<double> *trackdR;
    std::vector<double> *jetdR;
    std::vector<double> *l1Eta;
    std::vector<double> *l1Phi;
    std::vector<double> *cluEta;
    std::vector<double> *cluPhi;
    std::vector<int> *cluSyst; //-1 sigma, 0 (nominal) ,+1 sigma
    
    const double BarrelSF = 0.87;
    const double BarrelSyst = 0.01;
    const double EndcapSF = 0.96;
    const double EndcapSyst = 0.02;
    const double m_ClusterRadius = 0.4;
           
    void performJetIsolation(std::vector<double> *Jet_ET,std::vector<double> *Jet_eta,std::vector<double> *Jet_phi,std::vector<double> *Jet_logRatio);
    void performTrackIsolation(std::vector<double> *Track_pT,std::vector<double> *Track_eta,std::vector<double> *Track_phi);    
    void FindGoodTriggers(std::vector<double> &goodEta, std::vector<double> &goodPhi, std::vector<int> &goodRegion);
    void setZeroVectors();
    void setBranchAdresses(TChain *chain){ setBranchAdresses(chain, false); }
    void setBranchAdresses(TChain *chain, bool includeRoIs);
    void clearAllVectors(bool includeRoIs);
    void clearAllVectors(){ clearAllVectors(false); }
    void reclusterRoIs();
    void findAllRoIClusters(int n_RoI, const std::vector<lvl1_muclu_roi> &muRoIs, std::vector<lvl1_muclu_roi> &muClu1);
    int clusterMuonRoIs(const std::vector<lvl1_muclu_roi> &muonClu0, lvl1_muclu_roi &bestCluster, const int n_clu);
    std::vector<lvl1_muclu_roi> findUnusedRoIs(const std::vector<lvl1_muclu_roi> &muRoIs, const lvl1_muclu_roi &muClu);
    double wrapPhi(double phi);
    double DeltaR2(double phi1, double phi2, double eta1, double eta2);
    double DeltaR(double phi1, double phi2, double eta1, double eta2);
};


#endif

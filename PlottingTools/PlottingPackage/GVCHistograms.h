//
//  GVCHistograms.h
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#ifndef plotVertexParameters_GVCHistograms_h
#define plotVertexParameters_GVCHistograms_h


#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TString.h"
#include <iostream>
#include "TGraphAsymmErrors.h"

struct GVCHistograms {
public:
    
    TString object;
    TString locn;
    
    TH1D *h_denom_Pt;
    TH1D *h_denom_sumPt;
    TH1D *h_denom_dR;
    TH1D *h_denom_Et;
    TH1D *h_denom_trks;
    TH1D *h_denom_hits;
    
    TH1D *h_nTrks_fPass;
    TH1D *h_nMDT_fPass;
    TH1D *h_nMDT_up_fPass;
    TH1D *h_nRPC_fPass;
    TH1D *h_nTGC_fPass;

    TH1D *h_jetIso_vsEt_fPass;
    TH1D *h_jetIso_vsdR_fPass;
    TH1D *h_trkIso_vsdR_fPass;
    TH1D *h_trkIso_vsPt_fPass;
    TH1D *h_trkSumIso_vsPt_fPass;
    TH1D *h_trkSumIso_vsdR_fPass;
    
    TH2D *h_nMDT_vs_nRPC_fPass;
    TH2D *h_nMDT_vs_nTGC_fPass;
    
    TH2D *h_JetdR_vs_Et_highestE;
    TH2D *h_TrackdR_vs_Pt_highestPt;
    
    TH2D *h_JetdR_vs_Et_fPass;
    TH2D *h_TrackdR_vs_Pt_fPass;
    TH2D *h_TrackdR_vs_sumPt_fPass;
    
    TGraphAsymmErrors *g_jetIso_vsEt_fPass;
    TGraphAsymmErrors *g_jetIso_vsdR_fPass;
    TGraphAsymmErrors *g_trkIso_vsdR_fPass;
    TGraphAsymmErrors *g_trkIso_vsPt_fPass;
    TGraphAsymmErrors *g_trkSumIso_vsPt_fPass;
    TGraphAsymmErrors *g_trkSumIso_vsdR_fPass;
    
    TGraphAsymmErrors *g_nMDT_fPass;
    TGraphAsymmErrors *g_nMDT_up_fPass;
    TGraphAsymmErrors *g_nRPC_fPass;
    TGraphAsymmErrors *g_nTGC_fPass;
    
    void initializeHistograms(TString object, TString str);
    //void fillHits(VertexVariables* &vxVar, double finalWeight);
};

#endif

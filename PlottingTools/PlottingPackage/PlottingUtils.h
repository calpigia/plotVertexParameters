//
//  PlottingUtils.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef plotVertexParameters_PlottingUtils_h
#define plotVertexParameters_PlottingUtils_h

#include "TH1.h"
#include "TH2.h"
#include "TString.h"
#include "CommonVariables.h"
#include "GVCHistograms.h"
#include "TGraphAsymmErrors.h"

namespace PlottingUtils{
    //static TString m_plotDirectory;
    
    //static void setPlotDirectory(TString dir){m_plotDirectory = dir;}
    void make_2d_gvc_plot(std::vector<std::pair<int,int> > &by_vertices, TH2D* h_GVC_2D, const double xScale, const double yScale, const double finalWeight);
    void make_2d_gvc_hits(std::vector<std::pair<int,int> > &by_vertices, TH2D* h_GVC_2D, const double xScale, const double yScale, const double finalWeight);
    void make_1d_gvc_plot(std::vector<int> &by_vertices, TH1D* h_GVC_1D, const double xScale, const double finalWeight, const bool fillUp);
    
    void make_1d_gvc_plot(int threshold_bin, TH1D* h_GVC_1D, const double finalWeight, const bool fillUp);

    void make_jetIso_GVC(CommonVariables* &commonVar, const double eta, const double phi, GVCHistograms* &hists, const double dRCut, const double logRCut, const double ETCut, const double finalWeight);
    
    void make_trkIso_GVC(CommonVariables* &commonVar, const double eta, const double phi, GVCHistograms* &hists, const double dRCut, const double sumdRCut, const double sumpTCut, const double pTCut, const double finalWeight);
    
    void fillAllBins(const double weight, TH1D* h);
    void plotEfficiency(TString name, TString dir, TString Xtitle, TH1D *pass[2],TH1D *total, TString histType);
    void plotEfficiency(TString name, TString dir, TString Xtitle, TString Ytitle, TH2D *pass,TH2D *total, const double xMin, const double xMax,const double yMin, const double yMax);
    TGraphAsymmErrors* plotEfficiency(TString name, TString dir, TString Xtitle, TString Ytitle, TH1D *pass,TH1D *total, const double xMin, const double xMax);
    void plotMSEfficiency(TString name, TString dir, TString Xtitle, TH1D *pass[2],TH1D *total, TString histType);
    void plot(TString name, TString dir, TString Xtitle,TString Ytitle, TH1D *h1, TH1D *h2, TH1D *h3, bool log_scale, TString leg1, TString leg2, TString leg3);
    void plot(TString name, TString dir, TString Xtitle,TString Ytitle, TH1D *h1, TH1D *h2, bool log_scale, TString leg1, TString leg2);
    void plot(TString name, TString dir, TString Xtitle,TString Ytitle, TH1D *h1, bool log_scale, TString leg1);
    void plot(TString name, TString dir, TString Xtitle, TString Ytitle, TGraphAsymmErrors *h1,bool log_scale, TString leg1);
    void plots(TString name, TString dir, TString Xtitle, TH1D *h[2], bool log_scale, int legStyle = 2);
    void plot2DGVC(TString name, TString dir, TString Ytitle, TString Xtitle, TH2D *h, bool change_eff, double scale_factor, int plotType, const double yMin, const double yMax,  const double xMin, const double xMax, std::vector<double> &yProject, std::vector<double> &xProject);
    
    void plot2D(TString name, TString dir, TString Ytitle, TString Xtitle, TH2D *h);
    void plotWithRatio(TString name, TString dir, TString Ytitle, TString Xtitle, TH1D *h1, TH1D *h2, TString leg1, TString leg2,TString ratioTitle, bool logScale);
};

#endif

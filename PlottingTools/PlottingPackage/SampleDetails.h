//
//  SampleDetails.h
//  plotVertexParameters
//
//  Created by Heather Russell on 10/10/16.
//
//

#ifndef plotVertexParameters_SampleDetails_h
#define plotVertexParameters_SampleDetails_h
#include "TString.h"
#include <iostream>

namespace SampleDetails{

enum SampleID {mg250, mg500, mg800, mg1200, mg1500, mg2000, unknown};
int mediatorMass;
double MediatorXS =1.0; //in pb
double mediatorXS =1.0; //in pb
int llpMass;
double sim_ctau; //in mm
int pdgid;
double BVxSysErr_up =0;
double EVxSysErr_up =0;
double BTrigSysErr_up =0;
double ETrigSysErr_up =0;
double BVxSysErr_down =0;
double EVxSysErr_down =0;
double BTrigSysErr_down =0;
double ETrigSysErr_down =0;
double BJetBSysErr_up =0;
double BJetBSysErr_down =0;
double EJetBSysErr_up =0;
double EJetBSysErr_down =0;
double BJetESysErr_up =0;
double BJetESysErr_down =0;
double EJetESysErr_up =0;
double EJetESysErr_down =0;
double BJetSysErr_up =0;
double BJetSysErr_down =0;
double EJetSysErr_up =0;
double EJetSysErr_down =0;

double global2VxError_up =0;
double global2VxError_down =0;
double globalABCDError_up =0;
double globalABCDError_down =0;
double globalCombError_up =0;
double globalCombError_down =0;

    double bkg2VxError_up = 1.41;
    double bkg2VxError_down = 0.59;
    
    double bkgABCDError_up = 1.85;
    double bkgABCDError_down = 0.15;
    
    double bkgCombError_up = 1.85;
    double bkgCombError_down = 0.15;

double nObs2Vx =0;
double nEventsInSample =0;

SampleID s_id =SampleID::unknown;

void setGlobalVariables(TString sampleID){
 
    llpMass =100; pdgid =3000001;
    if(sampleID =="mg250"){
        s_id =SampleID::mg250;
        mediatorMass =250; mediatorXS =1190.35;
        sim_ctau =0.96; nEventsInSample =396000.;
        BVxSysErr_up=  0.011; EVxSysErr_up=0.006; BTrigSysErr_up=  0.012; ETrigSysErr_up=  0.019;
        BVxSysErr_down=0.005; EVxSysErr_down=  0.008; BTrigSysErr_down=0.012; ETrigSysErr_down=0.021;
        BJetBSysErr_up=0.036; EJetBSysErr_up=  0.060; BJetESysErr_up=  0.070; EJetESysErr_up=  0.070;
        BJetBSysErr_down=  0.027; EJetBSysErr_down=0.048; BJetESysErr_down=0.084; EJetESysErr_down=0.098;
        
        global2VxError_up = 1.06749; global2VxError_down = 0.93666;
        globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
        globalCombError_up = 1.09206; globalCombError_down = 0.922562;

    
    } else if(sampleID =="mg500"){
        s_id =SampleID::mg500;
        mediatorMass =500; mediatorXS =27.4171;
        sim_ctau =0.76; nEventsInSample =391514.;
        BVxSysErr_up=  0.004; EVxSysErr_up=0.006; BTrigSysErr_up=  0.012; ETrigSysErr_up=  0.016;
        BVxSysErr_down=0.004; EVxSysErr_down=  0.006; BTrigSysErr_down=0.012; ETrigSysErr_down=0.017;
        BJetBSysErr_up=0.016; EJetBSysErr_up=  0.027; BJetESysErr_up=  0.038; EJetESysErr_up=  0.036;
        BJetBSysErr_down=  0.013; EJetBSysErr_down=0.024; BJetESysErr_down=0.031; EJetESysErr_down=0.032;
        
        global2VxError_up = 1.06315; global2VxError_down = 0.940577;
        globalABCDError_up = 1.06315; globalABCDError_down = 0.958086;
        globalCombError_up = 1.05635; globalCombError_down = 0.962288;


    } else if(sampleID =="mg800"){
        s_id  =SampleID::mg800;
        mediatorMass =800; mediatorXS =1.4891;
        sim_ctau =0.62; nEventsInSample =389058.;
        BVxSysErr_up=  0.012; EVxSysErr_up=0.006; BTrigSysErr_up=  0.012; ETrigSysErr_up=  0.015;
        BVxSysErr_down=0.006; EVxSysErr_down=  0.006; BTrigSysErr_down=0.011; ETrigSysErr_down=0.016;
        BJetBSysErr_up=0.010; EJetBSysErr_up=  0.017; BJetESysErr_up=  0.014; EJetESysErr_up=  0.050;
        BJetBSysErr_down=  0.006; EJetBSysErr_down=0.009; BJetESysErr_down=0.014; EJetESysErr_down=0.025;
        
        global2VxError_up = 1.06152; global2VxError_down = 0.942328;
        globalABCDError_up = 1.05004; globalABCDError_down = 0.973087;
        globalCombError_up = 1.04671; globalCombError_down = 0.974656;


    } else if(sampleID =="mg1200"){
        s_id =SampleID::mg1200;
        mediatorMass =1200; mediatorXS =0.0856418;
        sim_ctau =0.5;nEventsInSample =396000.;
        BVxSysErr_up=  0.008; EVxSysErr_up=0.006; BTrigSysErr_up=  0.011; ETrigSysErr_up=  0.012;
        BVxSysErr_down=0.005; EVxSysErr_down=  0.010; BTrigSysErr_down=0.010; ETrigSysErr_down=0.013;
       
        BJetBSysErr_up= 0.006; EJetBSysErr_up=  0.004; BJetESysErr_up=  0.009; EJetESysErr_up=  0.009;
        BJetBSysErr_down=  0.003; EJetBSysErr_down=0.004; BJetESysErr_down=0.005; EJetESysErr_down=0.012;
        
        global2VxError_up = 1.06177; global2VxError_down = 0.942591;
        globalABCDError_up = 1.05116; globalABCDError_down = 0.974923;
        globalCombError_up = 1.04818; globalCombError_down = 0.976196;


    } else if(sampleID =="mg1500"){
        s_id =SampleID::mg1500;
        mediatorMass =1500; mediatorXS =0.0141903;
        sim_ctau =0.45;nEventsInSample =394000.;
        BVxSysErr_up=  0.007; EVxSysErr_up=0.004; BTrigSysErr_up=  0.011; ETrigSysErr_up=  0.016;
        BVxSysErr_down=0.004; EVxSysErr_down=  0.011; BTrigSysErr_down=0.010; ETrigSysErr_down=0.012;
        BJetBSysErr_up=0.003; EJetBSysErr_up=  0.005; BJetESysErr_up=  0.006; EJetESysErr_up=  0.016;
        BJetBSysErr_down=  0.004; EJetBSysErr_down=0.005; BJetESysErr_down=0.006; EJetESysErr_down=0.006;
        
        global2VxError_up = 1.06121; global2VxError_down = 0.944528;
        globalABCDError_up = 1.05301; globalABCDError_down = 0.974391;
        globalCombError_up = 1.0499; globalCombError_down = 0.975723;


    } else if(sampleID =="mg2000"){
        s_id =SampleID::mg2000;
        mediatorMass =2000; mediatorXS =0.000981077;
        sim_ctau =0.37;nEventsInSample =389729.;
        BVxSysErr_up=  0.004; EVxSysErr_up=0.005; BTrigSysErr_up=  0.011; ETrigSysErr_up=  0.012;
        BVxSysErr_down=0.004; EVxSysErr_down=  0.007; BTrigSysErr_down=0.009; ETrigSysErr_down=0.011;
        BJetBSysErr_up=0.003; EJetBSysErr_up=  0.006; BJetESysErr_up=  0.003; EJetESysErr_up=  0.009;
        BJetBSysErr_down=  0.004; EJetBSysErr_down=0.006; BJetESysErr_down=0.007; EJetESysErr_down=0.009;
        
        global2VxError_up = 1.06369; global2VxError_down = 0.942502;
        globalABCDError_up = 1.05381; globalABCDError_down = 0.972637;
        globalCombError_up = 1.05054; globalCombError_down = 0.974121;


    } else if (sampleID =="mH125mS5lt5"){
        //s_id =SampleID::mH125mS5lt5;
        mediatorMass =125; mediatorXS =48.58;//N3LO value 48.58. NNLO+NNLL =44.14.
        sim_ctau =0.127;
        BVxSysErr_up=0.010; EVxSysErr_up=0.004; BTrigSysErr_up=0.030; ETrigSysErr_up=0.051;
        BVxSysErr_down=0.000; EVxSysErr_down=0.001; BTrigSysErr_down=0.022; ETrigSysErr_down=0.048;

        globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
        global2VxError_up = 1.24739; global2VxError_down = 0.815351;


        nObs2Vx =68.4;

    } else if (sampleID =="mH125mS5lt9"){
        //s_id =SampleID::mH125mS5lt9;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =0.228;
        BVxSysErr_up =0.0; EVxSysErr_up =0.0;
        BTrigSysErr_up =0.0; ETrigSysErr_up =0.0;

        BVxSysErr_down =0.0; EVxSysErr_down =0.0;
        BTrigSysErr_down =0.0; ETrigSysErr_down =0.0;

        nObs2Vx =46.4;

    } else if (sampleID =="mH125mS8lt5"){
        //s_id =SampleID::mH125mS8lt5;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =0.200;
        BVxSysErr_up=0.015; EVxSysErr_up=0.002; BTrigSysErr_up=0.029; ETrigSysErr_up=0.051;
        BVxSysErr_down=0.008; EVxSysErr_down=0.005; BTrigSysErr_down=0.021; ETrigSysErr_down=0.045;

        globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
        global2VxError_up = 1.23121; global2VxError_down = 0.791191;


        nObs2Vx =84.5;//83.3;

    } else if (sampleID =="mH125mS8lt9"){
        //s_id =SampleID::mH125mS8lt9;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =0.375;
        BVxSysErr_up =0.025; EVxSysErr_up =0.015;
        BTrigSysErr_up =0.021; ETrigSysErr_up =0.044;

        BVxSysErr_down =0.015; EVxSysErr_down =0.011;
        BTrigSysErr_down =0.024; ETrigSysErr_down =0.048;

        nObs2Vx =72.5;//71.6;

    } else if (sampleID =="mH125mS15lt5"){
        //s_id =SampleID::mH125mS15lt5;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =0.580;
        BVxSysErr_up=0.009; EVxSysErr_up=0.001; BTrigSysErr_up=0.029; ETrigSysErr_up=0.048;
        BVxSysErr_down=0.005; EVxSysErr_down=0.005; BTrigSysErr_down=0.023; ETrigSysErr_down=0.043;

        globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
        global2VxError_up = 1.12689; global2VxError_down = 0.883134;


        nObs2Vx =262.7;//260.2;

    } else if (sampleID =="mH125mS15lt9"){
        //s_id =SampleID::mH125mS15lt9;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =0.710;
        BVxSysErr_up =0.0; EVxSysErr_up =0.0;
        BTrigSysErr_up =0.0; ETrigSysErr_up =0.0;

        BVxSysErr_down =0.0; EVxSysErr_down =0.0;
        BTrigSysErr_down =0.0; ETrigSysErr_down =0.0;

        nObs2Vx =116.5;//115.6;

    } else if (sampleID =="mH125mS25lt5"){
        //s_id =SampleID::mH125mS25lt5;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =0.76;
        BVxSysErr_up=0.017; EVxSysErr_up=0.001; BTrigSysErr_up=0.021; ETrigSysErr_up=0.041;
        BVxSysErr_down=0.005; EVxSysErr_down=0.001; BTrigSysErr_down=0.022; ETrigSysErr_down=0.042;
        
        globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
        global2VxError_up = 1.13826; global2VxError_down = 0.871713;


        nObs2Vx =186.3;//185.3;

    } else if (sampleID =="mH125mS25lt9"){
        //s_id =SampleID::mH125mS25lt9;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =1.21;
        BVxSysErr_up =0.043; EVxSysErr_up =0.01;
        BTrigSysErr_up =0.023; ETrigSysErr_up =0.046;

        BVxSysErr_down =0.031; EVxSysErr_down =0.009;
        BTrigSysErr_down =0.023; ETrigSysErr_down =0.044;

        nObs2Vx =129.1;//129.1;

    } else if (sampleID =="mH125mS40lt5"){
        //s_id =SampleID::mH125mS40lt5;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =1.18;
        BVxSysErr_up=0.002; EVxSysErr_up=0.002; BTrigSysErr_up=0.017; ETrigSysErr_up=0.045;
        BVxSysErr_down=0.003; EVxSysErr_down=0.006; BTrigSysErr_down=0.019; ETrigSysErr_down=0.039;

        globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
        global2VxError_up = 1.17738; global2VxError_down = 0.849188;
        nObs2Vx =117.9;

    } else if (sampleID =="mH125mS40lt9"){
        //s_id =SampleID::mH125mS40lt9;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =1.90;
        BVxSysErr_up =0.024; EVxSysErr_up =0.008;
        BTrigSysErr_up =0.033; ETrigSysErr_up =0.046;

        BVxSysErr_down =0.021; EVxSysErr_down =0.011;
        BTrigSysErr_down =0.022; ETrigSysErr_down =0.042;

        nObs2Vx =93.7;
    } else if (sampleID =="mH125mS55lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =1.54;
        BVxSysErr_up=0.007; EVxSysErr_up=0.013; BTrigSysErr_up=0.020; ETrigSysErr_up=0.056;
        BVxSysErr_down=0.003; EVxSysErr_down=0.006; BTrigSysErr_down=0.022; ETrigSysErr_down=0.043;

        globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
        global2VxError_up = 1.57844; global2VxError_down = 0.649186;

        nObs2Vx =6.7;
    } else if (sampleID =="mH125mS55lt9"){
        //s_id =SampleID::mH125mS55lt9;
        mediatorMass =125; mediatorXS =48.58;//N3LO value. NNLO+NNLL =44.14.
        sim_ctau =2.73;
        BVxSysErr_up =0.022; EVxSysErr_up =0.017;
        BTrigSysErr_up =0.020; ETrigSysErr_up =0.054;

        BVxSysErr_down =0.022; EVxSysErr_down =0.013;
        BTrigSysErr_down =0.021; ETrigSysErr_down =0.050;
        nObs2Vx =7.5;

    }  else if (sampleID =="mH100mS8lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =100; mediatorXS =1.;
        sim_ctau =0.24;
        BVxSysErr_up=0.014; EVxSysErr_up=0.004; BTrigSysErr_up=0.022; ETrigSysErr_up=0.049;
        BVxSysErr_down=0.012; EVxSysErr_down=0.013; BTrigSysErr_down=0.026; ETrigSysErr_down=0.050;

        global2VxError_up = 1.31333; global2VxError_down = 0.794639;
 
        nObs2Vx =40.6;//39.8;
    }  else if (sampleID =="mH100mS25lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =100; mediatorXS =1.;
        sim_ctau =0.74;
        BVxSysErr_up=0.003; EVxSysErr_up=0.001; BTrigSysErr_up=0.023; ETrigSysErr_up=0.048;
        BVxSysErr_down=0.018; EVxSysErr_down=0.001; BTrigSysErr_down=0.023; ETrigSysErr_down=0.041;
        
        global2VxError_up = 1.23472; global2VxError_down = 0.796234;

        nObs2Vx =58.1;//58.1;
    }  else if (sampleID =="mH200mS8lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =200; mediatorXS = 1.;
        sim_ctau =0.17;
        BVxSysErr_up=0.055; EVxSysErr_up=0.002; BTrigSysErr_up=0.021; ETrigSysErr_up=0.046;
        BVxSysErr_down=0.007; EVxSysErr_down=0.005; BTrigSysErr_down=0.023; ETrigSysErr_down=0.038;
        
        global2VxError_up = 1.17782; global2VxError_down = 0.85079;

        nObs2Vx =164.6;//161.2;

    }  else if(sampleID =="mH200mS8lt9"){
        mediatorMass =200; mediatorXS = 1.;
        sim_ctau =0.29;
        nObs2Vx =119.9;//116.7;
    }  else if (sampleID =="mH200mS25lt5"){

        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =200; mediatorXS = 1.;
        sim_ctau =0.54;
        BVxSysErr_up=0.001; EVxSysErr_up=0.001; BTrigSysErr_up=0.021; ETrigSysErr_up=0.038;
        BVxSysErr_down=0.002; EVxSysErr_down=0.001; BTrigSysErr_down=0.021; ETrigSysErr_down=0.034;

        global2VxError_up = 1.10582; global2VxError_down = 0.90231;

        nObs2Vx =432.5;//428.1;

    }  else if(sampleID =="mH200mS25lt9"){
        mediatorMass =200; mediatorXS = 1.;
        sim_ctau =0.95;
        nObs2Vx =310.5;//305.9;

    }  else if (sampleID =="mH200mS50lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =200; mediatorXS = 1.;
        sim_ctau =1.07;
        BVxSysErr_up=0.004; EVxSysErr_up=0.001; BTrigSysErr_up=0.019; ETrigSysErr_up=0.033;
        BVxSysErr_down=0.007; EVxSysErr_down=0.001; BTrigSysErr_down=0.018; ETrigSysErr_down=0.034;
        
        global2VxError_up = 1.10984; global2VxError_down = 0.891434;

        nObs2Vx =350.5;//345.2;

    }  else if(sampleID =="mH200mS50lt9"){
        mediatorMass =200; mediatorXS = 1.;
        sim_ctau =1.9;
        nObs2Vx =296.3;//294.5;

    }  else if (sampleID =="mH400mS50lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =400; mediatorXS = 1.;
        sim_ctau =0.7;
        BVxSysErr_up=0.012; EVxSysErr_up=0.002; BTrigSysErr_up=0.015; ETrigSysErr_up=0.024;
        BVxSysErr_down=0.003; EVxSysErr_down=0.002; BTrigSysErr_down=0.015; ETrigSysErr_down=0.024;

        global2VxError_up = 1.07138; global2VxError_down = 0.931024;

        nObs2Vx =1100.6;//975.8;

    }  else if(sampleID =="mH400mS50lt9"){
        mediatorMass =400; mediatorXS = 1.;
        sim_ctau =1.26;
        nObs2Vx =736.2;//656.9;

    }  else if (sampleID =="mH400mS100lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =400; mediatorXS = 1.;
        sim_ctau =1.46;
        BVxSysErr_up=0.001; EVxSysErr_up=0.001; BTrigSysErr_up=0.016; ETrigSysErr_up=0.030;
        BVxSysErr_down=0.003; EVxSysErr_down=0.001; BTrigSysErr_down=0.016; ETrigSysErr_down=0.025;

        global2VxError_up = 1.07834; global2VxError_down = 0.922821;
        nObs2Vx =955.15;//889.1;

    }  else if(sampleID =="mH400mS100lt9"){
        mediatorMass =400; mediatorXS = 1.;
        sim_ctau =2.64;
        nObs2Vx =618.5;//568.5;

    }  else if (sampleID =="mH600mS50lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =600; mediatorXS = 1.;
        sim_ctau =0.520;
        BVxSysErr_up=0.008; EVxSysErr_up=0.003; BTrigSysErr_up=0.015; ETrigSysErr_up=0.025;
        BVxSysErr_down=0.002; EVxSysErr_down=0.003; BTrigSysErr_down=0.014; ETrigSysErr_down=0.022;
        
        global2VxError_up = 1.06573; global2VxError_down = 0.936504;

        nObs2Vx =1302.3;//1014.8;

    }  else if(sampleID =="mH600mS50lt9"){
        mediatorMass =600; mediatorXS = 1.;
        sim_ctau =0.96;
        nObs2Vx =842.1;//707.4;
    }  else if (sampleID =="mH600mS150lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =600; mediatorXS = 1.;
        sim_ctau =1.72;
        BVxSysErr_up=0.009; EVxSysErr_up=0.002; BTrigSysErr_up=0.013; ETrigSysErr_up=0.021;
        BVxSysErr_down=0.003; EVxSysErr_down=0.003; BTrigSysErr_down=0.015; ETrigSysErr_down=0.021;
        
        global2VxError_up = 1.0703; global2VxError_down = 0.931534;

        nObs2Vx =1181.1;//992.5;

    }  else if(sampleID =="mH600mS150lt9"){
        mediatorMass =600; mediatorXS = 1.;
        sim_ctau =3.14;
        nObs2Vx =709.5;//617.6;
    }  else if (sampleID =="mH1000mS50lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =1000; mediatorXS = 1.;
        sim_ctau =0.38;
        BVxSysErr_up=0.001; EVxSysErr_up=0.006; BTrigSysErr_up=0.011; ETrigSysErr_up=0.019;
        BVxSysErr_down=0.009; EVxSysErr_down=0.008; BTrigSysErr_down=0.014; ETrigSysErr_down=0.019;

        global2VxError_up = 1.05794; global2VxError_down = 0.942251;

        nObs2Vx =1275.0;//690.6;

    }  else if(sampleID =="mH1000mS50lt9"){
        mediatorMass =1000; mediatorXS = 1.;
        sim_ctau =0.67;
        nObs2Vx =658.3;//437.5;

    }  else if (sampleID =="mH1000mS150lt5"){
        //s_id =SampleID::mH125mS55lt5;
        mediatorMass =1000; mediatorXS = 1.;
        sim_ctau =1.17;
        BVxSysErr_up=0.002; EVxSysErr_up=0.003; BTrigSysErr_up=0.011; ETrigSysErr_up=0.014;
        BVxSysErr_down=0.003; EVxSysErr_down=0.006; BTrigSysErr_down=0.011; ETrigSysErr_down=0.017;

        global2VxError_up = 1.05234; global2VxError_down = 0.947996;

        nObs2Vx =1757.0;//1114.3;

    }  else if(sampleID =="mH1000mS150lt9"){
        mediatorMass =1000; mediatorXS = 1.;
        sim_ctau =2.11;
        nObs2Vx =936.6;//646.5;

    }  else if (sampleID =="mH1000mS400lt5"){
        //s_id =SampleID::mH125mS55lt5;
        std::cout << "Sample: " << sampleID << std::endl;
        mediatorMass =1000; mediatorXS = 1.;
        sim_ctau =3.96;
        BVxSysErr_up=0.002; EVxSysErr_up=0.012; BTrigSysErr_up=0.014; ETrigSysErr_up=0.021;
        BVxSysErr_down=0.025; EVxSysErr_down=0.012; BTrigSysErr_down=0.017; ETrigSysErr_down=0.022;
        
        global2VxError_up = 1.12356; global2VxError_down = 0.888426;

        nObs2Vx =689.5;//582.1;

    }  else if(sampleID =="mH1000mS400lt9"){
        mediatorMass =1000; mediatorXS = 1.;
        sim_ctau =7.2;
        nObs2Vx =285.9;//231.8;

    } else if (sampleID == "HChiChi_nubb_mH125mChi10"){
      //s_id = SampleID::HChiChi_nubb_mH125mChi10;
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 0.92;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.12024; global2VxError_down = 0.891526;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_nubb_mH125mChi30"){
      //s_id = SampleID::HChiChi_nubb_mH125mChi30; 
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 2.75;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_nubb_mH125mChi50"){
      //s_id = SampleID::HChiChi_nubb_mH125mChi50; 
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 5.55;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_nubb_mH125mChi100"){
      //s_id = SampleID::HChiChi_nubb_mH125mChi100;
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 3.50;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_cbs_mH125mChi10"){
      //s_id = SampleID::HChiChi_cbs_mH125mChi10;
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 0.92;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_cbs_mH125mChi30"){
      //s_id = SampleID::HChiChi_cbs_mH125mChi30; 
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 2.75;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_cbs_mH125mChi50"){
      //s_id = SampleID::HChiChi_cbs_mH125mChi50; 
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 5.55;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_cbs_mH125mChi100"){
      //s_id = SampleID::HChiChi_cbs_mH125mChi100;
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 3.50;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_lcb_mH125mChi10"){
      //s_id = SampleID::HChiChi_lcb_mH125mChi10;
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 0.92;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_lcb_mH125mChi30"){
      //s_id = SampleID::HChiChi_lcb_mH125mChi30; 
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 2.75;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_lcb_mH125mChi50"){
      //s_id = SampleID::HChiChi_lcb_mH125mChi50; 
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 5.55;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_lcb_mH125mChi100"){
      //s_id = SampleID::HChiChi_lcb_mH125mChi100;
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 3.50;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_tatanu_mH125mChi10"){
      //s_id = SampleID::HChiChi_tatanu_mH125mChi10;
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 0.92;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_tatanu_mH125mChi30"){
      //s_id = SampleID::HChiChi_tatanu_mH125mChi30; 
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 2.75;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_tatanu_mH125mChi50"){
      //s_id = SampleID::HChiChi_tatanu_mH125mChi50; 
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 5.55;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    } else if (sampleID == "HChiChi_tatanu_mH125mChi100"){
      //s_id = SampleID::HChiChi_tatanu_mH125mChi100;
      mediatorMass = 125; mediatorXS = 1.;
      sim_ctau = 3.50;
      BVxSysErr_up = 0.043;   EVxSysErr_up = 0.01;    BTrigSysErr_up = 0.023;   ETrigSysErr_up = 0.046;
      BVxSysErr_down = 0.031; EVxSysErr_down = 0.009; BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;

      globalABCDError_up = 1.1387; globalABCDError_down = 0.884403;
      global2VxError_up = 1.04995; global2VxError_down = 0.950212;

      nObs2Vx = 0.0;

    }   else {
        s_id =SampleID::unknown;
        mediatorMass =0; mediatorXS =1.;
        sim_ctau =0.0;
        BVxSysErr_up =0.0; EVxSysErr_up =0.0;
        BTrigSysErr_up =0.0; ETrigSysErr_up =0.0;

        BVxSysErr_down =0.0; EVxSysErr_down =0.0;
        BTrigSysErr_down =0.0; ETrigSysErr_down =0.0;

        BJetSysErr_up =    0.0;   EJetSysErr_up =    0.0;
        BJetSysErr_down=0.0;   EJetSysErr_down=0.0;
    }
}

}


#endif

//
//  TriggerHistograms.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef __plotVertexParameters__TriggerHistograms__
#define __plotVertexParameters__TriggerHistograms__

#include <stdio.h>
#include "TH1.h"
#include "TH2.h"

struct TriggerHistograms {
public:

	TH1D *h_leadingJet_RoICluster_dR;
	TH1D *h_leadingTrk_RoICluster_dR;
	TH1D *h_highestpT_Trk_RoICluster;
	TH2D *h_RoICuster_EtaVsPhi;
    
    void initializeHistograms();
    void fillHistograms(CommonVariables *commonVar, bool vxMatched);

};



#endif /* defined(__plotVertexParameters__TriggerHistograms__) */

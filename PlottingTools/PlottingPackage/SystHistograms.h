//
//  SystHistograms.h
//  plotVertexParameters
//
//  Created by Heather Russell on 24/02/16.
//
//

#ifndef __plotVertexParameters__SystHistograms__
#define __plotVertexParameters__SystHistograms__

#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
struct SystHistograms{
  public:
    
    TH1D* h_eta;
    TH1D* h_phi;
    TH1D* h_pT;
    TH1D* h_ET;
    TH1D* h_logRatio;
    
    TH1D* h_width;

    TH1D* h_width_nAssocMSeg;
    TH1D* h_width_nMSTracklets;
    TH1D* h_width_nMuonRoIs;
    
    TH1D* h_eta_nAssocMSeg;
    TH1D* h_eta_nMSTracklets;
    TH1D* h_eta_nMuonRoIs;
    TH1D* h_eta_width;
    
    TH1D* h_pT_nAssocMSeg;
    TH1D* h_pT_nMSTracklets;
    TH1D* h_pT_nMuonRoIs;
    TH1D* h_pT_width;
    TH1D* h_pT_denom_all;
    
    TH1D* h_pT_nAssocMSeg_num;
    TH1D* h_pT_nMSTracklets_num;
    TH1D* h_pT_nMuonRoIs_num;
    
    TH1D* h_pT_nAssocMSeg_sq;
    TH1D* h_pT_nMSTracklets_sq;
    TH1D* h_pT_nMuonRoIs_sq;
    
    TH1D* h_nMSeg_nMSTracklets;
    TH1D* h_nMSeg_nMuonRoIs;
    
    TH1D* h_nAssocMSeg;
    TH1D* h_nMSTracklets;
    TH1D* h_cone_avgTracklets;
    TH1D* h_nMuonRoIs;
    TH1D* h_nMSVx;
    
    TH1D* h_MSVx_nMDT;
    
    TH2D* h_MSegVsTracklets;
    
    //prob. hists for systematics
    TH1D* h_pT_denom;
    TH1D* h_eta_denom;
    TH1D* h_nMSeg_denom;
    
    TH2D* h_pT_eta_denom;
    
    TH2D* h_pT_eta_pMSVx;
    TH2D* h_pT_eta_pTrig;
    
    TH1D* h_eta_pMSVx;
    TH1D* h_eta_pTrig;
    TH1D* h_pT_pMSVx;
    TH1D* h_pT_pTrig;
    TH1D* h_nMSeg_pMSVx;
    TH1D* h_nMSeg_pTrig;
    
    TH1D* h_CR_BDT;
    TH1D* h_CR_nTrk;
    TH1D* h_CR_sumTrkpT;
    TH1D* h_CR_maxTrkpT;
    TH1D* h_CR_width;
    TH1D* h_CR_jetDRTo2GeVTrack;
    TH1D* h_CR_l1frac;
    TH1D* h_CR_c_firenden;
    TH1D* h_CR_c_lat;
    TH1D* h_CR_c_long;
    TH1D* h_CR_c_r;
    TH1D* h_CR_c_lambda;
    TH1D* h_CR_logR;
    /*
    TH1D* h_CR_pT_BDT5;
    TH1D* h_CR_pT_BDT13;
    TH1D* h_CR_pT_nTrk;
    TH1D* h_CR_pT_sumTrkpT;
    TH1D* h_CR_pT_maxTrkpT;
    TH1D* h_CR_pT_width;
    TH1D* h_CR_pT_jetDRTo2GeVTrack;
    TH1D* h_CR_pT_l1frac;
    TH1D* h_CR_pT_c_firenden;
    TH1D* h_CR_pT_c_lat;
    TH1D* h_CR_pT_c_long;
    TH1D* h_CR_pT_c_r;
    TH1D* h_CR_pT_c_lambda;
    TH1D* h_CR_pT_logR;
    
    TH1D* h_CR_eta_BDT5;
    TH1D* h_CR_eta_BDT13;
    TH1D* h_CR_eta_nTrk;
    TH1D* h_CR_eta_sumTrkpT;
    TH1D* h_CR_eta_maxTrkpT;
    TH1D* h_CR_eta_width;
    TH1D* h_CR_eta_jetDRTo2GeVTrack;
    TH1D* h_CR_eta_l1frac;
    TH1D* h_CR_eta_c_firenden;
    TH1D* h_CR_eta_c_lat;
    TH1D* h_CR_eta_c_long;
    TH1D* h_CR_eta_c_r;
    TH1D* h_CR_eta_c_lambda;
    TH1D* h_CR_eta_logR;*/ //might need these later
    
    //look only at large-ish logR jets!
    TH1D* h_CR_logR_BDT;
    TH1D* h_CR_logR_nTrk;
    TH1D* h_CR_logR_sumTrkpT;
    TH1D* h_CR_logR_maxTrkpT;
    TH1D* h_CR_logR_width;
    TH1D* h_CR_logR_jetDRTo2GeVTrack;
    TH1D* h_CR_logR_l1frac;
    TH1D* h_CR_logR_c_firenden;
    TH1D* h_CR_logR_c_lat;
    TH1D* h_CR_logR_c_long;
    TH1D* h_CR_logR_c_r;
    TH1D* h_CR_logR_c_lambda;
    
    void initializeMSHistograms(TString obj, TString str);
    void initializeCRHistograms(TString obj, TString str);
    
};

#endif /* defined(__plotVertexParameters__SystHistograms__) */

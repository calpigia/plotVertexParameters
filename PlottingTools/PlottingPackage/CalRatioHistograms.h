//
//  CalRatioHistograms.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef __plotVertexParameters__CalRatioHistograms__
#define __plotVertexParameters__CalRatioHistograms__

#include <stdio.h>
#include "TH1.h"
#include "TH2.h"

struct CalRatioHistograms {
public:

    //
    TH2D *h_CalRatio_phieta;
    TH2D *h_CalRatio_phieta_matched;

    TH2D *h_CalRatio_TrackdR_vs_Pt_fPass;
    TH2D *h_CalRatio_TrackdR_vs_sumPt_fPass;
    TH2D *h_CalRatio_JetdR_vs_Et_fPass;

    
    void initializeHistograms();
};



#endif /* defined(__plotVertexParameters__CalRatioHistograms__) */

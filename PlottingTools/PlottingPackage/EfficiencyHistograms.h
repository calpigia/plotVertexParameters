//
//  EfficiencyHistograms.h
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#ifndef __plotVertexParameters__EfficiencyHistograms__
#define __plotVertexParameters__EfficiencyHistograms__

#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TEfficiency.h"

struct EfficiencyHistograms {
public:
    std::vector<TH1D* > h_numerator; //good vertices only
    std::vector<TH1D* > h_numeratorALL; //all vertices, good or bad.

    std::vector<TEfficiency* > t_eff;
    
    std::vector<TH1D* > h_denominator;
    std::vector<TH2D* > h_numerator2D;
    std::vector<TH2D* > h_denominator2D;

    void addHist(TString name, int nx, Double_t xbins[], int ny, Double_t ybins[]);
    void addHist(TString name, int nx, Double_t x[]);
    void addHist(TString name, int xBin, double xMin, double xMax, int yBin, double yMin, double yMax);
    void addHist(TString name, int xBin, double xMin, double xMax);
    void addHist2(TString name, int xBin, double xMin, double xMax);
    void addEffHist(TString name, int xBin, double xMin, double xMax);
    void fill(TString name, double value, double weight);
    void fillEff(TString name, double value, double weight, bool pass);
    void fill(TString name, double valueX, double valueY, double weight);
    void plotEfficiencies(TString histDir);
    TString findHistType(TString histName);
    
};
#endif /* defined(__plotVertexParameters__EfficiencyHistograms__) */

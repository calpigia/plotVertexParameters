//
//  CalRatioVariables.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#include "PlottingPackage/CalRatioVariables.h"
#include <iostream>

void CalRatioVariables::setZeroVectors(){
    BDT = 0;
    nTrk = 0;
    sumTrkpT = 0;
    maxTrkpT = 0;
    width = 0;
    minDRTrkpt2 = 0;
    l1frac = 0;
    c_firenden = 0;
    c_lat = 0;
    c_long = 0;
    c_r = 0;
    c_lambda = 0;
    logR = 0;
}
void CalRatioVariables::setBranchAdresses(TChain *chain){
    chain->SetBranchAddress("CalibJet_BDT13Lxy",&BDT);
    chain->SetBranchAddress("CalibJet_nTrk",&nTrk);
    chain->SetBranchAddress("CalibJet_sumTrkpT",&sumTrkpT);
    chain->SetBranchAddress("CalibJet_maxTrkpT",&maxTrkpT);
    chain->SetBranchAddress("CalibJet_minDRTrkpt2",&minDRTrkpt2);
    chain->SetBranchAddress("CalibJet_width",&width);
    chain->SetBranchAddress("CalibJet_l1frac",&l1frac);
    chain->SetBranchAddress("CalibJet_cluster_lead_first_Eden",&c_firenden);
    chain->SetBranchAddress("CalibJet_cluster_lead_lateral",&c_lat);
    chain->SetBranchAddress("CalibJet_cluster_lead_longitudinal",&c_long);
    chain->SetBranchAddress("CalibJet_cluster_lead_r",&c_r);
    chain->SetBranchAddress("CalibJet_cluster_lead_lambda",&c_lambda);
    //chain->SetBranchAddress("CalibJet_logRatio",&logR);

}
void CalRatioVariables::clearAllVectors(){
    BDT->clear();
    nTrk->clear();
    sumTrkpT->clear();
    maxTrkpT->clear();
    width->clear();
    minDRTrkpt2->clear();
    l1frac->clear();
    c_firenden->clear();
    c_lat->clear();
    c_long->clear();
    c_r->clear();
    c_lambda->clear();
    //logR->clear();
}

//
//  EventHistograms.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#include "PlottingPackage/EventHistograms.h"
#include <iostream>

void EventHistograms::initializeHistograms(){
   
    std::cout << "inside initialize histograms " << std::endl;
    
    int nBins=100;
    
    //4 instances: pass jet + trk iso, fail jet pass trk, fail trk pass jet, fail both. create 4 EventHistogram instances, though.
    
   // for ( int l=0;l<4;l++)
   // {
        TString str = ""; //str+=l+1;
        h_Jet_Et                  = new TH1D("h_Jet_Et_"                 +str ,"h_Jet_Et_"+str                 ,nBins,0,200);
        h_Jet_pT                  = new TH1D("h_Jet_pT_"                 +str ,"h_Jet_pT_"+str                 ,nBins,0,200);

        h_Jet_eta                 = new TH1D("h_Jet_eta_"                +str ,"h_Jet_eta_"+str                ,50,-2.7,2.7);
        h_Jet_phi                 = new TH1D("h_Jet_phi_"                +str ,"h_Jet_phi_"+str                ,50,-3.5,3.5);
        h_Jet_nJetPerEvt          = new TH1D("h_Jet_nJetPerEvt_"         +str ,"h_Jet_nJetPerEvt_"+str         ,20,0,20);
        
        h_Track_eta                 = new TH1D("h_Track_eta_"                +str ,"h_Track_eta_"+str                ,50,-2.7,2.7);
        h_Track_phi                 = new TH1D("h_Track_phi_"                +str ,"h_Track_phi_"+str                ,50,-3.5,3.5);
        h_Track_pT                 = new TH1D("h_Track_pT_"                +str ,"h_Track_pT_"+str                ,nBins,0,50);
        h_Track_nTrkPerEvt          = new TH1D("h_Track_nTrkPerEvt_"         +str ,"h_Track_nTrkPerEvt_"+str         ,50,0,50);
        
        h_HT                      = new TH1D("h_HT_"    +str ,"h_HT_"+str                    ,nBins,0,2500);
        h_HTMiss                  = new TH1D("h_HTMiss_"+str ,"h_HTMiss_"+str                 ,nBins,0,500);
        h_NJets                   = new TH1D("h_NJets_" +str ,"h_NJets_"+str                  ,15,0,15);
        h_Meff                    = new TH1D("h_Meff_"  +str ,"h_Meff_"+str                  ,nBins,0,2500);

        //variables in events where the trigger is also associated with a vertex passing the same set of iso requirements as the trigger object
   // }
    //
    
    h_MSVertex_TrigMatched = new TH1D("h_MSVertex_TrigMatched","h_MSVertex_TrigMatched",2,0,2);
    h_TrigPassed = new TH1D("h_TrigPassed","h_TrigPassed",2,0,2);

    return;
    
}

void EventHistograms::fillHistograms(CommonVariables *commonVar, bool vxMatched){
    
    for(size_t i = 0; i < commonVar->Jet_ET->size(); i++){
  	 histograms->h_Jet_Et->Fill(commonVar->Jet_ET->at(i));
  	 histograms->h_Jet_pT->Fill(commonVar->Jet_pT->at(i));
  	 histograms->h_Jet_eta->Fill(commonVar->Jet_eta->at(i));
  	 histograms->h_Jet_phi->Fill(commonVar->Jet_phi->at(i));
  	 histograms->h_Jet_nJetPerEvt->Fill(commonVar->Jet_pT->size());
    }
    
    for(size_t i = 0; i < commonVar->Track_ET->size(); i++){
  	 histograms->h_Track_pT->Fill(commonVar->Track_pT->at(i));
  	 histograms->h_Track_eta->Fill(commonVar->Track_eta->at(i));
  	 histograms->h_Track_phi->Fill(commonVar->Track_phi->at(i));
  	 histograms->h_Track_nTrackPerEvt->Fill(commonVar->Track_pT->size());
  	 
    } 
    histograms->h_HT->Fill(commonVar->eventHT);
    histograms->h_HTMiss->Fill(commonVar->eventHTMiss);
    histograms->h_NJets->Fill(commonVar->eventNJets);
    histograms->h_Meff->Fill(commonVar->eventMeff);
  
  //}//end if one trigger passed.
}
